package fri.android;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;

public class Units extends Activity {
	
	public static final String PREFS_NAME = "MyPrefsFile";
	
	public void onCreate(Bundle savedInstanceState) {

	    super.onCreate(savedInstanceState);
	    
	    
        requestWindowFeature(Window.FEATURE_NO_TITLE);  
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN); 
       
	    
	    setContentView(R.layout.units);
	    
	    SharedPreferences settings = getSharedPreferences(PREFS_NAME, 0);
	       int backgroundindex=settings.getInt("background"+settings.getInt("activeProf", 1), 3);
	       if(backgroundindex==0){
	       	getWindow().setBackgroundDrawable(getResources().getDrawable(R.drawable.back00));
	       }else if(backgroundindex==1){
	       	getWindow().setBackgroundDrawable(getResources().getDrawable(R.drawable.back01));
	       }else if(backgroundindex==2){
	       	getWindow().setBackgroundDrawable(getResources().getDrawable(R.drawable.back02));
	       }else if(backgroundindex==3){
	       	getWindow().setBackgroundDrawable(getResources().getDrawable(R.drawable.back03));
	       }else if(backgroundindex==4){
	       	getWindow().setBackgroundDrawable(getResources().getDrawable(R.drawable.back04));
	       }
	    /*
	    Variables var = ((Variables)getApplicationContext());
	    Drawable drawable = var.getD();
	    if(drawable!=null)
	    	getWindow().setBackgroundDrawable(drawable);
	    */
	    //Spinner spinner = (Spinner) findViewById(R.id.Spinner01);
	    
	    final Button buttonunitsback= (Button) findViewById(R.id.ButtonUnitsBack);
	    final Spinner unitsspinner = (Spinner) findViewById(R.id.Spinner01);
	    
	    int position=settings.getInt("spinset"+settings.getInt("activeProf", 1), 0);
	    unitsspinner.setSelection(position);
	    
	    buttonunitsback.setOnClickListener(new View.OnClickListener() {
	           public void onClick(View v) {
	        	   try{
	        		   
	        		   int set=unitsspinner.getSelectedItemPosition();
	        		   
	        		   SharedPreferences settings = getSharedPreferences(PREFS_NAME, 0);
	        		   SharedPreferences.Editor editor = settings.edit();
	          	       editor.putInt("spinset"+settings.getInt("activeProf", 1), set);
	          	       
	          	       editor.commit();
	        		   
	        		   Units.this.startActivity(new Intent(Units.this, Settings.class));
	        		   finish();
	        	   }catch(Exception e){
	        		   Utils.logW(e.getMessage());
	        	   }
	           }
	       });

	}
}
