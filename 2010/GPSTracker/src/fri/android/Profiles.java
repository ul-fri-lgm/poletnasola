package fri.android;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.RadioButton;

public class Profiles extends Activity {
	
	public static final String PREFS_NAME = "MyPrefsFile";
	
	public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        
        requestWindowFeature(Window.FEATURE_NO_TITLE);  
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN); 
        
        setContentView(R.layout.profiles);
        /*
        Variables var = ((Variables)getApplicationContext());
	    Drawable drawable = var.getD();
	    if(drawable!=null)
	    	getWindow().setBackgroundDrawable(drawable);
	    */
        
        SharedPreferences settings = getSharedPreferences(PREFS_NAME, 0);
        int backgroundindex=settings.getInt("background"+settings.getInt("activeProf", 1), 3);
        if(backgroundindex==0){
        	getWindow().setBackgroundDrawable(getResources().getDrawable(R.drawable.back00));
        }else if(backgroundindex==1){
        	getWindow().setBackgroundDrawable(getResources().getDrawable(R.drawable.back01));
        }else if(backgroundindex==2){
        	getWindow().setBackgroundDrawable(getResources().getDrawable(R.drawable.back02));
        }else if(backgroundindex==3){
        	getWindow().setBackgroundDrawable(getResources().getDrawable(R.drawable.back03));
        }else if(backgroundindex==4){
        	getWindow().setBackgroundDrawable(getResources().getDrawable(R.drawable.back04));
        }
        
	    
        final Button buttonprofback= (Button) findViewById(R.id.ButtonProfileBack);
        final Button buttonprofnew= (Button) findViewById(R.id.ButtonNewProfile);
        final RadioButton prof1=(RadioButton) findViewById(R.id.RadioButtonprof1);
        final RadioButton prof2=(RadioButton) findViewById(R.id.RadioButtonprof2);
        
        if(settings.getInt("activeProf", 1)!=1){
        	prof2.setChecked(true);
        }
        
        if(prof2.isChecked()){
        	SharedPreferences.Editor editor = settings.edit();
   	      	editor.putInt("activeProf", 2);
   	      	editor.commit();
        }else{
        	prof1.setChecked(true);
        	SharedPreferences.Editor editor = settings.edit();
        	editor.putInt("activeProf", 1);
        	editor.commit();
        }
        
        prof1.setText(settings.getString("name1", "John"));
        prof2.setText(settings.getString("name2", "John"));
        
        buttonprofnew.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
         	   try{
         		  SharedPreferences settings = getSharedPreferences(PREFS_NAME, 0);
         		  if(prof2.isChecked()){
         	        	SharedPreferences.Editor editor = settings.edit();
         	   	      	editor.putInt("activeProf", 2);
         	   	      	editor.commit();
         	        }else{
         	        	prof1.setChecked(true);
         	        	SharedPreferences.Editor editor = settings.edit();
         	        	editor.putInt("activeProf", 1);
         	        	editor.commit();
         	        }
         		   Profiles.this.startActivity(new Intent(Profiles.this, CreateProfile.class));
         		   finish();
         	   }catch(Exception e){
         		   Utils.logW(e.getMessage());
         	   }
            }
        });
        
        buttonprofback.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
         	   try{
         		  SharedPreferences settings = getSharedPreferences(PREFS_NAME, 0);
         		  if(prof2.isChecked()){
         	        	SharedPreferences.Editor editor = settings.edit();
         	   	      	editor.putInt("activeProf", 2);
         	   	      	editor.commit();
         	        }else{
         	        	prof1.setChecked(true);
         	        	SharedPreferences.Editor editor = settings.edit();
         	        	editor.putInt("activeProf", 1);
         	        	editor.commit();
         	        }
         		   Profiles.this.startActivity(new Intent(Profiles.this, Settings.class));
         		   finish();
         	   }catch(Exception e){
         		   Utils.logW(e.getMessage());
         	   }
            }
        });
	}
	
}
