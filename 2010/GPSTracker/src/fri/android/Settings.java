package fri.android;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;

public class Settings extends Activity {
	
	public static final String PREFS_NAME = "MyPrefsFile";
	
	public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        
        
        requestWindowFeature(Window.FEATURE_NO_TITLE);  
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN); 
       
        
        SharedPreferences settings = getSharedPreferences(PREFS_NAME, 0);
        int backgroundindex=settings.getInt("background"+settings.getInt("activeProf", 1), 3);
        if(backgroundindex==0){
        	getWindow().setBackgroundDrawable(getResources().getDrawable(R.drawable.back00));
        }else if(backgroundindex==1){
        	getWindow().setBackgroundDrawable(getResources().getDrawable(R.drawable.back01));
        }else if(backgroundindex==2){
        	getWindow().setBackgroundDrawable(getResources().getDrawable(R.drawable.back02));
        }else if(backgroundindex==3){
        	getWindow().setBackgroundDrawable(getResources().getDrawable(R.drawable.back03));
        }else if(backgroundindex==4){
        	getWindow().setBackgroundDrawable(getResources().getDrawable(R.drawable.back04));
        }
        
        // Set the layout for this activity.  You can find it
        // in res/layout/hello_activity.xml
       setContentView(R.layout.settings);
       /*
       Variables var = ((Variables)getApplicationContext());
	   Drawable drawable = var.getD();
	   if(drawable!=null)
	    	getWindow().setBackgroundDrawable(drawable);
	   */
       final Button buttonsetunits= (Button) findViewById(R.id.ButtonSetUnits);
       final Button buttonsetprofiles= (Button) findViewById(R.id.ButtonSetProfiles);
       final Button buttonsetbackground= (Button) findViewById(R.id.ButtonSetBackground);
       final Button buttonsetback= (Button) findViewById(R.id.ButtonSetBack);
       
       buttonsetunits.setOnClickListener(new View.OnClickListener() {
           public void onClick(View v) {
        	   try{
        		   Settings.this.startActivity(new Intent(Settings.this, Units.class));
        		   finish();
        	   }catch(Exception e){
        		   Utils.logW(e.getMessage());
        	   }
           }
       });
       
       buttonsetprofiles.setOnClickListener(new View.OnClickListener() {
           public void onClick(View v) {
        	   try{
        		   Settings.this.startActivity(new Intent(Settings.this, Profiles.class));
        		   finish();
        	   }catch(Exception e){
        		   Utils.logW(e.getMessage());
        	   }
           }
       });
       
       buttonsetbackground.setOnClickListener(new View.OnClickListener() {
           public void onClick(View v) {
        	   try{
        		   Settings.this.startActivity(new Intent(Settings.this, Background.class));
        		   finish();
        	   }catch(Exception e){
        		   Utils.logW(e.getMessage());
        	   }
           }
       });
       
       buttonsetback.setOnClickListener(new View.OnClickListener() {
           public void onClick(View v) {
        	   try{
        		   Settings.this.startActivity(new Intent(Settings.this, Main.class));
        		   finish();
        	   }catch(Exception e){
        		   Utils.logW(e.getMessage());
        	   }
           }
       });
       
	}

}
