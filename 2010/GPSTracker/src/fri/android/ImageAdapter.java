package fri.android;


import android.content.Context;
import android.graphics.drawable.Drawable;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Gallery;
import android.widget.ImageView;

public class ImageAdapter extends BaseAdapter {
    int mGalleryItemBackground;
    private Context mContext;

    private Integer[] mImageIds = {
            R.drawable.back00,
            R.drawable.back01,
            R.drawable.back02,
            R.drawable.back03,
            R.drawable.back04
    };

    public ImageAdapter(Context c) {
        mContext = c;
       // TypedArray a = obtainStyledAttributes(android.R.styleable.Theme);
     //   mGalleryItemBackground = a.getResourceId(
      //          android.R.styleable.Theme_galleryItemBackground, 0);
     //   a.recycle();
    }

    public int getCount() {
        return mImageIds.length;
    }

    public Object getItem(int position) {
        return position;
    }

    public long getItemId(int position) {
        return position;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        ImageView i = new ImageView(mContext);

        i.setImageResource(mImageIds[position]);
        i.setLayoutParams(new Gallery.LayoutParams(150, 150));
        //i.setScaleType(ImageView.ScaleType.FIT_XY);
        i.setBackgroundResource(mGalleryItemBackground);
        
        return i;
    }
}
