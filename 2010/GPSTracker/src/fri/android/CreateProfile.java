package fri.android;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.text.Editable;
import android.view.KeyEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.CompoundButton.OnCheckedChangeListener;

public class CreateProfile extends Activity {
	
	public static final String PREFS_NAME = "MyPrefsFile";

	
	public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        
        
        requestWindowFeature(Window.FEATURE_NO_TITLE);  
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN); 
       
        
        setContentView(R.layout.createprofile);
        
        SharedPreferences settings = getSharedPreferences(PREFS_NAME, 0);
	       int backgroundindex=settings.getInt("background"+settings.getInt("activeProf", 1), 3);
	       if(backgroundindex==0){
	        getWindow().setBackgroundDrawable(getResources().getDrawable(R.drawable.back00));
	       }else if(backgroundindex==1){
	        getWindow().setBackgroundDrawable(getResources().getDrawable(R.drawable.back01));
	       }else if(backgroundindex==2){
	        getWindow().setBackgroundDrawable(getResources().getDrawable(R.drawable.back02));
	       }else if(backgroundindex==3){
	        getWindow().setBackgroundDrawable(getResources().getDrawable(R.drawable.back03));
	       }else if(backgroundindex==4){
	        getWindow().setBackgroundDrawable(getResources().getDrawable(R.drawable.back04));
	       }
        

	    final Button buttoncreateprofsave= (Button) findViewById(R.id.ButtonProfileSave);
        final EditText et=(EditText) findViewById(R.id.EditTextName);
        final EditText etw=(EditText) findViewById(R.id.EditTextWeight);
        final DatePicker dp=(DatePicker) findViewById(R.id.BirthDate);
        final RadioButton rbFemale=(RadioButton) findViewById(R.id.RadioButtonFemale);
        final RadioButton rbMale=(RadioButton) findViewById(R.id.RadioButtonMale);
        final Button buttonresetprof= (Button) findViewById(R.id.Buttonresetprofile);
        final TextView allTracked = (TextView) findViewById(R.id.profileAllTracked);
        
        
        if(dp.hasFocus())
        	et.requestFocus();
        //String gender=CreateProfile.this.getResources().getString(R.string.p_gender);
       

        boolean isMale=settings.getBoolean("isMale"+settings.getInt("activeProf", 1), true);
    
       
        
        if(isMale){
        	rbMale.setChecked(true);
        	rbFemale.setChecked(false);
        }else{
        	rbFemale.setChecked(true);
        	rbMale.setChecked(false);
        }
      
        String name=settings.getString("name"+settings.getInt("activeProf", 1),"John");
        et.setText(name);
        
        int allDist=settings.getInt("complete_distance"+settings.getInt("activeProf", 1), 0);
        int allTime=settings.getInt("complete_time"+settings.getInt("activeProf", 1), 0);
        
        allTracked.setText("You tracked "+allDist+"meters in "+allTime+"s!");
        
        int weight=settings.getInt("weight"+settings.getInt("activeProf", 1), 70);
        etw.setText(Integer.toString(weight));
        
        int day=settings.getInt("day"+settings.getInt("activeProf", 1), 1);
        int month=settings.getInt("month"+settings.getInt("activeProf", 1), 0);
        int year=settings.getInt("year"+settings.getInt("activeProf", 1), 2000);
        
        dp.init(year, month, day, null);
        
	    buttoncreateprofsave.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
         	   try{
         		   
         		   //GET DATA FOR SAVING
         		   //name
         		   CharSequence sc=et.getText();
         		   String name=sc.toString();
         		   //weight
         		   String w="70";
         		   CharSequence sc2=etw.getText();
        		   w=sc2.toString();
        		   int weight=Integer.parseInt(w);
         		   //birth date
         		   int day=dp.getDayOfMonth();
         		   int month=dp.getMonth();
         		   int year=dp.getYear();
         		   String date=(day+"."+month+"."+year);
         		   //gender
         		   boolean gender=true;
         		   if(rbFemale.isChecked()){
         		      gender=false;
         		   }else {
         			  gender=true;
         		   }
         		   Utils.logV(date);
         		   //save data
         		  SharedPreferences settings = getSharedPreferences(PREFS_NAME, 0);
         		  SharedPreferences.Editor editor = settings.edit();
         	      editor.putBoolean("isMale"+settings.getInt("activeProf", 1), gender);
         	      editor.putString("name"+settings.getInt("activeProf", 1), name);
         	      editor.putInt("weight"+settings.getInt("activeProf", 1),weight );
         	      editor.putInt("day"+settings.getInt("activeProf", 1),day );
         	      editor.putInt("month"+settings.getInt("activeProf", 1),month );
         	      editor.putInt("year"+settings.getInt("activeProf", 1),year );
         	      
         	      editor.commit();

         		   //return to previous menu
         		  CreateProfile.this.startActivity(new Intent(CreateProfile.this, Settings.class));
         		   finish();
         	   }catch(Exception e){
         		   Utils.logW(e.getMessage());
         	   }
            }
        });
	    
	    buttonresetprof.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
            	SharedPreferences settings = getSharedPreferences(PREFS_NAME, 0);
       		  	SharedPreferences.Editor editor = settings.edit();
       		  	editor.putBoolean("isMale"+settings.getInt("activeProf", 1), true);
       		  	editor.putString("name"+settings.getInt("activeProf", 1), "John");
       		  	editor.putInt("weight"+settings.getInt("activeProf", 1),70 );
       	      	editor.putInt("day"+settings.getInt("activeProf", 1),1 );
       	      	editor.putInt("month"+settings.getInt("activeProf", 1),0 );
       	      	editor.putInt("year"+settings.getInt("activeProf", 1),2000 );
       	      	editor.putInt("complete_distance"+settings.getInt("activeProf", 1),0 );
       	      	editor.putInt("complete_time"+settings.getInt("activeProf", 1),0 );
       	        editor.putInt("background"+settings.getInt("activeProf", 1),3 );
       	      	editor.commit();
       	      	CreateProfile.this.startActivity(new Intent(CreateProfile.this, Profiles.class));
       	      	finish();
            }
	    });
	    
	    
	}
}
