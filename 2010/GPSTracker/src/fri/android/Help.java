package fri.android;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;

public class Help extends Activity {
	
	public static final String PREFS_NAME = "MyPrefsFile";
	
	public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        
        
        requestWindowFeature(Window.FEATURE_NO_TITLE);  
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN); 
       

        // Set the layout for this activity.  You can find it
        // in res/layout/hello_activity.xml
       setContentView(R.layout.help);
       
       SharedPreferences settings = getSharedPreferences(PREFS_NAME, 0);
       int backgroundindex=settings.getInt("background"+settings.getInt("activeProf", 1), 3);
       if(backgroundindex==0){
       	getWindow().setBackgroundDrawable(getResources().getDrawable(R.drawable.back00));
       }else if(backgroundindex==1){
       	getWindow().setBackgroundDrawable(getResources().getDrawable(R.drawable.back01));
       }else if(backgroundindex==2){
       	getWindow().setBackgroundDrawable(getResources().getDrawable(R.drawable.back02));
       }else if(backgroundindex==3){
       	getWindow().setBackgroundDrawable(getResources().getDrawable(R.drawable.back03));
       }else if(backgroundindex==4){
       	getWindow().setBackgroundDrawable(getResources().getDrawable(R.drawable.back04));
       }
       /*
       Variables var = ((Variables)getApplicationContext());
	   Drawable drawable = var.getD();
	   if(drawable!=null)
	    	getWindow().setBackgroundDrawable(drawable);
       */
       final Button buttonback= (Button) findViewById(R.id.ButtonHelpBack);
       
       buttonback.setOnClickListener(new View.OnClickListener() {
           public void onClick(View v) {
        	   Help.this.startActivity(new Intent(Help.this, Main.class));
        	   finish();
           }
       });
       
	}
}