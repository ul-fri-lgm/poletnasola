package fri.android;



import fri.android.R.drawable;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.text.Layout;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.Gallery;
import android.widget.AdapterView.OnItemClickListener;

public class Background extends Activity {
	
	public static final String PREFS_NAME = "MyPrefsFile";
	
	public void onCreate(Bundle savedInstanceState) {
		 super.onCreate(savedInstanceState);
		 
	        requestWindowFeature(Window.FEATURE_NO_TITLE);  
	        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN); 
	       
		 
		    setContentView(R.layout.background);
		    
		    SharedPreferences settings = getSharedPreferences(PREFS_NAME, 0);
	        int backgroundindex=settings.getInt("background"+settings.getInt("activeProf", 1), 3);
	        if(backgroundindex==0){
	        	getWindow().setBackgroundDrawable(getResources().getDrawable(R.drawable.back00));
	        }else if(backgroundindex==1){
	        	getWindow().setBackgroundDrawable(getResources().getDrawable(R.drawable.back01));
	        }else if(backgroundindex==2){
	        	getWindow().setBackgroundDrawable(getResources().getDrawable(R.drawable.back02));
	        }else if(backgroundindex==3){
	        	getWindow().setBackgroundDrawable(getResources().getDrawable(R.drawable.back03));
	        }else if(backgroundindex==4){
	        	getWindow().setBackgroundDrawable(getResources().getDrawable(R.drawable.back04));
	        }
		    
		    Gallery g = (Gallery) findViewById(R.id.Gallery01);
		    g.setAdapter(new ImageAdapter(this));

		    g.setOnItemClickListener(new OnItemClickListener() {
		        public void onItemClick(AdapterView parent, View v, int position, long id) {
		        	Drawable drawable=null;
		        	if(position==0){
		        		drawable = Background.this.getResources().getDrawable(R.drawable.back00);
		        	}
		        	else if(position==1){
		        		drawable = Background.this.getResources().getDrawable(R.drawable.back01);
		        	}
		        	else if(position==2){
		        		drawable = Background.this.getResources().getDrawable(R.drawable.back02);
		        	}
		        	else if(position==3){
		        		drawable = Background.this.getResources().getDrawable(R.drawable.back03);
		        	}
		        	else if(position==4){
		        		drawable = Background.this.getResources().getDrawable(R.drawable.back04);
		        	}
		        	getWindow().setBackgroundDrawable(drawable);
		        	
		        	SharedPreferences settings = getSharedPreferences(PREFS_NAME, 0);
	         		SharedPreferences.Editor editor = settings.edit();
	         	    editor.putInt("background"+settings.getInt("activeProf", 1), position);
	         	    
	         	    editor.commit();
		        	
		        	
		        }
		    });
		    
		    final Button buttonbackgroundback= (Button) findViewById(R.id.ButtonBackgroundBack);
		       
		    buttonbackgroundback.setOnClickListener(new View.OnClickListener() {
		           public void onClick(View v) {
		        	   try{
		        		   Background.this.startActivity(new Intent(Background.this, Settings.class));
		        		   finish();
		        	   }catch(Exception e){
		        		   Utils.logW(e.getMessage());
		        	   }
		           }
		    });
	}
	
}
