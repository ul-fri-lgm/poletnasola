/**
 * 
 */
package fri.android;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.TextView;

/**
 * @author andrejs
 *
 */
public class Results extends Activity {
	
	public static final String PREFS_NAME = "MyPrefsFile";
	
	 public void onCreate(Bundle savedInstanceState) {
		 super.onCreate(savedInstanceState);
		 
	        requestWindowFeature(Window.FEATURE_NO_TITLE);  
	        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN); 
	       
		 
	     setContentView(R.layout.results);
	     
	     Utils.logW("ppp");
	       SharedPreferences settings = getSharedPreferences(PREFS_NAME, 0);
	       int backgroundindex=settings.getInt("background"+settings.getInt("activeProf", 1),3);
	       if(backgroundindex==0){
	       	getWindow().setBackgroundDrawable(getResources().getDrawable(R.drawable.back00));
	       }else if(backgroundindex==1){
	       	getWindow().setBackgroundDrawable(getResources().getDrawable(R.drawable.back01));
	       }else if(backgroundindex==2){
	       	getWindow().setBackgroundDrawable(getResources().getDrawable(R.drawable.back02));
	       }else if(backgroundindex==3){
	       	getWindow().setBackgroundDrawable(getResources().getDrawable(R.drawable.back03));
	       }else if(backgroundindex==4){
	       	getWindow().setBackgroundDrawable(getResources().getDrawable(R.drawable.back04));
	       }
	       
	       final Button buttonBack= (Button) findViewById(R.id.resultsBack);
	       buttonBack.setOnClickListener(new View.OnClickListener() {
	           public void onClick(View v) {
	        	   try{
	        		   Results.this.startActivity(new Intent(Results.this, Main.class));
	        		   finish();
	        	   }catch(Exception e){
	        		   Utils.logW(e.getMessage());
	        	   }
	           }
	       });
	       
	       SharedPreferences s = getSharedPreferences(PREFS_NAME, 0);
	       String fullTime=s.getString("fullTime"+settings.getInt("activeProf", 1), "00.00");
	       int fullDistance=s.getInt("fullDistance"+settings.getInt("activeProf", 1), 0);
	       
	       char[] minutes = fullTime.toCharArray();
	       int min = (Integer.parseInt(minutes[0]+"")*10)   + Integer.parseInt(minutes[1]+"");
	       int sec = (Integer.parseInt(minutes[3]+"")*10)   + Integer.parseInt(minutes[4]+"");
	       
	       int timeToSec=60*min+sec;
	       
	       
	       final TextView ftText = (TextView)this.findViewById(R.id.resultsTime);
	       final TextView fdText = (TextView)this.findViewById(R.id.resultsDistance);
	       final TextView faText = (TextView)this.findViewById(R.id.resultsAvgSpeed);
	       
	       ftText.setText(fullTime + "");
	       fdText.setText(fullDistance+" meters");
	       
	       int tempdist=settings.getInt("complete_distance"+settings.getInt("activeProf", 1), 0);
	       int temptime=settings.getInt("complete_time"+settings.getInt("activeProf", 1), 0);
	       
	       SharedPreferences.Editor editor = settings.edit();
	       editor.putInt("complete_distance"+settings.getInt("activeProf", 1),(tempdist+fullDistance) );
	       editor.putInt("complete_time"+settings.getInt("activeProf", 1),(temptime+timeToSec) );
	       editor.commit();
	       
	       
	       double avgspeed=((double)fullDistance/(double)timeToSec);
	       //calories
	       final TextView calories = (TextView)this.findViewById(R.id.TextViewKcal);  
	       double met=0.5*(avgspeed*2.24)+1;
	       double tempCal= ((double)timeToSec/(double)60)*met*3.5*((double)(settings.getInt("weight"+settings.getInt("activeProf", 1), 70))/(double)200);
	       
	       calories.setText(Math.round(tempCal)+"kcal");
	       
	       int units = s.getInt("spinset"+settings.getInt("activeProf", 1), 0);
	       
	       
	       
	       if (units==0){
	    	   faText.setText(Math.round(avgspeed*3.6) +" km/h");
	       } else if (units==1){
	    	   faText.setText(Math.round(avgspeed*1) +" m/s");
	       } else if (units==2){
	    	   faText.setText(Math.round(avgspeed*2.24) +" miles/h");
	       } else if (units==3){
	    	   faText.setText(Math.round(1/(avgspeed*0.06)) +" min/km");
	       } else if (units==4){
	    	   faText.setText(Math.round(1/(avgspeed*0.04)) +" min/mile");
	       }
	       
	 }
}
