package fri.android;

public class Record {
	double time;
	double id;
	double lon;
	double alt;
	double lan;
	public Record(double time, double id, double lon, double alt, double lan) {
		this.time = time;
		this.id = id;
		this.lon = lon;
		this.alt = alt;
		this.lan = lan;
	}
	public double getTime() {
		return time;
	}
	public void setTime(double time) {
		this.time = time;
	}
	public double getId() {
		return id;
	}
	public void setId(double id) {
		this.id = id;
	}
	public double getLon() {
		return lon;
	}
	public void setLon(double lon) {
		this.lon = lon;
	}
	public double getAlt() {
		return alt;
	}
	public void setAlt(double alt) {
		this.alt = alt;
	}
	public double getLan() {
		return lan;
	}
	public void setLan(double lan) {
		this.lan = lan;
	}
	
}
