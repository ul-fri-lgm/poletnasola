package fri.android;



import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;

public class Main extends Activity {
	
	public static final String PREFS_NAME = "MyPrefsFile";
	
	public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        
        
        requestWindowFeature(Window.FEATURE_NO_TITLE);  
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN); 
        // Set the layout for this activity.  You can find it
        // in res/layout/hello_activity.xml
       setContentView(R.layout.main);
           
       /*
       Variables var = ((Variables)getApplicationContext());
       Drawable drawable = var.getD();
       if(drawable!=null)
       	getWindow().setBackgroundDrawable(drawable);
        */
       SharedPreferences settings = getSharedPreferences(PREFS_NAME, 0);
       int backgroundindex=settings.getInt("background"+settings.getInt("activeProf", 1), 3);
       if(backgroundindex==0){
       	getWindow().setBackgroundDrawable(getResources().getDrawable(R.drawable.back00));
       }else if(backgroundindex==1){
       	getWindow().setBackgroundDrawable(getResources().getDrawable(R.drawable.back01));
       }else if(backgroundindex==2){
       	getWindow().setBackgroundDrawable(getResources().getDrawable(R.drawable.back02));
       }else if(backgroundindex==3){
       	getWindow().setBackgroundDrawable(getResources().getDrawable(R.drawable.back03));
       }else if(backgroundindex==4){
       	getWindow().setBackgroundDrawable(getResources().getDrawable(R.drawable.back04));
       }
       
       
       final Button buttonbegin= (Button) findViewById(R.id.ButtonBegin);
       final Button buttonset= (Button) findViewById(R.id.ButtonSet);
       final Button buttonhelp= (Button) findViewById(R.id.ButtonHelp);
       final Button buttonexit= (Button) findViewById(R.id.ButtonExit);
       
       buttonbegin.setOnClickListener(new View.OnClickListener() {
           public void onClick(View v) {
        	   try{
        		   Main.this.startActivity(new Intent(Main.this, Begin.class));
        		   finish();
        	   }catch(Exception e){
        		   Utils.logW(e.getMessage());
        	   }
           }
       });
       
       buttonset.setOnClickListener(new View.OnClickListener() {
           public void onClick(View v) {
        	   try{
        		   Main.this.startActivity(new Intent(Main.this, Settings.class));
        		   finish();
        	   }catch(Exception e){
        		   Utils.logW(e.getMessage());
        	   }
           }
       });
       
       buttonhelp.setOnClickListener(new View.OnClickListener() {
           public void onClick(View v) {
        	   try{
        		   Main.this.startActivity(new Intent(Main.this, Help.class));
        		   finish();
        	   }catch(Exception e){
        		   Utils.logW(e.getMessage());
        	   }
           }
       });
       
       buttonexit.setOnClickListener(new View.OnClickListener() {
           public void onClick(View v) {
        	   finish();
           }
       });

    }
	
	public void onRestart() {
    	super.onRestart();
    	Utils.logV("onRestart");
    }
    
    public void onStart() {
    	super.onStart();
    	Utils.logV("onStart");
    	
    }
    
    public void onStop() {
    	super.onStop();
    	Utils.logV("onStop");
    }
    
    public void onResume() {
    	super.onResume();
    	Utils.logV("onResume");
    }
    
    public void onPause() {
    	super.onPause();
    	Utils.logV("onPause");
    }
    
    public void onDestroy() {
    	super.onDestroy();
    	Utils.logV("onDestroy");
    }
    
}
