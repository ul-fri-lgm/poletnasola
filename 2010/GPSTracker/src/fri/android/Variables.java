package fri.android;

import android.app.Application;
import android.graphics.drawable.Drawable;

public class Variables extends Application {
	private Drawable d=null;

	public Drawable getD() {
		return d;
	}

	public void setD(Drawable d) {
		this.d = d;
	}
	
}
