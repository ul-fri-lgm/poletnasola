package student.futr;

import java.lang.reflect.Constructor;
import java.util.ArrayList;
import java.util.HashMap;

import org.json.JSONArray;
import org.json.JSONException;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.app.TabActivity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.view.View.OnClickListener;
import android.view.View.OnFocusChangeListener;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.SimpleAdapter;
import android.widget.TabHost;
import android.widget.TabWidget;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.AdapterView.OnItemClickListener;

public class StudentFutr extends TabActivity 
{
	// http://www.netmite.com/android/mydroid/development/samples/ApiDemos/src/com/example/android/apis/app/AlertDialogSamples.java
	private static final int DIALOG_RESTAURANT_SETTINGS = 1;
	private static final int DIALOG_RESTAURANT_SETTINGS_CATEGORIES = 2;
	private static final int DIALOG_RESTAURANT_SETTINGS_LOCATIONS = 3;
	private static final int DIALOG_MEAL_SETTINGS = 4;
	private static final int DIALOG_MEAL_SETTINGS_CATEGORIES = 5;
	
	
	private static final int LOAD_DATA_ALL = 1;
	private static final int LOAD_DATA_MEALS = 2;
	private static final int LOAD_DATA_RESTAURANTS = 3;

	// http://developer.android.com/guide/topics/data/data-storage.html#pref
    public static final String PREFS_NAME = "ConfigurationFile";
	public static SharedPreferences settings;
    
	
	protected static ArrayList<Restaurant> restaurants;
	protected static ArrayList<Meal> meals;
	public ProgressDialog startupDialog;
	
	
	public static int openTab = 0;
	
	public ArrayList<Restaurant> getRestaurants() 
	{
		return restaurants;
	}
	public Restaurant getRestaurant(int id) 
	{
		
		for(Restaurant restaurant : restaurants)
		{
			if(restaurant.id == id)
				return restaurant;
		}
		return null;
	}
	public ArrayList<Meal> getMeals() 
	{
		return meals;
	}
	public static Meal getMeal(int id) 
	{
		for(Meal meal : StudentFutr.meals)
		{
			if(meal.id == id)
				return meal;
		}
		return null;
	}
	

    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
        
        
        TabHost mTabHost = getTabHost();
        mTabHost.addTab(mTabHost.newTabSpec("tab1").setIndicator("Kosila").setContent(R.id.tab1_layout));
        mTabHost.addTab(mTabHost.newTabSpec("tab2").setIndicator("Restavracije").setContent(R.id.tab2_layout));   
        mTabHost.setCurrentTab(StudentFutr.openTab);
        
        
        // light theme support
        final TabHost tabHost = getTabHost();
        //tabHost.setBackgroundColor(Color.WHITE);
        tabHost.getTabWidget().setBackgroundColor(Color.BLACK);

        // hack to set font size
        LinearLayout ll = (LinearLayout) tabHost.getChildAt(0);
        TabWidget tw = (TabWidget) ll.getChildAt(0);

        // first tab
        RelativeLayout rllf = (RelativeLayout) tw.getChildAt(0);
        TextView lf = (TextView) rllf.getChildAt(1);
        lf.setTextSize(22);
        lf.setPadding(0, 0, 0, 6);

        // second tab
        RelativeLayout rlrf = (RelativeLayout) tw.getChildAt(1);
        TextView rf = (TextView) rlrf.getChildAt(1);
        rf.setTextSize(22);
        rf.setPadding(0, 0, 0, 6);

        // Restore preferences
        settings = getSharedPreferences(PREFS_NAME, 0);

        // initialize default list
        this.loadRestaurantsAndMeals();

    }
    
    // http://developer.android.com/reference/android/os/AsyncTask.html
    private class DownloadTask extends AsyncTask<String, String, String> 
    {	
    	@Override
        protected String doInBackground(String... args) 
        {
    		switch(Integer.parseInt(args[0]))
    		{
    			case LOAD_DATA_ALL:
    				/*
    				SQLite sql = SQLite.getSingleton();
    				sql.setContext(StudentFutr.this);
    				sql.initializeHelper();
    				sql.fill();
    				
                	StudentFutr.restaurants = sql.getAllRestaurants();
                	StudentFutr.meals = sql.getAllMeals();
                	sql.close();
                	*/
                	/*
            		StudentFutr.this.fetchRestaurants(args[1]);
            		StudentFutr.this.fetchMeals(args[2]);   
            		*/   
    				StudentFutr.this.fetchMeals();
    				StudentFutr.this.fetchRestaurants();
    				break;
    			case LOAD_DATA_MEALS:
            		StudentFutr.this.fetchMeals(args[2]);       				
    				break;
    			case LOAD_DATA_RESTAURANTS:
            		StudentFutr.this.fetchRestaurants(args[1]);     				
    				break;    				
    		}
    		
    		/*
            Storage data = (Storage)getLastNonConfigurationInstance(); 
            if (data!=null) {
            	StudentFutr.restaurants = data.restaurants;
            	StudentFutr.meals = data.meals;
            } else {
            	SQLite sql = new SQLite(StudentFutr.this);
            	StudentFutr.restaurants = sql.getAllRestaurants();
            	StudentFutr.meals = sql.getAllMeals();
            	sql.close();
            }*/

            Utils.logV("Background thread starting");
            StudentFutr.this.runOnUiThread(new Runnable() {
                public void run() 
                {
                	fillRestaurants();
                	fillMeals();           	
                }
            }); 
            Utils.logV("Background thread finished");
            return "";
        }

        protected void onPostExecute(String result) 
        {
            if (StudentFutr.this.startupDialog != null) 
            {
            	StudentFutr.this.startupDialog.dismiss();
            }
        }
   }
    
    public Object onRetainNonConfigurationInstance() {
    	 return new Storage(StudentFutr.restaurants, StudentFutr.meals);
    }
   
    private class Storage {

    	protected ArrayList<Restaurant> restaurants;
    	protected ArrayList<Meal> meals;
    	
    	Storage(ArrayList<Restaurant> restaurants, ArrayList<Meal> meals) {
    		this.restaurants = restaurants;
    		this.meals = meals;
    	}
    	
    }


    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
    	menu.clear();
        MenuInflater inflater = getMenuInflater();

        switch(getTabHost().getCurrentTab())
        {
        	case 0:
        		inflater.inflate(R.menu.meals_menu, menu);
        		break;
        	case 1:
        		inflater.inflate(R.menu.restaurants_menu, menu);
        		break;
        }
        
        return true;
    }
    
    @Override
    protected Dialog onCreateDialog(int id) 
    {
    	switch(id)
    	{
    		case DIALOG_RESTAURANT_SETTINGS:
    			return new AlertDialog.Builder(this)
                //.setIcon(R.drawable.alert_dialog_icon)
                .setTitle(R.string.settings)
                .setItems(R.array.restaurant_settings, new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int which) 
					{
						switch(which)
						{
							case 0:
								showDialog(DIALOG_RESTAURANT_SETTINGS_CATEGORIES);
								break;
							case 1:
								showDialog(DIALOG_RESTAURANT_SETTINGS_LOCATIONS);
								break;							
						}
					}
                })
                .setPositiveButton(R.string.alert_dialog_close_and_refresh, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) 
                    {
                    	StudentFutr.this.loadRestaurantsAndMeals();
                    	dialog.dismiss();
                    }
                })                
                .create();
    		case DIALOG_RESTAURANT_SETTINGS_CATEGORIES:
    			// size based on number of categories
    			final boolean[] storedCategories = getStoredSettingsAsBooleans("restaurantCategories", 
    					new boolean[]{true, true, true, true, true, true, true, true, true, true, true, true});

    			return new AlertDialog.Builder(this)
                //.setIcon(R.drawable.alert_dialog_icon)
                .setTitle(R.string.settings_restaurant_categories)
                .setMultiChoiceItems(R.array.restaurant_categories,
                		storedCategories,
                        new DialogInterface.OnMultiChoiceClickListener() {
                            public void onClick(DialogInterface dialog, int whichButton, boolean isChecked) 
                            {
                                /* User clicked on a check box do some stuff */
                            	storedCategories[whichButton] = isChecked;
                            	saveBooleansAsStoredSettings("restaurantCategories", storedCategories);
                            }
                        })
                .setPositiveButton(R.string.alert_dialog_close, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) 
                    {
                    	showDialog(DIALOG_RESTAURANT_SETTINGS);
                    }
                })
                .create();  
    		case DIALOG_RESTAURANT_SETTINGS_LOCATIONS:
    			// size based on number of categories
    			final boolean[] storedRestaurantLocations = getStoredSettingsAsBooleans("restaurantLocations", 
    					new boolean[]{true, true, true, true});

    			return new AlertDialog.Builder(this)
                //.setIcon(R.drawable.alert_dialog_icon)
                .setTitle(R.string.settings_restaurant_locations)
                .setMultiChoiceItems(R.array.restaurant_locations,
                		storedRestaurantLocations,
                        new DialogInterface.OnMultiChoiceClickListener() {
                            public void onClick(DialogInterface dialog, int whichButton, boolean isChecked) 
                            {
                                /* User clicked on a check box do some stuff */
                            	storedRestaurantLocations[whichButton] = isChecked;
                            	saveBooleansAsStoredSettings("restaurantLocations", storedRestaurantLocations);
                            }
                        })
                .setPositiveButton(R.string.alert_dialog_close, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) 
                    {
                    	showDialog(DIALOG_RESTAURANT_SETTINGS);
                    }
                })
                .create();      			
    		case DIALOG_MEAL_SETTINGS:
    			return new AlertDialog.Builder(this)
                //.setIcon(R.drawable.alert_dialog_icon)
                .setTitle(R.string.settings)
                .setItems(R.array.meals_settings, new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int which) 
					{
						switch(which)
						{
							case 0:
								showDialog(DIALOG_MEAL_SETTINGS_CATEGORIES);
								break;		
							case 1:
								showDialog(DIALOG_RESTAURANT_SETTINGS_CATEGORIES);
								break;									
						}
					}
                })
                .setPositiveButton(R.string.alert_dialog_close_and_refresh, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) 
                    {
                    	StudentFutr.this.loadMeals();
                    	dialog.dismiss();
                    }
                })                
                .create();    
    		case DIALOG_MEAL_SETTINGS_CATEGORIES:
    			// size based on number of categories
    			final int storedMealCategory = settings.getInt("mealCategory", 0);

    			return new AlertDialog.Builder(this)
                //.setIcon(R.drawable.alert_dialog_icon)
                .setTitle(R.string.settings_meal_categories)
				.setSingleChoiceItems(R.array.meals_categories, storedMealCategory, new DialogInterface.OnClickListener() {
				    public void onClick(DialogInterface dialog, int item) 
				    {
				        SharedPreferences.Editor editor = settings.edit();
				        editor.putInt("mealCategory", item);
				        editor.commit();  
				    }
				})            
                .setPositiveButton(R.string.alert_dialog_close, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) 
                    {
                    	Utils.logV("reload restaurants list");
                    	showDialog(DIALOG_MEAL_SETTINGS);
                    }
                })
                .create();     			

    	}
    	return null;
    }
    
    
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		Utils.logV("onOptionsItemSelected");
		switch (item.getItemId()) 
		{
			case R.id.menu_meals_search_item: 
			{
				Utils.logV("onOptionsItemSelected - menu_meals_search_item");
				LinearLayout searchLayout = (LinearLayout)findViewById(R.id.tab1_search_layout);
				searchLayout.setVisibility(View.VISIBLE);
				
				Button close = (Button) findViewById(R.id.search_meal_close_button);
				close.setOnClickListener(new OnClickListener() {
					public void onClick(View v) {
						LinearLayout searchLayout = (LinearLayout)findViewById(R.id.tab1_search_layout);
						searchLayout.setVisibility(View.GONE);
						
						EditText et = (EditText)findViewById(R.id.search_meal_input);
						et.setText("");
						InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
						imm.hideSoftInputFromWindow(et.getWindowToken(), 0);
						
						
						//StudentFutr.getInstance().fillMeals();
					}
				});
				
				Button search = (Button) findViewById(R.id.search_meal_button);
				search.setOnClickListener(new OnClickListener() {
					public void onClick(View v) {
						LinearLayout searchLayout = (LinearLayout)findViewById(R.id.tab1_search_layout);
						searchLayout.setVisibility(View.GONE);
						
						EditText et = (EditText)findViewById(R.id.search_meal_input);
						InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
						imm.hideSoftInputFromWindow(et.getWindowToken(), 0);
						
						StudentFutr.this.searchMeals(et.getText().toString());
						
					}
				});
				
				Button showAll = (Button) findViewById(R.id.search_meal_showAll_button);
				showAll.setOnClickListener(new OnClickListener() {
					public void onClick(View v) {
						LinearLayout searchLayout = (LinearLayout)findViewById(R.id.tab1_search_layout);
						searchLayout.setVisibility(View.GONE);
						
						EditText et = (EditText)findViewById(R.id.search_meal_input);
						InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
						imm.hideSoftInputFromWindow(et.getWindowToken(), 0);
						
						StudentFutr.this.loadMeals();
						
					}
				});
				
				return true;
			}
			case R.id.menu_restaurants_search_item: 
			{
				Utils.logV("onOptionsItemSelected - menu_restaurants_search_item");
				LinearLayout searchLayout = (LinearLayout)findViewById(R.id.tab2_search_layout);
				searchLayout.setVisibility(View.VISIBLE);
				
				Button close = (Button) findViewById(R.id.search_restaurant_close_button);
				close.setOnClickListener(new OnClickListener() {
					public void onClick(View v) {
						LinearLayout searchLayout = (LinearLayout)findViewById(R.id.tab2_search_layout);
						searchLayout.setVisibility(View.GONE);
						
						EditText et = (EditText)findViewById(R.id.search_restaurant_input);
						et.setText("");
						InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
						imm.hideSoftInputFromWindow(et.getWindowToken(), 0);
					}

				});
				
				Button search = (Button) findViewById(R.id.search_restaurant_button);
				search.setOnClickListener(new OnClickListener() {
					public void onClick(View v) {
						LinearLayout searchLayout = (LinearLayout)findViewById(R.id.tab2_search_layout);
						searchLayout.setVisibility(View.GONE);
						
						EditText et = (EditText)findViewById(R.id.search_restaurant_input);
						InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
						imm.hideSoftInputFromWindow(et.getWindowToken(), 0);
						
						StudentFutr.this.searchRestaurants(et.getText().toString());
						
					}
				});
				
				Button showAll = (Button) findViewById(R.id.search_restaurant_showAll_button);
				showAll.setOnClickListener(new OnClickListener() {
					public void onClick(View v) {
						LinearLayout searchLayout = (LinearLayout)findViewById(R.id.tab2_search_layout);
						searchLayout.setVisibility(View.GONE);
						
						EditText et = (EditText)findViewById(R.id.search_restaurant_input);
						InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
						imm.hideSoftInputFromWindow(et.getWindowToken(), 0);
						
						StudentFutr.this.loadRestaurants();
						
					}
				});
				
				return true;
			}
			case R.id.menu_restaurants_settings_item:
			{
				Utils.logV("onOptionsItemSelected - menu_restaurants_settings_item");		
				
				showDialog(DIALOG_RESTAURANT_SETTINGS);
				return true;
			}	
			case R.id.menu_meals_settings_item:
			{
				Utils.logV("onOptionsItemSelected - menu_meals_settings_item");		
				
				showDialog(DIALOG_MEAL_SETTINGS);
				return true;
			}				
		}
		return false;
	}    
	
	public void loadRestaurantsAndMeals()
	{
		this.showDownloadDialog();
		new DownloadTask().execute(Integer.toString(LOAD_DATA_ALL), null, null);	
	}
	
	public void searchRestaurants(String searchText)
	{
		this.showDownloadDialog();
        new DownloadTask().execute(Integer.toString(LOAD_DATA_RESTAURANTS),searchText, null);		
	}
	
	public void loadRestaurants()
	{
		this.showDownloadDialog();
        new DownloadTask().execute(Integer.toString(LOAD_DATA_RESTAURANTS),null, null);
	}
	
	public void loadMeals()
	{
		this.showDownloadDialog();
        new DownloadTask().execute(Integer.toString(LOAD_DATA_MEALS),null, null);		
	}
	
	public void searchMeals(String searchText)
	{
		this.showDownloadDialog();
        new DownloadTask().execute(Integer.toString(LOAD_DATA_MEALS), null, searchText);		
	}
	
	public void showDownloadDialog()
	{
		// http://stackoverflow.com/questions/1979524/android-splashscreen
		if(this.startupDialog == null || !this.startupDialog.isShowing())    
			this.startupDialog = ProgressDialog.show(this, "Please wait..", "Loading Data...", true, false);		
	}
	
	public void fetchRestaurants() 
	{
		this.fetchRestaurants(null);
    }
	
	public void fetchRestaurants(String searchText) 
	{
		SQLite sql = SQLite.getSingleton();
		sql.setContext(StudentFutr.this);
		sql.initializeHelper();
		sql.fill();

		StudentFutr.restaurants = sql.getAllRestaurants(searchText);
		
		sql.close();

	}
	
    public void fillRestaurants() 
    {
        ArrayList<HashMap<String, String>> mylist = new ArrayList<HashMap<String, String>>();
        HashMap<String, String> map;
        
        for(Restaurant restaurant : StudentFutr.restaurants)
        {
        	map = new HashMap<String, String>();
        	
        	map.put("name", restaurant.name);
        	map.put("address", restaurant.address.toString());
	        map.put("price", "Cena bona: " + restaurant.price.toString() + " EUR");
	        
	        mylist.add(map);
        }
        
        ListView list = (ListView) findViewById(R.id.tab2_list);
        ListAdapter la = new SimpleAdapter(this, mylist, R.layout.restaurants,
                    new String[] {"name", "address", "price"}, new int[] {R.id.tab2_list_row_name, R.id.tab2_list_row_address, R.id.tab2_list_row_price});
        list.setAdapter(la);
        
        
        list.setOnItemClickListener(new OnItemClickListener() {
			public void onItemClick(AdapterView arg0, View arg1, int arg2, long arg3) {
				Intent i = new Intent(StudentFutr.this, RestaurantActivity.class);	
				
				Restaurant restaurant = new StudentFutr().getRestaurants().get(arg2);
				i.putExtra("id", restaurant.id);
				
				startActivity(i);
			}
		});
        
    }
    
    public void fetchMeals() 
    {
    	this.fetchMeals(null);
    }
    
	public void fetchMeals(String searchText) {
		SQLite sql = SQLite.getSingleton();
		sql.setContext(StudentFutr.this);
		sql.initializeHelper();
		sql.fill();

		StudentFutr.meals = sql.getAllMeals(searchText);
		
		sql.close();

	}

    public void fillMeals() 
    {
   	
        ArrayList<HashMap<String, String>> mylist = new ArrayList<HashMap<String, String>>();
        HashMap<String, String> map;
        
        for(Meal meal : StudentFutr.meals)
        {
        	map = new HashMap<String, String>();
        	map.put("name", meal.name);
        	map.put("category", meal.getFormattedCategory() + " -");
	        map.put("restaurant_category", meal.restaurantCategory);        	
	        mylist.add(map);
        }
        
        ListView list = (ListView) findViewById(R.id.tab1_list);
        ListAdapter la = new SimpleAdapter(this, mylist, R.layout.meals, 
        		new String[] {"name", "category", "restaurant_category"}, new int[] {R.id.tab1_list_row_name, R.id.tab1_list_row_category, R.id.tab1_list_row_restaurant_category});
        list.setAdapter(la);
        
        
        list.setOnItemClickListener(new OnItemClickListener() {
			public void onItemClick(AdapterView arg0, View arg1, int arg2, long arg3) {
				Intent i = new Intent(StudentFutr.this, MealActivity.class);
				
			    Meal meal = new StudentFutr().getMeals().get(arg2);
				i.putExtra("id", meal.id);
				
				startActivity(i);
			}
		});
        
    }
    
    /**
     * Loads string from SharedPreferences by key and converts stored JSON string into array of booleans
     * 
     * @param key
     * @return
     * @throws JSONException
     */
    protected static boolean[] getStoredSettingsAsBooleans(String key, boolean[] defaultSettings)
    {
		try
		{
			String jsonString = settings.getString(key, null);
			if(jsonString == null)
				return defaultSettings;
			
			JSONArray arrayCategories = new JSONArray(jsonString);
			boolean[] storedCategories = new boolean[arrayCategories.length()];
			for(int i=0;i<arrayCategories.length();i++)
				storedCategories[i] = arrayCategories.getBoolean(i);   
			
			return storedCategories;
		}
		catch(JSONException ex)
		{
			return defaultSettings;
		}    	
    }
    
    /**
     * Converts supplied booleans into JSON string and saves them to SharedPreferences by key
     * 
     * @param key
     * @param values
     */
    protected static void saveBooleansAsStoredSettings(String key, boolean[] values)
    {
    	JSONArray jsonArray = new JSONArray();
    	for(boolean value:values)
    	{
    		jsonArray.put(value);
    	}
    	
        SharedPreferences.Editor editor = settings.edit();
        editor.putString(key, jsonArray.toString());
        editor.commit();   	
    }
}