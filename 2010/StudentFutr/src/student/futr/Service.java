package student.futr;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.SharedPreferences;

public class Service 
{
	/**/
	protected static final String PREFERENCES_LAST_UPDATE_RESTAURANTS = "restaurantsLastUpdate";
	protected static final String PREFERENCES_LAST_UPDATE_MEALS = "mealsLastUpdate";
	
	/**/
	protected static final String SERVICE_PATH_RESTAURANTS = "restaurants";
	protected static final String SERVICE_PATH_MEALS = "meals";
	protected static final String SERVICE_PATH_LASTUPDATE_RESTAURANTS = "lastupdate/restaurants";
	protected static final String SERVICE_PATH_LASTUPDATE_MEALS = "lastupdate/meals";
	protected static final String SERVICE_URL = "http://emandem.eu/studentfutr/";
	//protected static final String SERVICE_URL = "http://93.103.159.32:3000/";
	
	/**/
	protected static Service instance;
	
	/**
	 * 
	 * @param name
	 * @return
	 */
	public static ArrayList<Restaurant> getRestaurants()
	{
		ArrayList<Restaurant> restaurants = new ArrayList<Restaurant>();
		JSONArray array = request(SERVICE_PATH_RESTAURANTS);
		if(array != null)
		{
			try
			{
				for(int i = 0; i < array.length(); i++)
				{
					JSONObject object = (JSONObject)array.get(i);
					Restaurant restaurant = new Restaurant(object.getJSONObject("restaurant"));
					restaurants.add(restaurant);
				}
				
			}
			catch(JSONException ex)
			{
				Utils.logV("Error in parsing restaurants!");
			}
		}
		return restaurants;
	}
	
	public static ArrayList<Meal> getMeals()
	{
		ArrayList<Meal> meals = new ArrayList<Meal>();
		JSONArray array = request(SERVICE_PATH_MEALS);
		if(array != null)
		{
			try
			{
				for(int i = 0; i < array.length(); i++)
				{
					JSONObject object = (JSONObject)array.get(i);
					
					Meal meal = new Meal(object.getJSONObject("meal"));	
					meals.add(meal);
				}
				
			}
			catch(JSONException ex)
			{
				Utils.logV("Error in parsing meals!");
			}			
		}			
		return meals;
	}	
	
	public static String getRestaurantImagePath(int id)
	{
		return SERVICE_URL + "images/restaurants/restaurant_" + id + ".jpg";
	}
	
	/**
	 * Checks with the service if restaurants data has changed
	 * 
	 * @return
	 */
	public static Boolean mustUpdateRestaurants()
	{
		Utils.logV("Service.mustUpdateRestaurants");
		JSONObject object = requestSingle(SERVICE_PATH_LASTUPDATE_RESTAURANTS);	
		try 
		{
			int serviceLastUpdate = object.getInt("last_update");
			int databaseLastUpdate = StudentFutr.settings.getInt(PREFERENCES_LAST_UPDATE_RESTAURANTS, 0);
			if(serviceLastUpdate > databaseLastUpdate)
			{
				// store back to preferences
		        SharedPreferences.Editor editor = StudentFutr.settings.edit();
		        editor.putInt(PREFERENCES_LAST_UPDATE_RESTAURANTS, serviceLastUpdate);
		        editor.commit(); 
		        
				return true;
			}
		} 
		catch (JSONException ex) 
		{
			Utils.logV("error parsing restaurants last update: " + ex.getMessage());
		}
		
		// TODO think what to return here
		return false;	
	}
	
	/**
	 * Checks with the service if meals data has changed
	 * 
	 * @return
	 */
	public static Boolean mustUpdateMeals()
	{
		JSONObject object = requestSingle(SERVICE_PATH_LASTUPDATE_MEALS);	
		try 
		{
			int serviceLastUpdate = object.getInt("last_update");
			int databaseLastUpdate = StudentFutr.settings.getInt(PREFERENCES_LAST_UPDATE_MEALS, 0);
			if(serviceLastUpdate > databaseLastUpdate)
			{
				// store back to preferences
		        SharedPreferences.Editor editor = StudentFutr.settings.edit();
		        editor.putInt(PREFERENCES_LAST_UPDATE_MEALS, serviceLastUpdate);
		        editor.commit(); 
		        
				return true;
			}
		} 
		catch (JSONException ex) 
		{
			Utils.logV("error parsing meals last update: " + ex.getMessage());
		}
		
		// TODO think what to return here
		return false;			
	}
	
	/**
	 * Connects to service by given path and returns JSON string
	 * 
	 * @param path
	 * @return
	 */
	protected static String getJsonString(String path)
	{
		try
		{
			URL url = new URL(SERVICE_URL + path + ".json");
			BufferedReader br = new BufferedReader(new InputStreamReader(url.openStream()), 100000);
			
			//TODO clean up
			StringBuffer fileData = new StringBuffer(1000);
			char[] buf = new char[1024];
	        int numRead=0;
	        while((numRead=br.read(buf)) != -1)
	        {
	            fileData.append(buf, 0, numRead);
	        }
	        br.close();	
			
			return fileData.toString();	
		}
		catch(Exception ex)
		{
			Utils.logV("error getting json: " + ex.getMessage());
			return null;
		}	
	}	
	
	/**
	 * 
	 * @param path
	 * @return
	 */
	protected static JSONObject requestSingle(String path)
	{
		JSONObject response = null;
		
		try
		{	
			response = new JSONObject(getJsonString(path));	
		}
		catch(Exception ex)
		{
			Utils.logV("error parsing json: " + ex.getMessage());
		}
		
		return response;		
	}
	
	/**
	 * 
	 * @param path
	 * @return
	 */
	protected static JSONArray request(String path)
	{
		JSONArray response = null;
		
		try
		{	
			response = new JSONArray(getJsonString(path));	
		}
		catch(Exception ex)
		{
			Utils.logV("error parsing json: " + ex.getMessage());
		}
		
		return response;
	}	
}
