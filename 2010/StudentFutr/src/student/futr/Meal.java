package student.futr;

import java.sql.Date;
import java.util.ArrayList;

import org.json.JSONException;
import org.json.JSONObject;

import android.database.Cursor;

public class Meal 
{
	
	public static final int NORMAL = 1;
	public static final int DAILY = 2;
	
	public static enum CATEGORY { ALL, DAILY, NORMAL }
		
	public int id;
	public String name;
	public String category;
	public Date date;
	public int restaurantId;
	public String restaurantCategory; 
	
	public Meal(String name) {
		this.name = name;
	}
	
	public Meal(JSONObject object)
	{
		try 
		{
			// every meal must have this parameters
			this.id = object.getInt("id");
			this.name = object.getString("name");			
			this.restaurantId = object.getInt("restaurant_id");
			this.category = object.getString("category");
			this.restaurantCategory = object.getString("restaurant_category");
		} 
		catch (JSONException e) 
		{
			// log it
		}		
	}
	
	public Meal(Cursor c)
	{
		this.id = c.getInt(c.getColumnIndex("id"));
		this.name = c.getString(c.getColumnIndex("name"));
		this.category = c.getString(c.getColumnIndex("category"));
		this.restaurantId = c.getInt(c.getColumnIndex("restaurantId"));
		this.restaurantCategory = c.getString(c.getColumnIndex("restaurantCategory"));
	}
	
	public String getFormattedCategory()
	{
		if(this.category.compareToIgnoreCase(Meal.CATEGORY.DAILY.toString()) == 0)
			return "dnevna ponudba";
		
		return "stalna ponudba";
	}
}
