package student.futr;

import java.util.ArrayList;
import android.app.Activity;
import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.os.Bundle;
import android.content.Intent;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;

public class MealActivity extends Activity
{

	private Meal meal;
	private Restaurant restaurant;
	public ProgressDialog startupDialog;
	
   public void onCreate(Bundle icicle)
   {
      super.onCreate(icicle);
      setContentView(R.layout.meal);
      
      // http://stackoverflow.com/questions/1979524/android-splashscreen
      this.startupDialog = ProgressDialog.show(this, "Working..", "Loading Data...", true, false); 
      
      new DownloadTask().execute();
      
   }
   
   public void fill() {
	   
	   
	   TextView tv;
	   tv = (TextView) findViewById(R.id.restaurant_name);
	   tv.setText(restaurant.name);
	   tv = (TextView) findViewById(R.id.meal_name);
	   tv.setText(meal.name);
	   
	   tv = (TextView) findViewById(R.id.meal_category);
	   tv.setText(this.meal.getFormattedCategory());
	   
	   tv = (TextView) findViewById(R.id.meal_restaurant_category);
	   tv.setText(" - " + this.meal.restaurantCategory);	  
	   
	   // kateri tab je odprt ko se vrnemo nazaj na glavno stran
	   StudentFutr.openTab = 0;
	   
	   
	   
	   
	   
	   Button btRes = (Button) findViewById(R.id.showRestaurant);
	   btRes.setOnClickListener(new OnClickListener() {
				public void onClick(View v) {
					Intent i = new Intent(MealActivity.this, RestaurantActivity.class);
					i.putExtra("id", MealActivity.this.meal.restaurantId);
					startActivity(i);
				}
	
			});
	   
	   
	   Button bt = (Button) findViewById(R.id.goBack);
	   bt.setOnClickListener(new OnClickListener() {
				public void onClick(View v) {
					/*
					Intent i = new Intent(MealActivity.this, StudentFutr.class);
					startActivity(i);
					*/
					finish();
					Utils.logV("Klik goBack button!");
				}
	
			});
	   
   }
   
   public Object onRetainNonConfigurationInstance() {
	   return new Storage(this.restaurant, this.meal);
   }
   private class Storage {

   	protected Restaurant restaurant;
   	protected Meal meal;
   	
   	Storage(Restaurant restaurant, Meal meal) {
   		this.restaurant = restaurant;
   		this.meal = meal;
   	}
   	
   }

   // http://developer.android.com/reference/android/os/AsyncTask.html
   private class DownloadTask extends AsyncTask<String, String, String> 
   {	
   	@Override
       protected String doInBackground(String... args) 
       {
   		
   		Storage data = (Storage)getLastNonConfigurationInstance(); 
        if (data!=null) {
        	MealActivity.this.restaurant = data.restaurant;
        	MealActivity.this.meal = data.meal;
        } else {
        	int id = getIntent().getExtras().getInt("id");
       		//MealActivity.this.meal = StudentFutr.getMeal(id);
       		//MealActivity.this.restaurant = new Gatherer().getRestaurant(MealActivity.this.meal.restaurantId);
       		SQLite db = SQLite.getSingleton();
       		db.setContext(MealActivity.this);
        	db.initializeHelper();
        	
       		MealActivity.this.meal = db.getMeal(id);
       		MealActivity.this.restaurant = db.getRestaurant(MealActivity.this.meal.restaurantId);
       		db.close();
        }
   		
           Utils.logV("Background thread starting");
           MealActivity.this.runOnUiThread(new Runnable() {
               public void run() 
               {
            	   fill();
               }
           }); 
           Utils.logV("Background thread finished");
           
           return "";
       }

       protected void onPostExecute(String result) 
       {
           if (MealActivity.this.startupDialog != null) 
           {
        	   MealActivity.this.startupDialog.dismiss();
           }
       }
  }
   
}