package student.futr;

import java.util.List;

import com.google.android.maps.GeoPoint;
import com.google.android.maps.MapActivity;
import com.google.android.maps.MapController;
import com.google.android.maps.MapView;
import com.google.android.maps.Overlay;
import com.google.android.maps.OverlayItem;

import android.graphics.drawable.Drawable;
import android.os.Bundle;

public class GoogleMap extends MapActivity {
	
	@Override
	protected boolean isRouteDisplayed() {
	    return false;
	}
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
	    super.onCreate(savedInstanceState);
	    setContentView(R.layout.map);
	    
	    MapView mapView = (MapView) findViewById(R.id.mapview);
	    mapView.setBuiltInZoomControls(true);
	    
	    List<Overlay> mapOverlays = mapView.getOverlays();
	    Drawable drawable = this.getResources().getDrawable(R.drawable.androidmarker);
	    GoogleMapOverlay itemizedoverlay = new GoogleMapOverlay(drawable, this);
	    
	    int longitude = (int) (getIntent().getExtras().getFloat("longitude")*Math.pow(10, 6));
	    int latitude = (int) (getIntent().getExtras().getFloat("latitude")*Math.pow(10, 6));
	    String name = getIntent().getExtras().getString("name");
	    String address = getIntent().getExtras().getString("address");
	    
	    
	    GeoPoint point = new GeoPoint(latitude,longitude);
	    OverlayItem overlayitem = new OverlayItem(point, name, address);
	    
	    itemizedoverlay.addOverlay(overlayitem);
	    mapOverlays.add(itemizedoverlay);
	    
	    MapController myMapController = mapView.getController();
	    myMapController.setZoom(16);
	    
	    myMapController.animateTo(point);
	    
	    
	    
	}
}