package student.futr;

import java.io.InputStream;
import java.net.URL;

import android.app.Activity;
import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.os.Bundle;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

public class RestaurantActivity extends Activity
{
	
	public Restaurant restaurant;
	public ProgressDialog startupDialog;
	public Bitmap image;
	
   public void onCreate(Bundle icicle)
   {
      super.onCreate(icicle);
      setContentView(R.layout.restaurant);
      
      // http://stackoverflow.com/questions/1979524/android-splashscreen
      this.startupDialog = ProgressDialog.show(this, "Working..", "Loading Data...", true, false); 
      
      new DownloadTask().execute();
      
   }
   
   public void fill() {
   
	   TextView tv;
	   tv = (TextView) findViewById(R.id.restaurant_name);
	   tv.setText(restaurant.name);
	   tv = (TextView) findViewById(R.id.restaurant_address);
	   tv.setText(restaurant.address);
	   if(restaurant.telephone != null)
	   {
		   tv = (TextView) findViewById(R.id.restaurant_telephone);
		   tv.setText("Telefon: "+restaurant.telephone);	
		   tv.setVisibility(View.VISIBLE);
	   }

	   if(restaurant.email != null)
	   {
		   tv = (TextView) findViewById(R.id.restaurant_email);
		   tv.setText("E-mail: "+restaurant.email);
		   tv.setVisibility(View.VISIBLE);
	   }
	   
	   tv = (TextView) findViewById(R.id.restaurant_price);
	   tv.setText("Cena bona: " + restaurant.price.toString() + " EUR");
	   
	   if(restaurant.description != null && restaurant.description.length() > 0)
	   {	   
		   tv = (TextView) findViewById(R.id.restaurant_description);
		   tv.setText(restaurant.description);
		   tv.setVisibility(View.VISIBLE);
	   }
	   
	   ImageView imageView=(ImageView) findViewById(R.id.imageRestaurant);
	   if(this.image != null)
		   imageView.setImageBitmap(this.image);
	   
	   
	   // kateri tab je odprt ko se vrnemo nazaj na glavno stran
	   StudentFutr.openTab = 1;
	   
	   Button close = (Button) findViewById(R.id.goBack);
	   close.setOnClickListener(new OnClickListener() {
				public void onClick(View v) {
					finish();
				}
	
			});
	   
	   
	   Button map = (Button) findViewById(R.id.showMap);
	   map.setOnClickListener(new OnClickListener() {
				public void onClick(View v) {
					Intent i = new Intent(RestaurantActivity.this, GoogleMap.class);
					i.putExtra("longitude", RestaurantActivity.this.restaurant.longitude);
					i.putExtra("latitude", RestaurantActivity.this.restaurant.latitude);
					i.putExtra("name", RestaurantActivity.this.restaurant.name);
					i.putExtra("address", RestaurantActivity.this.restaurant.address);
					startActivity(i);
				}
	
			});
	   
   }
   
   
   public Object onRetainNonConfigurationInstance() {
	   return new Storage(this.restaurant, this.image);
   }
   private class Storage {

   	protected Restaurant restaurant;
   	protected Bitmap image;
   	
   	Storage(Restaurant restaurant, Bitmap image) {
   		this.restaurant = restaurant;
   		this.image = image;
   	}
   	
   }
   
   // http://developer.android.com/reference/android/os/AsyncTask.html
   private class DownloadTask extends AsyncTask<String, String, String> 
   {	
   	@Override
       protected String doInBackground(String... args) 
       {
   		
   		Storage data = (Storage)getLastNonConfigurationInstance(); 
        if (data!=null) {
        	RestaurantActivity.this.restaurant = data.restaurant;;
        	RestaurantActivity.this.image = data.image;
        } else {
        	int id = getIntent().getExtras().getInt("id");
       		//RestaurantActivity.this.restaurant = new Gatherer().getRestaurant(id);      	
        	SQLite db = SQLite.getSingleton();
        	db.setContext(RestaurantActivity.this);
        	db.initializeHelper();
       		RestaurantActivity.this.restaurant = db.getRestaurant(id);
       		db.close();
       		try {
                URL url = new URL(Service.getRestaurantImagePath(id));
                InputStream in=url.openStream();
                RestaurantActivity.this.image = BitmapFactory.decodeStream(in);
            } catch (Exception e) {
            	Utils.logV("internet failed: "+e.getMessage()); 
            }
        }
   		
   		
           Utils.logV("Background thread starting");
           RestaurantActivity.this.runOnUiThread(new Runnable() {
               public void run() 
               {
            	   fill();
               }
           }); 
           Utils.logV("Background thread finished");
           
           return "";
       }

       protected void onPostExecute(String result) 
       {
           if (RestaurantActivity.this.startupDialog != null) 
           {
        	   RestaurantActivity.this.startupDialog.dismiss();
           }
       }
  }  
   
   
}