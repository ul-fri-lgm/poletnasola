package student.futr;

import java.util.ArrayList;

import org.json.JSONException;
import org.json.JSONObject;

import android.database.Cursor;

public class Restaurant 
{
	protected int id;
	protected String name;
	protected String address;
	protected String telephone;
	protected String email;
	protected String category;
	protected String region;
	protected String description;
	protected Float price;
	protected Float latitude;
	protected Float longitude;
	
	public static enum CATEGORY { 
	DOMACA, 
	KITAJSKA, 
	HITRA_PREHRANA, 
	SAMOPOSTREZNA, 
	MEHISKA, 
	DOSTAVA_NA_DOM, 
	ITALIJANSKA, 
	PICERIJA, 
	SPANSKA, 
	BALKANSKA, 
	VEGETERJANSKA,
	DRUGO };	
		  
	public static enum REGION {
	LJUBLJANA,
	NOVO_MESTO,
	KRSKO,
	JESENICE};	  
	
	protected ArrayList<Meal> meals;
	
	public Restaurant(String name, Float price)
	{
		this.meals = new ArrayList<Meal>();
		
		this.name = name;
		this.price = price;
	}
	
	public Restaurant(JSONObject object)
	{
		
		try 
		{
			this.id = object.getInt("id");
			this.name = object.getString("name");
			this.address = object.getString("address");
			this.telephone = object.getString("telephone");
			this.email = object.getString("email");
			this.description = object.getString("description");
			this.category = object.getString("category");
			this.region = object.getString("region");
			this.price = Float.parseFloat(object.getString("price"));			
			this.latitude = Float.parseFloat(object.getString("latitude"));
			this.longitude = Float.parseFloat(object.getString("longitude"));
		} 
		catch (JSONException e) 
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	
	public Restaurant(Cursor c)
	{
		this.id = c.getInt(c.getColumnIndex("id"));
		this.name = c.getString(c.getColumnIndex("name"));
		
		this.address = c.getString(c.getColumnIndex("address"));
		if(this.address.compareTo("null") == 0)
			this.address = null;
		
		this.telephone = c.getString(c.getColumnIndex("telephone"));
		if(this.telephone.compareTo("null") == 0)
			this.telephone = null;
		
		this.email = c.getString(c.getColumnIndex("email"));
		if(this.email.compareTo("null") == 0)
			this.email = null;
			
		this.description = c.getString(c.getColumnIndex("description"));
		if(this.description.compareTo("null") == 0)
			this.description = null;
		
		this.category = c.getString(c.getColumnIndex("category"));
		if(this.category.compareTo("null") == 0)
			this.category = null;
		
		this.region = c.getString(c.getColumnIndex("region"));
		if(this.region.compareTo("null") == 0)
			this.region = null;
		
		this.price = c.getFloat(c.getColumnIndex("price"));
		this.latitude = c.getFloat(c.getColumnIndex("latitude"));
		this.longitude = c.getFloat(c.getColumnIndex("longitude"));
	}
	
	
	public void addMeals()
	{

	}
	
	
	public void getDailyOffer()
	{
		
	}
	
	public void getAllOffer()
	{
		
	}
	

	
}
