package student.futr;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.test.IsolatedContext;
import android.util.Log;

import java.util.ArrayList;


public class SQLite {

   private static final String DATABASE_NAME = "example.db";
   private static final int DATABASE_VERSION = 1;
   private static final String TABLE_RESTAURANTS = "restaurants";
   private static final String TABLE_MEALS = "meals";

   private Context context;
   private SQLiteDatabase db;
   
   public static boolean filled = false;

   protected static SQLite instance;
   
   private SQLite()
   {
	   instance = this;
   }
   
   public void initializeHelper()
   {
	   OpenHelper openHelper = new OpenHelper(this.context);
	   this.db = openHelper.getWritableDatabase();
	   
	   // pobrisemo bazo
	   //openHelper.destroyMeals(this.db);
	   //openHelper.destroyRestaurants(this.db);
	   //System.exit(1);
	   openHelper.onCreate(this.db);	   
   }
   
   public void setContext(Context context)
   {
	   this.context = context;
   }
   
   public static SQLite getSingleton()
   {
	   if(instance == null)
		   instance = new SQLite();
	   
	   return instance;
   }

   public void close() {
	   this.db.close();
   }
   
   public void fill() 
   {
	   if(this.isEmpty() || Service.mustUpdateMeals())
	   {
		   ArrayList<Meal> meals = Service.getMeals();
		   for(Meal meal : meals) {
			   insertMeal(meal);
		   }		   
	   }
	   
	   if(this.isEmpty() || Service.mustUpdateRestaurants())
	   {
		   ArrayList<Restaurant> restaurants = Service.getRestaurants();
		   for(Restaurant restaurant : restaurants) {
			   insertRestaurant(restaurant);
		   }		   
	   }
	   
	   SQLite.filled = true;
	   
   }
   
   public boolean isEmpty() {
	   
	   Cursor c = this.db.rawQuery("SELECT COUNT(`id`) as `count` FROM " + TABLE_RESTAURANTS,null);
	   c.moveToFirst();
	   
	   if(c.getInt(c.getColumnIndex("count")) == 0) 
		   return true;
	   else
		   return false;
   }
   
   public void insertRestaurant(Restaurant restaurant) {
	   if(this.getRestaurant(restaurant.id) != null) {
		   this.db.execSQL(
			   "UPDATE "
				+ TABLE_RESTAURANTS
				+ " SET name='"+restaurant.name+"', address='"+restaurant.address+"', telephone='"+restaurant.telephone+"',"
				+ "email='"+restaurant.email+"', category='"+restaurant.category+"', description='"+restaurant.description+"',"
				+ "price='"+restaurant.price+"', latitude='"+restaurant.latitude+"', longitude='"+restaurant.longitude+"',"
				+ "region='"+restaurant.region+"'"
				+ "WHERE id = '"+restaurant.id+"'"
			   );
		   
	   } else {
		   this.db.execSQL(
		   "INSERT INTO "
			+ TABLE_RESTAURANTS
			+ " (id, name, address, telephone, email, category, region, description, price, latitude, longitude)"
			+ " VALUES ('"+restaurant.id+"', '"+restaurant.name+"', '"+restaurant.address+"', '"+restaurant.telephone+"'," +
						"'"+restaurant.email+"', '"+restaurant.category+"', '"+restaurant.region+"', '"+restaurant.description+
						"', '"+restaurant.price+"', '"+restaurant.latitude+"', '"+restaurant.longitude+"')"
		   );
	   }
   }
   
   
   public void insertMeal(Meal meal) {
	   
	   if(this.getMeal(meal.id) != null) {
		   this.db.execSQL(
			   "UPDATE "
				+ TABLE_MEALS
				+ " SET name='"+meal.name+"', category='"+meal.category+"',"
				+ "restaurantId='"+meal.restaurantId+"', restaurantCategory='"+meal.restaurantCategory+"'"
				+ "WHERE id='"+meal.id+"'"
			   );
	   } else {
		   this.db.execSQL(
			   "INSERT INTO "
				+ TABLE_MEALS
				+ " (id, name, category, restaurantId, restaurantCategory)"
				+ " VALUES ('"+meal.id+"', '"+meal.name+"', '"+meal.category+"'," +
							"'"+meal.restaurantId+"', '"+meal.restaurantCategory+"')"
			   );
	   }
	   

   }
   

   public void deleteAllRestaurants() {
      this.db.delete(TABLE_RESTAURANTS, null, null);
   }
   public void deleteAllMeals() {
	  this.db.delete(TABLE_MEALS, null, null);
   }

   public ArrayList<Restaurant> getAllRestaurants() 
   {
	   return this.getAllRestaurants(null);
   }
   
   public ArrayList<Restaurant> getAllRestaurants(String searchText) 
   {
	   	ArrayList<Restaurant> restaurants = new ArrayList<Restaurant>();
	   	String query = "SELECT * FROM " + TABLE_RESTAURANTS + " WHERE 1";
	   	
	   	if(searchText != null)
	   	{
	   		query += " AND name LIKE '%" + searchText + "%'";
	   	}
	   
		// set restaurant categories			
		boolean[] restaurantCategories = StudentFutr.getStoredSettingsAsBooleans("restaurantCategories", null);		
		Restaurant.CATEGORY[] restaurantCategoriesName = Restaurant.CATEGORY.values();
		if(restaurantCategories != null)
		{
			query += " AND category IN(";
			for(int i=0; i < restaurantCategoriesName.length; i++)
				if(restaurantCategories[i] == true)
				{
					query = query + "'" + restaurantCategoriesName[i].toString().toLowerCase() + "',";	
				}
			
			// remove last comma
			query = query.substring(0, query.length()-1);
			query += ") ";
		}		
		
		// set restaurant locations			
		boolean[] restaurantLocations = StudentFutr.getStoredSettingsAsBooleans("restaurantLocations", null);		
		Restaurant.REGION[] restaurantLocationsName = Restaurant.REGION.values();
		if(restaurantLocations != null)
		{
			query += " AND region IN(";
			for(int i=0; i < restaurantLocationsName.length; i++)
				if(restaurantLocations[i] == true)
				{
					query = query + "'" + restaurantLocationsName[i].toString().toLowerCase() + "',";	
				}
			
			// remove last comma
			query = query.substring(0, query.length()-1);
			query += ") ";
		}		

		//Utils.logV("executing sqlite query: " + query);
		Cursor c = this.db.rawQuery(query,null);
      
		if (c.moveToFirst()) {
			do {
				restaurants.add(new Restaurant(c));
			} while (c.moveToNext());
		}
		if(c != null && !c.isClosed()) {
			c.close();
		}
		
		return restaurants;
   }
   public Restaurant getRestaurant(int id) {

	   Cursor c = this.db.rawQuery("SELECT * FROM " + TABLE_RESTAURANTS + " WHERE id = ?", new String[]{Integer.toString(id)});
	   if(c.getCount() == 0 || !c.moveToFirst())
		   return null;
	   else
		   return new Restaurant(c);
		   
   }
   
   public ArrayList<Meal> getAllMeals()
   {
	   return this.getAllMeals(null);
   }
   
   
   public ArrayList<Meal> getAllMeals(String searchText) 
   {
	   	ArrayList<Meal> meals = new ArrayList<Meal>();
		String query = "SELECT * FROM " + TABLE_MEALS + " WHERE 1";
			
		if(searchText != null)
		{
			query += " AND name LIKE '%" + searchText + "%' ";
		}
	   		
		// set categories
		int category = StudentFutr.settings.getInt("mealCategory", 0);
		if(category != 0)
		{
			Meal.CATEGORY[] categoriesName = Meal.CATEGORY.values();
			query += " AND category = '" + categoriesName[category].toString().toLowerCase() + "' ";
		}	 
		
		// set restaurant categories			
		boolean[] restaurantCategories = StudentFutr.getStoredSettingsAsBooleans("restaurantCategories", null);		
		Restaurant.CATEGORY[] restaurantCategoriesName = Restaurant.CATEGORY.values();
		if(restaurantCategories != null)
		{
			query += " AND restaurantCategory IN(";
			for(int i=0; i < restaurantCategoriesName.length; i++)
				if(restaurantCategories[i] == true)
				{
					query = query + "'" + restaurantCategoriesName[i].toString().toLowerCase() + "',";	
				}
			
			// remove last comma
			query = query.substring(0, query.length()-1);
			query += ") ";
		}			
		
		//Utils.logV("executing sqlite query: " + query);
		Cursor c = this.db.rawQuery(query,null);
      
		if (c.moveToFirst()) {
			do {
				meals.add(new Meal(c));
			} while (c.moveToNext());
		}
		if(c != null && !c.isClosed()) {
			c.close();
		}
		return meals;
   }
   
   public Meal getMeal(int id) {

	   Cursor c = this.db.rawQuery("SELECT * FROM " + TABLE_MEALS + " WHERE id = ?", new String[]{Integer.toString(id)});
	   if(c.getCount() == 0 || !c.moveToFirst())
		   return null;
	   else
		   return new Meal(c);
		   
   }

   private static class OpenHelper extends SQLiteOpenHelper {

      OpenHelper(Context context) {
         super(context, DATABASE_NAME, null, DATABASE_VERSION);
      }

      @Override
      public void onCreate(SQLiteDatabase db) {
         db.execSQL("CREATE TABLE IF NOT EXISTS " + TABLE_RESTAURANTS + 
        		 "(id INTEGER PRIMARY KEY, name VARCHAR(255), address VARCHAR(255), " +
        		 "telephone VARCHAR(255), email VARCHAR(255), category VARCHAR(255), " +
        		 "region VARCHAR(255),description TEXT, price FLOAT, latitude FLOAT, longitude FLOAT)");
         db.execSQL("CREATE TABLE IF NOT EXISTS " + TABLE_MEALS + 
        		 "(id INTEGER PRIMARY KEY, name VARCHAR(255), category VARCHAR(255)," +
        		 "restaurantId INT, restaurantCategory VARCHAR(255))");
      }
      
      public void destroyRestaurants(SQLiteDatabase db) {
    	  db.execSQL("DROP TABLE restaurants");
      }
      public void destroyMeals(SQLiteDatabase db) {
    	  db.execSQL("DROP TABLE meals");
      }

      @Override
      public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
         Log.w("Example", "Upgrading database, this will drop tables and recreate.");
         db.execSQL("DROP TABLE IF EXISTS " + TABLE_RESTAURANTS);
         db.execSQL("DROP TABLE IF EXISTS " + TABLE_MEALS);
         onCreate(db);
      }
   }
}
