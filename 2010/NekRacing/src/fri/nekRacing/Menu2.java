package fri.nekRacing;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;

public class Menu2 extends Activity{
	public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.menu2);
        
        Button backButton = (Button) findViewById(R.id.Back2);
        backButton.setOnClickListener(backListener);      
        
        
    }
	
	private OnClickListener backListener = new OnClickListener()
    {
        public void onClick(View v)
        {
            // Here we start the next activity, and then call finish()
            // so that our own will stop running and be removed from the
            // history stack.
        	try{
	            Intent intent = new Intent();
	            intent.setClass(Menu2.this, Menu.class);
	            startActivity(intent);
	            finish();
        	}catch(Exception e){
	        	Log.d("tag", e.getMessage());
	        }
        }
    };
}
