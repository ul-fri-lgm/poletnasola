package fri.nekRacing;


import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;

public class Menu extends Activity {
    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
        Button goButton = (Button)findViewById(R.id.Button01);
        goButton.setOnClickListener(mGoListener);
        Button goExit = (Button)findViewById(R.id.Exit);
        goExit.setOnClickListener(mGoListener1);
        Button goInstruction = (Button)findViewById(R.id.Instruction);
        goInstruction.setOnClickListener(mGoListener2);
        
        
    }
    
    private OnClickListener mGoListener = new OnClickListener()
    {
        public void onClick(View v)
        {
            // Here we start the next activity, and then call finish()
            // so that our own will stop running and be removed from the
            // history stack.
        	try{
	            Intent intent = new Intent();
	            intent.setClass(Menu.this, Avto.class);
	            startActivity(intent);
	            finish();
        	}catch(Exception e){
	        	Log.d("tag", e.getMessage());
	        }
        }
    };
    
    private OnClickListener mGoListener2 = new OnClickListener()
    {
        public void onClick(View v)
        {
            // Here we start the next activity, and then call finish()
            // so that our own will stop running and be removed from the
            // history stack.
        	try{
	            Intent intent = new Intent();
	            intent.setClass(Menu.this, Menu2.class);
	            startActivity(intent);
	            finish();
        	}catch(Exception e){
	        	Log.d("tag", e.getMessage());
	        }
        }
    };
    
    private OnClickListener mGoListener1 = new OnClickListener()
    {
        public void onClick(View v)
        {
            // Here we start the next activity, and then call finish()
            // so that our own will stop running and be removed from the
            // history stack.
        	try{
	            finish();
        	}catch(Exception e){
	        	Log.d("tag", e.getMessage());
	        }
        }
    };
}