package fri.nekRacing;

import java.util.ArrayList;

import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.*;
import android.os.Bundle;
import android.os.Environment;
import android.os.Looper;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;

public class PictureView extends View{
	
	static final int MAX_SPEED = 15;
	static final int MIN_SPEED = 7;
	static final int MAX_REVERSE = -4;
	
	private int view_w, view_h;
	private int xPos;
	private int yPos;
	private int d;
	private int rotation;
	private ArrayList<Bitmap> images;
	private Bitmap image;
	private boolean onTrack;
	private double phi;
	private boolean flag;

	
	
    Context c = null;
    Thread nit = null;
	public PictureView (Context context, AttributeSet attrSet){
		super(context, attrSet);
		c = context;
		this.images = new ArrayList<Bitmap>();
		for(int i = 0; i < 24; i++){
			images.add(BitmapFactory.decodeFile(Environment.getExternalStorageDirectory().toString() + "/andy/android_logo_"+i+".png"));
		}
		this.image = images.get(0);
		this.d = 0;
		this.xPos = 100;
		this.yPos = 100;
		this.rotation = 0;
		this.onTrack = true;
		this.phi=0;
		this.setFlag(true);
		
		nit = new Thread() {
			public void run() {
				int counter = 0;
				while (isFlag()) {
					try {
						moveCar();
						
						if (counter > 125){
							d -=1;
							counter = 0;
						}
						postInvalidate();
						Thread.sleep(40);
						counter ++;
					} catch (Exception e) {
						// TODO: handle exception
					}
				}
				
			}
		};
	
		
	}
	
	
	public double getPhi() {
		return phi;
	}

	public void setPhi(double phi) {
		this.phi = phi;
	}

	public int getRotation() {
		return rotation;
	}
	

	public Bitmap getImage() {
		return image;
	}
	public void setImage(int tem0) {
		if(tem0<0){
			tem0 += 24;
		}
		this.image = images.get(tem0%24);
	}
	public void setRotation(int rotation) {
		this.rotation = rotation % 360;
	}
	
	
	/*Drawable avto = (D)R.drawable.the_car;
	Bitmap bmp= BitmapFactory.decodeResource(avto, null);*/

	public void init(Bundle savedInstanceState) {
        setFocusable(true);
        setFocusableInTouchMode(true);
        requestFocus(); //for keyevent to go to this view
		if (savedInstanceState==null) return;
	}
	
	public void saveState(Bundle outState) {
	}
	
	public void moveCar(){
		this.yPos += this.d*Math.cos(phi);
		this.xPos += this.d*Math.sin(phi);
		if (yPos <= 0 || xPos <= 0 || yPos >= view_h - 50 || xPos >= view_w -50){
			this.setFlag(false);
			
		}
		
	}
	
	public void changeSpeed (int ds){
		int max = this.onTrack ? MAX_SPEED:MIN_SPEED;
		if((-1)*d<max && -d>MAX_REVERSE){
			this.d += ds;
		}
		else if(-d==max && ds>0){
			this.d += ds;
		}
		else if(-d==MAX_REVERSE && ds<0){
			this.d += ds;
		}
		Log.d("speed",this.d + "");
	}
	
	protected void onDraw(Canvas canvas){
		super.onDraw(canvas);
		
		int x = view_w / 2;
		int y = view_h / 2;
		
		Paint paint = new Paint();
		Matrix matrika = new Matrix();
		matrika.postRotate(rotation);
//		int height = (int) Math.floor(Math.abs(image.getHeight()*Math.cos(rotation/Math.PI)) + Math.abs(image.getWidth()*Math.sin(rotation/Math.PI)));
//		int width = (int) Math.floor(Math.abs(image.getWidth()*Math.cos(rotation/Math.PI)) + Math.abs(image.getHeight()*Math.sin(rotation/Math.PI)));
//		Log.d("tag", width + " " + height + " " + image.getWidth());
//		Bitmap rotiranaSlika = Bitmap.createBitmap(image, xPos, yPos, 141, 141, matrika, true);
		
		if (image == null)
			return;
		
		Rect src =  new Rect(0,0,image.getWidth(),image.getHeight());
//		Rect src =  new Rect(0,0,rotiranaSlika.getWidth(),rotiranaSlika.getHeight());
		Rect dst = new Rect(xPos,yPos, xPos + image.getWidth(), yPos + image.getHeight());
//		Rect dst = new Rect(xPos,yPos, xPos + 141, yPos + 141);
		//Log.d("tag", image.getHeight() + " " + image.getWidth());
//		canvas.drawBitmap(rotiranaSlika, src, dst, null);
		canvas.drawBitmap(image, src, dst, null);
		
	}
	
	@Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        super.onSizeChanged(w, h, oldw, oldh);
        view_w=w;
        view_h=h;
        xPos = view_w/2 -25;
        yPos = view_h/2 -25;
    	nit.start();
    }


	public void setFlag(boolean flag) {
		this.flag = flag;
	}


	public boolean isFlag() {
		return flag;
	}
}
