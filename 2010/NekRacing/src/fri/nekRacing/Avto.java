package fri.nekRacing;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.Chronometer;
import android.widget.ImageView;

public class Avto extends Activity{
	
	private  PictureView podlaga = null;
	boolean started = false;
	Chronometer cajt = null;
	
	public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.avto);
         
        Button backButton = (Button) findViewById(R.id.BackButton);
        backButton.setOnClickListener(backListener);
        
        
        cajt = (Chronometer) findViewById(R.id.Chronometer01); 
        
        podlaga = (PictureView) findViewById(R.id.picture_view);
        
        
        
    }
	
	
	
	
	
	
	
	int i=0;
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		
		if (podlaga.isFlag()) {
			switch (keyCode) {
			case KeyEvent.KEYCODE_DPAD_LEFT:
				if (started) {
					podlaga.setRotation(podlaga.getRotation() + 15);
					podlaga.setPhi(podlaga.getPhi() + Math.PI / 12);
					podlaga.setImage(--i);
					podlaga.postInvalidate();
					return true;
				}
			case KeyEvent.KEYCODE_DPAD_RIGHT:
				if (started) {
					podlaga.setRotation(podlaga.getRotation() - 15);
					podlaga.setPhi(podlaga.getPhi() - Math.PI / 12);
					podlaga.setImage(++i);
					podlaga.postInvalidate();
					return true;
				}
			case KeyEvent.KEYCODE_DPAD_UP:
				if (!started) {
					cajt.start();
					started = true;
					podlaga.changeSpeed(-3);
				}
				//podlaga.changeSpeed(-1);
				podlaga.moveCar();
				podlaga.postInvalidate();
				return true;
			case KeyEvent.KEYCODE_DPAD_DOWN:
				//podlaga.changeSpeed(1);
				//podlaga.moveCar();
				//podlaga.postInvalidate();
				return true;
			}
		}else{
			cajt.stop();
		}
		return false;
	}
	
	private OnClickListener backListener = new OnClickListener()
    {
        public void onClick(View v)
        {
            // Here we start the next activity, and then call finish()
            // so that our own will stop running and be removed from the
            // history stack.
        	try{
	            Intent intent = new Intent();
	            intent.setClass(Avto.this, Menu.class);
	            startActivity(intent);
	            finish();
        	}catch(Exception e){
	        	Log.d("tag", e.getMessage());
	        }
        }
    };
}
