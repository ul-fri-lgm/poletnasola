package sudoku.solver;

//import android.widget.TextView;



public class Sudoku{
	int[][] polje = new int[9][9];
	boolean reseno = false;
	
	public Sudoku(){
		//tu se kreira nov sudoku
		for (int i = 0; i < 9; ++i){
			for (int j = 0; j < 9; ++j){
				polje[i][j] = 0;
			}
		}
	}
	public String rawout(){
		String a = new String();
		for (int i = 0; i < 9; i++){
			for (int j = 0; j < 9; j++){
				a += polje[i][j];
			}
			a += "\n";
		}
		return a;
	}
	public void narisi(){

	}
	public void spremeni(int x, int y, int stevilka){
		polje[x][y] = stevilka;
	}
	
	public void spremeniVrstico(int x, int[] st){
		for (int i = 0; i < 9; i++){
			polje[x][i] = st[i];
		}
	}
	
	public boolean preveri(){
		for (int i = 0; i < 9; ++i){
			for (int j = 0; j < 9; ++j) {
				int a = polje[i][j];
				int b = polje[j][i];
				for (int k = j+1; k < 9; ++k){
					if (a != 0){
						if (a == polje[i][k]) return false;
					}
					if (b != 0){
						if (b == polje[k][i]) return false;
					}
				}
			}
		}
		for (int i = 1; i<8; i = i+3){ /*more bit do 8*/
			for (int j = 1; j<8; j = j+3){
				int[] a = {polje[i-1][j-1], polje[i][j-1], polje[i+1][j-1], polje[i-1][j], polje[i][j], polje[i+1][j], polje[i-1][j+1], polje[i][j+1], polje[i+1][j+1]};
				for (int k = 0; k < 9; ++k){
					if (a[k] == 0) continue;
					for (int l = k+1; l < 9; ++l){
						if (a[k] == a[l]) return false;
					}
				}
			}
		} 
		return true;
	}
	
	public int stNul(){
		int stnul = 0;
		for (int i = 0; i < 9; ++i){
			for (int j = 0; j < 9; ++j){
				if (polje[i][j] == 0) stnul++;
			}
		}
		return stnul;
	}
	
	public int stTrue (boolean[] a){
		int r = 0;
		for (int i = 0; i < 9; i++){
			if (a[i]) r++;
		}
		return r;
	}
	
	public int indexT (boolean[] a){
		for (int i = 0; i < 9; i++){
			if (a[i]) 
				return i;
		}
		return -1;
	}
	
	public void restart() {
		for (int i = 0; i < 9; i++){
			for (int j = 0; j < 9; j++){
				polje[i][j] = 0;
			}
		}
	}
	
	public boolean preveriPolje(int x, int y){
		if (polje[x][y] == 0) return true;
		int a = polje[x][y];
		for (int i = 0; i < 9; ++i){
			if ((polje[x][i] == a && i != y) || (polje[i][y] == a && x != i)) return false;
		}
		int i = ((int) (x/3))*3+1;
		int j = ((int) (y/3))*3+1;
		int[] b = {polje[i-1][j-1], polje[i][j-1], polje[i+1][j-1], polje[i-1][j], polje[i][j], polje[i+1][j], polje[i-1][j+1], polje[i][j+1], polje[i+1][j+1]};
		int c = 0;
		for (int k = 0; k < 9; k++){
			if (a == b[k]) c += 1;
		}
		if (c > 1) return false;
		return true;
	}
	public void resi(){
		boolean[][][] s1 = new boolean[9][9][9];
		while (true){
			int z = 0;
			while (true){
				int c = 0;
				// naredi s1
				for (int i = 0; i < 9; i++){
					for(int j = 0; j < 9; j++){
						for(int k = 0; k < 9; k++){
							s1[i][j][k] = false;
						}
					}
				}
				// konec delanja s1
				for (int i = 0; i < 9; i++){
					for(int j = 0; j < 9; j++){
						if (polje[i][j] == 0){
							for (int k = 1; k < 10; k++){
								polje[i][j] = k;
								if (preveriPolje(i,j)){
									s1[i][j][k-1] = true;
								}
							}
							if (stTrue(s1[i][j]) == 1){
								polje[i][j] = indexT(s1[i][j])+1;
								c++;
							} else polje[i][j] = 0;
						}
					}
				}
				if (c == 0) break;
			}
			
			for (int i = 0; i < 9; i++) // preveri za vrstice, ce je lahko ena stavilka samo na enem mestu
			{
				for (int j = 0; j < 9; j++)
				{
					boolean[] zacasni = new boolean[9];
					for (int k = 0; k < 9; k++)
					{
						zacasni[k] = s1[i][k][j];
					}
					if (stTrue(zacasni) == 1)
					{
						polje[i][indexT(zacasni)] = j+1;
						z++;
					}
				}
			} 
			 
			for (int i = 0; i < 9; i++) //preveri za stolpce, ce je lahko neka sevilka samo na enem mestu
			{
				for (int j = 0; j < 9; j++)
				{
					boolean[] zacasni = new boolean[9];
					for (int k = 0; k < 9; k++)
					{
						zacasni[k] = s1[k][j][i];
					}
					if (stTrue(zacasni) == 1)
					{
						polje[indexT(zacasni)][j] = i+1;
						z++;
					}
				}
			}
			for (int i = 0; i < 9; i++){
				for (int l = 0; l < 9; l += 3){
					for (int m = 0; m < 9; m += 3){
						boolean[] zacasni = new boolean[9];
						for (int j = 0; j < 3; j++){
							for (int k = 0; k < 3; k++){
								zacasni[j*3+k] = s1[l+j][m+k][i];
							}
						}
						if (stTrue(zacasni) == 1) {
							int in = indexT(zacasni);
							polje[l+in/3][m+in%3] = i+1;
							z++;
						}
					}
				}
			}
			if (z == 0) break;
			if (stNul() == 0) break;
		}
	}
	
	public void brute(int x, int y){
		if(polje[x][y] == 0){
			for (int i = 1; i < 11; i++){
				if (reseno) break;
				if (i != 10){
					polje[x][y] = i;
					if (preveriPolje(x, y)){
						if (x == y && y == 8){
							narisi();
							reseno = true;
						} else {
							brute(x+((y+1)/9), (y+1)%9);
						}
					}
				} else polje[x][y] = 0;
			}
		} else {
			if (x == y && y == 8){
				narisi();
				reseno = true;
			} else {
				brute(x+((y+1)/9), (y+1)%9);
			}
		}
	}
}
