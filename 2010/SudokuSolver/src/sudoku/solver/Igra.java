package sudoku.solver;

import android.app.Activity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class Igra extends Activity {
	Sudoku s = new Sudoku();
	public int[][] tabela =  {{R.id.s00, R.id.s01, R.id.s02, R.id.s03, R.id.s04, R.id.s05, R.id.s06, R.id.s07, R.id.s08}, {R.id.s10, R.id.s11, R.id.s12, R.id.s13, R.id.s14, R.id.s15, R.id.s16, R.id.s17, R.id.s18},{R.id.s20, R.id.s21, R.id.s22, R.id.s23, R.id.s24, R.id.s25, R.id.s26, R.id.s27, R.id.s28},{R.id.s30, R.id.s31, R.id.s32, R.id.s33, R.id.s34, R.id.s35, R.id.s36, R.id.s37, R.id.s38},{R.id.s40, R.id.s41, R.id.s42, R.id.s43, R.id.s44, R.id.s45, R.id.s46, R.id.s47, R.id.s48},{R.id.s50, R.id.s51, R.id.s52, R.id.s53, R.id.s54, R.id.s55, R.id.s56, R.id.s57, R.id.s58},{R.id.s60, R.id.s61, R.id.s62, R.id.s63, R.id.s64, R.id.s65, R.id.s66, R.id.s67, R.id.s68},{R.id.s70, R.id.s71, R.id.s72, R.id.s73, R.id.s74, R.id.s75, R.id.s76, R.id.s77, R.id.s78},{R.id.s80, R.id.s81, R.id.s82, R.id.s83, R.id.s84, R.id.s85, R.id.s86, R.id.s87, R.id.s88},};
	//Toast toast = Toast.makeText(this, R.string.Navodila, Toast.LENGTH_LONG);
	
	public void preberi(){
		EditText text;
		String message = new String();
		Utils.logI("prebiram");
		for(int x = 0; x < 9; x++){
			for(int y = 0; y < 9; y++){
				text = (EditText) findViewById(tabela[x][y]);
				message = text.getText().toString();
				if (message.length() != 0 && message != ""){
					s.spremeni(x, y, Integer.valueOf(message));
				}
			}
		}

	}
	//s.polje[x][y]
	public void zapisi() {
		EditText text;
		for(int x = 0; x < 9; x++){
			for(int y = 0; y < 9; y++){
				text = (EditText) findViewById(tabela[x][y]);
				if (s.polje[x][y] == 0) text.setText(null, TextView.BufferType.EDITABLE);
				else text.setText(String.valueOf(s.polje[x][y]), TextView.BufferType.EDITABLE);
			}
		}
	}
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		//Utils.logI("START"); 
		super.onCreate(savedInstanceState);	
		setContentView(R.layout.main);
	
	}
	@Override
	public boolean onCreateOptionsMenu(Menu menu){
		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.menu, menu);
		return true;
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item){
		Toast toast1 = Toast.makeText(this, R.string.Neresljiv, Toast.LENGTH_SHORT);
		toast1.setGravity(0, 0, 15);
		Toast toast = Toast.makeText(this, R.string.Navodila, Toast.LENGTH_LONG);
		toast.setGravity(0, 0, 15);
		switch (item.getItemId()){
		case R.id.mnov:{
			s.restart();
			s.reseno = false;
			setContentView(R.layout.main);
			zapisi();
			return true;
		}
		case R.id.mresi:{
			preberi();
			if (!s.preveri()){
				s.restart();
				toast1.show();
			} else {
				long t1 = System.nanoTime();
				s.resi();					s.brute(0,0);
				long t2 = System.nanoTime();
				double time=(1.0*t2-t1)/(1000*1000);
				Utils.logI("Resil v: " + time + " ms."); 
				if (s.stNul() != 0){
					toast1.show();
				} else 
				zapisi(); 
			}
		}
		return true;		
		case R.id.mnavodila:{
			toast.show();
		}
		}
		return false;
	}
}
