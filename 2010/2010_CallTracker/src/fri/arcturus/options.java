package fri.arcturus;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class options extends Activity
{
	Button buttonStart, buttonStop;
	public void onCreate(Bundle icicle)
	{
      super.onCreate(icicle);
      setContentView(R.layout.options);      
  
      String data[];

      try{
    	  data = ReadSettings().split(";");
          }
      catch (Exception e) {      
    	  WriteSettings(arcturus.defaults);
    	  data = ReadSettings().split(";");
      }      
  	  
  	  
   	 EditText et1 = (EditText)findViewById(R.id.OptionsInterval1);
     et1.setText(data[0]);
     EditText et2 = (EditText)findViewById(R.id.OptionsInterval2);
     et2.setText(data[1]);
  	 EditText et3 = (EditText)findViewById(R.id.call_limit);
     et3.setText(data[2]);
     EditText et4 = (EditText)findViewById(R.id.sms_limit);
     et4.setText(data[3]);
     EditText et5 = (EditText)findViewById(R.id.mms_limit);
     et5.setText(data[4]);
     EditText et6 = (EditText)findViewById(R.id.reset_day);
     et6.setText(data[5]);
     EditText et7 = (EditText)findViewById(R.id.whitelist);
     et7.setText(data[6]);
      
      Button b = (Button) findViewById(R.id.btnClick2);
      b.setOnClickListener(new View.OnClickListener() {
         public void onClick(View arg0) {
          	 EditText et1 = (EditText)findViewById(R.id.OptionsInterval1);
             EditText et2 = (EditText)findViewById(R.id.OptionsInterval2);
          	 EditText et3 = (EditText)findViewById(R.id.call_limit);
             EditText et4 = (EditText)findViewById(R.id.sms_limit);
             EditText et5 = (EditText)findViewById(R.id.mms_limit);
             EditText et6 = (EditText)findViewById(R.id.reset_day);
             EditText et7 = (EditText)findViewById(R.id.whitelist);
             
             String newData[] = new String[10];
             newData[0] = et1.getText().toString();
             newData[1] = et2.getText().toString();
             newData[2] = et3.getText().toString();
             newData[3] = et4.getText().toString();
             newData[4] = et5.getText().toString();
             newData[5] = et6.getText().toString();
             newData[6] = et7.getText().toString();
              
             String settings = "";
             for (int i = 0; i < newData.length; i++)
             {
            	 settings += newData[i]+";";
             }
             
             WriteSettings(settings);
             
         	arcturus.interval = Integer.parseInt(newData[0]);
         	arcturus.interval2 = Integer.parseInt(newData[1]);
         	arcturus.limit = Integer.parseInt(newData[2]);
         	arcturus.smsLimit = Integer.parseInt(newData[3]);
         	arcturus.mmsLimit = Integer.parseInt(newData[4]);
         	arcturus.resetDay = Integer.parseInt(newData[5]);
         	arcturus.whiteList = newData[6];
             
         	Context context = getApplicationContext();
         	
             setResult(RESULT_OK);
             finish();
         } 
      });
   }
   

   public void WriteSettings(String data){
       FileOutputStream fOut = null;
       OutputStreamWriter osw = null;

       try{
        fOut = openFileOutput("settings.dat",MODE_PRIVATE);      
        osw = new OutputStreamWriter(fOut);
        osw.write(data);
        osw.flush();
        Toast.makeText(this, "Nastavitve shranjene!",Toast.LENGTH_SHORT).show();
        }

        catch (Exception e) {      
        e.printStackTrace();
        Toast.makeText(this, "Prislo je do napake",Toast.LENGTH_SHORT).show();
        }
        finally {
           try {
                  osw.close();
                  fOut.close();
                  } catch (IOException e) {
                  e.printStackTrace();
                  }
        }
   }
  

   public String ReadSettings(){
       FileInputStream fIn = null;
       InputStreamReader isr = null;

       char[] inputBuffer = new char[255];
       String data = null;

       try{
        fIn = openFileInput("settings.dat");      
        isr = new InputStreamReader(fIn);
        isr.read(inputBuffer);
        data = new String(inputBuffer);
        }
        catch (Exception e) {      
        e.printStackTrace();
        }
        finally {
           try {
                  isr.close();
                  fIn.close();
                  } catch (IOException e) {
                  e.printStackTrace();
                  }
        }
              return data;
   }   
}
