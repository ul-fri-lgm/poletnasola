package fri.arcturus;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

public class NewServiceReceiver extends BroadcastReceiver
{
    @Override
    public void onReceive(Context context, Intent intent)
    {
        if(intent.getAction() != null)
        {
                if(intent.getAction().equals(Intent.ACTION_BOOT_COMPLETED))
                {
                        context.startService(new Intent(context, MyService.class));
                }
        }
    }
};