package fri.arcturus;

import fri.arcturus.Utils;
import fri.arcturus.R;

import android.app.Activity;
import android.os.Bundle;
import android.os.IBinder;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.TextView;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;


public class arcturus extends Activity /*implements OnClickListener*/ {
	static int interval = 60;
	static int interval2 = 1;
	static int limit = 1000;
	static int smsLimit = 1000;
	static int mmsLimit = 1000;
	static int resetDay = 15;
	static String whiteList = "040200815";
	static String defaults = "60;1;1000;1000;1000;15;040";
	
	static int minutes = 0;
	static int smsCount = 0;
	static int mmsCount = 0;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Context context = getApplicationContext();
        context.startService(new Intent(context, MyService.class));

        setContentView(R.layout.main);
        
        doBindService();
    	
        
    }
    
	MyService.Test mBoundService = null;

	private ServiceConnection mConnection = new ServiceConnection() {
	    public void onServiceConnected(ComponentName className, IBinder service) {
	    	
	        mBoundService = ((MyService.Test)service);
	        mBoundService.calculator();
	        TextView tv1 = (TextView)findViewById(R.id.smsCounter);
	        TextView tv2 = (TextView)findViewById(R.id.mmsCounter);
	        
	        TextView tv3 = (TextView)findViewById(R.id.callCounter);
	    	Utils.logW("555555555555");


	        Utils.logW("ACTIVITY IZVEDEN!");
	        tv1.setText(Integer.toString(smsCount)+" / "+smsLimit);
	        tv2.setText(Integer.toString(mmsCount)+" / "+mmsLimit);
	        tv3.setText(Integer.toString(minutes)+" / "+limit);
	        
	    }

	    public void onServiceDisconnected(ComponentName className) {
	        mBoundService = null;
	        
	    }

		
	};
	private boolean mIsBound =false;
	void doBindService() {
	    bindService(new Intent(this,MyService.class), mConnection, Context.BIND_AUTO_CREATE);
	    mIsBound = true;
	}

	void doUnbindService() {
	    if (mIsBound) {
	        unbindService(mConnection);
	        mIsBound = false;
	    }
	}

	@Override
	protected void onDestroy() {
	    super.onDestroy();
	    doUnbindService();
	}
    
    
    
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu, menu);
        return true;
    }
    
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case R.id.options: {
			Intent i = new Intent(arcturus.this, options.class);
	        startActivity(i);
			return true;
		}
		/*case R.id.stats: {
			//Intent i = new Intent(screen1.this, screen2.class);
	        //startActivity(i);
			return true;
		}*/
		}
		return false;
	}
	
}