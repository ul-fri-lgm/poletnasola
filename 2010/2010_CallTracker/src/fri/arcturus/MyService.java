package fri.arcturus;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Timer;
import java.util.TimerTask;

import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.app.Service;
import android.net.Uri;
import android.os.Binder;
import android.os.IBinder;

public class MyService extends Service {

	private Timer timer = new Timer();
    private long INTERVAL = 3600000;
	
	public IBinder onBind(Intent intent) {
		  return new Test();
		}
	public class Test extends Binder 
	{
		public void calculator()
		{
			calculate();
			getSmsCount();
		}
		
	}

	@Override
	public void onCreate() {
		super.onCreate();
		
	}

	@Override
	public void onDestroy() {
		stopservice();

	}
	
	@Override
	public void onStart(Intent intent, int startid) {
		startService();
		startTimerService();
	}

	private void startTimerService() {
        timer.scheduleAtFixedRate( new TimerTask() {
            public void run() {
            	getSmsCount();
            	Context context = getApplicationContext();
            	if(arcturus.smsLimit*0.9 < arcturus.smsCount)
            		context.startService(new Intent(context, smsNotificator.class));
            }
        }, 0, INTERVAL);
    ; }
	
	private void startService()
	{
		getSmsCount();
		getMmsCount();
    	calculate();
    	Utils.logW("SERVICE IZVEDEN!");
	}
	
    private void stopservice() {
        if (timer != null){
            timer.cancel();
        }
    }


    private void getSmsCount()
    {
    	String datArg="1234567890000";
    	try {
			datArg = Long.toString(dateCalc());
			
        	}
		catch (ParseException e) 
			{
			
			}
    	
         ContentResolver cr2 = getContentResolver();
         Cursor c2 = cr2.query(Uri.parse("content://sms/sent"),
         			new String[] { "_id", "thread_id", "address", "person", "date", "body" },
         			"date >="+datArg,
         			null,
         			null) ;
         //startManagingCursor(c2);
         c2.moveToNext();
         int count = c2.getCount();
         arcturus.smsCount = count;
         c2.close();
    }
    
    private void getMmsCount()
    {
    	String datArg="1234567890000";
    	try {
			datArg = Long.toString(dateCalc());
			
        	}
		catch (ParseException e) 
			{
			
			}
    	
         ContentResolver cr2 = getContentResolver();
         Cursor c2 = cr2.query(Uri.parse("content://mms/sent"),
         			new String[] { "date" },
         			"date >="+datArg,
         			null,
         			null) ;
         //startManagingCursor(c2);
         c2.moveToNext();
         int count = c2.getCount();
         arcturus.mmsCount = count;
         c2.close();
    }
    
    void calculate()
    {

    	String datArg="1234567890000";
		
        try {
			datArg = Long.toString(dateCalc());
			
        	}
		catch (ParseException e) 
			{
			
			}
		
		String numbers = "";
		String wl[] = arcturus.whiteList.split(",");
		for(int i=0; i<wl.length; i++)
		{
			  if(wl[i].length()<4) //ce je vnesena samo omrezna stevilka
			  {
				  numbers += android.provider.CallLog.Calls.NUMBER + " LIKE '" + wl[i] + "%%' OR ";
				  numbers += android.provider.CallLog.Calls.NUMBER + " LIKE '%%386" + wl[i].substring(1,wl[i].length()-1) + "%%' OR ";
			  }
			  else
				  numbers += android.provider.CallLog.Calls.NUMBER + " LIKE '%%" + wl[i] + "' OR ";
		}
		
		numbers = "("+numbers.substring(0,numbers.length()-4)+")";
		Utils.logW("NUMBERS: "+numbers);
		
		ContentResolver cr =  getContentResolver();
        Cursor c = cr.query(android.provider.CallLog.Calls.CONTENT_URI, 
        					null,
        					android.provider.CallLog.Calls.TYPE + "=2 AND " +
        					android.provider.CallLog.Calls.DATE	+ ">="+datArg+" AND " +
        					numbers,
        					null,
        					null);

        //startManagingCursor(c);
        //Dobimo indexe od stolpcov
        /*int numberColumn = c.getColumnIndex(android.provider.CallLog.Calls.NUMBER);
        int dateColumn = c.getColumnIndex(android.provider.CallLog.Calls.DATE);
        int typeColumn = c.getColumnIndex(android.provider.CallLog.Calls.TYPE);
        int personColumn = c.getColumnIndex(android.provider.CallLog.Calls.CACHED_NAME);*/
        int durationColumn = c.getColumnIndex(android.provider.CallLog.Calls.DURATION);
        double skupaj=0;
        double odklon=0;
        /*for (j = 0;j<columns.length;j++)
        	f+=columns[j];*/
        
        for(int i =0;i<c.getCount();i++)
        {
        	c.moveToNext();
        	double temp = Integer.parseInt(c.getString(durationColumn));
        	if (temp >= 3)
        		{
        		odklon = (temp>60) ? temp : 60;
        		}
        	else
        		odklon=0;
        	
        	skupaj+=odklon;
        	
        }
        Utils.logW("Dolzina "+Double.toString(skupaj)+" stevilo "+c.getCount());
        arcturus.minutes = (int)skupaj/60;
        c.close();
    }
   

    private static Long dateCalc() throws ParseException
    {
    Calendar cal = Calendar.getInstance();
    DateFormat dan = new SimpleDateFormat("dd");
    DateFormat mesec = new SimpleDateFormat("MM");
    DateFormat leto = new SimpleDateFormat("yyyy");
    String day = dan.format(cal.getTime());
    String month = mesec.format(cal.getTime());
    String year = leto.format(cal.getTime());
	String resetDan =Integer.toString(arcturus.resetDay);
	
    if(Integer.parseInt(day)<arcturus.resetDay)
    {
    	if(Integer.parseInt(month)==1)
    	{
    		year=Integer.toString(Integer.parseInt(year)-1);
    	}
    	month=Integer.toString(Integer.parseInt(month)-1);
    	if(month == "00")
    	{
    		month = "12";
    	}
    }
    String finalDate = year+"-";
    finalDate += ((month.length()==1) ? "0"+month : month)+"-";
    finalDate += ((resetDan.length()==1) ? "0"+resetDan : resetDan)+" 00:00:00";
    Utils.logW(finalDate);
    Date datumizer=new java.text.SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(finalDate);
    
    return datumizer.getTime();
    }
}
