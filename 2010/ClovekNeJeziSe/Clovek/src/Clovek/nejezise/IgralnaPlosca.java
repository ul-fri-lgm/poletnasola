package Clovek.nejezise;

import java.util.ArrayList;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;

public class IgralnaPlosca extends View{
	public static int view_w, view_h;
	public static int offset_x = 20, offset_y = 20;
	
	Igralec rdec;
	Igralec rumen;
	Igralec moder;
	Igralec zelen;
	
	int igralecNaPotezi = 1;
	
	
	static Kocka kocka;
	public static ArrayList<Polje> polja ;
	//private ArrayList<Hiska> hiske = new ArrayList<Hiska>();
//	Hiska[][]hiske=new Hiska[4][4];
	Dom[][]dom=new Dom[4][4];
	
	public IgralnaPlosca(Context context) {
		super(context);
		setFocusable(true);
		kocka = new Kocka(context);
	}
	
	public void initGraphics() {
		polja= new ArrayList<Polje>();

		rumen = new Igralec (1, this);
		moder = new Igralec (2, this);
		zelen= new Igralec( 3, this);
		rdec= new Igralec( 4, this);
        Log.i("clovek","dimenzije"+view_w+"h"+view_h); 
		//polja
		int razdalja_w=(view_w/2-offset_x)/8;
		float razdalja_w2=((float)view_w/2-offset_x-offset_x/4)/8;
		int razdalja_h=(view_h)/18;
		float razdalja_h2=((float)view_h/2-offset_y-offset_y/4)/8;

		polja.add(new Polje(view_w/2+offset_x,view_h+offset_y));//srednja spodaj
		
		polja.add(new Polje(view_w/2-offset_x/4, offset_y + view_h,Color.RED));//spodaj rdeca!
		for(int i=1;i<9;i++) {
			polja.add(new Polje(view_w/2 -offset_y/4 -(int)(i * razdalja_w2), offset_y + (view_h - i * razdalja_h)));//levo spodaj!	
		}
		
		polja.add(new Polje(offset_x,view_h/2+offset_y));//srednja levo!
		polja.add(new Polje(offset_x, view_h/2-5,Color.YELLOW));//levo rumena !
		
		for(int i=1;i<9;i++) {
		polja.add(new Polje(offset_x + (i * razdalja_w)-offset_x/4, view_h/2 -offset_y/4- (int)(i * razdalja_h2)));//levo zgoraj!
		}
		
		polja.add(new Polje((view_w/2)+offset_x,offset_y));//srednja zgoraj
		
		polja.add(new Polje(view_w/2+2*offset_x+offset_x/4, offset_y,Color.BLUE));//desno modra
		
		for(int i=1;i<9;i++) {
			polja.add(new Polje(view_w/2+2*offset_x + offset_x/4+(int)(i * razdalja_w2), offset_y + (int)(i * razdalja_h2)));//desno zgoraj			
		}
		polja.add(new Polje(view_w+offset_x,(view_h/2)+offset_y));//srednja desno

		polja.add(new Polje(view_w+offset_x,(view_h/2+ 2* offset_y +offset_y/4),Color.GREEN));//desno zelena
		
		for(int i=1;i<9;i++) {
			polja.add(new Polje(view_w+offset_x - (int)(i * razdalja_w2),(view_h/2)+ 2* offset_y +offset_y/4 + (int)(i * razdalja_h2)));//desno spodaj			
		}

		
		//dom
		for(int i=0;i<4;i++){
		dom[0][i]=new Dom(view_w/2+offset_x,view_h-i*offset_y-offset_y/4);//1
		dom[0][i].barva.setColor(Color.RED);
		dom[1][i]=new Dom((i+2)*offset_x+offset_x/4,view_h/2+offset_y);//2
		dom[1][i].barva.setColor(Color.YELLOW);
		dom[2][i]=new Dom(view_w/2+offset_x,(i+2)*offset_y+offset_y/4);//3
		dom[2][i].barva.setColor(Color.BLUE);
		dom[3][i]=new Dom(view_w-i*offset_x-offset_x/4,view_h/2+offset_y);//4
		dom[3][i].barva.setColor(Color.GREEN);
		}

	}
	
    @Override
	public boolean onTouchEvent(MotionEvent event) {
		if (event.getAction() == MotionEvent.ACTION_DOWN){
			float x=event.getX();
		float y=event.getY();
		Log.i("clovek", "Dotik na os x:"+ x + "os y:" + y);
	  if(x>(view_w/2-10) && x<(view_w/2+60) && y>(view_h/2-20) && y<(view_h/2)+50) {
			vrziKocko();
			postInvalidate(); 
		}
			
	}
		return true;
}
	
	/*public void init(Bundle savedInstanceState) {
        setFocusable(true);
        setFocusableInTouchMode(true);
        requestFocus(); //for keyevent to go to this view
		if (savedInstanceState==null) return;
	}*/
	
	/*public void saveState(Bundle outState) {
	}*/

	@Override
    protected void onDraw(Canvas canvas) {

        super.onDraw(canvas);
        
		rdec.figure.get(0).risi(canvas);
        
        Log.i("clovek", "risanje");
        rdec.draw(canvas);
        rumen.draw(canvas);
        moder.draw(canvas);
        zelen.draw(canvas);
        
		for(Polje p : polja) {
			p.draw(canvas);
			Log.i("clovek","draw polje "+p.x+" x "+p.y);
		}

		for(int j=0;j<4;j++)
		for(int i=0;i<4;i++){
			dom[j][i].draw(canvas);			
		}

		kocka.risi(canvas);
	 }

    
    @Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        super.onSizeChanged(w, h, oldw, oldh);
        view_w=w-40;
        view_h=h-40;
        Log.i("clovek","dimenzije"+view_w+"h"+view_h); 
        initGraphics();
    }
    
    public int vrziKocko() {
    	int v = kocka.vrzi();
    	switch (igralecNaPotezi) {
    	case 1:
			rumen.figure.get(0).move(v);
    		break;
    	case 2:
			moder.figure.get(0).move(v);
    		break;
    	case 3:
			zelen.figure.get(0).move(v);
    		break;
    	case 0:
			rdec.figure.get(0).move(v);
    		break;
    	}
    	igralecNaPotezi = (igralecNaPotezi +1)%4;
    	return 0;
    }
}
