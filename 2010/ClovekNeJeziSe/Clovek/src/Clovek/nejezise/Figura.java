package Clovek.nejezise;

import android.graphics.Canvas;
import android.graphics.Color;

public class Figura {
	boolean vHiski = true;
	boolean vDomu = false;
	int polozaj = 0;
	int id;
	int barva;
	int zac_p;
	public Figura(int d,int b,int z) {
		id=d;
		barva=b;
		zac_p=z;
	}
	public void move(int i){

		if (vHiski) {
			vHiski = false;
			
			polozaj = zac_p;
			IgralnaPlosca.polja.get(polozaj).zasedeno = true;
			IgralnaPlosca.polja.get(polozaj).barva.setColor(barva);
			IgralnaPlosca.polja.get(polozaj).resetColor();
		} else {
			IgralnaPlosca.polja.get(polozaj).zasedeno = false;
			IgralnaPlosca.polja.get(polozaj).resetColor();
			polozaj = (polozaj+i+1)%40;
			IgralnaPlosca.polja.get(polozaj).zasedeno = true;
			IgralnaPlosca.polja.get(polozaj).barva.setColor(barva);
		}

	}
	public void risi(Canvas c) {
		// TODO !!
	}
}
