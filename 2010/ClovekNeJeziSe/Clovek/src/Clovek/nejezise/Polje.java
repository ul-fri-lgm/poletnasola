package Clovek.nejezise;

import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;

public class Polje {
	public static final int radij = 10;
	
	int x, y;
	boolean zasedeno;
	Paint barva = new Paint();
	int originalnaBarva;
	
	public void nastavi(int x, int y) {

		barva.setStrokeWidth(2);
		barva.setAntiAlias(true);
		barva.setColor(Color.WHITE);
		zasedeno = false;
		this.x = x;
		this.y = y;
		
	}
	public Polje(int x, int y) {
         nastavi(x,y);
		originalnaBarva = Color.WHITE;
	}
	public Polje(int x, int y,int r) {
        nastavi(x,y);
        barva.setColor(r);
        originalnaBarva = r;
        		
	}
	public Polje(int x, int y,int r,boolean zased) {
        nastavi(x,y);
        barva.setColor(r);
        originalnaBarva = r;
        zasedeno=zased;		
	}
	public Polje(int x, int y, boolean zased) {
        nastavi(x,y);
		zasedeno=zased;
	}
	
	public void draw(Canvas c) {
		if(zasedeno==true)
			barva.setStyle(Paint.Style.FILL); 	
		else
			barva.setStyle(Paint.Style.STROKE); 
		c.drawCircle(x, y, radij, barva);
	}
	public void resetColor() {
		barva.setColor(originalnaBarva);		
	}
}
