package Clovek.nejezise;

import java.util.ArrayList;

import android.graphics.Canvas;
import android.graphics.Color;

public class Igralec {
	ArrayList<Figura> figure = new ArrayList<Figura>();
	Hiska mojaHiska;
	Dom mojDom;
	Canvas platno;
	int id;
	int barva;
	int zacetno_polje;
	IgralnaPlosca ip;
	
	public Igralec(int st, IgralnaPlosca ipl) {
		ip = ipl;
		id=st;
		switch(id){
		case 1:barva=Color.YELLOW;break;
		case 2:barva=Color.BLUE;break;
		case 3:barva=Color.GREEN;break;
		case 4:barva= Color.RED;
		}
		mojaHiska = new Hiska(st,barva);
		
		zacetno_polje=mojaHiska.zac_p;
		
		for (int i = 0; i < 4; i++)
			figure.add(new Figura(id,barva,zacetno_polje));
		for(int i=0;i<4;i++){
			figure.get(i).polozaj-=i+1;
		}
	}
	
	public void draw(Canvas c) {
		int temp;
		for(int i=0;i<4;i++){
			mojaHiska.polja.get(i).zasedeno=false;
			temp=figure.get(i).polozaj;
			if(temp<0)
				mojaHiska.polja.get(Math.abs(temp+1)).zasedeno=true;
		}
		mojaHiska.draw(c);
		/// izris figur!!! TODO
	}
	
	public boolean narediPotezo() {
		int vrednost = ip.vrziKocko();
		int a = 0;
		
		for (Figura f : figure)
			a += f.polozaj;
		
		if (a  == -4) {	// ce so vsi v hiski
			for (int i = 0; i < 2; i++) {
				if (vrednost != 6) {  // mecemo najvec 3x
					vrednost = ip.vrziKocko();
				} else {
					break;
				}
			}
			if (vrednost == 6) {  // dobili smo 6
				// ali je kdo ze na tem polju?  TODO !!
				
				figure.get(0).polozaj = zacetno_polje;
				
			}
		} else {  // zunaj imamo figuro
			for (Figura f : figure) {
				if (!f.vHiski && !f.vDomu) {
					// ali je ze doma TODO !!
					f.polozaj += vrednost;
					break;
				}
			}
		}
			
		
		return true;
	}
}
