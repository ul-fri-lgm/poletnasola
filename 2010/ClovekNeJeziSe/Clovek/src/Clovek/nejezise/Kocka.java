package Clovek.nejezise;

import java.util.ArrayList;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.drawable.Drawable;
import android.util.Log;

public class Kocka {
	ArrayList<Drawable> slike = new ArrayList<Drawable>();
	int x = IgralnaPlosca.offset_x + IgralnaPlosca.view_w/2, y = IgralnaPlosca.offset_y + IgralnaPlosca.view_h/2;
	Context context;
	
	int vrednost = 1;
	public Kocka(Context context) {
		vrzi();
		this.context = context;
		slike.add(context.getResources().getDrawable(R.drawable.ena));
		slike.add(context.getResources().getDrawable(R.drawable.dva));
		slike.add(context.getResources().getDrawable(R.drawable.tri));
		slike.add(context.getResources().getDrawable(R.drawable.stiri));
		slike.add(context.getResources().getDrawable(R.drawable.pet));
		slike.add(context.getResources().getDrawable(R.drawable.sest));
	}
	
	public int vrzi() {
		vrednost = (int)(Math.random()*6);
        Log.i("clovek", "kocka je vrgla "+ vrednost);
		return (vrednost);
	}
	
	public void risi(Canvas c) {
		Bitmap bmp;
		switch (vrednost) {
		case 0:
			slike.get(vrednost).setBounds(IgralnaPlosca.view_w/2, IgralnaPlosca.view_h/2, IgralnaPlosca.view_w/2+37, IgralnaPlosca.view_h/2+37);
			slike.get(vrednost).draw(c);
			break;
		case 1:
			slike.get(vrednost).setBounds(IgralnaPlosca.view_w/2, IgralnaPlosca.view_h/2, IgralnaPlosca.view_w/2+37, IgralnaPlosca.view_h/2+37);
			slike.get(vrednost).draw(c);
			break;
		case 2:
			slike.get(vrednost).setBounds(IgralnaPlosca.view_w/2, IgralnaPlosca.view_h/2, IgralnaPlosca.view_w/2+37, IgralnaPlosca.view_h/2+37);
			slike.get(vrednost).draw(c);
			break;
		case 3:
			slike.get(vrednost).setBounds(IgralnaPlosca.view_w/2, IgralnaPlosca.view_h/2, IgralnaPlosca.view_w/2+37, IgralnaPlosca.view_h/2+37);
			slike.get(vrednost).draw(c);
			break;
		case 4:
			slike.get(vrednost).setBounds(IgralnaPlosca.view_w/2, IgralnaPlosca.view_h/2, IgralnaPlosca.view_w/2+37, IgralnaPlosca.view_h/2+37);
			slike.get(vrednost).draw(c);
			break;
		case 5:
			slike.get(vrednost).setBounds(IgralnaPlosca.view_w/2, IgralnaPlosca.view_h/2, IgralnaPlosca.view_w/2+37, IgralnaPlosca.view_h/2+37);
			slike.get(vrednost).draw(c);
			break;
			
		}
		Log.d("Kocka", ""+vrednost);
	}
}
