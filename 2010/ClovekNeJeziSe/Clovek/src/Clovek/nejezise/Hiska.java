package Clovek.nejezise;

import java.util.ArrayList;

import android.graphics.Canvas;
import android.graphics.Color;

public class Hiska{
	ArrayList<Polje> polja = new ArrayList<Polje>();
	int hisaX, hisaY;
	int barva;
	int zac_p;
	int id;
	

	public Hiska(int mesto,int c){
		barva = c;


		if(mesto==1){ //zgoraj levo
		//hiške
		polja.add(new Polje(IgralnaPlosca.offset_x,IgralnaPlosca.offset_y,c,true));//prva vrstica
		polja.add(new Polje(2*IgralnaPlosca.offset_x,IgralnaPlosca.offset_y,c,true));//prva vrstica
		polja.add(new Polje(IgralnaPlosca.offset_x,2*IgralnaPlosca.offset_y,c,true));//srednja sredinska desno
		polja.add(new Polje(2*IgralnaPlosca.offset_x,2*IgralnaPlosca.offset_y,c,true));//srednja sredinska desno
		zac_p=11;
		}
		
		if(mesto==2){// zgoraj desno
			//hiške
			polja.add(new Polje(IgralnaPlosca.view_w,IgralnaPlosca.offset_y,c,true));//prva vrstica
			polja.add(new Polje(IgralnaPlosca.offset_x+IgralnaPlosca.view_w,IgralnaPlosca.offset_y,c,true));//prva vrstica
			polja.add(new Polje(IgralnaPlosca.view_w,2*IgralnaPlosca.offset_y,c,true));//prva vrstica
			polja.add(new Polje(IgralnaPlosca.offset_x+IgralnaPlosca.view_w,2*IgralnaPlosca.offset_y,c,true));//prva vrstica
			zac_p=21;
		}
		
		if(mesto==3){ //spodaj desno
			//hiške
			polja.add(new Polje(IgralnaPlosca.view_w,IgralnaPlosca.view_h,c,true));//prva vrstica
			polja.add(new Polje(IgralnaPlosca.view_w+IgralnaPlosca.offset_x,IgralnaPlosca.view_h,c,true));//prva vrstica
			polja.add(new Polje(IgralnaPlosca.view_w,IgralnaPlosca.view_h+IgralnaPlosca.offset_y,c,true));//prva vrstica
			polja.add(new Polje(IgralnaPlosca.view_w+IgralnaPlosca.offset_x,IgralnaPlosca.view_h+IgralnaPlosca.offset_y,c,true));//prva vrstica
			zac_p=31;
			}
			
			if(mesto==4){// spodaj levo
				//hiške
				polja.add(new Polje(IgralnaPlosca.offset_x,IgralnaPlosca.view_h,c,true));//prva vrstica
				polja.add(new Polje(2*IgralnaPlosca.offset_x,IgralnaPlosca.view_h,c,true));//prva vrstica
				polja.add(new Polje(IgralnaPlosca.offset_x,IgralnaPlosca.view_h+IgralnaPlosca.offset_y,c,true));//srednja sredinska desno
				polja.add(new Polje(2*IgralnaPlosca.offset_x,IgralnaPlosca.view_h+IgralnaPlosca.offset_y,c,true));//srednja sredinska desno
				zac_p=1;
			}
	
		
	}

	public void draw(Canvas c) {
		for (Polje p : polja)
			p.draw(c);
		
	}
}
