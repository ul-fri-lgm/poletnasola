package SendSMSonTime.com;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Environment;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

public class ReadSentSMS extends Activity {

	ListView lv;

	ArrayList<String> al = new ArrayList<String>();

	File root = Environment.getExternalStorageDirectory();
	File fajl = new File(root, "SendSMSonTime.txt");
	String str = "";

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.menubrisi, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {

		switch (item.getItemId()) {
		case R.id.itemDelete:
			if (android.os.Environment.getExternalStorageState()
					.equals(android.os.Environment.MEDIA_MOUNTED)) {
			boolean deleted = fajl.delete();

			if (deleted) {
				Toast t = Toast.makeText(getApplicationContext(),
						R.string.deleteAll, Toast.LENGTH_SHORT);
				t.show();
				
			}}
			else{
				
				Toast t = Toast.makeText(getApplicationContext(),
						R.string.noSDcardD, Toast.LENGTH_SHORT);
				t.show();	
			}

			Intent back = new Intent(this, ReadSentSMS.class);
			startActivity(back);
			break;
		}

		return true;
	}

	@Override
	public void onCreate(Bundle icicle) {
		super.onCreate(icicle);
		setContentView(R.layout.berisms);
		lv = (ListView) findViewById(R.id.ListView01);

		try {
			if (root.canRead()) {
				FileReader fr = new FileReader(fajl);
				BufferedReader rd = new BufferedReader(fr);
				try {
					while (rd.read() != -1) {

						str = rd.readLine();
						al.add(str);
					}
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

			} else {
				Toast toast = Toast.makeText(getApplicationContext(),
						R.string.nifajlaerror, Toast.LENGTH_SHORT);
				toast.setGravity(Gravity.CENTER | Gravity.CENTER, 0, 0);
				toast.show();

			}
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		lv.setAdapter(new ArrayAdapter<String>(this,
				android.R.layout.simple_list_item_1, al));

	}

}
