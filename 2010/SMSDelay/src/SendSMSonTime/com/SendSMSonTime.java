package SendSMSonTime.com;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Timer;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Typeface;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Environment;
import android.os.Vibrator;
import android.provider.Contacts.People;
import android.provider.Contacts.PeopleColumns;
import android.provider.Contacts.PhonesColumns;
import android.telephony.SmsManager;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class SendSMSonTime extends Activity {

	private static final int casSe = 1;
	/** Called when the activity is first created. */
	private static final int PICK_CONTACT = 0;
	private static String tel = ""; // telefonska iz kontaktov
	private static String tel1 = ""; // telefonska iz lastnega vpisa!
	private static int zastavica = 0; // if(zastavica = 0) iz kontakotov else
	private String besedilo = "";
	private NotificationManager mNotManager;
	private String cas = "";
	private int dovolji1 = 0;
	private int dovolji2 = 0;
	private int dovolji3 = 0;
	String trenutnicas;
	static String sendTime;
	Button findinPB;
	EditText inSMS;
	EditText inTel;
	private float mAccel; // acceleration apart from gravity
	private float mAccelCurrent; // current acceleration including gravity
	private float mAccelLast; // last acceleration including gravity
	// lastni vpis
	

	
	public static void pisifajl(String besedilo, String trenutnicas){
		
		Context x = null;
		GregorianCalendar gc = new GregorianCalendar();
		Date d = gc.getTime();
		
		if(d.getMinutes()<10){
			sendTime = " "+d.getDate()+"."+(d.getMonth()+1)+" "+d.getHours()+":0"+d.getMinutes();
		}else
		{
		
			sendTime = " "+d.getDate()+"."+(d.getMonth()+1)+" "+d.getHours()+":"+d.getMinutes();
		}
		
		

		if (android.os.Environment.getExternalStorageState()
				.equals(android.os.Environment.MEDIA_MOUNTED)) {
			try {
				File root = Environment
						.getExternalStorageDirectory();

				if (root.canWrite()) {

					File fajl = new File(root, "SendSMSonTime.txt");
					FileWriter f = new FileWriter(fajl, true);

					if (zastavica == 0)
						f.append("\n SMS was sent to: " + tel
								+ ": " + " at " + trenutnicas + " "
								+ besedilo+" recvied at "+sendTime);
					else
						f.append("\n SMS was sent to: " + tel1
								+ ": " + " at " + trenutnicas + " "
								+ besedilo+" recvied at "+sendTime);

					f.close();

				}

				else {
					Toast t = Toast.makeText(
							x.getApplicationContext(),
							R.string.impo,
							Toast.LENGTH_SHORT);
					t.show();

				}

			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();

			}
		} else {
			Toast t = Toast
					.makeText(
							x.getApplicationContext(),
							R.string.noSDcard,
							Toast.LENGTH_SHORT);
			t.show();

		}
		
	}
	
	

	private final SensorEventListener mSensorListener = new SensorEventListener() {

		@Override
		public void onAccuracyChanged(Sensor sensor, int accuracy) {
			// TODO Auto-generated method stub
			if (mAccel > 8) {
				inSMS.setText("");

				Vibrator vib = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
				vib.vibrate(250); // 1/4 sekunde za brisanje text inputa

			}
		}

		public void onSensorChanged(SensorEvent se) {
			float x = se.values[0];
			float y = se.values[1];
			float z = se.values[2];
			mAccelLast = mAccelCurrent;
			mAccelCurrent = (float) Math.sqrt((x * x + y * y + z * z));
			float delta = mAccelCurrent - mAccelLast;
			mAccel = mAccel * 0.9f + delta;
		}

	};
	private SensorManager mSensorManager;
	private String MyNotifiyText = "SendSMSonTime is running!";
	private String MyNotifyTitle = "SendSMSonTime app";
	Button send;
	Button setDT; // set date and time
	Timer time;

	private int YOURAPP_NOTIFICATION_ID;

	@Override
	public void onActivityResult(int reqCode, int resultCode, Intent data) {
		super.onActivityResult(reqCode, resultCode, data);

		switch (reqCode) {
		case (PICK_CONTACT):
			if (resultCode == Activity.RESULT_OK) {
				Uri contactData = data.getData();
				Cursor c = managedQuery(contactData, null, null, null, null);
				if (c.moveToFirst()) {
					String name = c.getString(c
							.getColumnIndexOrThrow(PeopleColumns.NAME));
					tel = c.getString(c
							.getColumnIndexOrThrow(PhonesColumns.NUMBER));
					if (tel == null) {
						Toast t = Toast.makeText(getApplicationContext(),
								R.string.notificationNoPN, Toast.LENGTH_SHORT);
						t.setGravity(Gravity.CENTER | Gravity.CENTER, 0, -80);
						t.show();

					} else
						findinPB.setText("SMS will be sent to " + name + " "
								+ tel);
				}
			}
			break;
		case (casSe):
			if (resultCode == Activity.RESULT_OK) {
				cas = data.getAction();

			}
		}

	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		setContentView(R.layout.main);

		inTel = (EditText) findViewById(R.id.EditText01);
		inSMS = (EditText) findViewById(R.id.EditText02);
		send = (Button) findViewById(R.id.Button01);
		findinPB = (Button) findViewById(R.id.Button02);
		setDT = (Button) findViewById(R.id.Button03);
		inTel.setText("Phone");
		inSMS.setText("Text");
		inSMS.setHeight(125);
		send.setText("Send SMS");
		findinPB.setText("Search phonebook");
		setDT.setText("Set date and time of sending");
		send.setEnabled(false);

		mSensorManager = (SensorManager) getSystemService(Context.SENSOR_SERVICE);
		mSensorManager.registerListener(mSensorListener, mSensorManager
				.getDefaultSensor(Sensor.TYPE_ACCELEROMETER),
				SensorManager.SENSOR_DELAY_NORMAL);
		mAccel = 0.00f;
		mAccelCurrent = SensorManager.GRAVITY_EARTH;
		mAccelLast = SensorManager.GRAVITY_EARTH;
		
		
		
		
	
		inTel.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				send.setEnabled(true);

				dovolji1 = 1;
				inTel.setText("");
				zastavica = 1;
				Toast toast = Toast
						.makeText(getApplicationContext(),
								"Put recivers phone number in here",
								Toast.LENGTH_SHORT);
				toast.setGravity(Gravity.TOP | Gravity.CENTER, 0, 17);
				toast.show();

			}
		});

		setDT.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				setDT.setTypeface(null, Typeface.BOLD);
				send.setTypeface(null, Typeface.NORMAL);
				findinPB.setTypeface(null, Typeface.NORMAL);
				send.setEnabled(true);
				dovolji2 = 1;

				Toast toast = Toast.makeText(getApplicationContext(),
						R.string.set, Toast.LENGTH_SHORT);
				toast.setGravity(Gravity.CENTER | Gravity.CENTER, 0, 85);
				toast.show();

				Intent o = new Intent(SendSMSonTime.this, DateTime.class);
				SendSMSonTime.this.startActivityForResult(o, casSe);

			}
		});

		inSMS.setOnClickListener(new OnClickListener() { // listner za besedilo
					// smsja..

					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub
						Toast toast = Toast
								.makeText(getApplicationContext(),
										R.string.write,
										Toast.LENGTH_SHORT);
						toast.setGravity(Gravity.CENTER | Gravity.CENTER, 0,
								-40);
						toast.show();
					}
				});

		send.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {

				send.setTypeface(null, Typeface.BOLD);
				setDT.setTypeface(null, Typeface.NORMAL);
				findinPB.setTypeface(null, Typeface.NORMAL);

				tel1 = inTel.getText().toString();

				if ((zastavica == 1) && (tel1.length() < 9) || (dovolji2 == 0)) {
					Toast toast = Toast.makeText(getApplicationContext(),
							R.string.type,
							Toast.LENGTH_SHORT);
					toast.show();
				}

				else {
					// TODO Auto-generated method stub
					besedilo = inSMS.getText().toString();

					GregorianCalendar gc = new GregorianCalendar();
					Date d = gc.getTime();
					if(d.getMinutes()<10){
						trenutnicas = " "+d.getDate()+"."+(d.getMonth()+1)+" "+d.getHours()+":0"+d.getMinutes();
					}else{
						trenutnicas = " "+d.getDate()+"."+(d.getMonth()+1)+" "+d.getHours()+":"+d.getMinutes();
					}
					// zapisovanje vseh poslanih smsjev v datoteko na SD'card prej preverimo, �e imamo SD card!
					
					// string to long
					long l = Long.parseLong(cas.trim());
					l = l * 1000;
					

					new CountDownTimer(l, 1000) {

						@Override
						public void onFinish() {
							// TODO Auto-generated method stub
							SmsManager sms = SmsManager.getDefault();
							ArrayList<String> smstext = sms
									.divideMessage(besedilo);
							if (zastavica == 0)
								sms.sendMultipartTextMessage(tel, null,
										smstext, null, null);
							else if (tel1.length() > 8)
								sms.sendMultipartTextMessage(tel1, null,
										smstext, null, null);

							send.setText("SMS sent!");
							
							pisifajl(besedilo, trenutnicas);
						}

						@Override
						public void onTick(long doKonca) {
							// TODO Auto-generated method stub

							MyNotifiyText = trenutnicas;
							send.setText(" " + doKonca / 1000 + " seconds");

						}
					}.start();

				}
			}
		});
		
		

		
		
		findinPB.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				send.setEnabled(true);
				zastavica = 0;
				dovolji3 = 1;
				findinPB.setTypeface(null, Typeface.BOLD);
				setDT.setTypeface(null, Typeface.NORMAL);
				send.setTypeface(null, Typeface.NORMAL);

				Toast t = Toast.makeText(getApplicationContext(),
						R.string.FCNinPOB, Toast.LENGTH_SHORT);
				t.setGravity(Gravity.CENTER | Gravity.CENTER, 0, -80);
				t.show();

				Intent intent = new Intent(Intent.ACTION_PICK,
						People.CONTENT_URI);
				startActivityForResult(intent, PICK_CONTACT);
				
			}
		});

	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.menu, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		mNotManager=(NotificationManager)getSystemService(Context.NOTIFICATION_SERVICE);
		switch (item.getItemId()) {
		case R.id.icon:
			AlertDialog.Builder builder = new AlertDialog.Builder(this);
			builder.setMessage("Are you sure you want to exit?").setCancelable(
					false).setPositiveButton("Close APP, SMS still sent",
					new DialogInterface.OnClickListener() {
						public void onClick(DialogInterface dialog, int id) {
						

							String MyText = "Send SMS on time!";
						Notification mNotification = new Notification(
									R.drawable.icon, MyText, System
											.currentTimeMillis());

							Intent MyIntent = new Intent(
									getApplicationContext(),
									SendSMSonTime.class);
							MyIntent.putExtra("extendedTitle", MyNotifyTitle);
							MyIntent.putExtra("extendedText", MyNotifiyText);
							PendingIntent StartIntent = PendingIntent
									.getActivity(getApplicationContext(), 0,
											MyIntent, 0);

							mNotification.setLatestEventInfo(
									getApplicationContext(), MyNotifyTitle,
									MyNotifiyText, StartIntent);
							/* Sent Notification to notification bar */
							mNotManager.notify(YOURAPP_NOTIFICATION_ID,
									mNotification);
							finish();
						}
					}).setNegativeButton("Exit",
					new DialogInterface.OnClickListener() {
						public void onClick(DialogInterface dialog, int id) {
							mNotManager.cancel(YOURAPP_NOTIFICATION_ID);
							
							System.exit(0);

						}

					});
			AlertDialog alert = builder.create();
			alert.show();

			break;

		case R.id.item01:
			// help menu!
			AlertDialog.Builder ab = new AlertDialog.Builder(this);
			ab.setMessage(R.string.helpme).setCancelable(false)
					.setPositiveButton("Back",
							new DialogInterface.OnClickListener() {
								public void onClick(DialogInterface dialog,
										int id) {

								}
							});
			AlertDialog al = ab.create();
			al.show();
			break;

		case R.id.item02:

			Intent read = new Intent(SendSMSonTime.this, ReadSentSMS.class);
			SendSMSonTime.this.startActivity(read);

			break;
		}

		return true;
	}

	@Override
	protected void onResume() {
		super.onResume();
		mSensorManager.registerListener(mSensorListener, mSensorManager
				.getDefaultSensor(Sensor.TYPE_ACCELEROMETER),
				SensorManager.SENSOR_DELAY_NORMAL);
	}

	@Override
	protected void onStop() {
		mSensorManager.unregisterListener(mSensorListener);
		
		super.onStop();
	}

}
