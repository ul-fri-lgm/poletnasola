package SendSMSonTime.com;

import java.util.Calendar;
import java.util.GregorianCalendar;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

public class DateTime extends Activity {

	Button b;
	DatePicker dp;
	Button stS; // start service!
	long timetoSend = 0; // razlika v sekundah! Po�ljemo nazaj v glavni Activity
							// in tam naprej...
	TimePicker tp;
	TextView tv;

	@Override
	public void onCreate(Bundle icicle) {
		super.onCreate(icicle);
		setContentView(R.layout.dateintime);
		b = (Button) findViewById(R.id.Butt1);
		dp = (DatePicker) findViewById(R.id.DatePicker01);
		tv = (TextView) findViewById(R.id.TextView01);
		tp = (TimePicker) findViewById(R.id.TimePicker01);

		tp.setIs24HourView(true);
		b.setText(R.string.bsettext);

		b.setOnClickListener(new View.OnClickListener() {
			public void onClick(View arg0) {

				Calendar c = Calendar.getInstance();
				Calendar nast = Calendar.getInstance();

				int ura = tp.getCurrentHour();
				int minuta = tp.getCurrentMinute();

				nast.set(dp.getYear(), dp.getMonth(), dp.getDayOfMonth());

				int nastDay = nast.get(Calendar.DAY_OF_YEAR);
				int danDay = c.get(Calendar.DAY_OF_YEAR);

				long nD = nast.getTimeInMillis();
				long dD = c.getTimeInMillis();
				long diff = nD - dD;

				long diffSeconds = diff / 1000;
				int sendInS = (int) diffSeconds;

				int uraMinSecNastTime = ura * 3600 + minuta * 60; // nasltevljena
																	// ura
																	// pretvorjena
																	// v
																	// sekunde!

				Calendar calendar = new GregorianCalendar();

				int hour = calendar.get(Calendar.HOUR_OF_DAY); // trenutna ura
				int minute = calendar.get(Calendar.MINUTE); // trenutna minuta

				long uraMinSecTrenTime = hour * 3600 + minute * 60; // nastevljena
																	// trenutna
																	// ura
																	// pretovrjena
																	// v sekunde

				long razlika = 0;

				

				razlika = (uraMinSecNastTime - uraMinSecTrenTime) + diffSeconds;
				String nizRazlika = String.valueOf(razlika);
				setResult(RESULT_OK, (new Intent().setAction(nizRazlika)));

				if (razlika < 0) {
					Toast toast = Toast.makeText(getApplicationContext(), R.string.FutureError,
							Toast.LENGTH_SHORT);

					toast.setGravity(Gravity.CENTER | Gravity.CENTER, 0, 0);
					toast.show();

				} else {

					String text = "SMS will be send in " + razlika
							+ " seconds. Press SEND to confirm!";

					Toast toast = Toast.makeText(getApplicationContext(), text,
							Toast.LENGTH_LONG);
					toast.setGravity(Gravity.CENTER | Gravity.CENTER, 0, 0);
					toast.show();

					finish();

				}

			}

		});

	}

}
