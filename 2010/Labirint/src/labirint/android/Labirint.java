package labirint.android;

import android.app.Activity;
import android.content.Context;
import android.content.pm.ActivityInfo;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.os.PowerManager;
import android.os.PowerManager.WakeLock;
import android.view.KeyEvent;
import android.view.Window;

public class Labirint extends Activity {
	/** Called when the activity is first created. */
	SensorManager mSensorManager;
	private LabirintView LView = null; 
	private Zoga zogica;
	float sprememba_hitrosti = 0.001f;
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
	//	 PowerManager pm = (PowerManager) getSystemService(Context.POWER_SERVICE);  
	//     WakeLock wl = pm.newWakeLock(PowerManager.FULL_WAKE_LOCK, "DoNotDimScreen");  
		mSensorManager = (SensorManager) getSystemService(SENSOR_SERVICE);
		zogica = new Zoga(0, 0, this);
		LView = new LabirintView(this, zogica);
		zogica.View = LView;
		Thread nit = new Thread(zogica);
		nit.start();
		
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		Utils.logW("Created");

		setContentView(LView);
		setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);  

	}
	public void onRestart() {
		super.onRestart();
		Utils.logV("onRestart");
	}

	public void onStart() {
		super.onStart();
		Utils.logV("onStart");

	}

	public void onStop() {
		super.onStop();
		Utils.logV("onStop");
        mSensorManager.unregisterListener(LView);
	}

	public void onResume() {
		super.onResume();
		Utils.logV("onResume");
        mSensorManager.registerListener(LView, 
                SensorManager.SENSOR_ACCELEROMETER | 
                SensorManager.SENSOR_MAGNETIC_FIELD | 
                SensorManager.SENSOR_ORIENTATION,
                SensorManager.SENSOR_DELAY_FASTEST);
	}

	public void onPause() {
		super.onPause();
		Utils.logV("onPause");
	}

	public void onDestroy() {
		super.onDestroy();
		Utils.logV("onDestroy");
	}

	public void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);
		Utils.logV("SavedInstance");

	}

	@Override
	public Object onRetainNonConfigurationInstance() {
		Utils.logV("onRetainNonConfigurationInstance");
		return null;
	}

	public boolean onKeyUp(int keyCode, KeyEvent event) {
		Utils.logV("onKeyUp");
		switch (keyCode) {
		case KeyEvent.KEYCODE_DPAD_LEFT:
			premikLevo();
			return true;
		case KeyEvent.KEYCODE_DPAD_RIGHT:
			premikDesno();
			return true;
		case KeyEvent.KEYCODE_DPAD_UP:
			premikGor();
			return true;
		case KeyEvent.KEYCODE_DPAD_DOWN:
			premikDol();
			return true;
		}
		return false;
	}

	public void premikLevo(){
	//	zogica.dvx = zogica.dvx - sprememba_hitrosti; 

	}
	public void premikDesno(){
	//	zogica.dvx = zogica.dvx + sprememba_hitrosti; 

	}
	public void premikGor(){
	//	zogica.dvy = zogica.dvy + sprememba_hitrosti; 

	}
	public void premikDol(){
	//	zogica.dvy = zogica.dvy + sprememba_hitrosti; 

	}

}