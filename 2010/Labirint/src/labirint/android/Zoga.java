package labirint.android;


import java.util.ArrayList;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Rect;
import android.os.Environment;
import android.util.Log;

public class Zoga implements Runnable{
	
	private boolean flag = true;
	private int t = 30;
	LabirintView View;
	Context context;
	
	private float vx = 0, vy = 0;
	float dvx = 0,dvy = 0;
	int x,y;
	ArrayList<Luknja> lukne = new ArrayList<Luknja>();
	
	Zoga(int pozx, int pozy, Context con) {
		context = con;
		x = pozx;
		y = pozy;
		lukne.add(new Luknja(55,74,20,false));
		lukne.add(new Luknja(199,34,20,false));
		lukne.add(new Luknja(362,94,20,false));
		lukne.add(new Luknja(418,160,20,false));
		lukne.add(new Luknja(456,256,20,false));
		lukne.add(new Luknja(218,259,20,false));
		lukne.add(new Luknja(32,259,20,true));
	}


	String zogapot = Environment.getExternalStorageDirectory().toString() + "/images/zoga2.png";
	Bitmap zoga = Utils.loadBitmap(zogapot);
	
	
	
	public void izris(Canvas c){
		Rect windowRect = new Rect(x, y, x+30, y+30);
		Rect pictureRect = new Rect(0, 0, zoga.getWidth(), zoga.getHeight());
    	c.drawBitmap(zoga, pictureRect, windowRect, null);
	}

	@Override
	public void run() {
		while (flag) {

			vx += dvx;
			vy += dvy;
			
			
	//------------------------------------------------		
			if (vx>0)
				vx -= vx/30;
			else
				vx -= vx/30;
		//	
			if (vy>0)
				vy -= vy/40;
			else
				vy -= vy/40;
			
			if(x<0)
			{
				x=0;
				vx = -vx;
			}
			if(x>450)
			{
				x=450;
				vx = -vx;
			}
			if(y<0)
			{
				y=0;
				vy = -vy;
			}
			if(y>290)
			{
				y=290;
				vy = -vy;
			}
	//---------------------------------------------
			if((y>=89)&&(y<=128) && (x<=395))
			{
				vy = -vy;
		//		//vx = -vx;
			}
			if((y>=99 && y<=200) && (x<=395 && x>=390))
			{
				vx = -vx;
			
			}
			if((y>190) && (y<202) && (x<=395))
			{
				vy = -vy;
		
			}
    //---------------------------------------------
			x += (int)(vx*t);
			y += (int)(vy*t);
			
//			preverimo luknje
			int zogaX = x+15;
			int zogaY = y+15;
			for (Luknja l : lukne) {
				if (l.game == false) {
					Log.d("luknje", "" + l.radij*l.radij + " " + (zogaX-l.x)*(zogaX-l.x) + (zogaY-l.y)*(zogaY-l.y));
					if(l.radij*l.radij > ((zogaX-l.x)*(zogaX-l.x) + (zogaY-l.y)*(zogaY-l.y)))
						gameOver();
				} else {
					if((l.radij*l.radij) > ((zogaX-l.x)*(zogaX-l.x) + (zogaY-l.y)*(zogaY-l.y)))
						zmaga();
				}
			}
			
			
			View.postInvalidate();
	//	x=x--;
	//	y=y--;
			try {
				Thread.sleep(t);
			} catch (Exception e) {
			}
		}
	}

	private void zmaga() {
	//	Toast zmag = Toast.makeText(context, "Uspelo Vam je!", Toast.LENGTH_SHORT);
	//	zmag.show();
		x = 0;
		y = 0;
		
		Log.d("luknje", "zmaga");
	}

	private void gameOver() {
		//flag = false;
	//	String zacetnapot = Environment.getExternalStorageDirectory().toString() + "/images/game.png";
	//	Bitmap image = Utils.loadBitmap(zacetnapot);
		
	//	Rect splashRect = new Rect(0, 0, 480, 320);
	//	Rect splashRectp = new Rect(0, 0, image.getWidth(), image.getHeight());
   // LabirintView.canvas.drawBitmap(image, splashRect, splashRectp, null);
		//Toast toast = Toast.makeText(context, "Game Over!", Toast.LENGTH_SHORT);
	//	toast.show();
		//flag = true;
		x = 0;
		y = 0;
		Log.d("luknje", "game over");
		
		
	}

	public void podajMeritve(float[] meritve) {
		dvy= -meritve[1]/2500;
		dvx= -meritve[2]/2500;
	}

	
}
