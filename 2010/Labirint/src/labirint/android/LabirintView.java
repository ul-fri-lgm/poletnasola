package labirint.android;


import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Rect;
import android.hardware.SensorListener;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.View;


public class LabirintView extends View implements SensorListener {
	private Zoga zoga;
	private float meritve[] = new float[3];
	
	//String test;
	String zacetnapot = Environment.getExternalStorageDirectory().toString() + "/images/zacetna.png";
	Bitmap image = loadBitmap(zacetnapot);
	String map1pot = Environment.getExternalStorageDirectory().toString() + "/images/mapa01.png";
	Bitmap map1 = loadBitmap(map1pot);
	
	private boolean splash = true;
	private int view_w, view_h;

	public LabirintView(Context context, Zoga zoga) {
		super(context);
//		Log.v("sensors", meritve[0] + " " + meritve[1] + " " + meritve[2]);

		this.zoga = zoga;
		  
		Thread splashnit = new Thread() {
	      	public void run() {
	      			try 
	      			{
	      				Thread.sleep(2000);
	      				splash = false;
	      			}
	      			catch(Exception e) 
	      			{
	      				
	      		} 
	      	}
	      };
	      splashnit.start();
	}
	
  
   
  
  
	
	public void init(Bundle savedInstanceState) {
        setFocusable(true);
        setFocusableInTouchMode(true);
        requestFocus(); //for keyevent to go to this view
		if (savedInstanceState==null) return;
	}
	
	public static Bitmap loadBitmap(String filename) {
    	Bitmap bmp = BitmapFactory.decodeFile(filename);
    	return bmp;
    }
	
	public void saveState(Bundle outState) {
	}

	@Override
	protected synchronized void onDraw(Canvas canvas) {
		super.onDraw(canvas);

		
		Rect windowRect = new Rect(0, 0, view_w, (int)(view_w/((double)map1.getWidth()/map1.getHeight())));
		Rect pictureRect = new Rect(0, 0, map1.getWidth(), map1.getHeight());
    	canvas.drawBitmap(map1, pictureRect, windowRect, null);
    	
    	zoga.izris(canvas);

		if (splash) {
			Rect splashRect = new Rect(0, 0, view_w, (int)(view_w/((double)image.getWidth()/image.getHeight())));
			Rect splashRectp = new Rect(0, 0, image.getWidth(), image.getHeight());
	    	canvas.drawBitmap(image, splashRect, splashRectp, null);
		}
	}
	
	
	
    @Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        super.onSizeChanged(w, h, oldw, oldh);
        view_w=w;
        view_h=h;
  
    }

	@Override
	public void onAccuracyChanged(int sensor, int accuracy) {
		// TODO Auto-generated method stub
	}

	@Override
	public void onSensorChanged(int sensor, float[] values) {
		synchronized (this) {
			if (sensor == SensorManager.SENSOR_ORIENTATION) {
				for (int i = 0; i < 3; i++)
					meritve[i] = values[i];
			}
			zoga.podajMeritve(meritve)
;			invalidate();
		}
		Log.v("sensors", meritve[0] + " " + meritve[1] + " " + meritve[2]);
	}
    
    
    
}
