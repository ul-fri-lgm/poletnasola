package Start.android;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;

public class Alien {
	int x;
	int y;
	
	int dx = 2;
	Bitmap slika;
	
	Alien(int a, int b, Bitmap bmp){
		x=a;
		y=b;
		slika = bmp;
	}
	public void izris(Canvas canvas) {
        canvas.drawBitmap(slika, x, y, null);
		
	}

}
