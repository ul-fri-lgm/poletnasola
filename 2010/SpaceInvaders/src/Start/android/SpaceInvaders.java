package Start.android;

import java.util.ArrayList;
import java.util.Vector;

import android.app.Activity;
import android.app.AlertDialog;
import android.graphics.Bitmap;
import android.media.MediaPlayer;
import android.os.Environment;
import android.view.KeyEvent;
import android.content.Context;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.widget.TextView;
import android.widget.Toast;


public class SpaceInvaders extends Activity
{
	
	public static final int NUM_ENEMIES = 5;
	
	private Panel igralnaPovrsina;
	private boolean moveLeft = false;
	private boolean moveRight = false;
	
	private boolean flag = true;
	
	
	
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		igralnaPovrsina = new Panel(this);
		setContentView(igralnaPovrsina);
		


		try {
				mpstart =  MediaPlayer.create(this, R.raw.bitsound);
				
				mpstart.start();
				
		} catch (Exception e) {
			e.printStackTrace();
		}

	}
		private MediaPlayer mpstart;
		

		

	public boolean onKeyDown(int keyCode, KeyEvent event) {


		Utils.logV("onKeyDown");
		switch (keyCode) {
		case KeyEvent.KEYCODE_DPAD_LEFT:
			moveLeft = true;
			return true;
		case KeyEvent.KEYCODE_DPAD_RIGHT:
			moveRight = true;
			return true;
		}

		return false;

	}
	public boolean onKeyUp(int keyCode, KeyEvent event) {


		Utils.logV("onKeyUp");
		switch (keyCode) {
		case KeyEvent.KEYCODE_DPAD_LEFT:
			moveLeft = false;
			igralnaPovrsina.postInvalidate();
			return true;
		case KeyEvent.KEYCODE_DPAD_RIGHT:
			moveRight = false;
			igralnaPovrsina.postInvalidate();
			return true;
		case KeyEvent.KEYCODE_DPAD_CENTER:
			igralnaPovrsina.Shoot();
			return true;
		}

		return false;

	}
	

	class Panel extends View implements Runnable
	{
		Andy robo = new Andy(BitmapFactory.decodeResource(getResources(), R.drawable.andy_igralec));

		Vector<Metek> vecMetki = new Vector<Metek>();
		Vector<Alien> vecAlien = new Vector<Alien>();
		Bitmap slikaMetek = BitmapFactory.decodeResource(getResources(), R.drawable.metek);
		int aStevilka = 0;
		
		

		public Panel(Context context) {
			super(context);
			
			Bitmap slikca = BitmapFactory.decodeResource(getResources(), R.drawable.alien);
			int ax= 35;
			for(int j=0; j<NUM_ENEMIES; j++){
				vecAlien.add(new Alien(ax, 70, slikca));
				ax += 50;
			}
			ax= 35;
			for(int j=0; j<NUM_ENEMIES; j++){
				vecAlien.add(new Alien(ax, 120, slikca));
				ax += 50;
			}
			ax=35;
			for(int j=0; j<NUM_ENEMIES; j++){
				vecAlien.add(new Alien(ax, 170, slikca));
				ax += 50;
			}
			
			Thread th = new Thread(this);
			th.start();
			
		
		}

		@Override
		public void onDraw(Canvas canvas) {
			
			if(moveLeft)
				igralnaPovrsina.moveLeft(5);
			if(moveRight)
				igralnaPovrsina.moveRight(5);
			canvas.drawColor(Color.BLACK);
			//muvanje veslojckov
			int stDotikovZaslona = 0;
			boolean naprej = true;
			for(Alien premik : vecAlien){
				if(premik.x <= 280){
					premik.x += premik.dx;
					if(premik.x <=0){
						premik.dx= -premik.dx;
						premik.y += 30;
						if(premik.y >= robo.y-20) {
							MessageBox("GAME OVER\nVec srece prihonjic");
							flag = false;
//							finish();
							mpstart.stop();
						}
					}
				}
				else {
					stDotikovZaslona++;
					if(stDotikovZaslona == 2) premik.dx = 3;
					if(stDotikovZaslona == 3)premik.dx = 4;
					premik.dx = - premik.dx;
					premik.x = 279;
					premik.y += 30;
					if(premik.y >= robo.y-20) {
						MessageBox("GAME OVER\nVec srece prihonjic");
						//finish();
						flag = false;
						mpstart.stop();
					}
				}
				
			}
			
			int i = 0;
			while(i < vecMetki.size())
			{
				Metek m = vecMetki.get(i);
				m.y -= 2;
				if(m.y < 2)
				{
					vecMetki.remove(i); 
						
				}
				else
				{
					i++;
				}
			}
			for(Metek m : vecMetki)
			{
				m.izris(canvas);
			}
			robo.izris(canvas);
			
			
			i = 0;
			while(i < vecAlien.size())
			{
				Alien et = vecAlien.get(i);
				boolean removeAlien = false;
				Metek krivMetek= null;
				for(Metek met : vecMetki)
				{	
					int x2 = et.x + 40;
					int x1 = et.x;
					if(met.x <= x2 && met.x>= x1 && met.y >= et.y && met.y <= et.y+30 )
					{
						removeAlien = true;
						krivMetek = met;
					}
				}
				
				if(removeAlien)
				{
					vecMetki.remove(krivMetek);
					vecAlien.remove(i);
					if(vecAlien.size()==0){
						MessageBox("Cestitam zmagal si!!");
						mpstart.stop();
						flag= false;
						//finish();
						
					}
				}
				else
				{
					i++;
				}
			}
			
			
			for(Alien pivo : vecAlien)
				pivo.izris(canvas);
			
		}
		//tost
		public void MessageBox(String message){
		    Toast.makeText(SpaceInvaders.this,message,Toast.LENGTH_SHORT).show();
		}

		public void moveLeft(int dx) {
			if(robo.x+dx > 10)
				robo.x -= dx;
		}
		public void moveRight(int dx) {
			if(robo.x+dx < 280)
				robo.x += dx;
		}


		private void Shoot() {
			if(vecMetki.size() < 1){
				Metek metek = new Metek(robo.x, robo.y, slikaMetek);
				vecMetki.add(metek);
			}

		}
		

		@Override
		public void run() {
			while(flag)
			{
				postInvalidate();
				try {
					Thread.sleep(25);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			// TODO Auto-generated method stub
			
		}
	}

	@Override
	public boolean onTouchEvent(MotionEvent event) {
		if (event.getAction() == MotionEvent.ACTION_DOWN)
			finish();
		return true;
	}
	

}






