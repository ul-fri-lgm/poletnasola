package fridroid.krizanka;

public class Polje 
{
	public static final int PRAZNO_POLJE 			= 0;
	public static final int VNOSNO_POLJE 			= 1;
	public static final int VNOSNO_POLJE_STEVILKA 	= 2;
	
	/**
	 * Tabela objektov PoljeEnota
	 */
	private PoljeEnota[][] polje;
	
	/**
	 * Ustvari nov objekt polje
	 * @param sirina
	 * @param visina
	 */
	public Polje(int sirina, int visina)
	{
		polje = new PoljeEnota[sirina][visina];
		
		for (int i=0;i<sirina;i++)
			for (int j=0;j<visina;j++)
				polje[i][j] = new PoljeEnota(PRAZNO_POLJE);
	}
	
	/**
	 * Na lokacijo x,y nastavi �rko c
	 * @param x
	 * @param y
	 * @param c
	 */
	public void setVrednost(int x, int y, char c)
	{
		if (x >= 0 && x < polje.length && y >= 0 && y < polje[0].length)
			polje[x][y].Znak = c;
	}
	
	/**
	 * Vrne objekt PoljeEnota, ki vsebuje podatke o polju na lokaciji x,y
	 * @param x
	 * @param y
	 * @return
	 */
	public PoljeEnota getVrednost(int x, int y)
	{
		x = Math.max(0, Math.min(x, polje.length-1));
		y = Math.max(0, Math.min(y, polje[0].length-1));
		return polje[x][y];
	}
	
	/**
	 * Pridobi �tevilko vpra�anja na poziciji x,y. �e tip polja ni VNOSNO_POLJE_STEVILKA
	 * vrne vrednost 0.
	 * @param x
	 * @param y
	 * @return
	 */
	public int getStevilka(int x, int y)
	{
		if (getTipPolja(x,y) == VNOSNO_POLJE_STEVILKA)
			return polje[x][y].Stevilka;
		return 0;
	}
	
	/**
	 * Pridobi tip polja na lokaciji x,y
	 * @param x
	 * @param y
	 * @return
	 */
	public int getTipPolja(int x, int y)
	{
		return polje[x][y].Tip;
	}

}
