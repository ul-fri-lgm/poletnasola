package fridroid.krizanka;

import java.util.ArrayList;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.Typeface;
import android.text.InputType;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.ContextMenu;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.ScaleGestureDetector;
import android.view.View;
import android.view.View.OnTouchListener;
import android.view.inputmethod.BaseInputConnection;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputConnection;
import android.widget.EditText;
import android.widget.Toast;

public class KrizankaView extends EditText implements OnTouchListener
{
	Paint paint = new Paint();
	Krizanka krizanka;
	float velikostVnosnegaPolja;
	int odmikOdRoba;
	Boolean touchEvent = false;
	int visina;
	int sirina;
	int resSirina;
	int resVisina;
	float dpi;
	
	long multitouch = 0;
	float posX = 0, posY = 0;
	float preX = 0, preY = 0;
	float scale = 1.0f;
	ScaleGestureDetector scaleDetektor;
	
	Bitmap vnosnoPolje;
	Bitmap izbranoPolje;
	Bitmap izbranoGeslo;
	Rect vnosnoPoljeKvadrat;
	
	int izbranX = -1;
	int izbranY = -1;
	
	public boolean SmerPisanja = Vprasanje.VODORAVNO;
	OnVprasanjeChanged vprasanjeChangedCallback;
	OnSmerChanged smerChangedCallback;
	Vprasanje trenutnoVprasanje = null;
	Paint font;
	
	public KrizankaView(Context context, AttributeSet attr)
	{
		super(context, attr);
		paint.setColor(Color.BLACK);
		
		// Zgeneriramo na�o testno kri�anko
		krizanka = Krizanka.zgenerirajKrizanko();
		sirina = krizanka.getSirina();
		visina = krizanka.getVisina();
		
		//dobimo dpi napreve, vecji kot je dpi vecje moramo risati
		dpi = getResources().getDisplayMetrics().density;
		
		vnosnoPolje = BitmapFactory.decodeResource(getResources(), R.drawable.vnosno_polje);
		izbranoPolje = BitmapFactory.decodeResource(getResources(), R.drawable.izbrano_polje);
		izbranoGeslo = BitmapFactory.decodeResource(getResources(), R.drawable.polje_geslo);
		vnosnoPoljeKvadrat = new Rect(0,0,vnosnoPolje.getWidth(), vnosnoPolje.getHeight());
		
		setFocusable(true);
		scaleDetektor = new ScaleGestureDetector(context,
				new ScaleListener());
		
		font = new Paint();
	    font.setTypeface(Typeface.DEFAULT_BOLD);
	    font.setTextSize(vnosnoPolje.getWidth()/3);
		
	    //setPadding(0, 0, 0, 0);
	    
		setOnTouchListener(this);
	}
	
	@Override
	protected void onDraw(Canvas canvas)
	{
		//super.onDraw(canvas);
		
		canvas.drawRect(0, 0,canvas.getWidth(), canvas.getHeight(), paint);
		
		resSirina = canvas.getWidth();
		resVisina = canvas.getHeight();
		
		//glede na �irino zaslona dolo�imo velikost polja
		velikostVnosnegaPolja = (resSirina/(float)sirina);
		
		Polje p = krizanka.getPolje();
		
		canvas.save();
		canvas.scale(scale, scale,resSirina/2,resVisina/2);
		canvas.translate(posX, posY);
		
		ArrayList<PoljeEnota> izbranaPolja = null;
		
		if (izbranX >= 0 && izbranY >=0 && izbranX < sirina && izbranY < visina)
		{
			if (p.getVrednost(izbranX, izbranY).jeVnosno())
			{
				if (SmerPisanja == Vprasanje.VODORAVNO)
				{
					if ((p.getVrednost(izbranX-1, izbranY).jeVnosno() && izbranX-1 >= 0) || 
							(p.getVrednost(izbranX+1, izbranY).jeVnosno() && izbranX+1 < sirina))
					{
						izbranaPolja = dobiVodoravnaPolja(izbranX, izbranY);
					}
					else
					{
						spremeniSmer(Vprasanje.NAVPICNO);
						izbranaPolja = dobiNavpicnaPolja(izbranX, izbranY);
					}
				}
				else
				{
					if ((p.getVrednost(izbranX, izbranY-1).jeVnosno() && izbranY-1 >= 0) ||
							(p.getVrednost(izbranX, izbranY+1).jeVnosno() && izbranY+1 < visina))
					{
						izbranaPolja = dobiNavpicnaPolja(izbranX, izbranY);
					}
					else
					{
						spremeniSmer(Vprasanje.VODORAVNO);
						izbranaPolja = dobiVodoravnaPolja(izbranX, izbranY);
					}
				}
			}
		}
		
		//narisemo Koordinata	
		for (int i=0;i<sirina;i++)
		{
			
			for (int j=0;j<visina;j++)
			{
				Rect polje = new Rect(Math.round(i*velikostVnosnegaPolja), Math.round(j*velikostVnosnegaPolja), Math.round(i*velikostVnosnegaPolja+velikostVnosnegaPolja), Math.round(j*velikostVnosnegaPolja+velikostVnosnegaPolja));
				
				if (p.getTipPolja(i, j) == Polje.PRAZNO_POLJE)
					canvas.drawRect(polje, paint);
				else
				{
					if (izbranX == i && izbranY == j)
						canvas.drawBitmap(izbranoPolje, vnosnoPoljeKvadrat, polje, paint);
					else if (poljeJeIzbranoGeslo(p.getVrednost(i, j), izbranaPolja))
						canvas.drawBitmap(izbranoGeslo, vnosnoPoljeKvadrat, polje, paint);
					else
						canvas.drawBitmap(vnosnoPolje, vnosnoPoljeKvadrat, polje, paint);
					if (p.getTipPolja(i, j) == Polje.VNOSNO_POLJE_STEVILKA)
						canvas.drawText(p.getStevilka(i, j) + "", polje.left+2, polje.top+15, paint);
					if (p.getVrednost(i, j).jeVnosno())
						canvas.drawText(p.getVrednost(i, j).Znak+"", polje.left+(int)(velikostVnosnegaPolja*0.2), polje.top+(int)(velikostVnosnegaPolja*0.9), font);
				}
			}
		}
		canvas.restore();

	}
	public InputConnection onCreateInputConnection(EditorInfo outAttrs) {
		Log.d("mytag", "onCreateInputConnection");

		BaseInputConnection fic = new BaseInputConnection(this, false);
		outAttrs.actionLabel = null;
		//outAttrs.inputType = InputType.TYPE_NULL;
		//outAttrs.imeOptions = EditorInfo.IME_ACTION_NEXT;
		outAttrs.inputType = InputType.TYPE_TEXT_FLAG_NO_SUGGESTIONS;
		outAttrs.imeOptions = EditorInfo.IME_ACTION_DONE;
		return fic;
	}

	public boolean onTouch(View arg0, MotionEvent event) 
	{
		super.onTouchEvent(event);
		scaleDetektor.onTouchEvent(event);
		if (event.getPointerCount() > 1)
		{
			multitouch = event.getEventTime();
			return true;
		}
		switch (event.getAction())
		{
			case MotionEvent.ACTION_DOWN:
				preX = event.getX()/scale;
				preY = event.getY()/scale;
				Log.d("CANVAS","Pritisk dol");
				break;
			case MotionEvent.ACTION_UP:
				if (event.getEventTime() - event.getDownTime() < 150)
				{
					Log.d("IZBIRA",scale+"");
					pridobiIzbranoPolje(
						event.getX(), 
						event.getY() 
					);
					//pridobiIzbranoPolje(event.getX()/scale, event.getY()/scale-(1-scale)*posY);	
					Log.d("CANVAS","CLICK!" + (event.getEventTime() - event.getDownTime()));
				}
				else
					Log.d("CANVAS","Pritisk up"+(event.getEventTime() - event.getDownTime()));
				break;
			case MotionEvent.ACTION_MOVE:
				posX += event.getX()/scale - preX;
				posY += event.getY()/scale - preY;
				preX = event.getX()/scale;
				preY = event.getY()/scale;
				
				break;
		}
		
		posX = Math.min(resSirina/2.0f-resSirina/2.0f/scale,Math.max(posX, resSirina/2.0f/scale-resSirina/2.0f));
		posY = Math.min(resVisina/2.0f-resVisina/2.0f/scale,Math.max(posY, resVisina/2.0f/scale-resVisina/1.5f));
		
		Log.d("POS", "Pozicija "+ posY);
		
		invalidate();
		
		return true;
	}
	
	private void pridobiIzbranoPolje(float x, float y) 
	{
		Matrix m = new Matrix();
		m.postScale(1/scale, 1/scale, resSirina/2, resVisina/2);
		m.postTranslate(-posX, -posY);
		
		float[] k = new float[] { x, y };
		
		m.mapPoints(k);
		
		Log.d("IZBIRA","Pos: " + posX + "," +posY + " :: " + k[0] + "," + k[1]);
		izbranX = (int)(k[0] / velikostVnosnegaPolja);
		izbranY = (int)(k[1] / velikostVnosnegaPolja);
	}
	
	@Override
	public void onCreateContextMenu(ContextMenu menu)
	{
		
	}

	private class ScaleListener extends ScaleGestureDetector.SimpleOnScaleGestureListener {
		@Override
		public boolean onScale(ScaleGestureDetector detector) {
			scale *= detector.getScaleFactor();
			scale = Math.max(1.0f, Math.min(scale, 3.0f));
		
			Log.d("POS", "Scale "+ scale);
			invalidate();
			return true;
		}
	}
	
	/**
	 * Vrne vodoravna polja vse razen tistega, ki je trenutno izbran
	 * @param startX X-indeks izbranega polja
	 * @param startY Y-indeks izbranega polja
	 * @return
	 */
	public ArrayList<PoljeEnota> dobiVodoravnaPolja(int startX, int startY)
	{
		ArrayList<PoljeEnota> polja = new ArrayList<PoljeEnota>();
		Polje p = krizanka.getPolje();
		
		if (p.getVrednost(startX, startY).Tip == Polje.VNOSNO_POLJE_STEVILKA)
		{
			spremeniVprasanje(krizanka.najdiVprasanje(startX, startY, SmerPisanja));
		}
		int x = startX-1;
		while (x>=0)
		{
			if (p.getVrednost(x, startY).jeVnosno())
			{
				polja.add(p.getVrednost(x, startY));
				if (p.getVrednost(x, startY).Tip == Polje.VNOSNO_POLJE_STEVILKA)
				{
					spremeniVprasanje(krizanka.najdiVprasanje(x, startY, SmerPisanja));
				}
			}
			else
				break;
			x--;
		}
		
		x = startX+1;
		while (x<krizanka.getSirina())
		{
			if (p.getVrednost(x, startY).jeVnosno())
			{
				polja.add(p.getVrednost(x, startY));
				if (p.getVrednost(x, startY).Tip == Polje.VNOSNO_POLJE_STEVILKA)
				{
					spremeniVprasanje(krizanka.najdiVprasanje(x, startY, SmerPisanja));
				}
			}
			else
				break;
			x++;
		}
		
		return polja;
	}
	
	/**
	 * Vrne navpicna polja vse razen tistega, ki je trenutno izbran
	 * @param startX X-indeks izbranega polja
	 * @param startY Y-indeks izbranega polja
	 * @return
	 */
	public ArrayList<PoljeEnota> dobiNavpicnaPolja(int startX, int startY)
	{
		ArrayList<PoljeEnota> polja = new ArrayList<PoljeEnota>();
		Polje p = krizanka.getPolje();
		
		if (p.getVrednost(startX, startY).Tip == Polje.VNOSNO_POLJE_STEVILKA)
		{
			spremeniVprasanje(krizanka.najdiVprasanje(startX, startY, SmerPisanja));
		}
		
		int y = startY-1;
		while (y>=0)
		{
			if (p.getVrednost(startX, y).jeVnosno())
			{
				polja.add(p.getVrednost(startX, y));
				if (p.getVrednost(startX, y).Tip == Polje.VNOSNO_POLJE_STEVILKA)
				{
					spremeniVprasanje(krizanka.najdiVprasanje(startX, y, SmerPisanja));
				}
			}
			else
				break;
			y--;
		}
		
		y = startY+1;
		while (y<krizanka.getSirina())
		{
			if (p.getVrednost(startX, y).jeVnosno())
			{
				polja.add(p.getVrednost(startX, y));
				if (p.getVrednost(startX, y).Tip == Polje.VNOSNO_POLJE_STEVILKA)
				{
					spremeniVprasanje(krizanka.najdiVprasanje(startX, y, SmerPisanja));
				}
			}
			else
				break;
			y++;
		}
		
		return polja;
	}
	
	/**
	 * Preveri, �e se polje nahaja med izbranimiPolji
	 * @param vrednost
	 * @param izbranaPolja
	 * @return
	 */
	private boolean poljeJeIzbranoGeslo(PoljeEnota polje, ArrayList<PoljeEnota> izbranaPolja) 
	{
		if (izbranaPolja != null)
			for (PoljeEnota p : izbranaPolja)
			{
				if (p.equals(polje))
					return true;
			}
		return false;
	}
	
	public void spremeniVprasanje(Vprasanje v)
	{
		if (trenutnoVprasanje == null && v != null)
			if (vprasanjeChangedCallback != null)
			{
				vprasanjeChangedCallback.onVprasanjeChanged(v);
			}
	}
	
	public void spremeniSmer(boolean smer)
	{
		SmerPisanja = smer;
		if (smerChangedCallback != null)
			smerChangedCallback.onSmerChanged(smer);
	}
	
	/**
	 * Nastavi na�ega observerja za klicanje spreminjanja vpra�anja v na�em activityju
	 * @param event
	 */
	public void setVprasanjeCallback(OnVprasanjeChanged event)
	{
		vprasanjeChangedCallback = event;
	}
	
	/**
	 * Nastavi na�ega observerja za klicanje spreminjanja vpra�anja v na�em activityju
	 * @param event
	 */
	public void setSmerCallback(OnSmerChanged event)
	{
		smerChangedCallback = event;
	}
	
	/**
	 * Preveri, �e je kri�anka pravilno re�ena.
	 * @return
	 */
	public boolean preveriKrizanko()
	{
		return krizanka.jeResena();
	}
	
	@Override
	public boolean onKeyUp(int keyCode, KeyEvent event)
	{
		super.onKeyUp(keyCode, event);
		char znak = ' ';
		switch (keyCode)
		{
			case KeyEvent.KEYCODE_A: znak = 'A'; break;
			case KeyEvent.KEYCODE_B: znak = 'B'; break;
			case KeyEvent.KEYCODE_C: znak = 'C'; break;
			case KeyEvent.KEYCODE_D: znak = 'D'; break;
			case KeyEvent.KEYCODE_E: znak = 'E'; break;
			case KeyEvent.KEYCODE_F: znak = 'F'; break;
			case KeyEvent.KEYCODE_G: znak = 'G'; break;
			case KeyEvent.KEYCODE_H: znak = 'H'; break;
			case KeyEvent.KEYCODE_I: znak = 'I'; break;
			case KeyEvent.KEYCODE_J: znak = 'J'; break;
			case KeyEvent.KEYCODE_K: znak = 'K'; break;
			case KeyEvent.KEYCODE_L: znak = 'L'; break;
			case KeyEvent.KEYCODE_M: znak = 'M'; break;
			case KeyEvent.KEYCODE_N: znak = 'N'; break;
			case KeyEvent.KEYCODE_O: znak = 'O'; break;
			case KeyEvent.KEYCODE_P: znak = 'P'; break;
			case KeyEvent.KEYCODE_R: znak = 'R'; break;
			case KeyEvent.KEYCODE_S: znak = 'S'; break;
			case KeyEvent.KEYCODE_T: znak = 'T'; break;
			case KeyEvent.KEYCODE_U: znak = 'U'; break;
			case KeyEvent.KEYCODE_V: znak = 'V'; break;
			case KeyEvent.KEYCODE_Z: znak = 'Z'; break;
			case KeyEvent.KEYCODE_Y: znak = 'Y'; break;
			case KeyEvent.KEYCODE_X: znak = 'X'; break;
			case KeyEvent.KEYCODE_W: znak = 'W'; break;
			case KeyEvent.KEYCODE_Q: znak = 'Q'; break;
		}
		
		Polje p = krizanka.getPolje();
		
		if (keyCode == KeyEvent.KEYCODE_DEL)
		{
			krizanka.setVrednost(izbranX, izbranY, '\0');
			if (SmerPisanja == Vprasanje.VODORAVNO)
			{
				if (izbranX-1 >= 0 && p.getTipPolja(izbranX-1, izbranY) != Polje.PRAZNO_POLJE)
					izbranX--;
			}
			else
			{
				if (izbranY-1 >= 0 && p.getTipPolja(izbranX, izbranY-1) != Polje.PRAZNO_POLJE)
					izbranY--;
			}
		}
		
		if (znak != ' ')
		{
			krizanka.setVrednost(izbranX, izbranY, znak);
			if (SmerPisanja == Vprasanje.VODORAVNO)
			{
				if (izbranX+1 < sirina && p.getTipPolja(izbranX+1, izbranY) != Polje.PRAZNO_POLJE)
					izbranX++;
			}
			else
			{
				if (izbranY+1 < visina && p.getTipPolja(izbranX, izbranY+1) != Polje.PRAZNO_POLJE)
					izbranY++;
			}
		}
		
		return false;
	}
}