package fridroid.krizanka;

import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;
import android.widget.Toast;
import android.app.Activity;
import android.app.AlertDialog;

public class KrizankaActivity extends Activity {

	Activity instance;
	
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.krizanka);
        
        instance = this;
        
        ((KrizankaView)findViewById(R.id.risanjeKrizanke)).setVprasanjeCallback(new OnVprasanjeChanged()
        {
            public void onVprasanjeChanged(Vprasanje v)
            {
                TextView t = (TextView)findViewById(R.id.vprasanje);
                //t.setWidth(size);
                t.setText(v.getStevilka()+". " + v.getVprasanje());
            }
        });
        
        ((KrizankaView)findViewById(R.id.risanjeKrizanke)).setSmerCallback(new OnSmerChanged()
        {
			public void onSmerChanged(boolean smer) 
			{
				Log.d("EVENT","Spremenila se je smer");
				ActivityCompat.invalidateOptionsMenu(instance);
			}
        });
    }
    
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.krizanka, menu);
        return true;
    }
    
    public boolean onPrepareOptionsMenu (Menu menu)
    {
    	//super.onPrepareOptionsMenu(menu);
    	KrizankaView krizanka = (KrizankaView)findViewById(R.id.risanjeKrizanke);
    	MenuItem vodoravno = menu.findItem(R.id.vodoravno);
    	MenuItem navpicno = menu.findItem(R.id.navpicno);
    	
    	if (krizanka.SmerPisanja == Vprasanje.VODORAVNO)
    	{
    		vodoravno.setVisible(true);
    		navpicno.setVisible(false);
    	}
    	else
    	{
    		vodoravno.setVisible(false);
    		navpicno.setVisible(true);
    	}
		return true;
    }
    
    public boolean onOptionsItemSelected(MenuItem item)
	{
    	KrizankaView krizanka = (KrizankaView)findViewById(R.id.risanjeKrizanke);

		// Handle item selection
        switch (item.getItemId()) {
            case R.id.vprasanja:
                Toast.makeText(this, "Pritisnil se je gumb za vpra�anja", Toast.LENGTH_SHORT).show();
                return true;
            case R.id.vodoravno:
            	krizanka.SmerPisanja = Vprasanje.NAVPICNO;
            	krizanka.invalidate();
            	ActivityCompat.invalidateOptionsMenu(this);
            	return true;
            case R.id.navpicno:
            	krizanka.SmerPisanja = Vprasanje.VODORAVNO;
            	krizanka.invalidate();
            	//onPrepareOptionsMenu();
            	ActivityCompat.invalidateOptionsMenu(this);
            	return true;
            case R.id.preveri:
            	
            	boolean resena = krizanka.preveriKrizanko();
            	
            	String msg;
            	if (resena)
            		msg = getString(R.string.krizanka_resena);
            	else
            		msg = getString(R.string.krizanka_se_neresena);
            	
            	AlertDialog.Builder alertBuilder = new AlertDialog.Builder(this);
            	alertBuilder.setMessage(msg);
            	alertBuilder.setPositiveButton("V redu", null);
            	
            	AlertDialog ad = alertBuilder.create();
            	ad.show();
                //Toast.makeText(this, "Preverjamo kri�anko", Toast.LENGTH_SHORT).show();
                return true;
        }
        
        return false;
	}
    
    
	

    
}
