package fridroid.krizanka;

import android.app.Activity;
import android.content.Intent;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

public class MainMenuActions 
{
	
	public static boolean onOptionsItemSelected(MenuItem item, Activity activity)
	{
		// Handle item selection
        switch (item.getItemId()) {
            case R.id.menu_domov:
                Toast.makeText(activity, "Pritisnil se je gumb domov", Toast.LENGTH_SHORT).show();
                return true;
            case R.id.menu_krizanka:
            	prikaziKrizanko(activity.findViewById(R.id.menu_krizanka), activity);
                return true;
            case R.id.menu_vprasanja:
            	prikaziVprasanja(activity.findViewById(R.id.menu_vprasanja), activity);
                return true;
        }
        return false;
	}
	
	public static void prikaziKrizanko(View v, Activity activity)
    {
		/*KrizankaView kv = new KrizankaView(v.getContext());
		activity.setContentView(kv);
		return;*/
		Intent intent = new Intent(activity, KrizankaActivity.class);
    	//intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
    	activity.startActivity(intent);
    	Toast.makeText(activity, "Pritisnil se je gumb kri�anka", Toast.LENGTH_SHORT).show();
    }
    
    public static void prikaziVprasanja(View v, Activity activity)
    {
    	Intent intent = new Intent(activity, VprasanjaActivity.class);
    	//intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
    	activity.startActivity(intent);
    	Toast.makeText(activity, "Pritisnil se je gumb vpra�anja", Toast.LENGTH_SHORT).show();
    	//activity.finish();
    }

}
