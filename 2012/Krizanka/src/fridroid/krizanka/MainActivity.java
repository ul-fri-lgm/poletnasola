package fridroid.krizanka;

import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;
import android.support.v4.app.NavUtils;

public class MainActivity extends Activity {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
    }
    
    public void prikaziKrizanko(View v)
    {
    	Intent intent = new Intent(this, KrizankaActivity.class);
    	this.startActivity(intent);
    }
    
}
