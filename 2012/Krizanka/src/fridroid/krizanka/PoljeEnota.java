package fridroid.krizanka;

/**
 * Objekt vsebuje podatke o poljih na�ega polja
 * @author Mesko
 *
 */
public class PoljeEnota
{
	public int 	Tip;
	public char Znak;
	public int	Stevilka;
	public boolean Izbran;
	
	/**
	 * Nastavi tip na�ega polja
	 * @param Tip Tip na�ega polja (Polje.PRAZNO_POLJE, ...)
	 */
	public PoljeEnota(int Tip) { this.Tip = Tip; }
	
	/**
	 * Nastavi tip na�ega polja, ter mu dodeli �tevilko vpra�anja
	 * @param Tip Tip na�ega polja (Polje.PRAZNO_POLJE, ...)
	 * @param Stevilka �tevilka vpra�anja
	 */
	public PoljeEnota(int Tip, int Stevilka) { this.Tip = Tip; this.Stevilka = Stevilka; }
	
	/**
	 * Vrne true, �e je polje vnosno
	 * @return
	 */
	public boolean jeVnosno()
	{
		if (Tip == Polje.VNOSNO_POLJE || Tip == Polje.VNOSNO_POLJE_STEVILKA)
			return true;
		return false;
	}
}
