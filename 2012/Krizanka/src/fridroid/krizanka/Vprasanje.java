package fridroid.krizanka;

public class Vprasanje 
{
	public static final boolean VODORAVNO 	= true;
	public static final boolean NAVPICNO 	= false;
	
	/**
	 * �tevilka vpra�anja
	 */
	private int 		stevilka;
	/**
	 * Besedilo vpra�anja
	 */
	private String 		vprasanje;
	/**
	 * Re�itev vpra�anja
	 */
	private String 		resitev;
	/**
	 * Smer gesla
	 */
	private boolean 	smer;
	/**
	 * Pozicija gesla na polju
	 */
	private Pozicija 	pozicija;

	/**
	 * Ustvari vpra�anje
	 * @param stevilka
	 * @param vprasanje
	 * @param resitev
	 * @param smer
	 * @param pozicija
	 */
	public Vprasanje(int stevilka, String vprasanje, String resitev, boolean smer, Pozicija pozicija)
	{
		this.stevilka 	= stevilka;
		this.vprasanje 	= vprasanje;
		this.resitev 	= resitev;
		this.smer 		= smer;
		this.pozicija	= pozicija;
	}
	
	/**
	 * Pridobi �tevilko vpra�anja
	 */
	public int getStevilka()
	{
		return stevilka;
	}
	
	/**
	 * Pridobi besedilo vpra�anja
	 * @return
	 */
	public String getVprasanje()
	{
		return vprasanje;
	}
	
	/**
	 * Pridobi re�itev vpra�anja
	 * @return
	 */
	public String getResitev()
	{
		return resitev;
	}
	
	/**
	 * Pridobi smer gesla
	 * @return
	 */
	public boolean getSmer()
	{
		return smer;
	}
	
	/**
	 * Pridobi pozicijo gesla v polju
	 * @return
	 */
	public Pozicija getPozicija()
	{
		return pozicija;
	}
	
}
