package fridroid.krizanka;

import java.util.ArrayList;
import java.util.Collection;

import android.util.Log;

public class Krizanka 
{
	/**
	 * Naslov kri≈æanke
	 */
	private String naslov;
	/**
	 * Seznam vseh vpra≈°anj kri≈æanke
	 */
	private ArrayList<Vprasanje> vprasanja;
	/**
	 * ≈†irina kri≈æanke (≈°tevilo polj v horizontalni smeri)
	 */
	private int sirina;
	/**
	 * Vi≈°ina kri≈æanke (≈°tevilo polj v vertikalni smeri)
	 */
	private int visina;
	/**
	 * Polje, ki vsebuje podatke o poljih kri≈æanke
	 */
	private Polje polje;
	
	/**
	 * Ustvari novo prazno kri≈æanko z naslovom naslov
	 * @param naslov
	 */
	public Krizanka(String naslov, int sirina, int visina) 
	{
		this.naslov = naslov;
		this.sirina = sirina;
		this.visina = visina;
		
		polje = new Polje(sirina, visina);
		this.vprasanja = new ArrayList<Vprasanje>();
	}
	
	/**
	 * Ustvari novo kri≈æanko
	 * @param naslov Naslov kri≈æanke
	 * @param vprasanja Seznam vseh vpra≈°anj tipa Vprasanja
	 */
	public Krizanka(String naslov, int sirina, int visina, Collection<Vprasanje> vprasanja)
	{
		this.naslov = naslov;
		this.sirina = sirina;
		this.visina = visina;
		setVprasanja(vprasanja);
		polje = new Polje(sirina, visina);
		this.sestaviPolje();
	}

	/**
	 * Preveri, ƒçe je kri≈æanka pravilno re≈°ena.
	 * @return
	 */
	public boolean jeResena()
	{
		int i,dolzinaGesla,zacetekX,zacetekY;
		String resitev;
		for (Vprasanje v : vprasanja)
		{
			resitev = v.getResitev();
			dolzinaGesla = v.getResitev().length();
			
			zacetekX = v.getPozicija().X; 
			zacetekY = v.getPozicija().Y;
			
			if (v.getSmer() == Vprasanje.VODORAVNO)
			{
				//vprasanje je postavljeno vodoravno
				for (i=0;i<dolzinaGesla;i++)
				{
					Log.d("PREVERI","Pos: (" + (zacetekX+i)+ ","+zacetekY+")" + polje.getVrednost(zacetekX+i, zacetekY).Znak + " :: " + resitev.charAt(i));
					if (polje.getVrednost(zacetekX+i,zacetekY).Znak != resitev.charAt(i))
					{
						return false;
					}
				}
			}	
			else
			{
				//vprasanje je postavljeno navpisno
				for (i=0;i<dolzinaGesla;i++)
				{
					Log.d("PREVERI","Pos: (" + (zacetekX)+ ","+(zacetekY+i)+")" + polje.getVrednost(zacetekX, zacetekY+i).Znak + " :: " + resitev.charAt(i));
					if (polje.getVrednost(zacetekX,zacetekY+i).Znak != resitev.charAt(i))
					{
						return false;
					}
				}			
			}		
		}
		return true;
	}
	
	/**
	 * Vrne polje
	 * @return
	 */
	public Polje getPolje()
	{
		return polje;
	}
	
	/**
	 * Nastavi vpra≈°anja trenutni kri≈æanki. Prej≈°nja vpra≈°anja se izbri≈°ejo.
	 * @param vprasanja
	 */
	public void setVprasanja(Collection<Vprasanje> vprasanja)
	{
		this.vprasanja = new ArrayList<Vprasanje>(vprasanja);
		this.sestaviPolje();
	}
	
	/**
	 * Vrne ≈°irino kri≈æanke
	 * @return
	 */
	public int getSirina()
	{
		return sirina;
	}
	
	/**
	 * Vrne vi≈°ino kri≈æanke
	 * @return
	 */
	public int getVisina()
	{
		return visina;
	}
	
	/**
	 * Nastavi vrednost v nekem polju na polju.
	 * @param x 
	 * @param y
	 * @param c ƒårka, ki naj jo vpi≈°e v polje x,y
	 */
	public void setVrednost(int x, int y, char c)
	{
		polje.setVrednost(x, y, c);
	}
	
	/**
	 * Metoda sestavi polje tako, da vsebuje podatke za izris
	 * kri≈æanke.
	 */
	private void sestaviPolje()
	{
		for(Vprasanje v : vprasanja)
		{
			int i=1;
			String g = v.getResitev();
			
			polje.getVrednost(v.getPozicija().X, v.getPozicija().Y).Tip = Polje.VNOSNO_POLJE_STEVILKA;
			polje.getVrednost(v.getPozicija().X, v.getPozicija().Y).Stevilka = v.getStevilka();
			
			Log.d("CANVAS",v.getVprasanje());
			
			while (i<g.length())
			{
				if (v.getSmer() == Vprasanje.VODORAVNO)
					polje.getVrednost(v.getPozicija().X+i, v.getPozicija().Y).Tip = Polje.VNOSNO_POLJE;
				else
					polje.getVrednost(v.getPozicija().X, v.getPozicija().Y+i).Tip = Polje.VNOSNO_POLJE;
				i++;
			}
			
		}
	}
	
	
	public Vprasanje najdiVprasanje(int x, int y, boolean smer)
	{
		if (polje.getVrednost(x, y).Tip == Polje.VNOSNO_POLJE_STEVILKA)
		{
			int st = polje.getVrednost(x, y).Stevilka;
			for (Vprasanje v : vprasanja)
			{
				if (v.getSmer() == smer && v.getStevilka() == st)
					return v;
			}
		}
		return null;
	}
	
	/**
	 * Metoda zgenerira testno kri≈æanko, da bomo lahko preizkusili
	 * kako aplikacija deluje.
	 * 
	 * Izdela kri≈æanko velikosti 12*9 polj in vsebuje 19 vodoravnih vpra≈°anj
	 * in 21 navpiƒçnih vpra≈°anj.
	 */
	public static Krizanka zgenerirajKrizanko()
	{
		Vprasanje v;
		ArrayList<Vprasanje> vprasanja = new ArrayList<Vprasanje>();
		Krizanka k = new Krizanka("Testna kri≈æanka", 13, 13);
		
		v = new Vprasanje(1, 
				"Odprtokodni brskalnik podjetja Mozilla Corporation", 
				"FIREFOX", 
				Vprasanje.VODORAVNO, 
				new Pozicija(3, 0)
		);
		vprasanja.add(v);
		
		v = new Vprasanje(2, 
				"Trenutno največje socialno omrežje", 
				"FACEBOOK", 
				Vprasanje.NAVPICNO, 
				new Pozicija(7, 0)
		);
		vprasanja.add(v);
		
		v = new Vprasanje(3, 
				"Znakovni jezik, ki se ga uporablja za pisanje spletnih strani", 
				"HTML", 
				Vprasanje.NAVPICNO, 
				new Pozicija(2, 1)
		);
		vprasanja.add(v);
		
		v = new Vprasanje(4,
				"Vnosna naprava za računalnik. Ime je dobila po manjši živalici", 
				"MISKA", 
				Vprasanje.NAVPICNO, 
				new Pozicija(0, 2)
		);
		vprasanja.add(v);
		
		v = new Vprasanje(5, 
				"Brskalnik podjetja Apple", 
				"SAFARI", 
				Vprasanje.NAVPICNO, 
				new Pozicija(11, 2)
		);
		vprasanja.add(v);
		
		v = new Vprasanje(6, 
				"Računalniško podjetje, ki je veliko dalo v razvoj računalništva in informatike", 
				"IBM", 
				Vprasanje.VODORAVNO, 
				new Pozicija(0, 3)
		);
		vprasanja.add(v);
		
		v = new Vprasanje(7, 
				"Stvar, ki mobilne naprave drži pri življenju tudi, ko te niso priključene v električno omrežje", 
				"BATERIJA", 
				Vprasanje.VODORAVNO, 
				new Pozicija(4, 3)
		);
		vprasanja.add(v);
		
		v = new Vprasanje(8, 
				"Objekt, ki je shranjen na datotečnem sistemu in vsebuje podatke", 
				"DATOTEKA", 
				Vprasanje.VODORAVNO, 
				new Pozicija(4, 5)
		);
		vprasanja.add(v);
		
		v = new Vprasanje(8, 
				"Angleški izraz za iskanje napak v programu", 
				"DEBUG", 
				Vprasanje.NAVPICNO, 
				new Pozicija(4, 5)
		);
		vprasanja.add(v);
		
		v = new Vprasanje(9, 
				"Podjetje, ki je znano po njihovem drznem designu in svojim operacijskim sistemom OSX", 
				"APPLE", 
				Vprasanje.VODORAVNO, 
				new Pozicija(0, 6)
		);
		vprasanja.add(v);
		
		v = new Vprasanje(10, 
				"Predpis, ki ga ima razred ali metoda razreda, da je vidna samo metodam znotraj svojega razreda", 
				"PRIVATE", 
				Vprasanje.NAVPICNO, 
				new Pozicija(2, 6)
		);
		vprasanja.add(v);
		
		v = new Vprasanje(11, 
				"Program, ki ga uporabljamo za ogled spletnih strani", 
				"BRSKALNIK", 
				Vprasanje.VODORAVNO, 
				new Pozicija(4, 7)
		);
		vprasanja.add(v);
		
		v = new Vprasanje(12, 
				"Spletni portal, ki se ukvarja s spletno prodajo knjig", 
				"AMAZON", 
				Vprasanje.NAVPICNO, 
				new Pozicija(8, 7)
		);
		vprasanja.add(v);
		
		v = new Vprasanje(13, 
				"Podjetje, ki izdeluje grafičnne procesorje. Najbolj znano po svoji GeForce znamki.", 
				"NVIDIA", 
				Vprasanje.NAVPICNO, 
				new Pozicija(10, 7)
		);
		vprasanja.add(v);
		
		v = new Vprasanje(14, 
				"Fakulteta za računalništvo in informatiko", 
				"FRI", 
				Vprasanje.VODORAVNO, 
				new Pozicija(0, 8)
		);
		vprasanja.add(v);
		
		v = new Vprasanje(15, 
				"Slike, ki jih lahko grafična kartica izriše na zaslon", 
				"GRAFIKA", 
				Vprasanje.VODORAVNO, 
				new Pozicija(6, 9)
		);
		vprasanja.add(v);
		
		v = new Vprasanje(16, 
				"Spletni portal, kjer se opravljajo razni spletni nakupi", 
				"EBAY", 
				Vprasanje.VODORAVNO, 
				new Pozicija(0, 10)
		);
		vprasanja.add(v);
		
		v = new Vprasanje(17, 
				"Slovensko podjetje, ki omogoča mobilno telefonijo ter prodajo mobilnih naprav. Posebej znana zaradi paketov ORTO", 
				"SIMOBIL", 
				Vprasanje.VODORAVNO, 
				new Pozicija(5, 11)
		);
		vprasanja.add(v);
		
		v = new Vprasanje(18, 
				"Brskalnik norveškega podjetja posebej znan po svojem rdečem logotipu.", 
				"OPERA", 
				Vprasanje.VODORAVNO, 
				new Pozicija(0, 12)
		);
		vprasanja.add(v);
		
		k.setVprasanja(vprasanja);
		
		return k;
	}
	
}
