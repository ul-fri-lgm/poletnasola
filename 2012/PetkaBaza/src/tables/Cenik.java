package tables;

public class Cenik {
	private long id_c;
	private long id_l;
	private long id_pij;
	private String znesek;
	
	public long getId_c() {
		return id_c;
	}
	public void setId_c(long id_c) {
		this.id_c = id_c;
	}
	public long getId_l() {
		return id_l;
	}
	public void setId_l(long id_l) {
		this.id_l = id_l;
	}
	public long getId_pij() {
		return id_pij;
	}
	public void setId_pij(long id_pij) {
		this.id_pij = id_pij;
	}
	public String getZnesek() {
		return znesek;
	}
	public void setZnesek(String znesek) {
		this.znesek = znesek;
	}
}
