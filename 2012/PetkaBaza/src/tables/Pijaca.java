package tables;

public class Pijaca {
	private long id_pij;
	private String ime_p;
	
	public long getId_pij() {
		return id_pij;
	}
	public void setId_pij(long id_pij) {
		this.id_pij = id_pij;
	}
	public String getIme_p() {
		return ime_p;
	}
	public void setIme_p(String ime_p) {
		this.ime_p = ime_p;
	}
}
