package tables;

public class Posta {
	private long id_p;
	private String mesto;
	
	public long getId_p() {
		return id_p;
	}
	public void setId_p(long id_p) {
		this.id_p = id_p;
	}
	public String getMesto() {
		return mesto;
	}
	public void setMesto(String mesto) {
		this.mesto = mesto;
	}
}
