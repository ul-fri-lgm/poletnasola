package tables;

public class Lokal {
	private long id_l;
	private String ime_l;
	private String naslov_l;
	private String id_p;
	
	public long getId_l() {
		return id_l;
	}
	public void setId_l(long id_l) {
		this.id_l = id_l;
	}
	public String getIme_l() {
		return ime_l;
	}
	public void setIme_l(String ime_l) {
		this.ime_l = ime_l;
	}
	public String getNaslov_l() {
		return naslov_l;
	}
	public void setNaslov_l(String naslov_l) {
		this.naslov_l = naslov_l;
	}
	public String getId_p() {
		return id_p;
	}
	public void setId_p(String id_p) {
		this.id_p = id_p;
	}
}
