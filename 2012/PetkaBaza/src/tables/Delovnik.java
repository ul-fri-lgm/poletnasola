package tables;

public class Delovnik {
	private long id_u;
	private long id_l;
	private String dan;
	private String ura;
	public long getId_u() {
		return id_u;
	}
	public void setId_u(long id_u) {
		this.id_u = id_u;
	}
	public long getId_l() {
		return id_l;
	}
	public void setId_l(long id_l) {
		this.id_l = id_l;
	}
	public String getDan() {
		return dan;
	}
	public void setDan(String dan) {
		this.dan = dan;
	}
	public String getUra() {
		return ura;
	}
	public void setUra(String ura) {
		this.ura = ura;
	}
}
