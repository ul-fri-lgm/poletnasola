package helper;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

public class BazaHelper extends SQLiteOpenHelper{
	//imena tabel
	public static final String TABLE_LOKAL = "lokal";
	public static final String TABLE_DELOVNIK = "delovnik";
	public static final String TABLE_POSTA = "posta";
	public static final String TABLE_PIJACA = "pijaca";
	public static final String TABLE_CENIK = "cenik";
	
	//atributi tabele POSTA
	public static final String COLUMN_IDP = "id_p"; //primary key
	public static final String COLUMN_MESTO = "mesto";
		
	//atributi tabele PIJACA
	public static final String COLUMN_IDPIJ = "id_pij"; //primary key
	public static final String COLUMN_IMEP = "ime_p";
	
	//atributi tabele LOKAL
	public static final String COLUMN_IDL = "id_l"; //primary key
	public static final String COLUMN_IMEL = "ime_l";
	public static final String COLUMN_NASLOV = "naslov_l";
	public static final String COLUMN_IDP_L = "id_p_l"; //foreign key na id_p v tabele POSTA
	
	//atributi tabele DELOVNIK
	public static final String COLUMN_IDU = "id_u"; //primary key
	public static final String COLUMN_IDD_L = "id_l"; //foreign key na id_l v tabele LOKAL
	public static final String COLUMN_DAN = "dan";
	public static final String COLUMN_URA = "ura";
	
	//atributi tabele CENIK
	public static final String COLUMN_IDC = "id_c"; //primary key
	public static final String COLUMN_IDL_C = "id_l"; //foreign key na id_l v tabeli LOKAL
	public static final String COLUMN_IDPIJ_C = "id_pij"; //foreign key na id_pij v tabeli PIJACA
	public static final String COLUMN_ZNESEK = "znesek";
	
	private static final String DATABASE_NAME = "barrapp.db";
	private static final int DATABASE_VERSION = 1;
	
	//stavek za kreiranje baze
	public static final String DATABASE_CREATE_TABLES = 
			"create table " + TABLE_POSTA + " (" + COLUMN_IDP + " integer primary key autoincrement, " + COLUMN_MESTO + " text not null) " +
			"create table " + TABLE_PIJACA + " (" + COLUMN_IDPIJ + " integer primary key autoincrement, " +	COLUMN_IMEP + " text not null) " + 
			"create table " + TABLE_LOKAL + " (" + COLUMN_IDL + " integer primary key autoincrement, " + COLUMN_IMEL + " text not null, " +
					COLUMN_NASLOV + " text not null, " + COLUMN_IDP_L + " text not null" + 
					"foreign key(" + COLUMN_IDP_L + ") references " + TABLE_POSTA + "(" + COLUMN_IDP + "))" + 
			"create table " + TABLE_DELOVNIK + " (" + COLUMN_IDU + " integer primary key autoincrement, " + COLUMN_IDD_L + " text not null, " + 
					COLUMN_DAN + " text not null, " + COLUMN_URA + " text not null, " +
					"foreign key(" + COLUMN_IDD_L + ") references " + TABLE_LOKAL + "(" + COLUMN_IDL + "))" + 
			"create table " + TABLE_CENIK + " (" + COLUMN_IDC + " integer primary key constraint, " + COLUMN_IDL_C + " text not null, " +
					COLUMN_IDPIJ_C + "text not null, " + COLUMN_ZNESEK + " text not null, " + 
					"foreign key(" + COLUMN_IDL_C + " references " + TABLE_LOKAL + "(" + COLUMN_IDL + ")" + 
					"foreign key(" + COLUMN_IDPIJ_C + " references " + TABLE_PIJACA + "(" + COLUMN_IDPIJ + "));";
	
	public BazaHelper(Context context) {
		super(context, DATABASE_NAME, null, DATABASE_VERSION);
	}
	
	@Override
	public void onCreate(SQLiteDatabase db) {
		db.execSQL(DATABASE_CREATE_TABLES);
	}
	
	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		Log.w(BazaHelper.class.getName(), 
				"Nadgradnja baze od razlicice " + oldVersion + " na " + newVersion + " razlicico. Pozor, to bo unicilo vse podatke.");
		
		db.execSQL("drop table if exists " + TABLE_LOKAL + ", " + TABLE_POSTA + ", " + TABLE_DELOVNIK + ", " + TABLE_PIJACA + ", " + TABLE_CENIK);
		onCreate(db);
	}
}
