package konkurenca.qr.imenik;

import java.io.File;
import java.io.FileOutputStream;

import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Environment;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import com.google.zxing.BarcodeFormat;
import com.google.zxing.common.BitMatrix;
import com.google.zxing.qrcode.QRCodeWriter;

public class ActivityQR extends Activity {
	public static final String OUT = "OUT";
	private String out;
	private Bitmap bmp;

    @Override
    public void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_activity_qr);
        
        
    }
    @Override
    protected void onResume(){
    	super.onResume();
    	ImageView iv = (ImageView) findViewById(R.id.QRslika);
        bmp = Bitmap.createBitmap(400, 400, Bitmap.Config.RGB_565);
        //String contents="ime=jure\npriimek=kolenko\ntel=123456789\nloads and loads of other useless information......";
		BarcodeFormat format=BarcodeFormat.QR_CODE;
		int width=400, height=400;
		BitMatrix res;
		QRCodeWriter wr = new QRCodeWriter();
        try{
        	out = getIntent().getStringExtra(OUT);
        res=wr.encode(out, format, width, height, null);//ENCODE!!!!
        }
        catch(Exception e){
        	Toast.makeText(this, "Something went wrong",Toast.LENGTH_LONG).show();
        	return;
        }
        
        for (int i = 0; i < res.getHeight(); i++) {
			for (int j = 0; j < res.getWidth(); j++) {
				bmp.setPixel(i, j, (res.get(i, j))?Color.BLACK:Color.WHITE);
			}
		}
        
        iv.setImageBitmap(bmp);

      
    }
    public void saveQRcode(View v){
    	File path = new File(new File(Environment.getExternalStorageDirectory(), "QrImenik"), out.substring(0, Math.min(10, out.length()))+".png");
    	if(saveBmp(bmp, path)){
    		Toast.makeText(this, "Shranjeno kot " + path.getName(),Toast.LENGTH_LONG).show();
    	}
    	else{
    		Toast.makeText(this, "Izberi elemente za deljenje",Toast.LENGTH_LONG).show();
    	}
    }
    
    public boolean saveBmp(Bitmap bmp, File dest){
    	File destDir= dest.getParentFile();
    	if(!destDir.exists()){
    		destDir.mkdirs();
    	}
    	if(dest.exists()){
    		dest.delete();
    	}
    	try{
    		FileOutputStream out = new FileOutputStream(dest);
    		bmp.compress(Bitmap.CompressFormat.PNG, 90, out);
    		out.flush();
    		out.close();
    		
    		return true;
    	}
    	catch(Exception e){
    		e.printStackTrace();
    		return false;
    	}   	
    }


    
}
