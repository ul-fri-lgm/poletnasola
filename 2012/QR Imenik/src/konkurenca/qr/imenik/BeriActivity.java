package konkurenca.qr.imenik;


import java.util.ArrayList;
import java.util.StringTokenizer;

import android.app.Activity;
import android.content.ContentProviderOperation;
import android.content.ContentProviderResult;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Intent;
import android.content.OperationApplicationException;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.os.RemoteException;
import android.provider.ContactsContract;
import android.provider.ContactsContract.CommonDataKinds.Phone;
import android.provider.ContactsContract.CommonDataKinds.StructuredName;
import android.provider.ContactsContract.Data;
import android.provider.ContactsContract.RawContacts;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

public class BeriActivity extends Activity {
	private final static int requestCode=1338;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_beri);
		Intent i = new Intent("com.google.zxing.client.android.SCAN");
		i.putExtra("SCAN_MODE", "QR_CODE_MODE");
		try{
		startActivityForResult(i, requestCode);
		}
		catch(Exception e){
    		Toast.makeText(this, "Please install barcode scanner. its free.",Toast.LENGTH_LONG).show();
    		Intent market = new Intent(Intent.ACTION_VIEW).setData(Uri.parse("market://details?id=com.google.zxing.client.android"));
    				startIntent(market);
    		

		}
    }
    
    private void startIntent(Intent market) {
		// TODO Auto-generated method stub
		
	}

	private class Person
	{
		String name;
		String phone[];
		String mail[];
		String id;
		String org;
		String orgloc;
		String address;
		int nphones;
		int nmails;
		public Person()
		{
			name = "";
			mail = new String[30];
			phone = new String[30];
			id = "";
			nphones = 0;
			nmails = 0;
			org = "";
			orgloc = "";
			address = "";
		}
	}
	
    Person nperson = new Person();
    
	@Override
    public void onActivityResult(int requestCode, int resultCode, Intent intent) {
    	Log.d("neki", "OnActivityResult prozen");
    	   if (requestCode == this.requestCode) {
    	      if (resultCode == RESULT_OK) {
    	         String contents = intent.getStringExtra("SCAN_RESULT");
    	         String format = intent.getStringExtra("SCAN_RESULT_FORMAT");
    	         Log.d("neki", contents + "      " + format);
    	        
    	         ListView lw = (ListView) findViewById(R.id.listViewDodajaj);
    	         ArrayList<String> al = new ArrayList<String>();
    	         ArrayAdapter<String> aa = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, al);
    	         
    	         
    	         String tmp[] = contents.split("%%%");
    	         for(int i=0; i < tmp.length; i++)
    	         {
    	        	 if(tmp[i].contains("Ime:"))
    	        	 {
    	        		 nperson.name = tmp[i].substring(4);
    	        	 }
    	        	 else if(tmp[i].contains("E-Posta"))
    	        	 {
    	        		 nperson.mail[nperson.nmails] = tmp[i].substring(10);
    	        		 nperson.nmails++;
    	        	 }
    	        	 else if(tmp[i].contains("Telefon"))
    	        	 {
    	        		 nperson.phone[nperson.nphones] = tmp[i].substring(10);
    	        		 nperson.nphones++;
    	        	 }
    	        	 else if(tmp[i].contains("Organizacija:"))
    	        	 {
    	        		 nperson.org = tmp[i].substring(13);
    	        	 }
    	        	 else if(tmp[i].contains("Naslov organizacije:"))
    	        	 {
    	        		 nperson.orgloc =tmp[i].substring(21);
    	        	 }
    	        	 else if(tmp[i].contains("Naslov:"))
    	        	 {
    	        		 nperson.address =tmp[i].substring(8);
    	        	 }
    	        	 al.add(tmp[i]);
    	         }
    	         
    	         
    	         
    	         lw.setAdapter(aa);
    	         //napolni seznam stringov
    	      } else if (resultCode == RESULT_CANCELED) {
    	         // Handle cancel
    	    	  Log.d("neki", "canceled");
    	      }
    	   }
    	}
    
    public void shraniClick(View v)
    {
    	if(nperson.name.equals(""))
    		return;
    	Log.d("onClick", "CLICKED!!!!");
    	/*
    	ContentProviderOperation.Builder builder = ContentProviderOperation.newInsert(RawContacts.CONTENT_URI);
    	builder.withValue(RawContacts.ACCOUNT_NAME, nperson.name);
    	builder.withValue(RawContacts.ACCOUNT_TYPE, 1);*/
    	
    	ArrayList<ContentProviderOperation> ops = new ArrayList<ContentProviderOperation>();
        int rawContactInsertIndex = ops.size();
 
        ops.add(ContentProviderOperation.newInsert(ContactsContract.RawContacts.CONTENT_URI)
                .withValue(RawContacts.ACCOUNT_TYPE, null)
                .withValue(RawContacts.ACCOUNT_NAME, null)
                .build());

        ops.add(ContentProviderOperation.newInsert(ContactsContract.Data.CONTENT_URI)
                .withValueBackReference(ContactsContract.Data.RAW_CONTACT_ID,rawContactInsertIndex)
                .withValue(ContactsContract.Data.MIMETYPE, ContactsContract.CommonDataKinds.StructuredName.CONTENT_ITEM_TYPE)
                .withValue(ContactsContract.CommonDataKinds.StructuredName.DISPLAY_NAME, nperson.name) // Name of the person
                .build());
        for(int i=0; i < nperson.nphones; i++)
        {
        ops.add(ContentProviderOperation.newInsert(ContactsContract.Data.CONTENT_URI)
                .withValueBackReference(ContactsContract.Data.RAW_CONTACT_ID,rawContactInsertIndex)
                .withValue(ContactsContract.Data.MIMETYPE, ContactsContract.CommonDataKinds.Phone.CONTENT_ITEM_TYPE)
                .withValue(ContactsContract.CommonDataKinds.Phone.NUMBER, nperson.phone[i])
                .build());
        }
        for(int i=0; i < nperson.nmails; i++)
        {
            ops.add(ContentProviderOperation.newInsert(ContactsContract.Data.CONTENT_URI)
                    .withValueBackReference(ContactsContract.Data.RAW_CONTACT_ID,rawContactInsertIndex)
                    .withValue(ContactsContract.Data.MIMETYPE, ContactsContract.CommonDataKinds.Email.CONTENT_ITEM_TYPE)
                    .withValue(ContactsContract.CommonDataKinds.Email.ADDRESS, nperson.mail[i])
                    .build());
        }
        ops.add(ContentProviderOperation.newInsert(ContactsContract.Data.CONTENT_URI)
                .withValueBackReference(ContactsContract.Data.RAW_CONTACT_ID,rawContactInsertIndex)
                .withValue(ContactsContract.Data.MIMETYPE, ContactsContract.CommonDataKinds.StructuredPostal.CONTENT_ITEM_TYPE)
                .withValue(ContactsContract.CommonDataKinds.StructuredPostal.FORMATTED_ADDRESS, nperson.address) // Name of the person
                .build());

        ops.add(ContentProviderOperation.newInsert(ContactsContract.Data.CONTENT_URI)
                .withValueBackReference(ContactsContract.Data.RAW_CONTACT_ID,rawContactInsertIndex)
                .withValue(ContactsContract.Data.MIMETYPE, ContactsContract.CommonDataKinds.Organization.CONTENT_ITEM_TYPE)
                .withValue(ContactsContract.CommonDataKinds.Organization.COMPANY, nperson.org) // Name of the person
                .withValue(ContactsContract.CommonDataKinds.Organization.TITLE, nperson.orgloc)
                .build());
        
        /*
        ContentProviderResult[] res;
		try {
			res = getContentResolver().applyBatch(ContactsContract.AUTHORITY, ops);
            if (res!=null && res[0]!=null) {
            	Uri newContactUri = res[0].uri;
            	//02-20 22:21:09 URI added contact:content://com.android.contacts/raw_contacts/612
            	Log.d("TAGTAGTAG", "URI added contact:"+ newContactUri);
            }
            else Log.e("TAGTAGTAG", "Contact not added.");
		} catch (RemoteException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (OperationApplicationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}*/
		Toast.makeText(this, "Dodano", Toast.LENGTH_SHORT).show();
    	finish();
    }
}
