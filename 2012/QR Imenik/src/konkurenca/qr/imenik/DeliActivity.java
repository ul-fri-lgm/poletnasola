package konkurenca.qr.imenik;

import java.util.ArrayList;

import android.annotation.SuppressLint;
import android.app.ListActivity;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.provider.ContactsContract.PhoneLookup;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.google.zxing.client.android.CaptureActivity;

@SuppressLint({ "ParserError", "ParserError", "ParserError", "ParserError", "ParserError" })
public class DeliActivity extends ListActivity {

	boolean selection[];//= new boolean[1];
	
	// LIST OF ARRAY STRINGS WHICH WILL SERVE AS LIST ITEMS
	ArrayList<String> listItems = new ArrayList<String>();
	ArrayList<String> listDetails = new ArrayList<String>();

	// DEFINING STRING ADAPTER WHICH WILL HANDLE DATA OF LISTVIEW
	ArrayAdapter<String> adapter;

	// RECORDING HOW MUCH TIMES BUTTON WAS CLICKED
	// int clickCounter = 0;

	@Override
	public void onCreate(Bundle icicle) {
		super.onCreate(icicle);

		Cursor people = getContentResolver().query(
				ContactsContract.Contacts.CONTENT_URI, null, null, null, null);

		while (people.moveToNext()) {
			int nameFieldColumnIndex = people.getColumnIndex(PhoneLookup.DISPLAY_NAME);
			String contact = people.getString(nameFieldColumnIndex);
			//int numberFieldColumnIndex = people.getColumnIndex(PhoneLookup.NUMBER);
			//String number = people.getString(numberFieldColumnIndex);
			listItems.add(contact);
		}
		
		people.close();

		setContentView(R.layout.activity_deli);
		adapter = new ArrayAdapter<String>(this,
				android.R.layout.simple_list_item_1, listItems);
		adapter.notifyDataSetChanged();
		setListAdapter(adapter);
	}
	protected class Person
	{
		String name;
		String phone[];
		String mail[];
		String id;
		String org;
		String orgloc;
		String address;
		int nphones;
		int nmails;
		public Person()
		{
			name = "";
			mail = new String[30];
			phone = new String[30];
			id = "";
			nphones = 0;
			nmails = 0;
			org = "";
			orgloc = "";
			address = "";
		}
	}
	
	
	@Override
	public void onListItemClick(ListView l, View v, int position, long id) {
        //TextView tv = (TextView) findViewById(R.id.deliTextView1);
        String name = listItems.get(position);
        //tv.setText(name);
        //tv = (TextView) findViewById(R.id.deliTextView2);
  
        
        Cursor people = getContentResolver().query(ContactsContract.Contacts.CONTENT_URI, null, null, null, null);
        Cursor phones = getContentResolver().query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI, null,null,null, null);
        Cursor emails = getContentResolver().query(ContactsContract.CommonDataKinds.Email.CONTENT_URI, null, null, null, null);
        Cursor organizations = getContentResolver().query(ContactsContract.Data.CONTENT_URI, null, null, null, null);
        Cursor postal = getContentResolver().query(ContactsContract.CommonDataKinds.StructuredPostal.CONTENT_URI, null, null, null, null);
        
        Person persons[] = new Person[people.getCount()];
    	for(int i = 0; i < persons.length; i++)
    		persons[i] = new Person();
        int ctr=0;
        while(people.moveToNext())
        {
        	String pname = people.getString(people.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME));
        	persons[ctr].name = pname;
        	persons[ctr].id = people.getString(people.getColumnIndex(ContactsContract.Contacts._ID));
        	ctr++;
        }
        while(phones.moveToNext())
        {
        	String pnum = phones.getString(phones.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));
        	String pname = phones.getString(phones.getColumnIndex(ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME));
        	String pid = phones.getString(phones.getColumnIndex(ContactsContract.CommonDataKinds.Phone.CONTACT_ID));
        	//System.out.println("ID phone:" + phones.getString(phones.getColumnIndex(ContactsContract.CommonDataKinds.Phone.CONTACT_ID)));
        	for(int i=0; i < persons.length; i++)
        	{
        		if(persons[i].id.equals(pid))
        		{
        			persons[i].phone[persons[i].nphones] = pnum;
        			persons[i].nphones++;
        			break;
        		}
        	}
        }
        while(emails.moveToNext())
        {
        	String pid = emails.getString(emails.getColumnIndex(ContactsContract.CommonDataKinds.Email.CONTACT_ID));
        	String pmail = emails.getString(emails.getColumnIndex(ContactsContract.CommonDataKinds.Email.ADDRESS));
        	//System.out.println("ID mail: " + pid + " mail : " + pmail);
        	for(int i = 0; i < persons.length; i++)
        	{
        		if(persons[i].id.equals(pid))
        		{
        			persons[i].mail[persons[i].nmails] = pmail;
        			persons[i].nmails++;
        			break;
        		}
        	}	
        }
        while(postal.moveToNext())
        {
        	String pid = postal.getString(postal.getColumnIndex(ContactsContract.CommonDataKinds.StructuredPostal.CONTACT_ID));
        	String address = postal.getString(postal.getColumnIndex(ContactsContract.CommonDataKinds.StructuredPostal.FORMATTED_ADDRESS));
        	for(int i=0; i < persons.length; i++)
        	{
        		if(persons[i].id.equals(pid))
        		{
        			persons[i].address = address;
        		}
        	}
        }
        
        while(organizations.moveToNext())
        {
        	String ptitle = organizations.getString(organizations.getColumnIndex(ContactsContract.CommonDataKinds.Organization.COMPANY));
        	String plocation = organizations.getString(organizations.getColumnIndex(ContactsContract.CommonDataKinds.Organization.TITLE));
        	String pid = organizations.getString(organizations.getColumnIndex(ContactsContract.CommonDataKinds.Organization.CONTACT_ID));
        	System.out.println("person" + ptitle + " " + pid);
        	if(ptitle == null || pid == null)
        		continue;
        	for(int i=0; i<persons.length; i++)
            {
        		if(persons[i].id.equals(pid))
        		{
        			
        			if(persons[i].name.equals(ptitle) || persons[i].address.equals(ptitle))
        				break;
        			boolean bk=false;
        			for(int j=0; j < persons[i].nmails; j++)
        				if(persons[i].mail[j].equals(ptitle))
        					bk = true;
        			for(int j=0; j < persons[i].nphones; j++)
        				if(persons[i].phone[j].equals(ptitle))
        					bk = true;
        			if(bk)
        				break;
        			
        			if(ptitle != null)
        				persons[i].org = "" + ptitle;
        			if(plocation != null)
        				persons[i].orgloc = "" + plocation;
        			break;
        		}
        	}
        }
        /*
    	for(int i = 0; i < persons.length; i++)
    	{
    		System.out.println(persons[i].name);
    		for(int j = 0; j < persons[i].nmails; j++)
    			System.out.println(persons[i].mail[j]);
    		for(int j = 0; j < persons[i].nphones; j++)
    			System.out.println(persons[i].phone[j]);
    		System.out.println(persons[i].org);
    	}*/
    	listDetails.clear();
        /*
    	for(int i=0; i < selection.length; i++)
    		selection[i] = false;*/
    	for(int i=0; i < persons.length; i++)
    	{
    		if(persons[i].name.equals(name))
    		{
    			selection = new boolean[1 + persons[i].nphones + persons[i].nmails+1+1+1];
    			listDetails.add("Ime: "  + persons[i].name);
    			for(int j=0; j < persons[i].nphones; j++)
    			{
    				String str = "Telefon " + (j+1) + ": " + persons[i].phone[j];
    				listDetails.add(str);
    			}
    			for(int j=0; j < persons[i].nmails; j++)
    			{
    				String str = "E-Posta " + (j+1) + ": " + persons[i].mail[j];
    				listDetails.add(str);
    			}
    			if(persons[i].org.length() > 2)
    				listDetails.add("Organizacija: " + persons[i].org);
    			if(persons[i].orgloc.length() > 2/* && !persons[i].orgloc.equals("")*/)
    				listDetails.add("Naslov organizacije: " + persons[i].orgloc);
    			if(persons[i].address.length() > 2)
    				listDetails.add("Naslov: " + persons[i].address);
    			ArrayAdapter<String> aa = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_checked, listDetails);
    			ListView lw = (ListView)findViewById(R.id.listViewDetails);
    			lw.setChoiceMode(2);
    			lw.setOnItemClickListener(new OnItemClickListener() {

					@Override
					public void onItemClick(AdapterView<?> arg0, View arg1,
							int arg2, long arg3) {
							selection[arg2] = !selection[arg2];
					}
				});
    			lw.setAdapter(aa);
    			break;
    		}
    	}
    	
        /*
        ContactsContract.Contacts.DISPLAY_NAME;
        ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME;
        ContactsContract.CommonDataKinds.Phone.NUMBER;
        ContactsContract.CommonDataKinds.Email.ADDRESS;
        ContactsContract.CommonDataKinds.Email.DISPLAY_NAME;
        //ContactsContract.CommonDataKinds.Organization.COMPANY;
        //ContactsContract.CommonDataKinds.Organization.DISPLAY_NAME;
        ContactsContract.CommonDataKinds.StructuredPostal.DISPLAY_NAME;
        ContactsContract.CommonDataKinds.StructuredPostal.FORMATTED_ADDRESS;*/
        
        /*
         * Make class containing all data, move across CommonDataKinds to get mails, phones, names, etc into classes
         * 
         */
        
        
        /*
        while (phones.moveToNext())
        {
        	if(name.equals(phones.getString(phones.getColumnIndex(ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME))))
        		{
        			String phoneNumber = phones.getString(phones.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));
        			
        		}
        }
        phones.close();*/

        /*while(people.moveToNext())
        {
        	if(name.equals(people.getString(people.getColumnIndex(PhoneLookup.DISPLAY_NAME))))
        	{
        		if(Boolean.parseBoolean(people.getString(people.getColumnIndex(PhoneLookup.HAS_PHONE_NUMBER))))
        			tv.setText(people.getString(people.getColumnIndex(PhoneLookup.NUMBER)));
        		else
        			tv.setText("Ni stevilke.");
        	}
        }*/
    }
	

	
	public void deliClick(View v)
	{
		Log.d("matrix", "IZPIS!");
		String out = "";
		for(int i=0; i < listDetails.size(); i++)
		{
			if(selection[i] == true)
			{
				out+=listDetails.get(i) + " %%%";
				System.out.println(listDetails.get(i));
		}
		}
		
		Intent i = new Intent(this, ActivityQR.class);
		i.putExtra(ActivityQR.OUT, out);
		startActivity(i);
	}
}
