package los.ganadores.moje.finance;

import android.os.Bundle;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.support.v4.app.NavUtils;


public class NewAccount extends Activity {
	private TextView registracija;
    
	@Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_account);
        
        
        registracija = (TextView)findViewById(R.id.textViewRegistracija);
        registracija.setTextColor(Color.rgb(50,181,229));

		
		TextView registracija1 = (TextView)findViewById(R.id.regtex1);
		TextView registracija2 = (TextView)findViewById(R.id.regtex2);
		TextView registracija3 = (TextView)findViewById(R.id.regtex3);
		TextView registracija4 = (TextView)findViewById(R.id.regtex4);
		TextView registracija5 = (TextView)findViewById(R.id.regtex5);		
		registracija1.setTextColor(Color.rgb(59,59,59));
		registracija2.setTextColor(Color.rgb(59,59,59));
		registracija3.setTextColor(Color.rgb(59,59,59));
		registracija4.setTextColor(Color.rgb(59,59,59));
		registracija5.setTextColor(Color.rgb(59,59,59));
		
        Button b1 = ((Button) findViewById(R.id.prekliciacc));
        b1.setTextColor(Color.rgb(59,59,59));
        b1.setOnClickListener(new OnClickListener() {

			public void onClick(View arg0) {
				finish();
			}
        });	
        
        Button b2 = (Button) findViewById(R.id.potrdiacc);
        b2.setTextColor(Color.rgb(59,59,59));
        b2.setOnClickListener(new OnClickListener() {

			public void onClick(View arg0) {
				EditText ime,priimek, pass, potPass, username;
				ime = (EditText) findViewById(R.id.imeuporabnikaacc);
		    	priimek = (EditText) findViewById(R.id.priimekuporabnikaacc);
		    	username = (EditText) findViewById(R.id.upimeacc);
		    	pass = (EditText) findViewById(R.id.gesloupacc);
		    	potPass = (EditText) findViewById(R.id.potrdigesloacc);
		    	
		    	if((pass.getText().toString()).equals(potPass.getText().toString())){
		    		PB entry = new PB(NewAccount.this);
		    		entry.open();
		    		entry.createEntryUsers(ime.getText().toString(), priimek.getText().toString(), username.getText().toString(), pass.getText().toString());
		    		entry.close();
		    		finish();
		    	}
		    	else{
		    		Dialog d = new Dialog(NewAccount.this);
					d.setTitle("Gesli nista enaki!");
					TextView text = new TextView(NewAccount.this);		
					d.setCanceledOnTouchOutside(true);
					d.show();	
		    	}
		  
			}
        });	
    }

    
}
