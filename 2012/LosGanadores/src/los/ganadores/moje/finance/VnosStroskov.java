package los.ganadores.moje.finance;

import java.io.File;
import java.io.FileOutputStream;
import java.util.Calendar;
import java.util.Date;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;


@SuppressLint({ "ParserError", "ParserError" })
public class VnosStroskov extends Activity implements OnClickListener,
		OnItemSelectedListener {
	private File path;
	private Bitmap photo;
	private Spinner spin;
	private EditText cela, ostanek, info;
	private Button vnesi;
	private DatePicker date;
	private ImageView imageView;
	private String[] category = { "Hrana", "Življenske potrebščine", "Avto",
			"Zabava" + "Pijača", "Stroški bivanja", "Drugo" };

	private static final int CAMERA_REQUEST = 1888; 
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.vnosstroskov);
		
		photo = null;
		imageView = (ImageView)this.findViewById(R.id.slikcastroski);
		
		TextView prvo = (TextView)findViewById(R.id.textViewstDatum);
		TextView dr = (TextView)findViewById(R.id.textViewstZnesek);
		TextView tr = (TextView)findViewById(R.id.textViewstOpis);
		prvo.setTextColor(Color.rgb(59,59,59));
		dr.setTextColor(Color.rgb(59,59,59));
		tr.setTextColor(Color.rgb(59,59,59));

		cela = (EditText) findViewById(R.id.vnosStroskovCelaSt);
		ostanek = (EditText) findViewById(R.id.vnosStroskovOstanekSt);
		info = (EditText) findViewById(R.id.vnosStroskovOpis);

		vnesi = (Button) findViewById(R.id.vnosStroskovBUTTONVnesi);
		
		date = (DatePicker) findViewById(R.id.datePickerstros);
		date.setDescendantFocusability(DatePicker.FOCUS_BLOCK_DESCENDANTS);

		spin = (Spinner) findViewById(R.id.vnosStroskovSpinner);

		ArrayAdapter<CharSequence> adapter = new ArrayAdapter<CharSequence>(
				this, android.R.layout.simple_spinner_item);
		adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		adapter.add("Hrana");
		adapter.add("Življenske potrebščine");
		adapter.add("Avto");
		adapter.add("Zabava");
		adapter.add("Pijača");
		adapter.add("Stroški bivanja");
		adapter.add("Drugo");

		spin.setAdapter(adapter);

		vnesi.setOnClickListener(VnosStroskov.this);

		spin.setOnItemSelectedListener(this);

		Button b2 = (Button) findViewById(R.id.nazajStrosek);
		b2.setTextColor(Color.rgb(59,59,59));
		b2.setOnClickListener(new OnClickListener() {

			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				finish();
			}
		});
		
		Button b3 = (Button) findViewById(R.id.slikajGub);
		b3.setTextColor(Color.rgb(59,59,59));
		b3.setOnClickListener(new OnClickListener() {

			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				Intent cameraIntent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE); 
                startActivityForResult(cameraIntent, CAMERA_REQUEST); 
			}
		});


	}



	public void onClick(View v) {
		// TODO Auto-generated method stub
		if (cela.getText().toString().length() != 0
				&& ostanek.getText().toString().length() != 0) {
			int value =( Integer.parseInt(cela.getText().toString()) * 100 + Integer.parseInt(ostanek.getText().toString()));
			PB entry = new PB(VnosStroskov.this);
			entry.open();

			int mesec = date.getMonth();
			int dan = date.getDayOfMonth();
			int leto = date.getYear();

			Calendar c = Calendar.getInstance();
			c.set(leto, mesec, dan,0,0,0);

			long millis = c.getTimeInMillis();
			
			if(photo == null)
			entry.createEntryEntries(-value, info.getText().toString(), millis,
					LoginMain.getUserID(), catID, " ");
			else entry.createEntryEntries(-value, info.getText().toString(), millis,
					LoginMain.getUserID(), catID, path.getAbsolutePath());
			
			entry.close();

			Dialog db = new Dialog(VnosStroskov.this);
			db.setTitle("Vnos uspesen!");
			TextView text = new TextView(VnosStroskov.this);
			text.setText("Uspesno ste dodali novi strosek!");
			db.setCanceledOnTouchOutside(true);
			db.setContentView(text);
			db.show();
			
			finish();
			
		} else {
			Dialog d = new Dialog(VnosStroskov.this);
			d.setTitle("Nekatera polja so prazna!");				
			d.setCanceledOnTouchOutside(true);
			d.show();
		}

	}

	int catID;

	public void onItemSelected(AdapterView<?> parent, View v, int position,
			long id) {
		// TODO Auto-generated method stub
		catID = position;
	}

	public void onNothingSelected(AdapterView<?> arg0) {
		// TODO Auto-generated method stub

	}
	
	
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {  
        if (requestCode == CAMERA_REQUEST) {  
        	Log.d("URI1", "asdasdasd asdasdad ");
            photo = (Bitmap) data.getExtras().get("data");  
            Log.d("URI1", "asdasdasd asdasdad ");
            imageView.setImageBitmap(photo);    

            
            path = new File(new File(Environment.getExternalStorageDirectory(), "Slike"), "bluaiee.png");

            Log.d("URI1", "asdasdasd asdasdad ");
            path = new File(new File(Environment.getExternalStorageDirectory(), "Slike"), "blua3.png");
            Log.d("URI1", "asdasdasd asdasdad ");

        	if(saveBmp(photo, path)){
        		Toast.makeText(this, "Saved!1!",Toast.LENGTH_LONG).show();
        	}
        	else{
        		Toast.makeText(this, "Something went wrong",Toast.LENGTH_LONG).show();
        	}
        	Log.d("URI", path.getAbsolutePath());
        	Log.d("URI1", "asdasdasd asdasdad ");
            
        }
        
    } 
    
   
    public boolean saveBmp(Bitmap bmp, File dest){
    	
    	File destDir= dest.getParentFile();
    	if(!destDir.exists()){
    		destDir.mkdirs();
    	}
    	if(dest.exists()){
    		dest.delete();
    	}
    	try{
    		FileOutputStream out = new FileOutputStream(dest);
    		bmp.compress(Bitmap.CompressFormat.PNG, 90, out);
    		out.flush();
    		out.close();
    		
    		return true;
    	}
    	catch(Exception e){
    		e.printStackTrace();
    		return false;
    	}   	
    }

}
