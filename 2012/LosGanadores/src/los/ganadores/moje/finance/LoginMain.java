package los.ganadores.moje.finance;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class LoginMain extends Activity {
	private static int userID;
	private static String nameSurname;
	EditText ime, pass;
	TextView login;
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_login);

		ime = (EditText) findViewById(R.id.username);
		pass = (EditText) findViewById(R.id.password);
		login = (TextView)findViewById(R.id.textViewLogin);
		login.setTextColor(Color.rgb(50,181,229));
		Button b = (Button) findViewById(R.id.vstopi);
		b.setOnClickListener(new OnClickListener() {

		
			
			
			public void onClick(View v) {
				PB entry = new PB(LoginMain.this);
				entry.open();
				if (ime.getText().toString().length() != 0
				&& pass.getText().toString().length() != 0) {

					try {
						if (entry.ifUserExists(ime.getText().toString(), pass.getText().toString())) {

							userID = entry.getUserID(ime.getText().toString());

							nameSurname = entry.getUserNamSurnam(userID);
							Intent i = new Intent(LoginMain.this, Tabs.class);
							entry.close();
							startActivity(i);
							finish();
						} else {
							Dialog d = new Dialog(LoginMain.this);
							d.setTitle("Napaka pri prijavi!");
							TextView text = new TextView(LoginMain.this);
							text.setText("Napacno !");
							// text.setTextSize(20);
							d.setCanceledOnTouchOutside(true);
							d.setContentView(text);
							d.show();
						}
					} catch (Exception e) {
						// TODO: handle exception
						Dialog d = new Dialog(LoginMain.this);
						d.setTitle("aca");
						TextView text = new TextView(LoginMain.this);
						text.setText("Napacno !");
						// text.setTextSize(20);
						d.setCanceledOnTouchOutside(true);
						d.setContentView(text);
						d.show();
					}
					entry.close();
				}
			}
		});

		Button b2 = (Button) findViewById(R.id.dodajracun);
		b2.setOnClickListener(new OnClickListener() {

			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				Intent j = new Intent(LoginMain.this, NewAccount.class);
				startActivity(j);
			}
		});

	}

	public static int getUserID() {
		return userID;
	}

	public static String getNameSurname() {
		return nameSurname;
	}

}
