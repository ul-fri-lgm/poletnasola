package los.ganadores.moje.finance;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Calendar;


import android.os.Bundle;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.PaintDrawable;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnDragListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckedTextView;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.PopupWindow;
import android.widget.TextView;


@SuppressLint("ParserError")
public class TableView extends Activity implements OnItemClickListener,
		OnItemLongClickListener {
	private ListView lv;
	private ArrayList<VrednostiTabele> a;
	private ArrayList<Integer> flag;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_table_view);
		
		flag = new ArrayList<Integer>();
		
		TextView datumkon = (TextView) findViewById(R.id.tabelaindexi1);
		TextView datumzac = (TextView) findViewById(R.id.tabelaindexi);
		datumzac.setTextColor(Color.rgb(59,59,59));
		datumkon.setTextColor(Color.rgb(59,59,59));
		
		PB entry = new PB(TableView.this);
		entry.open();
		

		long mills1 = InfoForPeriod.getZacetni();
		long mills2 = InfoForPeriod.getKoncni();
		a = entry.getDataForInterval(mills1, mills2, LoginMain.getUserID());

		ArrayList<String> view = new ArrayList<String>();
		for (VrednostiTabele b : a) {
			String vse;
			if (b.getDenar() >= 0)
				vse = b.getDatum() + "         " + b.getZnesek();
			else
				vse = b.getDatum() + "        " + b.getZnesek();
			view.add(vse);
		}

		entry.close();

		lv = (ListView) findViewById(R.id.tabelalistviev);

		ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(this,
				R.layout.mylistview, view);

		lv.setAdapter(arrayAdapter);
		lv.setOnItemLongClickListener(this);
		lv.setOnItemClickListener(this);
		
		Button b1 = ((Button) findViewById(R.id.nazajtabela));
		b1.setTextColor(Color.rgb(59,59,59));
		
		b1.setOnClickListener(new OnClickListener() {

			public void onClick(View arg0) {
				finish();
			}
		});
		
		
	}

	public void onItemClick(AdapterView<?> arg0, View arg1, int arg2, long arg3) {
		// TODO Auto-generated method stub
		Dialog d = new Dialog(TableView.this);
		
		PB entry = new PB(TableView.this);
		entry.open();
		
		File f;
        FileInputStream out;
        Bitmap bmp;
        ImageView imageVie; 
        imageVie = new ImageView(TableView.this);
		try {
			f = new File(entry.getURI(a.get(arg2).getEntry()));
			out = new FileInputStream(f);
			bmp = BitmapFactory.decodeStream(out);
			imageVie.setImageBitmap(bmp);
			d.setContentView(imageVie);
	
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		d.setTitle(a.get(arg2).getKategorija()+" -- "+a.get(arg2).getOpis());
		d.setCanceledOnTouchOutside(true);
	
		d.show();
		//entry.close();
	}

	public boolean onItemLongClick(AdapterView<?> arg0, View v, int arg2,
			long arg3) {
		Log.d(arg2+" ", "asdnalsjfdlksjafidsafbasjl");
			if (!flag.contains(arg2)){
				Log.d(arg2+" ", "asdnalsjfdlksjafidsafbasjl");
				flag.add(arg2);
				v.setBackgroundColor(Color.RED);
			}
			else{
				Log.d(arg2+" ", "asdnalsjfdlksjafidsafbasjl");
				Integer intt = new Integer(arg2);
				flag.remove(intt);
				v.setBackgroundColor(0x00000000);
				
			}
		
		return true;
	}

}
