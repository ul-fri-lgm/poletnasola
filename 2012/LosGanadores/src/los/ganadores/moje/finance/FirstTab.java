package los.ganadores.moje.finance;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;

@SuppressLint("ParserError")
public class FirstTab extends Activity {

	@SuppressLint("ParserError")
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.first_tab);

		TextView budget = (TextView) findViewById(R.id.zasluzekfirst);
		TextView skupnaPoraba = (TextView) findViewById(R.id.strosekfirstj);
		TextView skupaj = (TextView) findViewById(R.id.skupajfirst);
		TextView bud = (TextView) findViewById(R.id.tv1skupniPrihodek);
		TextView skp = (TextView) findViewById(R.id.tvStrosek12);
		
		skp.setTextColor(Color.rgb(59,59,59));
		bud.setTextColor(Color.rgb(59,59,59));
		skupaj.setTextColor(Color.rgb(59,59,59));
		TextView ime = (TextView) findViewById(R.id.imevfirsttabu);
		ime.setText(LoginMain.getNameSurname());
		ime.setTextColor(Color.rgb(59,59,59));
		budget.setTextColor(Color.rgb(50,181,229));
		skupnaPoraba.setTextColor(Color.rgb(168,69,69));

		PB entry = new PB(FirstTab.this);
		entry.open();

		int celo1 = entry.getValue(true, LoginMain.getUserID());
		
		budget.setText(celo1 / 100 + "." + Math.abs(celo1 % 100));
		

		int celo2 = entry.getValue(false, LoginMain.getUserID());
		
		skupnaPoraba.setText(celo2 / 100 + "." + Math.abs(celo2%100));
		

		int celo3 = celo1 + celo2;
		skupaj.setText(" " + celo3 / 100 + "." + Math.abs(celo3%100));
		entry.close();
		Button b1 = ((Button) findViewById(R.id.novstrosek));
		b1.setOnClickListener(new OnClickListener() {

			public void onClick(View arg0) {
				Intent i = new Intent(FirstTab.this, VnosStroskov.class);
				startActivity(i);
			}
		});

		Button b2 = ((Button) findViewById(R.id.novzasluzek));
		b2.setOnClickListener(new OnClickListener() {

			public void onClick(View arg0) {
				Intent i = new Intent(FirstTab.this, MoneyAdd.class);
				startActivity(i);
			}
		});
	}

	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		TextView budget = (TextView) findViewById(R.id.zasluzekfirst);
		TextView skupnaPoraba = (TextView) findViewById(R.id.strosekfirstj);
		TextView skupaj = (TextView) findViewById(R.id.skupajfirst);

		//TextView ime = (TextView) findViewById(R.id.imevfirsttabu);
		//skupaj.setText(LoginMain.getNameSurname());

		PB entry = new PB(FirstTab.this);
		entry.open();

		int celo1 = entry.getValue(true, LoginMain.getUserID());
		budget.setText(celo1 / 100 + "." + Math.abs(celo1 % 100));
		

		int celo2 = entry.getValue(false, LoginMain.getUserID());
		skupnaPoraba.setText(celo2 / 100 + "." + Math.abs(celo2%100));
		

		int celo3 = celo1 + celo2;
		skupaj.setText(" " + celo3 / 100 + "." + Math.abs(celo3%100));
		entry.close();
		
	}
	

}
