package los.ganadores.moje.finance;


import java.util.Calendar;
import java.util.Date;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.text.format.DateFormat;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnFocusChangeListener;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TextView;


public class InfoForPeriod extends Activity{
	DatePicker date1;
	DatePicker date2;
	private static long zacetni;
	private static long koncni;
    
	@Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.infozadoloceniperiod);  
        
        TextView datumkon1 = (TextView) findViewById(R.id.textstats1);
        TextView datumkon2 = (TextView) findViewById(R.id.textstats2);
        TextView datumkon3 = (TextView) findViewById(R.id.textstats3);
        TextView datumkon4= (TextView) findViewById(R.id.textstats4);
        TextView datumkon5= (TextView) findViewById(R.id.textstats5);
        datumkon1.setTextColor(Color.rgb(59,59,59));
        datumkon2.setTextColor(Color.rgb(59,59,59));
        datumkon3.setTextColor(Color.rgb(59,59,59));
        datumkon4.setTextColor(Color.rgb(59,59,59));
        datumkon5.setTextColor(Color.rgb(59,59,59));
        TextView dobicek = (TextView) findViewById(R.id.prihodekinfo);
		TextView poraba = (TextView) findViewById(R.id.porabainfo);
		
		dobicek.setTextColor(Color.rgb(50,181,229));
		poraba.setTextColor(Color.rgb(168,69,69));
        
        date1 = (DatePicker) findViewById(R.id.datePickerod);
        date2 = (DatePicker) findViewById(R.id.datePickerdo);
		date1.setDescendantFocusability(DatePicker.FOCUS_BLOCK_DESCENDANTS);
        date2.setDescendantFocusability(DatePicker.FOCUS_BLOCK_DESCENDANTS);


        
        Button b1 = ((Button) findViewById(R.id.prikazivsestroski));
        b1.setTextColor(Color.rgb(59,59,59));
		b1.setOnClickListener(new OnClickListener() {

			public void onClick(View arg0) {
				
				Calendar c = Calendar.getInstance(); 
				c.set(date1.getYear(), date1.getMonth(), date1.getDayOfMonth(),0,0,0);
				zacetni = c.getTimeInMillis();
				Calendar c1 = Calendar.getInstance();
				c1.set(date2.getYear(), date2.getMonth(), date2.getDayOfMonth(),0,0,0);
				koncni = c1.getTimeInMillis();			
				Intent i = new Intent(InfoForPeriod.this, TableView.class);
				startActivity(i);
			}
		});
		
        Button b2 = ((Button) findViewById(R.id.izracunajstats));
        b2.setTextColor(Color.rgb(59,59,59));
		b2.setOnClickListener(new OnClickListener() {


			public void onClick(View arg0) {
				Calendar c = Calendar.getInstance(); 
				c.set(date1.getYear(), date1.getMonth(), date1.getDayOfMonth(),0,0,0);
				
				final long mills1 = zacetni = c.getTimeInMillis();

				Calendar c1 = Calendar.getInstance();
				c1.set(date2.getYear(), date2.getMonth(), date2.getDayOfMonth(),0,0,0);
		        
		        final long mills2 = koncni = c1.getTimeInMillis();

				PB entry = new PB(InfoForPeriod.this);
;
				entry.open();
	
				TextView dobicek = (TextView) findViewById(R.id.prihodekinfo);
				TextView poraba = (TextView) findViewById(R.id.porabainfo);

				int celo = entry.getValueForInterval(mills1, mills2, LoginMain.getUserID(), true);
				int celo2 = entry.getValueForInterval(mills1, mills2, LoginMain.getUserID(), false);

				
				dobicek.setText(celo/100+"."+Math.abs(celo%100));
				poraba.setText(celo2/100+"."+Math.abs(celo2%100));
				
				entry.close();
			}
		});
    } 
    
    public static long getZacetni(){
    	return zacetni;
    }
    
    public static long getKoncni(){
    	return koncni;
    }

}
        
