package los.ganadores.moje.finance;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import android.annotation.SuppressLint;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

@SuppressLint("ParserError")
public class PB {

	public static final String KEY_USERID = "_uid";
	public static final String NAME = "name";
	public static final String SURNAME = "surname";
	public static final String USERNAME = "username";
	public static final String PASSWORD = "password";

	public static final String KEY_CATID = "_cid";
	public static final String CATNAME = "category_name";
	public static final String[] category = { "Hrana",
			"Življenske potrebščine", "Avto", "Zabava", "Pijača",
			"Stroški bivanja", "Drugo" };

	public static final String KEY_ENTRYID = "_eid";
	public static final String VALUE = "value";
	public static final String INFO = "value_info";
	public static final String DATE = "date";
	public static final String KEY_EUSERID = "_euid";
	public static final String KEY_ECATID = "_ecatid";
	public static final String URI = "_uri";

	
	public static final String KEY_CURRENCYID = "_cuid";
	public static final String CURRENCYNAME = "currency_name";

	private static final String DATABASE_NAME = "DBFinance";
	private static final String DATABASE_TABLE_USER = "User";
	private static final String DATABASE_TABLE_CATEGORY = "Category";
	private static final String DATABASE_TABLE_ENTRY = "Entry";
	private static final String DATABASE_TABLE_CURRENCY = "Currency";
	private static final int DATABASE_VERSION = 12;

	private static int countCategory = 7;

	private DbHelper ourHelper;
	private final Context ourContext;
	private SQLiteDatabase ourDatabase;

	private static class DbHelper extends SQLiteOpenHelper {

		public DbHelper(Context context) {
			super(context, DATABASE_NAME, null, DATABASE_VERSION);
			// TODO Auto-generated constructor stub
		}

		@Override
		public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
			// TODO Auto-generated method stub
			
			db.execSQL("DROP TABLE IF EXISTS " + DATABASE_TABLE_ENTRY);
			db.execSQL("DROP TABLE IF EXISTS " + DATABASE_TABLE_USER);
			db.execSQL("DROP TABLE IF EXISTS " + DATABASE_TABLE_CATEGORY);
			onCreate(db);
		}

		@Override
		public void onCreate(SQLiteDatabase db) {
			// TODO Auto-generated method stub

			db.execSQL("CREATE TABLE " + DATABASE_TABLE_CATEGORY + " ("
					+ KEY_CATID + " INTEGER PRIMARY KEY AUTOINCREMENT, "
					+ CATNAME + " TEXT NOT NULL);");
			db.execSQL("CREATE TABLE " + DATABASE_TABLE_USER + " ("
					+ KEY_USERID + " INTEGER PRIMARY KEY AUTOINCREMENT, "
					+ NAME + " TEXT NOT NULL, " + SURNAME + " TEXT NOT NULL, "
					+ USERNAME + " TEXT NOT NULL, " + PASSWORD
					+ " TEXT NOT NULL);");
			db.execSQL("CREATE TABLE " + DATABASE_TABLE_ENTRY + " ("
					+ KEY_ENTRYID + " INTEGER PRIMARY KEY AUTOINCREMENT, "
					+ VALUE + " INTEGER NOT NULL, " + INFO + " TEXT, " + DATE
					+ " NUMERABLE, " + KEY_EUSERID + " INTEGER NOT NULL, "
					+ KEY_ECATID + " INTEGER, "+URI + " TEXT);"
					);
			
		}

	}

	public PB(Context c) {
		ourContext = c;
	}

	public PB open() throws SQLException {
		ourHelper = new DbHelper(ourContext);
		ourDatabase = ourHelper.getWritableDatabase();
		return this;
	}

	public void close() {
		ourHelper.close();
	}

	public long createEntryUsers(String name, String surname, String user,
			String pass) {
		// TODO Auto-generated method stub
		ContentValues cv = new ContentValues();
		cv.put(NAME, name);
		cv.put(SURNAME, surname);
		cv.put(USERNAME, user);
		cv.put(PASSWORD, pass);
		return ourDatabase.insert(DATABASE_TABLE_USER, null, cv);
	}

	public long createEntryCategory() {
		// TODO Auto-generated method stub
		ContentValues cv = new ContentValues();
		for (int i = 0; i < category.length; i++)
			cv.put(CATNAME, category[i]);

		countCategory = 7;
		return ourDatabase.insert(DATABASE_TABLE_CATEGORY, null, cv);
	}

	public long createEntryEntries(int value, String info, long date,
			int euserID, int ecatID, String uri) {
		// TODO Auto-generated method stub
		ContentValues cv = new ContentValues();
		cv.put(VALUE, value);
		cv.put(INFO, info);
		cv.put(DATE, date);
		cv.put(KEY_EUSERID, euserID);
		cv.put(KEY_ECATID, ecatID);
		cv.put(URI, uri);
		return ourDatabase.insert(DATABASE_TABLE_ENTRY, null, cv);
	}

	public long createEntryCurrency(String currency) {
		// TODO Auto-generated method stub
		ContentValues cv = new ContentValues();
		cv.put(CURRENCYNAME, currency);
		return ourDatabase.insert(DATABASE_TABLE_CURRENCY, null, cv);
	}

	public String getDataUsers() {
		// TODO Auto-generated method stub
		String[] columns = new String[] { KEY_USERID, NAME, SURNAME, USERNAME,
				PASSWORD };
		Cursor c = ourDatabase.query(DATABASE_TABLE_USER, columns, null, null,
				null, null, null);
		String result = "";

		for (c.moveToFirst(); !c.isAfterLast(); c.moveToNext()) {
			result = result + c.getString(0) + " " + c.getString(1) + " "
					+ c.getString(2) + " " + c.getString(3) + " "
					+ c.getString(4) + "\n";
		}

		return result;
	}

	public String getDataCategory() {
		// TODO Auto-generated method stub
		String[] columns = new String[] { KEY_CATID, CATNAME };
		Cursor c = ourDatabase.query(DATABASE_TABLE_CATEGORY, columns, null,
				null, null, null, null);
		String result = "";

		for (c.moveToFirst(); !c.isAfterLast(); c.moveToNext()) {
			result = result + c.getString(0) + " " + c.getString(1) + "\n";
		}

		return result;
	}

	public String getDataEntry(int euserID) {
		// TODO Auto-generated method stub
		String[] columns = new String[] { KEY_ENTRYID, VALUE, INFO, DATE,
				KEY_EUSERID, KEY_ECATID, URI };
		Cursor c = ourDatabase.query(DATABASE_TABLE_ENTRY, columns, KEY_EUSERID
				+ "=" + euserID, null, null, null, null);
		String result = "";

		for (c.moveToFirst(); !c.isAfterLast(); c.moveToNext()) {
			result = result + c.getString(0) + " " + c.getString(1) + " "
					+ c.getString(2) + " " + c.getString(3) + " "
					+ c.getString(4) + " " + c.getString(5) + "\n";
		}

		return result;
	}

	public int getValue(boolean positive, int userID) {
		// TODO Auto-generated method stub
		Cursor c;
		String[] columns = new String[] { KEY_ENTRYID, VALUE, INFO, DATE,
				KEY_EUSERID, KEY_ECATID, URI };
		if (positive) {
			c = ourDatabase.query(DATABASE_TABLE_ENTRY, columns, VALUE + ">"
					+ 0 + " AND " + KEY_EUSERID + "=" + userID, null, null,
					null, null);
		} else {
			c = ourDatabase.query(DATABASE_TABLE_ENTRY, columns, VALUE + "<"
					+ 0 + " AND " + KEY_EUSERID + "=" + userID, null, null,
					null, null);
		}
		int result = 0;

		for (c.moveToFirst(); !c.isAfterLast(); c.moveToNext()) {
			result += c.getInt(1);
		}

		return result;
	}

	public String getDataCurrency() {
		// TODO Auto-generated method stub
		String[] columns = new String[] { KEY_CURRENCYID, CURRENCYNAME };
		Cursor c = ourDatabase.query(DATABASE_TABLE_CURRENCY, columns, null,
				null, null, null, null);
		String result = "";

		for (c.moveToFirst(); !c.isAfterLast(); c.moveToNext()) {
			result = result + c.getString(0) + " " + c.getString(1) + "\n";
		}

		return result;
	}

	public int getValueForInterval(long startDate, long endDate, int euserID,
			boolean positive) {

		String[] columns = new String[] { KEY_ENTRYID, VALUE, INFO, DATE,
				KEY_EUSERID, KEY_ECATID, URI };

		Cursor c;
		if (positive) {
			c = ourDatabase.query(DATABASE_TABLE_ENTRY, columns,
					DATE + ">=" + startDate + " AND " + DATE + "<=" + endDate
							+ " AND " + VALUE + ">" + 0 + "  AND  "
							+ KEY_EUSERID + "=" + euserID, null, null, null,
					null);

			
		} else {
			c = ourDatabase.query(DATABASE_TABLE_ENTRY, columns,
					DATE + ">=" + startDate + " AND " + DATE + "<=" + endDate
							+ " AND " + VALUE + "<" + 0 + "  AND  "
							+ KEY_EUSERID + "=" + euserID, null, null, null,
					null);

		}
		int result = 0;
		int bla = 0;
		for (c.moveToFirst(); !c.isAfterLast(); c.moveToNext()) {
			bla++;
			result += c.getInt(1);

		}
		
		return result;

	}

	
	public ArrayList<VrednostiTabele> getDataForInterval(long startDate, long endDate, int euserID) {

		String[] columns = new String[] { KEY_ENTRYID, VALUE, INFO, DATE,
				KEY_EUSERID, KEY_ECATID, URI };

		Cursor c = ourDatabase.query(DATABASE_TABLE_ENTRY, columns, DATE + ">="
				+ startDate + " AND " + DATE + "<=" + endDate + " AND " + KEY_EUSERID
				+ "=" + euserID, null, null, null, null);

		ArrayList<VrednostiTabele> vred = new ArrayList<VrednostiTabele>();
		
		for (c.moveToFirst(); !c.isAfterLast(); c.moveToNext()) {
			VrednostiTabele a;
			if(c.getInt(5)>0)
			a = new VrednostiTabele(Integer.parseInt(c.getString(1)), c.getString(2), Long.parseLong(c.getString(3)), c.getInt(0), category[c.getInt(5)]);
			else a = new VrednostiTabele(Integer.parseInt(c.getString(1)), c.getString(2), Long.parseLong(c.getString(3)), c.getInt(0), "");
			vred.add(a);
		}
		return vred;
	}

	public boolean ifUserExists(String username, String pass) {
		String[] columns = new String[] { KEY_USERID, NAME, SURNAME, USERNAME,
				PASSWORD };

		Cursor c = ourDatabase.query(DATABASE_TABLE_USER, columns, null, null,
				null, null, null);

		boolean exists = false;
		int i = -1;
		for (c.moveToFirst(); !c.isAfterLast(); c.moveToNext()) {
			if (c.getString(3).equals(username) && c.getString(4).equals(pass))
				i++;
		}
		Log.d(Integer.toString(i), "OVO JE I!!!");
		if (i == 0)
			exists = true;
		return exists;
	}

	public int getUserID(String username) {
		String[] columns = new String[] { KEY_USERID, NAME, SURNAME, USERNAME,
				PASSWORD };
		int i = 0;

		Cursor c = ourDatabase.query(DATABASE_TABLE_USER, columns, null, null,
				null, null, null);
		
		for (c.moveToFirst(); !c.isAfterLast(); c.moveToNext()) {
			if (c.getString(3).equals(username))
				i = c.getInt(0);
		}

		return i;
	}

	public int countCategory() {
		// TODO Auto-generated method stub
		return countCategory;
	}

	public String getUserNamSurnam(int userID) {
		String imeprimek = "";
		String[] columns = new String[] { KEY_USERID, NAME, SURNAME, USERNAME,
				PASSWORD };
		Cursor c = ourDatabase.query(DATABASE_TABLE_USER, columns, KEY_USERID
				+ "=" + userID, null, null, null, null);
		for (c.moveToFirst(); !c.isAfterLast(); c.moveToNext()) {
			if (c.getInt(0) == userID)
				imeprimek = c.getString(1) + " " + c.getString(2);
		}
		return imeprimek;
	}

	public float[] getCatValues(int userID) {
		float tabela[] = new float[countCategory];
		String[] columns = new String[] { KEY_ENTRYID, VALUE, INFO, DATE,
				KEY_EUSERID, KEY_ECATID, URI };

		for (int j = 0; j < tabela.length; j++)
			tabela[j] = 0;

		Cursor c = ourDatabase.query(DATABASE_TABLE_ENTRY, columns, KEY_EUSERID
				+ "=" + userID, null, null, null, null);

		for (c.moveToFirst(); !c.isAfterLast(); c.moveToNext()) {
			for (int i = 0; i < 7; i++) {
				if (i  == c.getInt(5)) {
					Log.d("OVO JE VALUE",""+-(((c.getFloat(1)/100)+((c.getFloat(1)%100)*0.01))));
					//tabela[i]+=-(((c.getFloat(1)/100)+((c.getFloat(1)%100)*0.01)));
					tabela[i]+=-(c.getFloat(1)*0.01);
					Log.d("OVO JE TABLE VALUE",""+tabela[i]+"OVO JE I: "+i);
				}

			}

		}

		// TODO Auto-generated method stub
		return tabela;
	}

	public float[] getOdnosStroskovInZasluzkov(int userID) {
		// TODO Auto-generated method stub
		
		
		
		return null;
	}
	
	public void deleteEntry(long entryID){
		ourDatabase.delete(DATABASE_TABLE_ENTRY, KEY_ENTRYID+"="+entryID, null);
	}
	
	public String getURI(long entryID){
		String[] columns = new String[] { KEY_ENTRYID, VALUE, INFO, DATE,
				KEY_EUSERID, KEY_ECATID, URI };

		

		Cursor c = ourDatabase.query(DATABASE_TABLE_ENTRY, columns, KEY_ENTRYID
				+ "=" + entryID, null, null, null, null);
		
		return c.getString(6);
	}
	
	

}
