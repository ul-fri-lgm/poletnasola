package los.ganadores.moje.finance;

import android.app.TabActivity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnFocusChangeListener;
import android.widget.TabHost;
import android.widget.TabHost.OnTabChangeListener;
import android.widget.TabHost.TabSpec;

public class Tabs extends TabActivity {
	   
	
	TabSpec s;
	protected void onCreate(Bundle savedInstanceState) {
	        super.onCreate(savedInstanceState);

	        final TabHost tabHost = getTabHost();
	        
	         s= tabHost.newTabSpec("tab1");
	        
	        tabHost.addTab(tabHost.newTabSpec("tab1")
	                .setIndicator(" ", getResources().getDrawable(R.drawable.euro_normal))
	                .setContent(new Intent(this, FirstTab.class)));
	       
	        
	        tabHost.addTab(tabHost.newTabSpec("tab2")
	                .setIndicator(" ", getResources().getDrawable(R.drawable.calc_normal))
	                .setContent(new Intent(this, InfoForPeriod.class)));
	        
	        // This tab sets the intent flag so that it is recreated each time
	        // the tab is clicked.
	        tabHost.addTab(tabHost.newTabSpec("tab3")
	                .setIndicator(" ", getResources().getDrawable(R.drawable.graph_normal))
	                .setContent(new Intent(this, Statistics.class)
	                        .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)));
	        
	    }
}
