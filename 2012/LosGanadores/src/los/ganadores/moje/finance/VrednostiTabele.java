package los.ganadores.moje.finance;

import android.annotation.SuppressLint;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

public class VrednostiTabele {
	private int znesek;
	private String opis;
	private long datum;
	private int entry;
	private String kategorija;
	
	public VrednostiTabele (int znesek, String opis, long datum, int entry, String kat){
		this.datum = datum;
		this.opis = opis;
		this.znesek = znesek;
		kategorija = kat;
	}
	

	public String getDatum(){		
		Date d = new Date(datum);
		SimpleDateFormat sdf = new SimpleDateFormat("dd.MM.yyyy");
		return sdf.format(d);	
	}
	
	public String getZnesek(){	
		return znesek/100+"."+Math.abs(znesek%100);	
	}
	
	public int getDenar(){	
		return znesek;
	}
	public String getOpis(){		
		return opis;
	}
	public String getKategorija(){		
		return kategorija;
	}
	
	public int getEntry(){
		return entry;	
	}
}
