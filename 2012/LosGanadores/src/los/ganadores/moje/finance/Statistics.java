package los.ganadores.moje.finance;

import android.app.Activity;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Point;
import android.graphics.RectF;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Display;
import android.view.View;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.AdapterView.OnItemSelectedListener;

public class Statistics extends Activity{

	TextView tv1, tv2, tv3, tv4, tv5, tv6, tv7, tvNaslov;
	Spinner spin;
	float values[];

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.statistics);

		tv1 = (TextView) findViewById(R.id.statisticstv1);
		tv2 = (TextView) findViewById(R.id.statisticstv2);
		tv3 = (TextView) findViewById(R.id.statisticstv3);
		tv4 = (TextView) findViewById(R.id.statisticstv4);
		tv5 = (TextView) findViewById(R.id.statisticstv5);
		tv6 = (TextView) findViewById(R.id.statisticstv6);
		tv7 = (TextView) findViewById(R.id.statisticstv7);
		//spin = (Spinner) findViewById(R.id.statisticsSpinner);
		Log.d("PRISLI SMO DO TLE!", "INICIJALIZACIJA");
//		ArrayAdapter<CharSequence> adapter = new ArrayAdapter<CharSequence>(
//				this, android.R.layout.simple_spinner_item);
//		adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
//		adapter.add("Odnos stroškov in zaslužkov");
//		adapter.add("Poraba po kategorijah");
//		spin.setAdapter(adapter);
//		Log.d("PRISLI SMO DO TLE!", "SETIRAN SPINNER");
//		
//		spin.setOnItemSelectedListener(Statistics.this);
//		Log.d("PRISLI SMO DO TLE!", "SETIRAN LISTENER");
		
		tv1.setTextColor(Color.rgb(189,55,62));
		tv2.setTextColor(Color.rgb(132,168,58));
		tv3.setTextColor(Color.rgb(172,98,214));
		tv4.setTextColor(Color.rgb(74,82,210));
		tv5.setTextColor(Color.rgb(226,133,21));
		tv6.setTextColor(Color.rgb(43,100,206));
		tv7.setTextColor(Color.rgb(53,166,202));
		
		
		
		
		PB entry = new PB(Statistics.this);
		entry.open();
		values =entry.getCatValues(LoginMain.getUserID());
		
		
		LinearLayout linear = (LinearLayout) findViewById(R.id.linearID);
		values = calculateData(values);
		linear.addView(new MyGraphview(this, values));

		entry.close();

	}

	

	

	private float[] calculateData(float[] data) {
		// TODO Auto-generated method stub
		float total = 0;
		for (int i = 0; i < data.length; i++) {
			total += data[i];
		}
		for (int i = 0; i < data.length; i++) {
			data[i] = 360 * (data[i] / total);
		}
		return data;

	}

	public class MyGraphview extends View {

		private Paint paint = new Paint(Paint.ANTI_ALIAS_FLAG);
		private float[] value_degree;
		private int[] COLORS = { Color.rgb(189,55,62), Color.rgb(132,168,58), Color.rgb(172,98,214),
				Color.rgb(74,82,210), Color.rgb(226,133,21), Color.rgb(43,100,206), Color.rgb(53,166,202) };


		RectF rectf = new RectF(200,0, 680 , 390);

		int temp = 0;

		public MyGraphview(Context context, float[] values) {

			super(context);
			value_degree = new float[values.length];
			for (int i = 0; i < values.length; i++) {
				value_degree[i] = values[i];
			}
		}

		@Override
		protected void onDraw(Canvas canvas) {
			// TODO Auto-generated method stub
			super.onDraw(canvas);
int zadnji=0;
			for (int i = 0; i < value_degree.length; i++) {// values2.length;
						if(value_degree[i]>0)
							zadnji=i;
				if (i == 0) {
					paint.setColor(COLORS[i]);
					canvas.drawArc(rectf, 0, value_degree[i], true, paint);
				} else if(i==zadnji){
					temp += (int) value_degree[i - 1];
					paint.setColor(COLORS[i]);
					canvas.drawArc(rectf, temp, value_degree[i]+(360-(temp+value_degree[i])), true, paint);
				}
				
				else {
					temp += (int) value_degree[i - 1];
					paint.setColor(COLORS[i]);
					canvas.drawArc(rectf, temp, value_degree[i], true, paint);
				}
			}
		}

	}

}