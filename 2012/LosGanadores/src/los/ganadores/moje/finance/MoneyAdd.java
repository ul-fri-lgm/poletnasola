package los.ganadores.moje.finance;

import java.util.Calendar;
import java.util.Date;

import android.os.Bundle;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.Dialog;
import android.graphics.Color;
import android.text.format.DateFormat;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TextView;


public class MoneyAdd extends Activity {

	private DatePicker date;
	
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_money_add);
		
		date = (DatePicker) findViewById(R.id.datePickervnos);
		date.setDescendantFocusability(DatePicker.FOCUS_BLOCK_DESCENDANTS);
		TextView prvo = (TextView)findViewById(R.id.textViewZnesek);
		TextView dr = (TextView)findViewById(R.id.textViewOpis);
		TextView tr = (TextView)findViewById(R.id.textViewDatum);
		prvo.setTextColor(Color.rgb(59,59,59));
		dr.setTextColor(Color.rgb(59,59,59));
		tr.setTextColor(Color.rgb(59,59,59));
		
		Button b1 = ((Button) findViewById(R.id.dodajbutdodaj));
		b1.setOnClickListener(new OnClickListener() {


			@SuppressLint("ParserError")
			public void onClick(View arg0) {
				EditText celDel, centi, opis;

				celDel = (EditText) findViewById(R.id.celdeldodaj);
				centi = (EditText) findViewById(R.id.centidodaj);
				opis = (EditText) findViewById(R.id.opisdodaj);
				if (celDel.getText().toString().length() != 0
						&& centi.getText().toString().length() != 0) {
					int mesec = date.getMonth();
					int dan = date.getDayOfMonth();
					int leto = date.getYear();

					Calendar c = Calendar.getInstance();
					c.set(leto, mesec, dan,0,0,0);

					long millis = c.getTimeInMillis();

					int denar = ((Integer.valueOf(celDel.getText().toString()) * 100) + Integer
							.valueOf(centi.getText().toString()));

					PB entry = new PB(MoneyAdd.this);
					entry.open();

					entry.createEntryEntries(denar, opis.getText().toString(),
							millis, LoginMain.getUserID(), -1, "");
					entry.close();
					finish();
					
				}else {
					Dialog d = new Dialog(MoneyAdd.this);
					d.setTitle("Nekatera polja so prazna!");				
					d.setCanceledOnTouchOutside(true);
					d.show();
				}

			}
		});
		
		
		Button b2 = (Button) findViewById(R.id.nazajDenarDodaj);
		b2.setOnClickListener(new OnClickListener() {

			public void onClick(View arg0) {
				finish();
			}
		});

	}
}
