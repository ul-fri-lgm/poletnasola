package com.example.androist;
import android.content.Context;
import android.content.SharedPreferences;
import android.database.sqlite.*;
import android.database.sqlite.SQLiteDatabase.CursorFactory;
import android.util.Log;

public class Baza{

	Context context;
	public Baza(Context c) {
		context = c;
		// TODO Auto-generated constructor stub
	}

	public String goreTb="Gore";
	public String idGoreTb="ID";
	public String imeGoreTb="IME";
	public String gpsSirina="SIRINA";//Svoja tabela
	public String gpsDolzina="DOLZINA";
	
	public void prepareDatabase(SQLiteDatabase db)
    {
    	String stmnt = "CREATE TABLE IF NOT EXISTS "+goreTb+" ("+idGoreTb+ " INTEGER PRIMARY KEY , "+
			    imeGoreTb+ " TEXT , " + gpsSirina + " FLOAT , " + gpsDolzina + " FLOAT);";
		db.execSQL(stmnt);
			  
		
		//Inserts pre-defined departments
	   InsertMountains(db);  
    }
    public void InsertMountains(SQLiteDatabase db) {
		// TODO Auto-generated method stub
		if(Core.seznam.size()>0)
		{
			for(int i=0;i<Core.seznam.size();i++)
			{
				String ime = Core.seznam.get(i).ime;
				double sirina = Core.seznam.get(i).lokacija.sirina;
				double dolzina = Core.seznam.get(i).lokacija.dolzina;
				if(sirina>0)
					if(dolzina>0)
					{
						final String stmnt="INSERT INTO " + goreTb + " VALUES("+i+",'"+ime+"',"+sirina+","+dolzina+");";
						db.execSQL(stmnt);
					}
				Log.d("AND","After insert into");
			}
			SharedPreferences prefs = context.getSharedPreferences("preference", context.MODE_PRIVATE);
			SharedPreferences.Editor e = prefs.edit();
			e.putBoolean("baza", true); // 1 - successfully created
		    e.commit();
		}
		
	}

	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		// TODO Auto-generated method stub
		
	}

}
