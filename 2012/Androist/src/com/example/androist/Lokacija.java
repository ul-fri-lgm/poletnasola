package com.example.androist;

import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.support.v4.app.NavUtils;

public class Lokacija extends Activity {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lokacija);
        TextView t = (TextView)this.findViewById(R.id.textView1);
        t.setText(MainActivity.core.a);
    }
    public void onBack(View v)
    {
    	Intent i = new Intent(this,Seznam.class);
		startActivity(i);
		finish();
    }
    public void onHome(View v)
    {
    	Intent i = new Intent(this,MainActivity.class);
		startActivity(i);
		finish();
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.activity_lokacija, menu);
        return true;
    }

    
}
