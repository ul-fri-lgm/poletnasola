package com.example.androist;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;

import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.provider.Settings;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.AssetFileDescriptor;
import android.database.sqlite.SQLiteDatabase;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.SeekBar;
import android.widget.SeekBar.OnSeekBarChangeListener;
import android.widget.TextView;
import android.widget.Toast;
import android.sax.Element;
import android.support.v4.app.NavUtils;

@SuppressLint({ "ParserError", "ParserError", "ParserError", "ParserError" })
public class MainActivity extends Activity  implements SeekBar.OnSeekBarChangeListener{

	public LocationListener locationListener;
	public final String imeBaze = "gore_baza.sqlite";
	public static Core core;
	public String goreTb="Gore";
	public String idGoreTb="ID";
	public String imeGoreTb="IME";
	public String gpsSirina="SIRINA";//Svoja tabela
	public String gpsDolzina="DOLZINA";
	SeekBar mSeekBar;
	TextView textView;
    @Override
    public void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        LocationManager locationManager = (LocationManager)MainActivity.this.getSystemService(Context.LOCATION_SERVICE);
        BufferedReader in = null;
        
       
        
        SQLiteDatabase bazaSQL = this.openOrCreateDatabase(imeBaze,SQLiteDatabase.OPEN_READWRITE, null);
		try {
			SharedPreferences prefs = getSharedPreferences("preference", MODE_PRIVATE);
			boolean bDatabaseExists = prefs.contains("baza");
			if(!bDatabaseExists)
			{
				String xml = prefs.getString("xml", "");
				if(xml=="") // Prvi zagon
				{
					in = new BufferedReader(new InputStreamReader 
							(this.getAssets().open("gore.xml"))); 
					String temp="";
			        while ((temp = in.readLine()) != null)        
			        xml+=temp;	       		        
			        SharedPreferences.Editor e = prefs.edit();
			       
			        e.putString("xml", xml);
			      
			        e.commit();
				} 
				core = new Core();
				Baza b = new Baza(this);
				if(core.prepareData(xml)>0)
				{
					//Baza
					b.prepareDatabase(bazaSQL);
				}
			}
			mSeekBar = (SeekBar)findViewById(R.id.seekBar1);
	        mSeekBar.setOnSeekBarChangeListener(this);
	        textView = (TextView)findViewById(R.id.textView1);
	        
			
	        
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
        
        /*Criteria locationCritera = new Criteria();
        locationCritera.setAccuracy(Criteria.ACCURACY_COARSE);
        locationCritera.setAltitudeRequired(false);
        locationCritera.setBearingRequired(false);
        locationCritera.setCostAllowed(true);
        locationCritera.setPowerRequirement(Criteria.NO_REQUIREMENT);

        String providerName = locationManager.getBestProvider(locationCritera, true);
        
        locationListener = new LocationListener(){

			@Override
			public void onLocationChanged(Location location) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void onProviderDisabled(String provider) {
				// TODO Auto-generated method stub
			
			}

			@Override
			public void onProviderEnabled(String provider) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void onStatusChanged(String arg0, int arg1, Bundle arg2) {
				// TODO Auto-generated method stub
				
			}
        	
        };
        
        if (providerName != null && locationManager.isProviderEnabled(providerName)) {
            // Provider is enabled
            locationManager.requestLocationUpdates(providerName, 20000, 100, MainActivity.this.locationListener);
        } else {
            // Provider not enabled, prompt user to enable it
            Toast.makeText(MainActivity.this, R.string.turnon_gps, Toast.LENGTH_LONG).show();
            Intent myIntent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
            MainActivity.this.startActivity(myIntent);
        }*/
    
    }
    
    public void onIsci(View v)
    {
    	Intent i = new Intent(this,Seznam.class);
		startActivity(i);
		//finish();
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.activity_main, menu);
        return true;
    }

    public void onProgressChanged(SeekBar seekBar, int progress, boolean fromTouch) {
       textView.setText(progress+"");
     }
 
    public void onStartTrackingTouch(SeekBar seekBar) {
        
    }
 
    public void onStopTrackingTouch(SeekBar seekBar) {
       
    }
    
    
}
