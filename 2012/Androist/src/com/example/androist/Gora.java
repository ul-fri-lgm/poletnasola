package com.example.androist;

public class Gora {
	public String ime;
	public GPSLokacija lokacija;
	public String povezava;
	Gora(String ime,double dolzina,double sirina, String povezava)
	{
		this.ime = ime;
		this.lokacija = new GPSLokacija(dolzina,sirina);
		this.povezava = povezava;
		
	}
	class GPSLokacija
	{
		double dolzina;
		double sirina;
		GPSLokacija(double d, double s)
		{
			this.dolzina = d;
			this.sirina = s;
		}
		
	}
}
