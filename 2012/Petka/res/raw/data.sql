insert into posta (id_p, mesto) values (1, 'Ljubljana');

insert into pijaca (id_pij, ime_p) values (1, 'Pivo');
insert into pijaca (id_pij, ime_p) values (2, 'Vino');
insert into pijaca (id_pij, ime_p) values (3, 'Sok');
insert into pijaca (id_pij, ime_p) values (4, 'Viski');
insert into pijaca (id_pij, ime_p) values (5, 'Kava');
insert into pijaca (id_pij, ime_p) values (6, 'Caj');
insert into pijaca (id_pij, ime_p) values (7, 'Voda');
insert into pijaca (id_pij, ime_p) values (8, 'Mleko');
insert into pijaca (id_pij, ime_p) values (9, 'Kakav');

insert into lokal (id_l, ime_l, naslov_l, id_p_l, lon, lat) values (1, 'Pivovarna in pivnica Kratochwill', 'Šmartinska cesta 152', 1, 46.0680029237, 14.5419654162);
insert into lokal (id_l, ime_l, naslov_l, id_p_l, lon, lat) values (2, 'UZITEK BAR', 'Tržaska cesta 90', 1,46.0413978212, 14.4749853339);
insert into lokal (id_l, ime_l, naslov_l, id_p_l, lon, lat) values (3, 'Bistro Alibi', 'Štihova ulica 11', 1, 46.061602682, 14.5149787624);
insert into lokal (id_l, ime_l, naslov_l, id_p_l, lon, lat) values (4, 'Pivovarna in pivnica Kratochwill', 'Kolodvorska ulica 14', 1, 46.0564663, 14.5096227);
insert into lokal (id_l, ime_l, naslov_l, id_p_l, lon, lat) values (5, 'Kinodvor.Kavarna', 'Kolodvorska ulica 13', 1, 46.0567929807, 14.5095797044);
insert into lokal (id_l, ime_l, naslov_l, id_p_l, lon, lat) values (6, 'Vita Caffe', 'Resljeva ulica 20', 1, 46.0553699609, 14.5113627967);
insert into lokal (id_l, ime_l, naslov_l, id_p_l, lon, lat) values (7, 'Pivovarna in pivnica Kratochwill', 'Jurčkova cesta 225', 1, 46.0202670174, 14.5366753234);
insert into lokal (id_l, ime_l, naslov_l, id_p_l, lon, lat) values (8, 'Kavarna Cacao', 'Petkovškovo nabrežje 3', 1, 46.0515995038, 14.5070538622);
insert into lokal (id_l, ime_l, naslov_l, id_p_l, lon, lat) values (9, 'Solist bar',	'Kongresni trg 10', 1, 46.0495109379, 14.5047075893);
insert into lokal (id_l, ime_l, naslov_l, id_p_l, lon, lat) values (10, 'Kava bar Lokal', 'Celovška cesta 479', 1, 46.100534402, 14.4584525444);
insert into lokal (id_l, ime_l, naslov_l, id_p_l, lon, lat) values (11, 'Clementina caffe bar', 'Gornji trg 25', 1, 46.0463503874, 14.5083408484);
insert into lokal (id_l, ime_l, naslov_l, id_p_l, lon, lat) values (12, 'Cafe bar & restaurant Roxly', 'Mala ulica 5', 1, 46.0531865828, 14.5078668783);
insert into lokal (id_l, ime_l, naslov_l, id_p_l, lon, lat) values (13, 'Delfin caffe', 'Hacquetova ulica 6', 1, 46.0603540206, 14.515454948);
insert into lokal (id_l, ime_l, naslov_l, id_p_l, lon, lat) values (14, 'Jugoslavija Caffe Bar', 'Kapiteljska ulica 7', 1, 46.0507634, 14.5124314);
insert into lokal (id_l, ime_l, naslov_l, id_p_l, lon, lat) values (15, 'Corner Pub', 'Tržaska cesta 19', 1, 46.0458921188, 14.4919604814);
insert into lokal (id_l, ime_l, naslov_l, id_p_l, lon, lat) values (16, 'Bar Luna', 'Linhartova cesta 32', 1, 46.0636921605, 14.5133319076);
insert into lokal (id_l, ime_l, naslov_l, id_p_l, lon, lat) values (17, 'Godec Pub', 'Celovška cesta 68A', 1, 46.064654, 14.4943057);
insert into lokal (id_l, ime_l, naslov_l, id_p_l, lon, lat) values (18, 'Guiness Pub', 'Gosposka ulica 3', 1, 46.0488176419, 14.5045974634);
insert into lokal (id_l, ime_l, naslov_l, id_p_l, lon, lat) values (19, 'Holidays pub', 'Slovenska cesta 36', 1, 46.0529720587, 14.5040176881);
insert into lokal (id_l, ime_l, naslov_l, id_p_l, lon, lat) values (20, 'C Bar', 'Cesta ljubljanske brigade 23', 1, 46.0866459734, 14.4787086038);
insert into lokal (id_l, ime_l, naslov_l, id_p_l, lon, lat) values (21, 'Okrepčevalnica Pomf', 'Trubarjeva cesta 40', 1, 46.0524262943, 14.5110268146);
insert into lokal (id_l, ime_l, naslov_l, id_p_l, lon, lat) values (22, 'Fresh bar', 'Orlova ulica 12', 1, 46.0377057575, 14.5151870787);

insert into delovnik (id_u, id_l, dan, ura) values (1, 1, 'Ponedeljek', '9.00 - 22.00');
insert into delovnik (id_u, id_l, dan, ura) values (2, 1, 'Torek', '9.00 - 22.00');
insert into delovnik (id_u, id_l, dan, ura) values (3, 1, 'Sreda', '9.00 - 22.00');
insert into delovnik (id_u, id_l, dan, ura) values (4, 1, 'Četrtek', '9.00 - 22.00');
insert into delovnik (id_u, id_l, dan, ura) values (5, 1, 'Petek', '9.00 - 24.00');
insert into delovnik (id_u, id_l, dan, ura) values (6, 1, 'Sobota', '9.00 - 24.00');
insert into delovnik (id_u, id_l, dan, ura) values (7, 1, 'Nedelja', '12.00 - 17.00');
insert into delovnik (id_u, id_l, dan, ura) values (8, 2, 'Ponedeljek', '6.00 - 22.30');
insert into delovnik (id_u, id_l, dan, ura) values (9, 2, 'Torek', '6.00 - 22.30');
insert into delovnik (id_u, id_l, dan, ura) values (10, 2, 'Sreda', '6.00 - 22.30');
insert into delovnik (id_u, id_l, dan, ura) values (11, 2, 'Četrtek', '6.00 - 22.30');
insert into delovnik (id_u, id_l, dan, ura) values (12, 2, 'Petek', '6.00 - 22.30');
insert into delovnik (id_u, id_l, dan, ura) values (13, 2, 'Sobota', '7.00 - 16.00');
insert into delovnik (id_u, id_l, dan, ura) values (14, 2, 'Nedelja', '7.00 - 16.00');
insert into delovnik (id_u, id_l, dan, ura) values (15, 3, 'Ponedeljek', '7.00 - 23.00');
insert into delovnik (id_u, id_l, dan, ura) values (16, 3, 'Torek', '7.00 - 23.00');
insert into delovnik (id_u, id_l, dan, ura) values (17, 3, 'Sreda', '7.00 - 23.00');
insert into delovnik (id_u, id_l, dan, ura) values (18, 3, 'Četrtek', '7.00 - 23.00');
insert into delovnik (id_u, id_l, dan, ura) values (19, 3, 'Petek', '7.00 - 23.00');
insert into delovnik (id_u, id_l, dan, ura) values (20, 3, 'Sobota', '9.00 - 23.00');
insert into delovnik (id_u, id_l, dan, ura) values (21, 3, 'Nedelja', '9.00 - 14.00');
insert into delovnik (id_u, id_l, dan, ura) values (22, 4, 'Torek', '9.00 - 23.00');
insert into delovnik (id_u, id_l, dan, ura) values (23, 4, 'Sreda', '9.00 - 23.00');
insert into delovnik (id_u, id_l, dan, ura) values (24, 4, 'Četrtek', '9.00 - 23.00');
insert into delovnik (id_u, id_l, dan, ura) values (25, 4, 'Petek', '9.00 - 23.00');
insert into delovnik (id_u, id_l, dan, ura) values (26, 4, 'Sobota', '12.00 - 22.00');
insert into delovnik (id_u, id_l, dan, ura) values (27, 4, 'Nedelja', 'zaprto');
insert into delovnik (id_u, id_l, dan, ura) values (28, 5, 'Ponedeljek', '10.00 - 24.00');
insert into delovnik (id_u, id_l, dan, ura) values (29, 5, 'Torek', '10.00 - 24.00');
insert into delovnik (id_u, id_l, dan, ura) values (30, 5, 'Sreda', '10.00 - 24.00');
insert into delovnik (id_u, id_l, dan, ura) values (31, 5, 'Četrtek', '10.00 - 24.00');
insert into delovnik (id_u, id_l, dan, ura) values (32, 5, 'Petek', '10.00 - 24.00');
insert into delovnik (id_u, id_l, dan, ura) values (33, 5, 'Sobota', '10.00 - 24.00');
insert into delovnik (id_u, id_l, dan, ura) values (34, 5, 'Nedelja', '10.00 - 24.00');
insert into delovnik (id_u, id_l, dan, ura) values (35, 6, 'Ponedeljek', '7.00 - 18.00');
insert into delovnik (id_u, id_l, dan, ura) values (36, 6, 'Torek', '7.00 - 18.00');
insert into delovnik (id_u, id_l, dan, ura) values (37, 6, 'Sreda', '7.00 - 18.00');
insert into delovnik (id_u, id_l, dan, ura) values (38, 6, 'Četrtek', '7.00 - 18.00');
insert into delovnik (id_u, id_l, dan, ura) values (39, 6, 'Petek',  '7.00 - 18.00');
insert into delovnik (id_u, id_l, dan, ura) values (40, 6, 'Sobota', 'zaprto');
insert into delovnik (id_u, id_l, dan, ura) values (41, 6, 'Nedelja', 'zaprto');
insert into delovnik (id_u, id_l, dan, ura) values (42, 7, 'Ponedeljek', '8.30 - 21.00');
insert into delovnik (id_u, id_l, dan, ura) values (43, 7, 'Torek', '8.30 - 21.00');
insert into delovnik (id_u, id_l, dan, ura) values (44, 7, 'Sreda', '8.30 - 21.00');
insert into delovnik (id_u, id_l, dan, ura) values (45, 7, 'Četrtek', '8.30 - 21.00');
insert into delovnik (id_u, id_l, dan, ura) values (46, 7, 'Petek', '8.30 - 21.00');
insert into delovnik (id_u, id_l, dan, ura) values (47, 7, 'Sobota', '8.00 - 21.00');
insert into delovnik (id_u, id_l, dan, ura) values (48, 7, 'Nedelja', '9.00 - 15.00');
insert into delovnik (id_u, id_l, dan, ura) values (49, 8, 'Ponedeljek', '8.00 - 24.00');
insert into delovnik (id_u, id_l, dan, ura) values (50, 8, 'Torek', '8.00 - 24.00');
insert into delovnik (id_u, id_l, dan, ura) values (51, 8, 'Sreda', '8.00 - 24.00');
insert into delovnik (id_u, id_l, dan, ura) values (52, 8, 'Četrtek', '8.00 - 24.00');
insert into delovnik (id_u, id_l, dan, ura) values (53, 8, 'Petek', '8.00 - 24.00');
insert into delovnik (id_u, id_l, dan, ura) values (54, 8, 'Sobota', '8.00 - 24.00');
insert into delovnik (id_u, id_l, dan, ura) values (55, 8, 'Nedelja', '8.00 - 24.00');
insert into delovnik (id_u, id_l, dan, ura) values (56, 9, 'Ponedeljek', '9.00 -  1.00');
insert into delovnik (id_u, id_l, dan, ura) values (57, 9, 'Torek', '9.00 -  1.00');
insert into delovnik (id_u, id_l, dan, ura) values (58, 9, 'Sreda', '9.00 -  1.00');
insert into delovnik (id_u, id_l, dan, ura) values (59, 9, 'Četrtek', '9.00 -  1.00');
insert into delovnik (id_u, id_l, dan, ura) values (60, 9, 'Petek', '9.00 -  1.00');
insert into delovnik (id_u, id_l, dan, ura) values (61, 9, 'Sobota', '9.00 -  1.00');
insert into delovnik (id_u, id_l, dan, ura) values (62, 9, 'Nedelja', '9.00 -  1.00');
insert into delovnik (id_u, id_l, dan, ura) values (63, 10, 'Ponedeljek', '8.00 - 21.00');
insert into delovnik (id_u, id_l, dan, ura) values (64, 10, 'Torek', '8.00 - 21.00');
insert into delovnik (id_u, id_l, dan, ura) values (65, 10, 'Sreda', '8.00 - 21.00');
insert into delovnik (id_u, id_l, dan, ura) values (66, 10, 'Četrtek', '8.00 - 21.00');
insert into delovnik (id_u, id_l, dan, ura) values (67, 10, 'Petek', '8.00 - 21.00');
insert into delovnik (id_u, id_l, dan, ura) values (68, 10, 'Sobota', '8.00 - 21.00');
insert into delovnik (id_u, id_l, dan, ura) values (69, 10, 'Nedelja', '8.00 - 13.00');
insert into delovnik (id_u, id_l, dan, ura) values (70, 11, 'Ponedeljek', '7.00 -  3.00');
insert into delovnik (id_u, id_l, dan, ura) values (71, 11, 'Torek', '7.00 -  3.00');
insert into delovnik (id_u, id_l, dan, ura) values (72, 11, 'Sreda', '7.00 -  3.00');
insert into delovnik (id_u, id_l, dan, ura) values (73, 11, 'Četrtek', '7.00 -  3.00');
insert into delovnik (id_u, id_l, dan, ura) values (74, 11, 'Petek', '7.00 -  3.00');
insert into delovnik (id_u, id_l, dan, ura) values (75, 11, 'Sobota', 'zaprto');
insert into delovnik (id_u, id_l, dan, ura) values (76, 11, 'Nedelja', 'zaprto');
insert into delovnik (id_u, id_l, dan, ura) values (77, 12, 'Ponedeljek', '6.30 - 22.00');
insert into delovnik (id_u, id_l, dan, ura) values (78, 12, 'Torek', '6.30 - 22.00');
insert into delovnik (id_u, id_l, dan, ura) values (79, 12, 'Sreda', '6.30 - 22.00');
insert into delovnik (id_u, id_l, dan, ura) values (80, 12, 'Četrtek', '6.30 - 22.00');
insert into delovnik (id_u, id_l, dan, ura) values (81, 12, 'Petek', '6.30 - 22.00');
insert into delovnik (id_u, id_l, dan, ura) values (82, 12, 'Sobota', '6.30 - 15.00');
insert into delovnik (id_u, id_l, dan, ura) values (83, 12, 'Nedelja', 'zaprto');
insert into delovnik (id_u, id_l, dan, ura) values (84, 13, 'Ponedeljek', '7.00 - 23.00');
insert into delovnik (id_u, id_l, dan, ura) values (85, 13, 'Torek', '7.00 - 23.00');
insert into delovnik (id_u, id_l, dan, ura) values (86, 13, 'Sreda', '7.00 - 23.00');
insert into delovnik (id_u, id_l, dan, ura) values (87, 13, 'Četrtek', '7.00 - 23.00');
insert into delovnik (id_u, id_l, dan, ura) values (88, 13, 'Petek', '7.00 - 24.00');
insert into delovnik (id_u, id_l, dan, ura) values (89, 13, 'Sobota', '7.00 - 24.00');
insert into delovnik (id_u, id_l, dan, ura) values (90, 13, 'Nedelja', 'zaprto');
insert into delovnik (id_u, id_l, dan, ura) values (91, 14, 'Ponedeljek', '6.30 -  2.00');
insert into delovnik (id_u, id_l, dan, ura) values (92, 14, 'Torek', '6.30 -  2.00');
insert into delovnik (id_u, id_l, dan, ura) values (93, 14, 'Sreda', '6.30 -  2.00');
insert into delovnik (id_u, id_l, dan, ura) values (94, 14, 'Četrtek', '6.30 -  2.00');
insert into delovnik (id_u, id_l, dan, ura) values (95, 14, 'Petek', '6.30 -  2.00');
insert into delovnik (id_u, id_l, dan, ura) values (96, 14, 'Sobota', '8.00 -  2.00');
insert into delovnik (id_u, id_l, dan, ura) values (97, 14, 'Nedelja', '9.00 -  2.00');
insert into delovnik (id_u, id_l, dan, ura) values (98, 15, 'Ponedeljek', '7.00 - 22.00');
insert into delovnik (id_u, id_l, dan, ura) values (99, 15, 'Torek', '7.00 - 22.00');
insert into delovnik (id_u, id_l, dan, ura) values (100, 15, 'Sreda', '7.00 - 22.00');
insert into delovnik (id_u, id_l, dan, ura) values (101, 15, 'Četrtek', '7.00 - 22.00');
insert into delovnik (id_u, id_l, dan, ura) values (102, 15, 'Petek', '7.00 - 22.00');
insert into delovnik (id_u, id_l, dan, ura) values (103, 15, 'Sobota', '7.00 - 22.00');
insert into delovnik (id_u, id_l, dan, ura) values (104, 15, 'Nedelja', '12.00 - 22.00');
insert into delovnik (id_u, id_l, dan, ura) values (105, 16, 'Ponedeljek', '7.00 - 24.00');
insert into delovnik (id_u, id_l, dan, ura) values (106, 16, 'Torek', '7.00 - 24.00');
insert into delovnik (id_u, id_l, dan, ura) values (107, 16, 'Sreda', '7.00 - 24.00');
insert into delovnik (id_u, id_l, dan, ura) values (108, 16, 'Četrtek', '7.00 - 24.00');
insert into delovnik (id_u, id_l, dan, ura) values (109, 16, 'Petek', '7.00 - 24.00');
insert into delovnik (id_u, id_l, dan, ura) values (110, 16, 'Sobota', '18.00 - 24.00');
insert into delovnik (id_u, id_l, dan, ura) values (111, 16, 'Nedelja', '18.00 - 24.00');
insert into delovnik (id_u, id_l, dan, ura) values (112, 17, 'Ponedeljek', '7.00 -  1.00');
insert into delovnik (id_u, id_l, dan, ura) values (113, 17, 'Torek', '7.00 -  1.00');
insert into delovnik (id_u, id_l, dan, ura) values (114, 17, 'Sreda', '7.00 -  1.00');
insert into delovnik (id_u, id_l, dan, ura) values (115, 17, 'Četrtek', '7.00 -  1.00');
insert into delovnik (id_u, id_l, dan, ura) values (116, 17, 'Petek', '7.00 -  1.00');
insert into delovnik (id_u, id_l, dan, ura) values (117, 17, 'Sobota', '18.00 -  1.00');
insert into delovnik (id_u, id_l, dan, ura) values (118, 17, 'Nedelja', '18.00 -  1.00');
insert into delovnik (id_u, id_l, dan, ura) values (119, 18, 'Ponedeljek', '7.00 -  3.00');
insert into delovnik (id_u, id_l, dan, ura) values (120, 18, 'Torek', '7.00 -  3.00');
insert into delovnik (id_u, id_l, dan, ura) values (121, 18, 'Sreda', '7.00 -  3.00');
insert into delovnik (id_u, id_l, dan, ura) values (122, 18, 'Četrtek', '7.00 -  3.00');
insert into delovnik (id_u, id_l, dan, ura) values (123, 18, 'Petek', '7.00 -  3.00');
insert into delovnik (id_u, id_l, dan, ura) values (124, 18, 'Sobota', '7.00 -  3.00');
insert into delovnik (id_u, id_l, dan, ura) values (125, 18, 'Nedelja', '7.00 -  3.00');
insert into delovnik (id_u, id_l, dan, ura) values (126, 19, 'Ponedeljek', '6.30 - 23.00');
insert into delovnik (id_u, id_l, dan, ura) values (127, 19, 'Torek', '6.30 - 23.00');
insert into delovnik (id_u, id_l, dan, ura) values (128, 19, 'Sreda', '6.30 - 23.00');
insert into delovnik (id_u, id_l, dan, ura) values (129, 19, 'Četrtek', '6.30 - 23.00');
insert into delovnik (id_u, id_l, dan, ura) values (130, 19, 'Petek', '6.30 - 23.00');
insert into delovnik (id_u, id_l, dan, ura) values (131, 19, 'Sobota', '6.30 - 3.00');
insert into delovnik (id_u, id_l, dan, ura) values (132, 19, 'Nedelja', '8.00 - 23.00');
insert into delovnik (id_u, id_l, dan, ura) values (133, 20, 'Ponedeljek', '9.00 - 24.00');
insert into delovnik (id_u, id_l, dan, ura) values (134, 20, 'Torek', '9.00 - 24.00');
insert into delovnik (id_u, id_l, dan, ura) values (135, 20, 'Sreda', '9.00 - 24.00');
insert into delovnik (id_u, id_l, dan, ura) values (136, 20, 'Četrtek', '9.00 - 24.00');
insert into delovnik (id_u, id_l, dan, ura) values (137, 20, 'Petek', '9.00 -  1.00');
insert into delovnik (id_u, id_l, dan, ura) values (138, 20, 'Sobota', '12.00 -  1.00');
insert into delovnik (id_u, id_l, dan, ura) values (139, 20, 'Nedelja', '12.00 - 24.00');
insert into delovnik (id_u, id_l, dan, ura) values (140, 21, 'Ponedeljek', '7.00 - 23.00');
insert into delovnik (id_u, id_l, dan, ura) values (141, 21, 'Torek', '7.00 - 23.00');
insert into delovnik (id_u, id_l, dan, ura) values (142, 21, 'Sreda', '7.00 - 23.00');
insert into delovnik (id_u, id_l, dan, ura) values (143, 21, 'Četrtek', '7.00 - 23.00');
insert into delovnik (id_u, id_l, dan, ura) values (144, 21, 'Petek', '7.00 - 23.00');
insert into delovnik (id_u, id_l, dan, ura) values (145, 21, 'Sobota', '11.00 - 23.00');
insert into delovnik (id_u, id_l, dan, ura) values (146, 21, 'Nedelja', '12.00 - 21.00');
insert into delovnik (id_u, id_l, dan, ura) values (147, 22, 'Ponedeljek', '7.00 - 23.00');
insert into delovnik (id_u, id_l, dan, ura) values (148, 22, 'Torek', '7.00 - 23.00');
insert into delovnik (id_u, id_l, dan, ura) values (149, 22, 'Sreda', '7.00 - 23.00');
insert into delovnik (id_u, id_l, dan, ura) values (150, 22, 'Četrtek', '7.00 - 23.00');
insert into delovnik (id_u, id_l, dan, ura) values (151, 22, 'Petek', '7.00 - 23.00');
insert into delovnik (id_u, id_l, dan, ura) values (152, 22, 'Sobota', '7.00 - 23.00');
insert into delovnik (id_u, id_l, dan, ura) values (153, 22, 'Nedelja', '7.00 - 23.00');

insert into cenik (id_c, id_l, id_pij, znesek) values (1, 1, 1, 2.5);
insert into cenik (id_c, id_l, id_pij, znesek) values (2, 1, 2, 2);
insert into cenik (id_c, id_l, id_pij, znesek) values (3, 1, 3, 2);
insert into cenik (id_c, id_l, id_pij, znesek) values (4, 1, 4, 4);
insert into cenik (id_c, id_l, id_pij, znesek) values (5, 1, 5, 1.5);
insert into cenik (id_c, id_l, id_pij, znesek) values (6, 1, 6, 1.8);
insert into cenik (id_c, id_l, id_pij, znesek) values (7, 1, 7, 1.5);
insert into cenik (id_c, id_l, id_pij, znesek) values (8, 1, 8, 2);
insert into cenik (id_c, id_l, id_pij, znesek) values (9, 1, 9, 2);
insert into cenik (id_c, id_l, id_pij, znesek) values (10, 2, 1, 2.7);
insert into cenik (id_c, id_l, id_pij, znesek) values (11, 2, 2, 2.2);
insert into cenik (id_c, id_l, id_pij, znesek) values (12, 2, 3, 1.8);
insert into cenik (id_c, id_l, id_pij, znesek) values (13, 2, 4, 3.9);
insert into cenik (id_c, id_l, id_pij, znesek) values (14, 2, 5, 1.6);
insert into cenik (id_c, id_l, id_pij, znesek) values (15, 2, 6, 1.7);
insert into cenik (id_c, id_l, id_pij, znesek) values (16, 2, 7, 2);
insert into cenik (id_c, id_l, id_pij, znesek) values (17, 2, 8, 2);
insert into cenik (id_c, id_l, id_pij, znesek) values (18, 2, 9, 2);
insert into cenik (id_c, id_l, id_pij, znesek) values (19, 3, 1, 2.6);
insert into cenik (id_c, id_l, id_pij, znesek) values (20, 3, 2, 3.9);
insert into cenik (id_c, id_l, id_pij, znesek) values (21, 3, 3, 2);
insert into cenik (id_c, id_l, id_pij, znesek) values (22, 3, 4, 4.5);
insert into cenik (id_c, id_l, id_pij, znesek) values (23, 3, 5, 1.5);
insert into cenik (id_c, id_l, id_pij, znesek) values (24, 3, 6, 1.5);
insert into cenik (id_c, id_l, id_pij, znesek) values (25, 3, 7, 2);
insert into cenik (id_c, id_l, id_pij, znesek) values (26, 3, 8, 2);
insert into cenik (id_c, id_l, id_pij, znesek) values (27, 3, 9, 2);
insert into cenik (id_c, id_l, id_pij, znesek) values (28, 4, 1, 3);
insert into cenik (id_c, id_l, id_pij, znesek) values (29, 4, 2, 5);
insert into cenik (id_c, id_l, id_pij, znesek) values (30, 4, 3, 2);
insert into cenik (id_c, id_l, id_pij, znesek) values (31, 4, 5, 1.7);
insert into cenik (id_c, id_l, id_pij, znesek) values (32, 4, 6, 1.7);
insert into cenik (id_c, id_l, id_pij, znesek) values (33, 4, 7, 2);
insert into cenik (id_c, id_l, id_pij, znesek) values (34, 4, 8, 2);
insert into cenik (id_c, id_l, id_pij, znesek) values (35, 4, 9, 2);
insert into cenik (id_c, id_l, id_pij, znesek) values (36, 5, 1, 3);
insert into cenik (id_c, id_l, id_pij, znesek) values (37, 5, 2, 4.5);
insert into cenik (id_c, id_l, id_pij, znesek) values (38, 5, 3, 1.5);
insert into cenik (id_c, id_l, id_pij, znesek) values (39, 5, 4, 4.4);
insert into cenik (id_c, id_l, id_pij, znesek) values (40, 5, 5, 2);
insert into cenik (id_c, id_l, id_pij, znesek) values (41, 5, 6, 1.8);
insert into cenik (id_c, id_l, id_pij, znesek) values (42, 5, 7, 2);
insert into cenik (id_c, id_l, id_pij, znesek) values (43, 5, 8, 2);
insert into cenik (id_c, id_l, id_pij, znesek) values (44, 5, 9, 2);
insert into cenik (id_c, id_l, id_pij, znesek) values (45, 6, 1, 2.9);
insert into cenik (id_c, id_l, id_pij, znesek) values (46, 6, 2, 3.7);
insert into cenik (id_c, id_l, id_pij, znesek) values (47, 6, 3, 2);
insert into cenik (id_c, id_l, id_pij, znesek) values (48, 6, 4, 6);
insert into cenik (id_c, id_l, id_pij, znesek) values (49, 6, 5, 2.1);
insert into cenik (id_c, id_l, id_pij, znesek) values (50, 6, 6, 2.1);
insert into cenik (id_c, id_l, id_pij, znesek) values (51, 6, 7, 2);
insert into cenik (id_c, id_l, id_pij, znesek) values (52, 6, 8, 2);
insert into cenik (id_c, id_l, id_pij, znesek) values (53, 6, 9, 2);
insert into cenik (id_c, id_l, id_pij, znesek) values (54, 7, 1, 2.3);
insert into cenik (id_c, id_l, id_pij, znesek) values (55, 7, 2, 4);
insert into cenik (id_c, id_l, id_pij, znesek) values (56, 7, 3, 3.9);
insert into cenik (id_c, id_l, id_pij, znesek) values (57, 7, 4, 4);
insert into cenik (id_c, id_l, id_pij, znesek) values (58, 7, 5, 1.4);
insert into cenik (id_c, id_l, id_pij, znesek) values (59, 7, 6, 1.4);
insert into cenik (id_c, id_l, id_pij, znesek) values (60, 7, 7, 2);
insert into cenik (id_c, id_l, id_pij, znesek) values (61, 7, 8, 2);
insert into cenik (id_c, id_l, id_pij, znesek) values (62, 7, 9, 2);
insert into cenik (id_c, id_l, id_pij, znesek) values (63, 8, 1, 3.1);
insert into cenik (id_c, id_l, id_pij, znesek) values (64, 8, 2, 4);
insert into cenik (id_c, id_l, id_pij, znesek) values (65, 8, 3, 2);
insert into cenik (id_c, id_l, id_pij, znesek) values (66, 8, 4, 5);
insert into cenik (id_c, id_l, id_pij, znesek) values (67, 8, 5, 1.7);
insert into cenik (id_c, id_l, id_pij, znesek) values (68, 8, 6, 1.8);
insert into cenik (id_c, id_l, id_pij, znesek) values (69, 8, 7, 2);
insert into cenik (id_c, id_l, id_pij, znesek) values (70, 8, 8, 2);
insert into cenik (id_c, id_l, id_pij, znesek) values (71, 8, 9, 2);
insert into cenik (id_c, id_l, id_pij, znesek) values (72, 9, 1, 3);
insert into cenik (id_c, id_l, id_pij, znesek) values (73, 9, 2, 4);
insert into cenik (id_c, id_l, id_pij, znesek) values (74, 9, 3, 2.2);
insert into cenik (id_c, id_l, id_pij, znesek) values (75, 9, 4, 4.5);
insert into cenik (id_c, id_l, id_pij, znesek) values (76, 9, 5, 1.6);
insert into cenik (id_c, id_l, id_pij, znesek) values (77, 9, 6, 1.5);
insert into cenik (id_c, id_l, id_pij, znesek) values (78, 9, 7, 2);
insert into cenik (id_c, id_l, id_pij, znesek) values (79, 9, 8, 2);
insert into cenik (id_c, id_l, id_pij, znesek) values (80, 9, 9, 2);
insert into cenik (id_c, id_l, id_pij, znesek) values (81, 10, 1, 2.6);
insert into cenik (id_c, id_l, id_pij, znesek) values (82, 10, 2, 4.1);
insert into cenik (id_c, id_l, id_pij, znesek) values (83, 10, 3, 2.1);
insert into cenik (id_c, id_l, id_pij, znesek) values (84, 10, 4, 4.2);
insert into cenik (id_c, id_l, id_pij, znesek) values (85, 10, 5, 2.2);
insert into cenik (id_c, id_l, id_pij, znesek) values (86, 10, 6, 2.2);
insert into cenik (id_c, id_l, id_pij, znesek) values (87, 10, 7, 2);
insert into cenik (id_c, id_l, id_pij, znesek) values (88, 10, 8, 2);
insert into cenik (id_c, id_l, id_pij, znesek) values (89, 10, 9, 2);
insert into cenik (id_c, id_l, id_pij, znesek) values (90, 11, 1, 2.7);
insert into cenik (id_c, id_l, id_pij, znesek) values (91, 11, 2, 4.1);
insert into cenik (id_c, id_l, id_pij, znesek) values (92, 11, 3, 2.1);
insert into cenik (id_c, id_l, id_pij, znesek) values (93, 11, 4, 4.2);
insert into cenik (id_c, id_l, id_pij, znesek) values (94, 11, 5, 2.2);
insert into cenik (id_c, id_l, id_pij, znesek) values (95, 11, 6, 2.2);
insert into cenik (id_c, id_l, id_pij, znesek) values (96, 11, 7, 2);
insert into cenik (id_c, id_l, id_pij, znesek) values (97, 11, 8, 2);
insert into cenik (id_c, id_l, id_pij, znesek) values (98, 11, 9, 2);
insert into cenik (id_c, id_l, id_pij, znesek) values (99, 12, 1, 3);
insert into cenik (id_c, id_l, id_pij, znesek) values (100, 12, 2, 4.1);
insert into cenik (id_c, id_l, id_pij, znesek) values (101, 12, 3, 2.1);
insert into cenik (id_c, id_l, id_pij, znesek) values (102, 12, 4, 4.2);
insert into cenik (id_c, id_l, id_pij, znesek) values (103, 12, 5, 2.2);
insert into cenik (id_c, id_l, id_pij, znesek) values (104, 12, 6, 2.2);
insert into cenik (id_c, id_l, id_pij, znesek) values (105, 12, 7, 2);
insert into cenik (id_c, id_l, id_pij, znesek) values (106, 12, 8, 2);
insert into cenik (id_c, id_l, id_pij, znesek) values (107, 12, 9, 2);
insert into cenik (id_c, id_l, id_pij, znesek) values (108, 13, 1, 3.2);
insert into cenik (id_c, id_l, id_pij, znesek) values (109, 13, 2, 4.1);
insert into cenik (id_c, id_l, id_pij, znesek) values (110, 13, 3, 2.1);
insert into cenik (id_c, id_l, id_pij, znesek) values (111, 13, 4, 4.2);
insert into cenik (id_c, id_l, id_pij, znesek) values (112, 13, 5, 2.2);
insert into cenik (id_c, id_l, id_pij, znesek) values (113, 13, 6, 2.2);
insert into cenik (id_c, id_l, id_pij, znesek) values (114, 13, 7, 2);
insert into cenik (id_c, id_l, id_pij, znesek) values (115, 13, 8, 2);
insert into cenik (id_c, id_l, id_pij, znesek) values (116, 13, 9, 2);
insert into cenik (id_c, id_l, id_pij, znesek) values (117, 14, 1, 2.8);
insert into cenik (id_c, id_l, id_pij, znesek) values (118, 14, 2, 4.1);
insert into cenik (id_c, id_l, id_pij, znesek) values (119, 14, 3, 2.1);
insert into cenik (id_c, id_l, id_pij, znesek) values (120, 14, 4, 4.2);
insert into cenik (id_c, id_l, id_pij, znesek) values (121, 14, 5, 2.2);
insert into cenik (id_c, id_l, id_pij, znesek) values (122, 14, 6, 2.2);
insert into cenik (id_c, id_l, id_pij, znesek) values (123, 14, 7, 2);
insert into cenik (id_c, id_l, id_pij, znesek) values (124, 14, 8, 2);
insert into cenik (id_c, id_l, id_pij, znesek) values (125, 14, 9, 2);
insert into cenik (id_c, id_l, id_pij, znesek) values (126, 15, 1, 2.1);
insert into cenik (id_c, id_l, id_pij, znesek) values (127, 15, 2, 4.1);
insert into cenik (id_c, id_l, id_pij, znesek) values (128, 16, 3, 2.1);
insert into cenik (id_c, id_l, id_pij, znesek) values (129, 16, 4, 4.2);
insert into cenik (id_c, id_l, id_pij, znesek) values (130, 16, 5, 2.2);
insert into cenik (id_c, id_l, id_pij, znesek) values (131, 16, 6, 2.2);
insert into cenik (id_c, id_l, id_pij, znesek) values (132, 16, 7, 2);
insert into cenik (id_c, id_l, id_pij, znesek) values (133, 16, 8, 2);
insert into cenik (id_c, id_l, id_pij, znesek) values (134, 16, 9, 2);
insert into cenik (id_c, id_l, id_pij, znesek) values (135, 17, 1, 3.1);
insert into cenik (id_c, id_l, id_pij, znesek) values (136, 17, 2, 4.1);
insert into cenik (id_c, id_l, id_pij, znesek) values (137, 17, 3, 2.1);
insert into cenik (id_c, id_l, id_pij, znesek) values (138, 17, 4, 4.2);
insert into cenik (id_c, id_l, id_pij, znesek) values (139, 17, 5, 2.2);
insert into cenik (id_c, id_l, id_pij, znesek) values (140, 17, 6, 2.2);
insert into cenik (id_c, id_l, id_pij, znesek) values (141, 17, 7, 2);
insert into cenik (id_c, id_l, id_pij, znesek) values (142, 17, 8, 2);
insert into cenik (id_c, id_l, id_pij, znesek) values (143, 17, 9, 2);
insert into cenik (id_c, id_l, id_pij, znesek) values (144, 18, 1, 2.6);
insert into cenik (id_c, id_l, id_pij, znesek) values (145, 18, 2, 4.1);
insert into cenik (id_c, id_l, id_pij, znesek) values (146, 18, 3, 2.1);
insert into cenik (id_c, id_l, id_pij, znesek) values (147, 18, 4, 4.2);
insert into cenik (id_c, id_l, id_pij, znesek) values (148, 18, 5, 2.2);
insert into cenik (id_c, id_l, id_pij, znesek) values (149, 18, 6, 2.2);
insert into cenik (id_c, id_l, id_pij, znesek) values (150, 18, 7, 2);
insert into cenik (id_c, id_l, id_pij, znesek) values (151, 18, 8, 2);
insert into cenik (id_c, id_l, id_pij, znesek) values (152, 18, 9, 2);
insert into cenik (id_c, id_l, id_pij, znesek) values (153, 19, 1, 2.8);
insert into cenik (id_c, id_l, id_pij, znesek) values (154, 19, 2, 4.1);
insert into cenik (id_c, id_l, id_pij, znesek) values (155, 19, 3, 2.1);
insert into cenik (id_c, id_l, id_pij, znesek) values (156, 19, 4, 4.2);
insert into cenik (id_c, id_l, id_pij, znesek) values (157, 19, 5, 2.2);
insert into cenik (id_c, id_l, id_pij, znesek) values (158, 19, 6, 2.2);
insert into cenik (id_c, id_l, id_pij, znesek) values (159, 19, 7, 2);
insert into cenik (id_c, id_l, id_pij, znesek) values (160, 19, 8, 2);
insert into cenik (id_c, id_l, id_pij, znesek) values (161, 19, 9, 2);
insert into cenik (id_c, id_l, id_pij, znesek) values (162, 20, 1, 3);
insert into cenik (id_c, id_l, id_pij, znesek) values (163, 20, 2, 4.1);
insert into cenik (id_c, id_l, id_pij, znesek) values (164, 20, 3, 2.1);
insert into cenik (id_c, id_l, id_pij, znesek) values (165, 20, 4, 4.2);
insert into cenik (id_c, id_l, id_pij, znesek) values (166, 20, 5, 2.2);
insert into cenik (id_c, id_l, id_pij, znesek) values (167, 20, 6, 2.2);
insert into cenik (id_c, id_l, id_pij, znesek) values (168, 20, 7, 2);
insert into cenik (id_c, id_l, id_pij, znesek) values (169, 20, 8, 2);
insert into cenik (id_c, id_l, id_pij, znesek) values (170, 20, 9, 2);
insert into cenik (id_c, id_l, id_pij, znesek) values (171, 21, 1, 2.9);
insert into cenik (id_c, id_l, id_pij, znesek) values (172, 21, 2, 4.1);
insert into cenik (id_c, id_l, id_pij, znesek) values (173, 21, 3, 2.1);
insert into cenik (id_c, id_l, id_pij, znesek) values (174, 21, 4, 4.2);
insert into cenik (id_c, id_l, id_pij, znesek) values (175, 21, 5, 2.2);
insert into cenik (id_c, id_l, id_pij, znesek) values (176, 21, 6, 2.2);
insert into cenik (id_c, id_l, id_pij, znesek) values (177, 21, 7, 2);
insert into cenik (id_c, id_l, id_pij, znesek) values (178, 21, 8, 2);
insert into cenik (id_c, id_l, id_pij, znesek) values (179, 21, 9, 2);
insert into cenik (id_c, id_l, id_pij, znesek) values (180, 22, 1, 2.9);
insert into cenik (id_c, id_l, id_pij, znesek) values (181, 22, 2, 4.1);
insert into cenik (id_c, id_l, id_pij, znesek) values (182, 22, 3, 2.1);
insert into cenik (id_c, id_l, id_pij, znesek) values (183, 22, 4, 4.2);
insert into cenik (id_c, id_l, id_pij, znesek) values (184, 22, 5, 2.2);
insert into cenik (id_c, id_l, id_pij, znesek) values (185, 22, 6, 2.2);
insert into cenik (id_c, id_l, id_pij, znesek) values (186, 22, 7, 2);
insert into cenik (id_c, id_l, id_pij, znesek) values (187, 22, 8, 2);
insert into cenik (id_c, id_l, id_pij, znesek) values (188, 22, 9, 2);