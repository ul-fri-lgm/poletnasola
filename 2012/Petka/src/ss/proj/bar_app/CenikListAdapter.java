package ss.proj.bar_app;

import java.util.List;

import ss.proj.bar_app.LokalListAdapter.ViewHolder;
import ss.proj.bar_app.db.tables.Cenik;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

public class CenikListAdapter extends BaseAdapter {

	private List<Cenik> allPrices;
	private LayoutInflater mInflater;

	public CenikListAdapter(Context context) {
		allPrices = ((BarApplication) context).getBazaHelper().getAllPrices();
		mInflater = (LayoutInflater) context
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	}

	@Override
	public int getCount() {
		return allPrices.size();
	}

	@Override
	public Cenik getItem(int i) {
		return allPrices.get(i);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		ViewHolder vh;

		if (convertView == null) {
			convertView = mInflater.inflate(R.layout.list_item, null);

			vh = new ViewHolder();
			vh.label1 = (TextView) convertView.findViewById(R.id.textView1);

			convertView.setTag(vh);
		} else {
			vh = (ViewHolder) convertView.getTag();
		}

		vh.label1.setText((allPrices.get(position).getZnesek() + ""));

		return convertView;
	}

	static class ViewHolder {
		TextView label1; // cena
	}
}
