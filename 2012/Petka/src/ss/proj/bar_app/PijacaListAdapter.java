package ss.proj.bar_app;

import java.util.List;

import ss.proj.bar_app.db.tables.Pijaca;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

public class PijacaListAdapter extends BaseAdapter {

	private List<Pijaca> allDrinks;
	private LayoutInflater mInflater;

	public PijacaListAdapter(Context context) {
		allDrinks = ((BarApplication) context).getBazaHelper().getAllDrinks();
		mInflater = (LayoutInflater) context
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	}

	public int getCount() {
		return allDrinks.size();
	}

	public Pijaca getItem(int i) {
		return allDrinks.get(i);
	}

	public long getItemId(int position) {
		return position;
	}

	public View getView(int position, View convertView, ViewGroup parent) {
		ViewHolder vh;

		if (convertView == null) {
			convertView = mInflater.inflate(R.layout.list_item, null);

			vh = new ViewHolder();
			vh.label = (TextView) convertView.findViewById(R.id.textView1);

			convertView.setTag(vh);
		} else {
			vh = (ViewHolder) convertView.getTag();
		}

		vh.label.setText(allDrinks.get(position).getIme_p());

		return convertView;
	}

	static class ViewHolder {
		TextView label;
	}
}
