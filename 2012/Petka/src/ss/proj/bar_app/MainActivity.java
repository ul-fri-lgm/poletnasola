package ss.proj.bar_app;

import java.util.List;

import ss.proj.bar_app.db.tables.Pijaca;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;
import android.widget.Toast;

public class MainActivity extends Activity implements OnClickListener,
		OnItemClickListener {

	Toast statusView;
	Context context;
	int duration;
	CharSequence statusText;

	private int counter = 0;
	long minTime = 5000;
	float minDistance = (float) 10.0;
	double lat;
	double lon;

	ListView lv;

	BroadcastReceiver internetStatusReceiver;
	LocationListener locationListenerGPS;
	LocationListener locationListenerNET;
	LocationManager locationManager;
	private boolean isListenerRegisteredGPS;
	private boolean isListenerRegisteredNET;
	boolean isGPSenabled = false;
	boolean isNETenabled = false;
	private String provider;

	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		// initialize toast parameters
		context = getApplicationContext();
		duration = Toast.LENGTH_SHORT;

		internetStatusReceiver = new BroadcastReceiver() {

			// dobivanje sistemskih klicov
			@Override
			public void onReceive(Context context, Intent intent) {
				if (!intent.getAction().equals(
						ConnectivityManager.CONNECTIVITY_ACTION))
					return;

				String reason = intent
						.getStringExtra(ConnectivityManager.EXTRA_REASON);
				boolean noConnectivity = intent.getBooleanExtra(
						ConnectivityManager.EXTRA_NO_CONNECTIVITY, false);
				String extra = intent
						.getStringExtra(ConnectivityManager.EXTRA_EXTRA_INFO);
				boolean isFailover = intent.getBooleanExtra(
						ConnectivityManager.EXTRA_IS_FAILOVER, false);

				NetworkInfo otherNetworkInfo = (NetworkInfo) intent
						.getParcelableExtra(ConnectivityManager.EXTRA_OTHER_NETWORK_INFO);
				String oni = (otherNetworkInfo == null) ? "Null"
						: otherNetworkInfo.getTypeName();

				Log.d("INTERNET", "jeNet: " + (noConnectivity ? "Ne" : "Ja")
						+ " failover: " + (isFailover ? "Ne" : "Ja")
						+ " razlog: " + reason + " drugaMreza: " + oni
						+ " ostalo: " + extra);
				// osvezimo status
				displayInternetStatus();
			}
		};
		// register listener
		registerReceiver(internetStatusReceiver, new IntentFilter(
				ConnectivityManager.CONNECTIVITY_ACTION));

		// LOCATION
		locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);

		// location listener for GPS
		locationListenerGPS = new LocationListener() {
			public void onLocationChanged(Location location) {
				// TODO Auto-generated method stub
				Log.d("LOCATION", "(GPS)Location changed!");
				if (provider == "gps")
					updateLocation(location);
			}

			public void onProviderDisabled(String provider) {
				Log.d("LOCATION", "(GPS)Provider disabled.");
				chooseBestProvider();
			}

			public void onProviderEnabled(String provider) {
				Log.d("LOCATION", "(GPS)Provider enabled.");
				chooseBestProvider();
			}

			public void onStatusChanged(String provider, int status,
					Bundle extras) {
				Log.d("LOCATION", "(GPS)Status changed.");
			}
		};

		// location listener for Network
		locationListenerNET = new LocationListener() {
			public void onLocationChanged(Location location) {
				// TODO Auto-generated method stub
				Log.d("LOCATION", "(NET)Location changed!");
				if (provider == "network")
					updateLocation(location);
			}

			public void onProviderDisabled(String provider) {
				Log.d("LOCATION", "(NET)Provider disabled.");
				chooseBestProvider();
			}

			public void onProviderEnabled(String provider) {
				Log.d("LOCATION", "(NET)New provider enabled.");
				chooseBestProvider();
			}

			public void onStatusChanged(String provider, int status,
					Bundle extras) {
				Log.d("LOCATION", "(NET)Status changed.");
			}
		};

		// SPREMENI: po koncu testiranja izbri�i seznam
		List<String> providers = locationManager.getProviders(true);
		Log.d("LOCATION", "All enabled providers:" + providers);

		chooseBestProvider();

		lv = (ListView) findViewById(R.id.list);
		lv.setAdapter(new PijacaListAdapter(getApplicationContext()));
		lv.setOnItemClickListener(this);

	}

	public void displayInternetStatus() {
		ConnectivityManager conMan = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
		// pridobimo info o trenutnem omre�ju
		NetworkInfo ni = conMan.getActiveNetworkInfo();
		// preverimo če ni omrežja ali ni informacij o omrežju
		// omogocimo/onemogocimo gumb glede na obstoj povezave v omre�je

		if (ni == null || !ni.isConnected()) {
			statusText = "Ni internetne povezave!";
			statusView = Toast.makeText(context, statusText, duration);
			statusView.show();
		} else {

			// preverimo tip omre�ja

			switch (ni.getType()) {
			case ConnectivityManager.TYPE_WIFI:
				// smo v poceni tarifi � prenasaj dolge datoteke
				statusText = "Najden je WIFI!";
				statusView = statusView.makeText(context, statusText, duration);
				statusView.show();

				switch (ni.getType()) {
				case ConnectivityManager.TYPE_WIFI:
					// smo v poceni tarifi � prena�aj dolge datoteke
					statusText = "Priključeni na WI_FI!";
					statusView = Toast.makeText(context, statusText, duration);
					statusView.show();
					break;
				case ConnectivityManager.TYPE_MOBILE:
					// smo v poceni tarifi � prenasaj dolge datoteke
					statusText = "Prikljuceni na mobilni internet!";
					statusView = Toast.makeText(context, statusText, duration);
					statusView.show();
					break;
				}
			}
		}
	}

	public void chooseBestProvider() {
		if (locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
			isGPSenabled = true;
			locationManager.requestLocationUpdates("gps", minTime, minDistance,
					locationListenerGPS);
			Log.d("LOCATION", "GPS listener registered to get updates");
			isListenerRegisteredGPS = true;
		} else
			isGPSenabled = false;

		if (locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER)) {
			isNETenabled = true;
			locationManager.requestLocationUpdates("network", minTime,
					minDistance, locationListenerNET);
			isListenerRegisteredNET = true;
		} else
			isNETenabled = false;

		if (isGPSenabled)
			provider = "gps";
		else if (isNETenabled)
			provider = "network";
		else
			provider = null;

		if (provider != null) {
			Log.d("LOCATION", "Provider " + provider + " has been selected.");
			Location location = locationManager.getLastKnownLocation(provider);
			if (location != null) {
				updateLocation(location);
			} else {
				Log.d("LOCATION", "Location not available.");
			}
		} else {
			Log.d("LOCATION", "Provider not available.");
		}
	}

	public void updateLocation(Location location) {
		counter++;
		String s = "";
		lat = location.getLatitude();
		lon = location.getLongitude();
		s = "lat:" + lat + "|lon:" + lon + " [" + counter + "] ";
		Log.d("LOCATION", "Current location:" + s);
	}

	/* Request updates at startup */
	@Override
	protected void onResume() {
		super.onResume();
		if (provider == "gps") {
			locationManager.requestLocationUpdates(provider, minTime,
					minDistance, locationListenerGPS);
			isListenerRegisteredGPS = true;
		} else if (provider == "network") {
			locationManager.requestLocationUpdates(provider, minTime,
					minDistance, locationListenerNET);
			isListenerRegisteredNET = true;
		}
	}

	/* Remove the locationlistener updates when Activity is paused */
	@Override
	public void onPause() {
		super.onPause();
		if (isListenerRegisteredGPS) {
			locationManager.removeUpdates(locationListenerGPS);
			isListenerRegisteredGPS = false;
		} else if (isListenerRegisteredNET) {
			locationManager.removeUpdates(locationListenerNET);
			isListenerRegisteredNET = false;
		}
	}

	@Override
	protected void onSaveInstanceState(Bundle outState) {
		// TODO Auto-generated method stub
		super.onSaveInstanceState(outState);
	}

	@Override
	protected void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
	}

	public void onClick(View v) {
		Intent i = new Intent(this, izpisActivity.class);

		i.putExtra("lat", lat);
		i.putExtra("lon", lon);
		startActivity(i);
	}

	@Override
	public void onItemClick(AdapterView<?> arg0, View arg1, int arg2, long id) {
		Intent intent = new Intent(this, izpisActivity.class);
		Pijaca pijaca = (Pijaca) lv.getAdapter().getItem((int) id);
		long id_pij = pijaca.getId_pij();
		String ime_p = pijaca.getIme_p();

		intent.putExtra("item", id);
		startActivity(intent);

	}

}
