package ss.proj.bar_app.db.helper;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

import ss.proj.bar_app.R;
import ss.proj.bar_app.db.tables.Cenik;
import ss.proj.bar_app.db.tables.Lokal;
import ss.proj.bar_app.db.tables.Pijaca;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class BazaHelper extends SQLiteOpenHelper {
	private Context context;

	// imena tabel
	public static final String TABLE_LOKAL = "lokal";
	public static final String TABLE_DELOVNIK = "delovnik";
	public static final String TABLE_POSTA = "posta";
	public static final String TABLE_PIJACA = "pijaca";
	public static final String TABLE_CENIK = "cenik";

	// atributi tabele POSTA
	public static final String COLUMN_IDP = "id_p"; // primary key
	public static final String COLUMN_MESTO = "mesto";

	// atributi tabele PIJACA
	public static final String COLUMN_IDPIJ = "id_pij"; // primary key
	public static final String COLUMN_IMEP = "ime_p";

	// atributi tabele LOKAL
	public static final String COLUMN_IDL = "id_l"; // primary key
	public static final String COLUMN_IMEL = "ime_l";
	public static final String COLUMN_NASLOV = "naslov_l";
	public static final String COLUMN_IDP_L = "id_p_l"; // foreign key na id_p v
														// tabele POSTA
	public static final String COLUMN_LONG = "lon";
	public static final String COLUMN_LAT = "lat";

	// atributi tabele DELOVNIK
	public static final String COLUMN_IDU = "id_u"; // primary key
	public static final String COLUMN_IDD_L = "id_l"; // foreign key na id_l v
														// tabele LOKAL
	public static final String COLUMN_DAN = "dan";
	public static final String COLUMN_URA = "ura";

	// atributi tabele CENIK
	public static final String COLUMN_IDC = "id_c"; // primary key
	public static final String COLUMN_IDL_C = "id_l"; // foreign key na id_l v
														// tabeli LOKAL
	public static final String COLUMN_IDPIJ_C = "id_pij"; // foreign key na
															// id_pij v tabeli
															// PIJACA
	public static final String COLUMN_ZNESEK = "znesek";

	private static final String DATABASE_NAME = "barrapp.db";
	private static final int DATABASE_VERSION = 1;

	// stavek za kreiranje baze
	public static final String DATABASE_CREATE_POSTA = "CREATE TABLE "
			+ TABLE_POSTA + " (" + COLUMN_IDP
			+ " INTEGER PRIMARY KEY AUTOINCREMENT, " + COLUMN_MESTO
			+ " TEXT NOT NULL);";

	public static final String DATABASE_CREATE_PIJACA = "CREATE TABLE "
			+ TABLE_PIJACA + " (" + COLUMN_IDPIJ
			+ " INTEGER PRIMARY KEY AUTOINCREMENT, " + COLUMN_IMEP
			+ " TEXT NOT NULL);";

	public static final String DATABASE_CREATE_LOKAL = "CREATE TABLE "
			+ TABLE_LOKAL + " (" + COLUMN_IDL
			+ " INTEGER PRIMARY KEY AUTOINCREMENT, " + COLUMN_IMEL
			+ " TEXT NOT NULL, " + COLUMN_NASLOV + " text not null, "
			+ COLUMN_IDP_L + " INTEGER, " + COLUMN_LONG + " REAL, "
			+ COLUMN_LAT + " REAL, " + "FOREIGN KEY(" + COLUMN_IDP_L
			+ ") REFERENCES " + TABLE_POSTA + "(" + COLUMN_IDP + "));";

	public static final String DATABASE_CREATE_DELOVNIK = "CREATE TABLE "
			+ TABLE_DELOVNIK + " (" + COLUMN_IDU
			+ " INTEGER PRIMARY KEY AUTOINCREMENT, " + COLUMN_IDD_L
			+ " INTEGER, " + COLUMN_DAN + " TEXT NOT NULL, " + COLUMN_URA
			+ " TEXT NOT NULL, " + "FOREIGN KEY(" + COLUMN_IDD_L
			+ ") REFERENCES " + TABLE_LOKAL + "(" + COLUMN_IDL + "));";

	public static final String DATABASE_CREATE_CENIK = "CREATE TABLE "
			+ TABLE_CENIK + " (" + COLUMN_IDC
			+ " INTEGER PRIMARY KEY AUTOINCREMENT, " + COLUMN_IDL_C
			+ " INTEGER, " + COLUMN_IDPIJ_C + " INTEGER, " + COLUMN_ZNESEK
			+ " REAL, " + "FOREIGN KEY(" + COLUMN_IDL_C + ") REFERENCES "
			+ TABLE_LOKAL + "(" + COLUMN_IDL + "), " + "FOREIGN KEY("
			+ COLUMN_IDPIJ_C + ") REFERENCES " + TABLE_PIJACA + "("
			+ COLUMN_IDPIJ + "));";

	public BazaHelper(Context context) {
		super(context, DATABASE_NAME, null, DATABASE_VERSION);
		this.context = context;
	}

	@Override
	public void onCreate(SQLiteDatabase db) {
		db.execSQL("PRAGMA foreign_keys=ON;");
		db.execSQL(DATABASE_CREATE_POSTA);
		db.execSQL(DATABASE_CREATE_PIJACA);
		db.execSQL(DATABASE_CREATE_LOKAL);
		db.execSQL(DATABASE_CREATE_DELOVNIK);
		db.execSQL(DATABASE_CREATE_CENIK);
		populateDatabase(db);
	}

	public void populateDatabase(SQLiteDatabase db) {
		try {
			InputStream raw = context.getResources()
					.openRawResource(R.raw.data);
			BufferedReader is = new BufferedReader(new InputStreamReader(raw,
					"UTF8"));

			String line = null;
			while ((line = is.readLine()) != null) {
				db.execSQL(line);
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public List<Pijaca> getAllDrinks() {
		List<Pijaca> allDrinks = new ArrayList<Pijaca>();

		SQLiteDatabase db = this.getReadableDatabase();
		Cursor cursor = db.query(TABLE_PIJACA, null, null, null, null, null,
				null);

		if (cursor.moveToFirst()) {
			int id_pijace = cursor.getColumnIndex(COLUMN_IDPIJ);
			int ime_pijace = cursor.getColumnIndex(COLUMN_IMEP);

			do {
				int id = cursor.getInt(id_pijace);
				String ime = cursor.getString(ime_pijace);

				allDrinks.add(new Pijaca(id, ime));
			} while (cursor.moveToNext());
		}

		cursor.close();

		return allDrinks;
	}

	public List<Lokal> getAllBars() {
		List<Lokal> allBars = new ArrayList<Lokal>();

		SQLiteDatabase db = this.getReadableDatabase();
		Cursor cursor = db.query(TABLE_LOKAL, null, null, null, null, null,
				null);

		if (cursor.moveToFirst()) {
			int id_lokala = cursor.getColumnIndex(COLUMN_IDL);
			int ime_lokala = cursor.getColumnIndex(COLUMN_IMEL);
			int naslov_lokala = cursor.getColumnIndex(COLUMN_NASLOV);
			int id_poste_lokala = cursor.getColumnIndex(COLUMN_IDP_L);
			int lon_lokala = cursor.getColumnIndex(COLUMN_LONG);
			int lat_lokala = cursor.getColumnIndex(COLUMN_LAT);

			do {
				int id = cursor.getInt(id_lokala);
				String ime = cursor.getString(ime_lokala);
				String naslov = cursor.getString(naslov_lokala);
				int id_poste = cursor.getInt(id_poste_lokala);
				double lon = cursor.getDouble(lon_lokala);
				double lat = cursor.getDouble(lat_lokala);

				allBars.add(new Lokal(id, ime, naslov, id_poste, lon, lat));
			} while (cursor.moveToNext());
		}

		cursor.close();

		return allBars;
	}

	public List<Cenik> getAllPrices() {
		List<Cenik> allPrices = new ArrayList<Cenik>();

		SQLiteDatabase db = this.getReadableDatabase();
		Cursor cursor = db.query(TABLE_CENIK, null, null, null, null, null,
				null);

		if (cursor.moveToNext()) {
			int id_cene = cursor.getColumnIndex(COLUMN_IDC);
			int id_lokala_cena = cursor.getColumnIndex(COLUMN_IDL_C);
			int id_pijaca_cena = cursor.getColumnIndex(COLUMN_IDPIJ_C);
			int znesek = cursor.getColumnIndex(COLUMN_ZNESEK);

			do {
				int id_c = cursor.getInt(id_cene);
				int id_l = cursor.getInt(id_lokala_cena);
				int id_pij = cursor.getInt(id_pijaca_cena);
				double z = cursor.getDouble(znesek);

				allPrices.add(new Cenik(id_c, id_l, id_pij, z));
			} while (cursor.moveToNext());
		}

		cursor.close();

		return allPrices;
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		if (oldVersion != newVersion) {
			db.execSQL("PRAGMA foreign_keys=OFF;");
			db.execSQL("DROP TABLE IF EXISTS " + TABLE_CENIK + ";");
			db.execSQL("DROP TABLE IF EXISTS " + TABLE_DELOVNIK + ";");
			db.execSQL("DROP TABLE IF EXISTS " + TABLE_LOKAL + ";");
			db.execSQL("DROP TABLE IF EXISTS " + TABLE_PIJACA + ";");
			db.execSQL("DROP TABLE IF EXISTS " + TABLE_POSTA + ";");
			db.execSQL("PRAGMA foreign_keys=ON;");
		}
		onCreate(db);
	}
}
