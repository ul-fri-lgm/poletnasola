package ss.proj.bar_app.db.tables;

public class Lokal {
	private long id_l;
	private String ime_l;
	private String naslov_l;
	private long id_p;
	private double lon;
	private double lat;

	public Lokal(long idl, String i, String n, long idp, double lon, double lat) {
		this.id_l = idl;
		this.ime_l = i;
		this.naslov_l = n;
		this.id_p = idp;
		this.lon = lon;
		this.lat = lat;
	}

	public long getId_l() {
		return id_l;
	}

	public void setId_l(long id_l) {
		this.id_l = id_l;
	}

	public String getIme_l() {
		return ime_l;
	}

	public void setIme_l(String ime_l) {
		this.ime_l = ime_l;
	}

	public String getNaslov_l() {
		return naslov_l;
	}

	public void setNaslov_l(String naslov_l) {
		this.naslov_l = naslov_l;
	}

	public long getId_p() {
		return id_p;
	}

	public void setId_p(long id_p) {
		this.id_p = id_p;
	}

	public double getLon() {
		return lon;
	}

	public void setLon(double lon) {
		this.lon = lon;
	}

	public double getLat() {
		return lat;
	}

	public void setLat(double lat) {
		this.lat = lat;
	}

}
