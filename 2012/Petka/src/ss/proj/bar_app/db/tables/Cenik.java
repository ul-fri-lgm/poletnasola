package ss.proj.bar_app.db.tables;

public class Cenik {
	private long id_c;
	private long id_l;
	private long id_pij;
	private double znesek;

	public Cenik(long idc, long idl, long idp, double z) {
		this.id_c = idc;
		this.id_l = idl;
		this.id_pij = idp;
		this.znesek = z;
	}

	public long getId_c() {
		return id_c;
	}

	public void setId_c(long id_c) {
		this.id_c = id_c;
	}

	public long getId_l() {
		return id_l;
	}

	public void setId_l(long id_l) {
		this.id_l = id_l;
	}

	public long getId_pij() {
		return id_pij;
	}

	public void setId_pij(long id_pij) {
		this.id_pij = id_pij;
	}

	public double getZnesek() {
		return znesek;
	}

	public void setZnesek(double znesek) {
		this.znesek = znesek;
	}
}
