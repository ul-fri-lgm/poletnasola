package ss.proj.bar_app;

import java.util.List;

import ss.proj.bar_app.db.tables.Lokal;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

public class LokalListAdapter extends BaseAdapter {

	private List<Lokal> allBars;
	private LayoutInflater mInflater;

	public LokalListAdapter(Context context) {
		allBars = ((BarApplication) context).getBazaHelper().getAllBars();
		mInflater = (LayoutInflater) context
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	}

	@Override
	public int getCount() {
		return allBars.size();
	}

	@Override
	public Lokal getItem(int i) {
		return allBars.get(i);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		ViewHolder vh;

		if (convertView == null) {
			convertView = mInflater.inflate(R.layout.list_item, null);

			vh = new ViewHolder();
			vh.label1 = (TextView) convertView.findViewById(R.id.textView1);

			convertView.setTag(vh);
		} else {
			vh = (ViewHolder) convertView.getTag();
		}

		vh.label1.setText(allBars.get(position).getIme_l());

		return convertView;
	}

	static class ViewHolder {
		TextView label1; // ime lokala
	}
}
