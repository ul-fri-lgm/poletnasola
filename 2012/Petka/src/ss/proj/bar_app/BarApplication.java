package ss.proj.bar_app;

import ss.proj.bar_app.db.helper.BazaHelper;
import android.app.Application;

public class BarApplication extends Application {

	private BazaHelper bazaHelper;

	@Override
	public void onCreate() {
		super.onCreate();

		bazaHelper = new BazaHelper(this);
		bazaHelper.getAllDrinks();
		bazaHelper.getAllBars();
	}

	public BazaHelper getBazaHelper() {
		return bazaHelper;
	}

	public void setBazaHelper(BazaHelper bazaHelper) {
		this.bazaHelper = bazaHelper;
	}
}
