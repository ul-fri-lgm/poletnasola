package summerschool.mywallet;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

public class AddExpense extends Activity {

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.add_expense);

		Button ok = (Button) findViewById(R.id.buttonOK);
		ok.setOnClickListener(handleOKButton);

		Button cancel = (Button) findViewById(R.id.buttonCancel);
		cancel.setOnClickListener(handleCancelButton);

		
		//tags to choose
		List<String> list = new ArrayList<String>();
		
		DatabaseHandler db = new DatabaseHandler(this);
		list = db.getAllTags();

		MultiSpinner multiSpinner = (MultiSpinner) findViewById(R.id.multi_spinner); 
		multiSpinner.setItems(list, "Select tag", null);
		
		
		//set user
		SharedPreferences sharedPreferences = this.getSharedPreferences("CURRENT_USER", MODE_PRIVATE);
		String currentUser = sharedPreferences.getString("CURRENT_USER", "Default user");
		
		TextView textCurrentUser = (TextView)findViewById(R.id.user);
		textCurrentUser.setText("User: " + currentUser);
		
		/*
		// set color of text
		TextView totalAllTime_textView = (TextView) findViewById(R.id.TextViewTitle);
		totalAllTime_textView.setTextColor(Color.RED);
		*/
	}

	
	//ok button clicked
	View.OnClickListener handleOKButton = new View.OnClickListener() {
		public void onClick(View v) {
			
			//get data from multiSpinner
			MultiSpinner multiSpinner = (MultiSpinner) findViewById(R.id.multi_spinner);
			ArrayList<Object> spinnerResult = multiSpinner.getSelected();
			
			List<String> items = (List<String>) spinnerResult.get(0);
			boolean[] selected = (boolean[]) spinnerResult.get(1);
			String tagsValue = "";
			int j=0;
			
			for (int i = 0; i < items.size(); i++) {
				if (selected[i] == true) {
					if(j==0) {
						tagsValue = items.get(i);
						j++;
					}
					else {
						tagsValue = tagsValue + "," + items.get(i);
					}
				}
			}

			
			//get other data
			EditText amount = (EditText) findViewById(R.id.amount);
			DatePicker dp = (DatePicker) findViewById(R.id.datePicker);
			
			String amountValue = amount.getText().toString();
			String strDateTime = (dp.getMonth() + 1) + "/" + dp.getDayOfMonth() + "/" + dp.getYear();
			
			//user
			SharedPreferences sharedPreferences = AddExpense.this.getSharedPreferences("CURRENT_USER", MODE_PRIVATE);
			String userValue = sharedPreferences.getString("CURRENT_USER", "Default_user");
			
			
			//control blank fields
			if(amountValue.equals("")) {
				Toast.makeText(getApplicationContext(), "Fill out the field 'Amount'!", Toast.LENGTH_LONG).show();
				return;
			}
			
			if(tagsValue.equals("")) {
				Toast.makeText(getApplicationContext(), "Choose at least one tag!", Toast.LENGTH_LONG).show();
				return;
			}
			
			String response = connect("https://docs.google.com/spreadsheet/formResponse?formkey=dF80VW5pdmlDTml4Sk9PNWdkR3BxVUE6MQ&ifq&entry.0.single="
					+ amountValue
					+ "&entry.4.single="
					+ tagsValue
					+ "&entry.6.single="
					+ userValue
					+ "&entry.5.single="
					+ strDateTime
					+ "&submit=Submit");
			
			
			//update stats
			//Stats statistics = new Stats();
			//statistics.refreshAndShowStats();
			
			
			/*
			//send response
			String response = connect("https://docs.google.com/spreadsheet/formResponse?formkey=dHZ3d3E5Z0g2Zkh6RnFBdGVCUjBVNUE6MQ&entry.0.single="
					+ amountValue
					+ "&entry.1.single="
					+ tagsValue
					+ "&entry.2.single="
					+ strDateTime
					+ "&entry.3.single="
					+ userValue
					+ "&submit=Submit");
			
			*/
			Intent i = new Intent(AddExpense.this, MainActivity.class);
			startActivity(i);
			finish();
			
		}
	};

	
	//cancel button clicked
	View.OnClickListener handleCancelButton = new View.OnClickListener() {
		public void onClick(View arg0) {
			finish();
		}
	};

	
	public static String connect(String url) {

		HttpClient httpclient = new DefaultHttpClient();

		// Prepare a request object
		// HttpGet httpget = new HttpGet(url);
		HttpPost httppost = new HttpPost(url);
		String result = "ERROR";

		// Execute the request
		HttpResponse response;
		try {
			// response = httpclient.execute(httpget);
			response = httpclient.execute(httppost);

			// Examine the response status
			Log.i("Praeda", "---" + response.getStatusLine().toString() + "---");

			// Get hold of the response entity
			HttpEntity entity = response.getEntity();
			// If the response does not enclose an entity, there is no need
			// to worry about connection release

			if (entity != null) {

				// A Simple JSON Response Read
				InputStream instream = entity.getContent();
				result = convertStreamToString(instream);
				// now you have the string representation of the HTML request
				instream.close();
			}

		} catch (Exception e) {
		}

		return result;
	}

	
	private static String convertStreamToString(InputStream is) {
		/*
		 * To convert the InputStream to String we use the
		 * BufferedReader.readLine() method. We iterate until the BufferedReader
		 * return null which means there's no more data to read. Each line will
		 * appended to a StringBuilder and returned as String.
		 */
		BufferedReader reader = new BufferedReader(new InputStreamReader(is));
		StringBuilder sb = new StringBuilder();

		String line = null;
		try {
			while ((line = reader.readLine()) != null) {
				sb.append(line + "\n");
			}
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			try {
				is.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		return sb.toString();
	}

}
