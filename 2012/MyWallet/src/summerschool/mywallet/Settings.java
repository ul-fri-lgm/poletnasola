package summerschool.mywallet;

import android.app.Activity;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class Settings extends Activity {

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.settings);

		Button changeUser_button = (Button) findViewById(R.id.button_change_user);
		changeUser_button.setOnClickListener(handleChangeUserButton);

		Button url_button = (Button) findViewById(R.id.button_url);
		url_button.setOnClickListener(handleUrlButton);

		Button cancel_button = (Button) findViewById(R.id.buttonCancel);
		cancel_button.setOnClickListener(handleCancelButton);
		
		
		//shared preferences for current user and url
		SharedPreferences sharedPreferences = this.getSharedPreferences("CURRENT_USER", MODE_PRIVATE);
		String currentUser = sharedPreferences.getString("CURRENT_USER", "Default user");
		String url = sharedPreferences.getString("URL", "Your url is not set!");
		
		TextView textCurrentUser = (TextView)findViewById(R.id.currentUser_value);
		textCurrentUser.setText(currentUser);
		
		TextView textUrl = (TextView)findViewById(R.id.url_value);
		textUrl.setText(url);

	}

	// user button clicked
	View.OnClickListener handleChangeUserButton = new View.OnClickListener() {
		public void onClick(View arg0) {
			
			SharedPreferences sharedPreferences = Settings.this.getSharedPreferences("CURRENT_USER", MODE_PRIVATE);
			SharedPreferences.Editor editor = sharedPreferences.edit();
			
			EditText newCurrentUser = (EditText) findViewById(R.id.text_change_user);
			String newCurrentUserValue = newCurrentUser.getText().toString();
			newCurrentUser.setText("");
			
			if(newCurrentUserValue.equals("")) {
				Toast.makeText(getApplicationContext(), "First enter new user, than click button!", Toast.LENGTH_LONG).show();
			}
			else {
				editor.putString("CURRENT_USER", newCurrentUserValue);
				editor.commit();
				
				//change text
				TextView textCurrentUser = (TextView)findViewById(R.id.currentUser_value);
				textCurrentUser.setText(newCurrentUserValue);
			}
			
		}
	};

	// url button clicked
	View.OnClickListener handleUrlButton = new View.OnClickListener() {
		public void onClick(View arg0) {
			
			SharedPreferences sharedPreferences = getPreferences(MODE_PRIVATE);
			SharedPreferences.Editor editor = sharedPreferences.edit();
			
			EditText newUrl = (EditText) findViewById(R.id.text_url);
			String newUrlValue = newUrl.getText().toString();
			newUrl.setText("");
			
			if(newUrlValue.equals("")) {
				Toast.makeText(getApplicationContext(), "First enter new URL, than click button!", Toast.LENGTH_LONG).show();
			}
			else {
				editor.putString("URL", newUrlValue);
				editor.commit();
				
				//change text
				TextView textUrl = (TextView)findViewById(R.id.url_value);
				textUrl.setText(newUrlValue);
			}
			
		}
	};

	// cancel button clicked
	View.OnClickListener handleCancelButton = new View.OnClickListener() {
		public void onClick(View arg0) {
			finish();
		}
	};

}
