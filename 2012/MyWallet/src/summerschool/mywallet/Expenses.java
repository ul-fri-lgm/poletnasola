package summerschool.mywallet;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.text.Format;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;
import java.util.StringTokenizer;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;

public class Expenses extends Activity implements OnClickListener {

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.expenses);

		// BUTTON
		Button b = (Button) findViewById(R.id.button1);
		b.setOnClickListener(this);

		naloziPodatke();
	}

	@Override
	public void onClick(View v) {
		Intent i = new Intent(this, AddExpense.class);
		startActivity(i);
		//finish();
	}

	public void naloziPodatke() {
		//ArrayList<String> rez = najdiZadnjiDeset();

		ArrayList<String> rez = new ArrayList<String>();
		
		rez.add("45.55€ - mercator - 13.7.2012");
		rez.add("50.00€ - bencin - 13.7.2012");
		rez.add("250.00€ - pocitnice - 11.7.2012");
		rez.add("3.07€- tuš - 10.7.2012");
		rez.add("25.90€ - spar - 10.7.2012");
		rez.add("85.10€ - oblačila - 7.7.2012");
		rez.add("7.80€ - kosilo - 5.7.2012");
		rez.add("8.53€ - kosilo - 4.7.2012");
		rez.add("15.66€ - večerja - 3.7.2012");
		rez.add("35.85€ - doma - 1.7.2012");
		rez.add("21.17€ - tus - 26.6.2012");
		rez.add("12.63€ - Mercator - 25.6.2012");
		rez.add("60.00€ - bencin - 25.6.2012");
		ListView lvSeznam = (ListView) findViewById(R.id.listView_expenses);
		ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
				android.R.layout.simple_list_item_1, rez);
		// Assign adapter to ListView
		lvSeznam.setAdapter(adapter);
	}

	public ArrayList<String> najdiZadnjiDeset() {
		String[] vrstice = new String[10];
		int zadnji = 0;
		StringBuffer buf = new StringBuffer();
		ArrayList<String> rez = new ArrayList<String>();
		Locale locale = Locale.GERMAN;
		Format formatter = new SimpleDateFormat("d.M.yy"); // dan.mesec.leto

		try {
			// za moj spreadsheet na gmail
			URL page = new URL("https://docs.google.com/spreadsheet/pub?key=0AiB9Zm9QGIZHdHZ3d3E5Z0g2Zkh6RnFBdGVCUjBVNUE&output=csv");

			String line;
			URLConnection conn = page.openConnection();
			conn.connect();
			InputStreamReader in = new InputStreamReader(conn.getInputStream());
			BufferedReader data = new BufferedReader(in);

			// prve vrstice ne beremo, ker je naslovna
			line = data.readLine();
			while ((line = data.readLine()) != null) {
				zadnji++;
				if (zadnji == 10)
					zadnji = 0;

				vrstice[zadnji] = line;
			}

			// recimo, da je zadnji 5, potem bere 6,7,8,9,0,1,2,3,4,5
			for (int i = zadnji + 1; i < zadnji + 11; i++) {
				StringTokenizer tokens = new StringTokenizer(vrstice[i % 10],
						",");
				String[] fields = new String[6];
				for (int j = 0; (j < fields.length && tokens.hasMoreTokens()); j++) {
					fields[j] = stripQuotes(tokens.nextToken()); // bri�e " iz
																	// stringa
				}
				// fields[1] 12,30
				fields[1] = NumberFormat.getNumberInstance(locale).format(
						Float.valueOf(fields[1]));
				// fields[3] d.M.yyy
				fields[3] = formatter.format(Date.parse(fields[3]));
				rez.add((i - 1 - zadnji) + ":" + fields[1] + "�," + fields[2]
						+ "," + fields[3]);
			}
			data.close();
		} catch (MalformedURLException mue) {
			Log.d("Napaka", "\nSlab URL!" + mue.getMessage());
		} catch (IOException e) {
			Log.d("Napaka", "IO Napaka: " + e.getMessage());
		}

		//unregisterReceiver(internetStatusReceiver);
		return rez;
	}

	private String stripQuotes(String input) {
		StringBuffer output = new StringBuffer();
		for (int i = 0; i < input.length(); i++)
			if (input.charAt(i) != '\"')
				output.append(input.charAt(i));
		return output.toString();
	}

}
