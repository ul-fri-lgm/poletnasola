package summerschool.mywallet;

import java.util.ArrayList;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class DatabaseHandler extends SQLiteOpenHelper {

	private static final int DATABASE_VERSION = 1;
	private static final String DATABASE_NAME = "database_mywallet.db";
	
	private static final String TABLE_TAGS = "Tags";
    private static final String KEY_ID = "tag_id"; 
    private static final String KEY_TAG = "tag_name"; 
  
    
    
    public DatabaseHandler(Context context) { 
        super(context, DATABASE_NAME, null, DATABASE_VERSION); 
    } 
  
    // Creating Tables 
    @Override
    public void onCreate(SQLiteDatabase db) { 
        String CREATE_TAGS_TABLE = "CREATE TABLE IF NOT EXISTS " + TABLE_TAGS + "("+ KEY_ID +" INTEGER PRIMARY KEY,"+ KEY_TAG +" VARCHAR)";
        db.execSQL(CREATE_TAGS_TABLE);
        
        
        //add some predefined values for tags
        ContentValues values = new ContentValues();
        values.put(KEY_TAG, "Clothes");
        db.insert(TABLE_TAGS, null, values);
        
        values.put(KEY_TAG, "Gas");
        db.insert(TABLE_TAGS, null, values);
        
        values.put(KEY_TAG, "Car");
        db.insert(TABLE_TAGS, null, values);
        
        values.put(KEY_TAG, "Food");
        db.insert(TABLE_TAGS, null, values);
        
        //db.close();
    } 
  
    // Upgrading database 
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) { 
        // Drop older table if existed 
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_TAGS); 
  
        // Create tables again 
        onCreate(db);
    } 
  
    /** 
     * All CRUD(Create, Read, Update, Delete) Operations 
     */

    // Adding new Tag
    void insertTag(String tag) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(KEY_TAG, tag);

        // Inserting Row
        db.insert(TABLE_TAGS, null, values);
        db.close();
    }

    
    // Getting Tag
    public ArrayList<String> getAllTags() {
    	ArrayList<String> values = new ArrayList<String>();
    	
    	String selectQuery = "SELECT * FROM " + TABLE_TAGS;
    	
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        
        if (cursor.moveToFirst()) {
            do {
                String tag_name = cursor.getString(1); //get tag_name
                values.add(tag_name);
            } while (cursor.moveToNext());
        }
        
        cursor.close();
        
        return values;
    }
    
    
    // Delete single tag
    public void deleteTag(String tag) {
    	
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(TABLE_TAGS, KEY_TAG + " = ?", new String[] { tag });
        db.close();
    }
    
    
    
    /*
    // Updating single contact 
    public int updateContact(Contact contact) { 
        SQLiteDatabase db = this.getWritableDatabase(); 
  
        ContentValues values = new ContentValues(); 
        values.put(KEY_NAME, contact.getName()); 
        values.put(KEY_PH_NO, contact.getPhoneNumber()); 
  
        // updating row 
        return db.update(TABLE_CONTACTS, values, KEY_ID + " = ?", 
                new String[] { String.valueOf(contact.getID()) }); 
    } 
  
    
  
    // Getting contacts Count 
    public int getContactsCount() { 
        String countQuery = "SELECT  * FROM " + TABLE_CONTACTS; 
        SQLiteDatabase db = this.getReadableDatabase(); 
        Cursor cursor = db.rawQuery(countQuery, null); 
        cursor.close(); 
  
        // return count 
        return cursor.getCount(); 
    }
	*/
}
