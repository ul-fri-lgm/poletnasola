package summerschool.mywallet;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.StringTokenizer;

import android.app.Activity;
import android.graphics.Color;
import android.os.Bundle;
import android.text.Layout;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

public class Stats extends Activity {

	@Override
	public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.stats);
        
        
        //set color of text
        TextView totalAllTime_textView = (TextView) findViewById(R.id.totalAllTime);
        totalAllTime_textView.setTextColor(Color.RED);
        
        TextView totalThisMonth_textView = (TextView) findViewById(R.id.totalThisMonth);
        totalThisMonth_textView.setTextColor(Color.YELLOW);
        
        TextView totalThisWeek_textView = (TextView) findViewById(R.id.totalThisWeek);
        totalThisWeek_textView.setTextColor(Color.GREEN);
        
        
        refreshAndShowStats();
    }
	
	
	public void refreshAndShowStats() {
		
		//set values
        TextView totalAllTime = (TextView) findViewById(R.id.totalAllTime_value);
        TextView totalThisMonth = (TextView) findViewById(R.id.totalThisMonth_value);
        TextView totalThisWeek = (TextView) findViewById(R.id.totalThisWeek_value);
        
        
        try {
        	URL page=new URL("https://docs.google.com/spreadsheet/pub?key=0Ap9FOE5Q1Yn8dF80VW5pdmlDTml4Sk9PNWdkR3BxVUE&output=csv");
        	//URL page=new URL("https://docs.google.com/spreadsheet/pub?key=0AiB9Zm9QGIZHdHZ3d3E5Z0g2Zkh6RnFBdGVCUjBVNUE&output=csv");

        	URLConnection conn = page.openConnection();
	        conn.connect();
	        
	        InputStreamReader in = new InputStreamReader(conn.getInputStream());
	        BufferedReader data = new BufferedReader(in);
	        
	        String line = data.readLine();
	
	        StringTokenizer tokens = new StringTokenizer(line,",");
	        String[] fields = new String[11];
	        
	        for (int i=0;(i<fields.length&&tokens.hasMoreTokens());i++) {
	            fields[i]=stripQuotes(tokens.nextToken());  // bri�e " iz stringa
	        }
	        
	        
	        String totalAllTime_value = fields[6] + " €";
	        String totalThisMonth_value = fields[7] + " €";
	        String totalThisWeek_value = fields[8] + " €";
	        
	        totalAllTime.setText(totalAllTime_value);
	        totalThisMonth.setText(totalThisMonth_value);
	        totalThisWeek.setText(totalThisWeek_value);
	        
        }
        catch (MalformedURLException mue) {
        	Log.d("Napaka","\nSlab URL!"+mue.getMessage());
        }
        catch (IOException e) {
        	Log.d("Napaka","IO Napaka: "+e.getMessage());
        }
        
	}
	
	
	private String stripQuotes(String input) {
        StringBuffer output=new StringBuffer();
        
	    for (int i=0;i<input.length();i++)
	     if (input.charAt(i)!='\"')
	       output.append(input.charAt(i));
    
	    return output.toString();
	}
	
}



