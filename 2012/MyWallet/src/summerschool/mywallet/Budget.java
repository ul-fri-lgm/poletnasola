package summerschool.mywallet;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;

public class Budget extends Activity implements OnClickListener {
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.budget);
        
        
        //BUTTON
        Button b = (Button) findViewById(R.id.budget_button1);
        b.setOnClickListener(this);
    }
	
	
	@Override
	public void onClick(View v) {
    	Intent i = new Intent(this, AddBudget.class);
		startActivity(i);
		//finish();
	}
	
}
