package summerschool.mywallet;

import java.util.ArrayList;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.view.ContextMenu;
import android.view.ContextMenu.ContextMenuInfo;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.AdapterContextMenuInfo;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;

public class Tags extends Activity implements OnClickListener {

	
	@Override
	public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.tags);
        
        Button addTag = (Button) findViewById(R.id.button_add_tag);
        addTag.setOnClickListener(this);
        
        showListView_tags();
    }
	
	
	//INSERT TAG
	@Override
	public void onClick(View v) {
		
		EditText newTag = (EditText) findViewById(R.id.text_add_tag);
		String newTagValue = newTag.getText().toString();
		
		if(!newTagValue.equals("")) {
			DatabaseHandler db = new DatabaseHandler(this);
			db.insertTag(newTagValue);
			
			newTag.setText("");
			
			//update list
			showListView_tags();
		}
		
	}
	
	
	//CONTEXT MENU - INFLATE MENU
	@Override
	public void onCreateContextMenu(ContextMenu menu, View v, ContextMenuInfo menuInfo) {
		if (v.getId()==R.id.listView_tags) {
			
			//get selected word
			AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) menuInfo;
			String selectedWord = ((TextView) info.targetView).getText().toString();
			
			menu.setHeaderTitle("Delete '"+ selectedWord +"'?");
			
			MenuInflater inflater = getMenuInflater();
			inflater.inflate(R.menu.delete_tag, menu);
		}
	}
	
	
	//CONTEXT MENU ACTION - DELETE TAG OR CANCEL
	@Override
	public boolean onContextItemSelected(MenuItem item) {
		AdapterContextMenuInfo info = (AdapterContextMenuInfo) item.getMenuInfo(); 
	    String tag = ((TextView) info.targetView).getText().toString();
		
	    switch (item.getItemId()) {
		    case R.id.menu_delete_tag:
		    	DatabaseHandler db = new DatabaseHandler(this);
		    	db.deleteTag(tag);
		    	showListView_tags();
		        return true;
		    default:
		        return super.onContextItemSelected(item);
	    }
	}
	
	
	//SHOW LIST VIEW WITH TAGS
	public void showListView_tags() {
		
		DatabaseHandler db = new DatabaseHandler(this);
		try {
        	ArrayList<String> values = db.getAllTags();
        	int valuesSize = values.size();
        	if(valuesSize == 0) {
        		values.add("No tags yet!");
        	}
        	
        	ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, android.R.id.text1, values);
        	
        	ListView listView = (ListView) findViewById(R.id.listView_tags);
        	listView.setAdapter(adapter);
        	
        	if(valuesSize != 0) {
        		registerForContextMenu(listView);
        	}
        }
        catch (Exception e) {
        	Log.i("ReadNWrite, readFile()", "Exception e = " + e);
        }
	}
	
}
