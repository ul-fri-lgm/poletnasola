package summerschool.mywallet;

import android.os.Bundle;
import android.app.Activity;
import android.app.ActivityGroup;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.graphics.Color;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.TabHost;
import android.widget.TabHost.TabSpec;
import android.widget.Toast;

public class MainActivity extends ActivityGroup {

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		// TABS
		TabHost tabHost = (TabHost) findViewById(R.id.tabhost);
		tabHost.setup(this.getLocalActivityManager());
		tabHost.getTabWidget().setBackgroundColor(Color.BLACK);

		TabSpec expenses = tabHost.newTabSpec("expenses");
		expenses.setIndicator("Expenses",
				getResources().getDrawable(R.drawable.dollar_sign_red_48));
		Intent expensesIntent = new Intent(this, Expenses.class);
		expenses.setContent(expensesIntent);

		TabSpec budget = tabHost.newTabSpec("budget");
		budget.setIndicator("Budget",
				getResources().getDrawable(R.drawable.dollar_sign_green_48));
		Intent budgetIntent = new Intent(this, Budget.class);
		budget.setContent(budgetIntent);

		TabSpec tags = tabHost.newTabSpec("tags");
		tags.setIndicator("Tags",
				getResources().getDrawable(R.drawable.tag_blue_48));
		Intent tagsIntent = new Intent(this, Tags.class);
		tags.setContent(tagsIntent);

		TabSpec stats = tabHost.newTabSpec("stats");
		stats.setIndicator("Stats",
				getResources().getDrawable(R.drawable.stats_48));
		Intent statsIntent = new Intent(this, Stats.class);
		stats.setContent(statsIntent);

		tabHost.addTab(expenses);
		tabHost.addTab(budget);
		tabHost.addTab(tags);
		tabHost.addTab(stats);

		// disable screen rotating
		setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.activity_main, menu);

		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case R.id.menu_settings:
			Intent settings = new Intent(this, Settings.class);
			startActivity(settings);
			break;
		case R.id.menu_exit:
			System.exit(0);
			break;
		}

		return true;
	}

}
