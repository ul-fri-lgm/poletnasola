package fridroid.krizankaview;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.util.Log;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.view.inputmethod.InputMethodManager;

public class KrizankaView extends View implements OnTouchListener
{
	
	Paint paint = new Paint();
	int velikostVnosnegaPolja;
	int odmikOdRoba;
	Polje izbranoPolje = new Polje();
	Boolean touchEvent = false;
	int visina;
	int sirina;
	Boolean keyboardVisible = false;
	
	public KrizankaView(Context context)
	{
		super(context);
		this.setOnTouchListener(this);
	}
	
	@Override
	protected void onDraw(Canvas canvas)
	{
		//preberemo velikost krizanke iz objekta krizanka
		visina = 20;
		sirina = 20;
		
		//dobimo dpi napreve, vecji kot je dpi vecje moramo risati
		float dpiNaprave = getResources().getDisplayMetrics().density;
		
		//glede na dpi dolocimo velikost vnosnega polja
		velikostVnosnegaPolja = (int)(15*dpiNaprave);
		
		//dobimo resolucijo naprave na x osi
		int resNaprave = getResources().getDisplayMetrics().widthPixels;
		odmikOdRoba = (int)(resNaprave-sirina*velikostVnosnegaPolja)/2;
			
		//narisemo polje	
		for (int i=0;i<sirina;i++)
		{
			
			for (int j=0;j<visina;j++)
			{
				if (i%2==0)
				{
					if (j%2==0)
						paint.setColor(Color.BLACK);
					else
						paint.setColor(Color.WHITE);
				}
				else
				{
					if (j%2==0)
						paint.setColor(Color.WHITE);
					else
						paint.setColor(Color.BLACK);
				}
				canvas.drawRect(odmikOdRoba+i*velikostVnosnegaPolja, odmikOdRoba+j*velikostVnosnegaPolja, odmikOdRoba+i*velikostVnosnegaPolja+velikostVnosnegaPolja, odmikOdRoba+j*velikostVnosnegaPolja+velikostVnosnegaPolja, paint);
			}
		}
		
		//narisemo kvadrat izbranega polja
		paint.setColor(Color.GREEN);
		if (touchEvent)	
		{
			Polje tocka = vrniNajblizjePolje(izbranoPolje);
			canvas.drawRect(tocka.x,tocka.y,tocka.x+velikostVnosnegaPolja,tocka.y+velikostVnosnegaPolja,paint);		
			touchEvent = false;
		}
	}
	
	private Polje vrniNajblizjePolje(Polje izbranoPolje)
	{
		//vrne koordinate kvadrata ki je najbljizje pritisku
		Polje tocka = new Polje();
		for (int i=0;i<sirina;i++)
		{
			for(int j=0;j<visina;j++)
			{
				if ((izbranoPolje.x>=odmikOdRoba+i*velikostVnosnegaPolja)&&(izbranoPolje.x<=odmikOdRoba+i*velikostVnosnegaPolja+velikostVnosnegaPolja))
				{
					if ((izbranoPolje.y>= odmikOdRoba+j*velikostVnosnegaPolja)&&(izbranoPolje.y<=odmikOdRoba+j*velikostVnosnegaPolja+velikostVnosnegaPolja))
					{
						tocka.x = odmikOdRoba+i*velikostVnosnegaPolja;
						tocka.y = odmikOdRoba+j*velikostVnosnegaPolja;
					}
				}
			}
		}
		return tocka;
	}
	
	public boolean onTouch(View view, MotionEvent event) 
	{	
		//Toast.makeText(this.getContext(), "TOUCH", 200).show();
		this.invalidate();
		
		//dobimo koordinate dotika
		izbranoPolje.x = (int)event.getX();
		izbranoPolje.y = (int)event.getY();
		touchEvent = true;
		
		//prikaz tipkovnice
		InputMethodManager IMM = (InputMethodManager)view.getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
		if (event.getAction() == MotionEvent.ACTION_DOWN)
		{	
			IMM.toggleSoftInput(0, 0);
			view.requestFocus();
		}
		Log.d("mytag","test");
		return true;	
	}
	
	//@Override
	/*public boolean dispatchKeyEventPreIme(KeyEvent event) 
	{
		Log.d("mytag","key");
		return true;
	}*/
	
	public boolean onKeyPreIme(int keyCode, KeyEvent event)
	{
		Log.d("mytag","key");
		return true;
	}
	

	
}


class Polje
{
	int x;
	int y;
}


