package poletnasola.financnik;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;

import android.content.Context;
import android.util.Log;

public class PostToGD implements Runnable {

	float znesek;
	String kategorija, prihodekOdhodek, datum;
	String url = "https://docs.google.com/spreadsheet/formResponse?formkey="+NastavitveShranjevanje.googleDocsFormId;
	Context ctx;
	
	public PostToGD(float znesek, String kategorija, String datum){
		this.znesek = znesek;
		this.kategorija = kategorija;
		this.datum = datum;
	}
	
	public PostToGD(float znesek, String kategorija, String vrstaKategorije, String datum, Context ctx){
		this.znesek = znesek;
		this.kategorija = kategorija;
		this.prihodekOdhodek = vrstaKategorije;
		this.datum = datum;
		this.ctx = ctx;
	}
	
	PbazaHelper baza = new PbazaHelper(ctx);
	
	//@Override
	public void run() {
		// TODO Auto-generated method stub
		HttpClient httpClient = new DefaultHttpClient();
		HttpPost httpPost = new HttpPost(url);

		List<BasicNameValuePair> post = new ArrayList<BasicNameValuePair>();
		// datum
		post.add(new BasicNameValuePair("entry.0.single", datum));
		// odhodek/prihodek
		post.add(new BasicNameValuePair("entry.1.single", prihodekOdhodek));
		// znesek
		post.add(new BasicNameValuePair("entry.2.single", Float.toString(znesek)));
		// kategorija
		post.add(new BasicNameValuePair("entry.3.single", kategorija));
		

		try {
			httpPost.setEntity(new UrlEncodedFormEntity(post));
		} catch (UnsupportedEncodingException e) {
			Log.e("POST", "UnsupportedEncodingException e");
		}

		try {
			httpClient.execute(httpPost);
		} catch (Exception e) {
			Log.e("POST", "ClientProtocolException e");
			//shrani v bazo		
			baza.dodajVrsticoZaSinhGd(datum, prihodekOdhodek, Float.toString(znesek), kategorija);
		} 
		
	}
}
