package poletnasola.financnik;

import java.util.ArrayList;
import java.util.HashMap;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class PbazaHelper extends SQLiteOpenHelper {
	
	// Ce se verzija spremeni se poklice onUpgrade
	static final int dbVerzija = 1;
	
	// Ime podatkovne baze
	static final String dbName = "financnikDB";

	// Tabela vrsta
	static final String tabelaVrsta = "vrsta";
	static final String colVrstaId = "vrstaId";
	static final String colVrstaIme = "vrstaIme";
	
	// Tabela kategorij
	static final String tabelaKategorij = "kategorije";
	static final String colKategorijId = "kategorijaId";
	static final String colKategorijIme = "kategorijaIme";
	static final String colKategorijVrstaId = "vrstaId";
	
	// Tabela odhodki/prihodki
	static final String tabelaOdhodkiPrihodki = "odhodkiPrihodki";
	static final String colOdhodkiPrihodkiId = "id";
	static final String colOdhodkiPrihodkiZnesek = "znesek";
	static final String colOdhodkiPrihodkiDatum = "datum";
	static final String colOdhodkiPrihodkiKategorijaId = "kategorijaId";
	
	// Tabela za se ne sinhronizerane podatke z Google Docs
	static final String tabelaSinhGd = "sinhronizacijaGd";
	static final String colSinhGdId = "id";
	static final String colSinhGdDatum = "datum";
	static final String colSinhGdOdhodekPrihodek = "odhodekPrihodek";
	static final String colSinhGdZnesek = "znesek";
	static final String colSinhGdKategorijaIme = "kategorijaIme";

	
	public PbazaHelper(Context context) {
		super(context, dbName, null, dbVerzija);
	}

	
	@Override
	public void onCreate(SQLiteDatabase db) {
		// Ustvari tabelo vrsta
		db.execSQL("CREATE TABLE " + tabelaVrsta + " (" + colVrstaId + " INTEGER PRIMARY KEY, " + colVrstaIme + " TEXT)");
		
		// Vstavi vrsti
		ContentValues cv;
		
		cv = new ContentValues();
		cv.put(colVrstaId, 1);
		cv.put(colVrstaIme, "Odhodki");
		db.insert(tabelaVrsta, null, cv);
		
		cv = new ContentValues();
		cv.put(colVrstaId, 2);
		cv.put(colVrstaIme, "Prihodki");
		db.insert(tabelaVrsta, null, cv);
		
		// Ustvari tabelo kategorij
		db.execSQL("CREATE TABLE " + tabelaKategorij + " (" + colKategorijId + " INTEGER PRIMARY KEY AUTOINCREMENT, " + colKategorijIme + " TEXT, " + colKategorijVrstaId + " INTEGER)");
		
		// Vstavi privzeti kategoriji.
		cv = new ContentValues();
		cv.put(colKategorijId, 1);
		cv.put(colKategorijIme, "Vse ostalo");
		cv.put(colKategorijVrstaId, Financnik.vrstaKategorijeOdhodkiId);
		db.insert(tabelaKategorij, null, cv);
		
		cv = new ContentValues();
		cv.put(colKategorijId, 2);
		cv.put(colKategorijIme, "Vse ostalo");
		cv.put(colKategorijVrstaId, Financnik.vrstaKategorijePrihodkiId);
		db.insert(tabelaKategorij, null, cv);
		
		// Ustvari tabelo odhodki/prihodki
		db.execSQL("CREATE TABLE " + tabelaOdhodkiPrihodki + " (" + colOdhodkiPrihodkiId + " INTEGER PRIMARY KEY AUTOINCREMENT, " + colOdhodkiPrihodkiZnesek + " REAL, " + colOdhodkiPrihodkiDatum + " INTEGER, " + colOdhodkiPrihodkiKategorijaId + " INTEGER)");
	
		// Ustvari tabelo za sinhronizacijo 
		db.execSQL("CREATE TABLE " + tabelaSinhGd + " (" + colSinhGdId + " INTEGER PRIMARY KEY AUTOINCREMENT, " + colSinhGdDatum + " TEXT, " + colSinhGdOdhodekPrihodek + " TEXT, " + colSinhGdZnesek + " TEXT, " + colSinhGdKategorijaIme + " TEXT)");
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		// TODO Auto-generated method stub
	}
	

	/*
	 * Vstavi novo kategorijo v tabelo kategorij.
	 * String imeKategorije - ime kategorije, ki bo prikazano.
	 * int vrstaKategorijeId - id vrste kateri bo pripadala ta kategorija, 1 za odhodke in 2 za prihodke.
	 */
	public void vstaviKategorijo(String imeKategorije, int vrstaKategorijeId) {
		SQLiteDatabase db = this.getWritableDatabase();
		
		ContentValues cv = new ContentValues();
		cv.put(colKategorijIme, imeKategorije);
		cv.put(colKategorijVrstaId, vrstaKategorijeId);
		db.insert(tabelaKategorij, null, cv);
		 
		db.close();
	}
	
	/*
	 * Vrne vse kategorije zeljene vrste. Vrne HashMap<String, Integer>
	 * int vrstaKategorijId - id vrste kategorij, 1 za odhodke in 2 za prihodke.
	 */
	public HashMap<String, Integer> dobiKategorije(int vrstaKategorijId) {
		HashMap<String, Integer> kategorije = new HashMap<String, Integer>();
		
		SQLiteDatabase db = this.getReadableDatabase();
		
		Cursor cursor = db.rawQuery("SELECT "+colKategorijId+" AS _id, "+colKategorijIme+" FROM "+tabelaKategorij+" WHERE "+colKategorijVrstaId+"="+vrstaKategorijId + " ORDER BY " + colKategorijIme + " ASC", null);
		
		cursor.moveToFirst();
		while (cursor.isAfterLast() == false) {
			int id = cursor.getInt(0);
			String ime = cursor.getString(1);
			kategorije.put(ime, id);
			cursor.moveToNext();
		}
		
		cursor.close();
		db.close();
		
		return kategorije;
	}
	
	/*
	 * Vstavi odhodek / prihodek.
	 */
	public void vstaviOdhodekPrihodek(float znesek, int datum, int idKategorije) {
		SQLiteDatabase db = this.getWritableDatabase();
		
		ContentValues cv = new ContentValues();
		cv.put(colOdhodkiPrihodkiZnesek, znesek);
		cv.put(colOdhodkiPrihodkiDatum, datum);
		cv.put(colOdhodkiPrihodkiKategorijaId, idKategorije);
		db.insert(tabelaOdhodkiPrihodki, null, cv);
		 
		db.close();
	}
	
	/*
	 * Vrne skupni znesek podane kategorije.
	 */
	public float vrniSkupniZnesekKategorije(int idKategorije) {
		SQLiteDatabase db = this.getReadableDatabase();
		
		Cursor cursor = db.rawQuery("SELECT SUM("+colOdhodkiPrihodkiZnesek+") FROM "+tabelaOdhodkiPrihodki+" WHERE "+colOdhodkiPrihodkiKategorijaId+"="+idKategorije, null);
		cursor.moveToFirst();
		float skupniZnesek = cursor.getFloat(0);
		
		cursor.close();
		db.close();
		
		return skupniZnesek;
	}
	
	/*
	 * Vrne skupni znesek podane kategorije za obdobje.
	 */
	public float vrniSkupniZnesekKategorijeObdobje(int idKategorije, int odDatuma, int doDatuma) {
		SQLiteDatabase db = this.getReadableDatabase();
		
		Cursor cursor = db.rawQuery("SELECT SUM("+colOdhodkiPrihodkiZnesek+") FROM "+tabelaOdhodkiPrihodki+" WHERE "+colOdhodkiPrihodkiKategorijaId+"="+idKategorije+" AND "+colOdhodkiPrihodkiDatum+" BETWEEN "+odDatuma+" AND "+doDatuma, null);
		cursor.moveToFirst();
		float skupniZnesek = cursor.getFloat(0);
		
		cursor.close();
		db.close();
		
		return skupniZnesek;
	}
	
	/*
	 * Vstavi v tabelo za sinhronizacijo z Google Docs.
	 */
	public void dodajVrsticoZaSinhGd(String datum, String odhodekPrihodek, String znesek, String kategorijaIme) {
		SQLiteDatabase db = this.getWritableDatabase();
		
		ContentValues cv = new ContentValues();
		cv.put(colSinhGdDatum, datum);
		cv.put(colSinhGdOdhodekPrihodek, odhodekPrihodek);
		cv.put(colSinhGdZnesek, znesek);
		cv.put(colSinhGdKategorijaIme, kategorijaIme);
		db.insert(tabelaSinhGd, null, cv);
		
		db.close();
	}
	
	/*
	 * Vrni vrstice iz tabele za sinhronizacijo z Google Docs
	 */
	public ArrayList<String[]> vrniVrsticeSinhGd() {
		ArrayList<String[]> vrstice = new ArrayList<String[]>();
		
		SQLiteDatabase db = this.getReadableDatabase();
		
		Cursor cursor = db.rawQuery("SELECT "+colSinhGdId+" AS _id, "+colSinhGdDatum+", "+colSinhGdOdhodekPrihodek+", "+colSinhGdZnesek+", "+colSinhGdKategorijaIme+" FROM "+tabelaSinhGd, null);
		cursor.moveToFirst();
		while (cursor.isAfterLast() == false) {
			String idVrstice	   = cursor.getString(0);
			String datum 		   = cursor.getString(1);
			String odhodekPrihodek = cursor.getString(2);
			String znesek          = cursor.getString(3);
			String kategorijaIme   = cursor.getString(4);
			
			String[] vrstica = new String[5];
			vrstica[0] = idVrstice;
			vrstica[1] = datum;
			vrstica[2] = odhodekPrihodek;
			vrstica[3] = znesek;
			vrstica[4] = kategorijaIme;
			
			vrstice.add(vrstica);
			
			cursor.moveToNext();
		}
		
		cursor.close();
		db.close();
		
		return vrstice;
	}
	
	/*
	 * Izprazni tabelo za sinhronizacijo.
	 */
	public void izprazniTabeloSinhGd() {
		SQLiteDatabase db = this.getReadableDatabase();
		
		db.delete(tabelaSinhGd, null, null);
		
		db.close();
	}
	
	/*
	 * Odstrani vrstico iz tabele za sinhronizacijo.
	 */
	public void odstraniVrsticoTabelaSinhGd(int idVrstice) {
		SQLiteDatabase db = this.getReadableDatabase();

		db.delete(tabelaSinhGd, colSinhGdId+"="+idVrstice, null);
		
		db.close();
	}
}
