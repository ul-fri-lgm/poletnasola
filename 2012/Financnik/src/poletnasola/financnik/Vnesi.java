package poletnasola.financnik;

import java.util.ArrayList;
import java.util.HashMap;

import android.app.Activity;
import android.content.Context;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.Spinner;
import android.widget.Toast;

public class Vnesi extends Activity {

	EditText znesek;
	Spinner kategorija;
	DatePicker datum;
	Button vnesi;
	RadioButton odhodek;

	PbazaHelper baza;

	// Seznama kategorij obeh vrst z id-ji.
	HashMap<String, Integer> kategorijeOdhodki;
	HashMap<String, Integer> kategorijePrihodki;
	// Seznama kategorij obeh vrst za prikaz na spinerju.
	ArrayList<String> kategorijeOdhodkiSpinner;
	ArrayList<String> kategorijePrihodkiSpinner;

	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.vnesi_layout);

		odhodek = (RadioButton) findViewById(R.id.radio0);

		baza = new PbazaHelper(this);

		// iz podatkovne baze prebere kategorije in jih shrani v arraylist za
		// prikaz in hashmap kjer je shranjen tudi id kategorije
		dobiKategorije(); 

		// Napolnitev dropdown menija kategorij.
		kategorija = (Spinner) findViewById(R.id.spinner1);
		napolniKategorijeSpinner(null);
		
	}

	protected void onResume() {
		super.onResume();
		dobiKategorije();
		napolniKategorijeSpinner(null);
	}

	private void dobiKategorije() {
		// Dobi kategorije odhodkov.
		kategorijeOdhodkiSpinner = new ArrayList<String>();
		kategorijeOdhodki = baza
				.dobiKategorije(Financnik.vrstaKategorijeOdhodkiId);
		for (String ime : kategorijeOdhodki.keySet()) {
			kategorijeOdhodkiSpinner.add(ime);
		}

		// Dobi kategorije prihodkov.
		kategorijePrihodkiSpinner = new ArrayList<String>();
		kategorijePrihodki = baza
				.dobiKategorije(Financnik.vrstaKategorijePrihodkiId);
		for (String ime : kategorijePrihodki.keySet()) {
			kategorijePrihodkiSpinner.add(ime);
		}
	}

	public void napolniKategorijeSpinner(View v) {
		// Napolni odhodek.

		if (odhodek.isChecked()) {
			ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
					android.R.layout.simple_spinner_item,
					kategorijeOdhodkiSpinner);
			kategorija.setAdapter(adapter);
		}
		// Napolni prihodek.
		else {
			ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
					android.R.layout.simple_spinner_item,
					kategorijePrihodkiSpinner);
			kategorija.setAdapter(adapter);
		}
	}

	public void vnesi(View v) {

		String prihodekOdhodek = "Odhodek";
		String vsebina, izbranDatum, izbranaKategorija;
		Toast toast;
		int toastDuration = Toast.LENGTH_SHORT;

		// Izbira prihodek / odhodek
		if (!odhodek.isChecked()) {
			prihodekOdhodek = "Prihodek";
		}

		// izbira zneska
		znesek = (EditText) findViewById(R.id.editText1);
		vsebina = znesek.getText().toString();
		if (vsebina.matches("")) {
			toast = Toast.makeText(this, "Vnesite znesek", toastDuration);
			toast.show();
			return;
		}
		float vnesenZnesek = Float.parseFloat(znesek.getText().toString());

		// izbira kategorije
		kategorija = (Spinner) findViewById(R.id.spinner1);
		izbranaKategorija = kategorija.getSelectedItem().toString();

		// izbira datuma
		datum = (DatePicker) findViewById(R.id.datePicker1);
		String leto  = Integer.toString(datum.getYear());
		String mesec = Integer.toString(datum.getMonth() + 1);
		String dan   = Integer.toString(datum.getDayOfMonth());
		if(dan.length() == 1)
			dan = "0"+dan;
		if(mesec.length() == 1)
			mesec = "0"+mesec;
		int izbranDatumZaVnosPB = Integer.parseInt(leto + mesec + dan);
		izbranDatum = dan + "/" + mesec + "/" + leto;

		Submit poslji = new Submit();
		if (NastavitveShranjevanje.nastavitveShraniGDocs) {
			if(isNetworkConnected()){
				PostToGD googleDocs = new PostToGD(vnesenZnesek, izbranaKategorija, prihodekOdhodek, izbranDatum, this);
				new Thread(googleDocs).start();
				toast = Toast.makeText(this, "Shranjeno tudi v GoogleDocs.", toastDuration); 
				toast.show();
			}else{
				toast = Toast.makeText(this, "Shranjeno. Z Google Docs bo sinhronizirano ob naslednjem zagonu aplikacije, ko bo na voljo internet!", Toast.LENGTH_LONG);
				toast.show();
				poslji.syncPB(vnesenZnesek, izbranaKategorija, izbranDatum);
			}
		}else{
			toast = Toast.makeText(this, "Shranjeno.", toastDuration); 
			toast.show();
		}

		// shrani v bazo
		poslji.submitPB(vnesenZnesek, izbranaKategorija, izbranDatumZaVnosPB);

		// Ponastavi GUI;
		znesek.setText("");
	}

	protected boolean isNetworkConnected() {
		Context ctx = this;
		ConnectivityManager cm = (ConnectivityManager) ctx
				.getSystemService(Context.CONNECTIVITY_SERVICE);
		return cm.getActiveNetworkInfo() != null && cm.getActiveNetworkInfo().isConnectedOrConnecting();
	}

	private class Submit {

		public void submitPB(float znesek, String kategorija, int izbranDatumZaVnosPB) {
			int idKategorije;
			if (odhodek.isChecked())
				idKategorije = kategorijeOdhodki.get(kategorija);
			else
				idKategorije = kategorijePrihodki.get(kategorija);

			baza.vstaviOdhodekPrihodek(znesek, izbranDatumZaVnosPB, idKategorije);
		}
		
		public void syncPB(float znesek, String kategorija, String izbranDatumZaVnosPB){
			String temp = Float.toString(znesek);
			
			if(odhodek.isChecked()){
				baza.dodajVrsticoZaSinhGd(izbranDatumZaVnosPB, "Odhodek", temp, kategorija);
			}else{
				baza.dodajVrsticoZaSinhGd(izbranDatumZaVnosPB, "Prihodek", temp, kategorija);
			}
		}

	}

}
