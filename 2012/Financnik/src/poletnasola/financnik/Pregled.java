package poletnasola.financnik;

import java.util.HashMap;
import java.util.Map.Entry;
import android.app.Activity;
import android.app.Dialog;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

public class Pregled extends Activity{
	
	RadioButton checkBoxOdhodki;
	TextView twInfoObdobje;
	
	HashMap<String, Float> skupniZneskiOdhodki;
	HashMap<String, Float> skupniZneskiPrihodki;
	
	// najvisji znesek je uporabljen za izracun setProgress bara
	float najvisjiZnesekOdhodki;
	float najvisjiZnesekPrihodki;
	
	PbazaHelper baza;
	
	Dialog obdobjeDialog;
	
	// datum za prikaz, izbranDatumOdPB=0 in izbranDatumDoPB=0 pomeni celotno obdobje
	String prikazObdobjaInfo;
	int izbranDatumOdPB;
	int izbranDatumDoPB;
	
	public void onCreate(Bundle savedInstanceState){
		super.onCreate(savedInstanceState);
		setContentView(R.layout.pregled_layout);

		checkBoxOdhodki = (RadioButton) findViewById(R.id.radio10);
		twInfoObdobje = (TextView) findViewById(R.id.textViewInfoObdobje);
		
		ustvariDialogZaObdobje();
		
		baza = new PbazaHelper(this);
				
		prikazObdobjaInfo = "Celotno obdobje";
		izbranDatumOdPB = 0;
		izbranDatumDoPB = 0;
			
		// dobi podatke in sestavi graf ter ga prikaz
		dobiSkupneZneskeKategorijPrikazi(null);
	}
	
	protected void onResume() {
		super.onResume();
		dobiSkupneZneskeKategorijPrikazi(null);
	}
	
	
	public void dobiSkupneZneskeKategorijPrikazi(View v) {
		najvisjiZnesekOdhodki = 0.0f;
		najvisjiZnesekPrihodki = 0.0f;
		
		if(checkBoxOdhodki.isChecked()){
			skupniZneskiOdhodki  = dobiSkupneZneskeKategorij( Financnik.vrstaKategorijeOdhodkiId );
		}else{
			skupniZneskiPrihodki = dobiSkupneZneskeKategorij( Financnik.vrstaKategorijePrihodkiId );
		}
		
		twInfoObdobje.setText(prikazObdobjaInfo);
		
		sestaviInPrikaziGraf();
	}	
	private HashMap<String, Float> dobiSkupneZneskeKategorij(int vrstaKategorije) {
		HashMap<String, Integer> kategorije = baza.dobiKategorije(vrstaKategorije);
		HashMap<String, Float> skupniZneski = new HashMap<String, Float>();
		
		for(Entry<String, Integer> kategorija : kategorije.entrySet()){
			float znesek;
			if(izbranDatumOdPB == 0 || izbranDatumDoPB == 0)// celotno obdobje
				znesek = baza.vrniSkupniZnesekKategorije( kategorija.getValue() );
			else
				znesek = baza.vrniSkupniZnesekKategorijeObdobje(kategorija.getValue(), izbranDatumOdPB, izbranDatumDoPB);
			
			if( (vrstaKategorije == Financnik.vrstaKategorijeOdhodkiId) && (Float.compare(znesek, najvisjiZnesekOdhodki) > 0) )
				najvisjiZnesekOdhodki = znesek;
			else if((vrstaKategorije == Financnik.vrstaKategorijePrihodkiId) && (Float.compare(znesek, najvisjiZnesekPrihodki) > 0))
				najvisjiZnesekPrihodki = znesek;
			
			skupniZneski.put(kategorija.getKey(), znesek);
		}
		
		return skupniZneski;
	}
	
	private void sestaviInPrikaziGraf() {
		LinearLayout layout = (LinearLayout)findViewById(R.id.llgraf);
		float skupaj = 0;
		
		
		// odstrani prejsni graf
		layout.removeAllViews();
		
		ScrollView sv = new ScrollView(this);
		LinearLayout ll = new LinearLayout(this);
		ll.setOrientation(LinearLayout.VERTICAL);
		sv.addView(ll);
		
		// graf odhodkov
		if(checkBoxOdhodki.isChecked()) {
			int componentId = 10;
			for(Entry<String, Float> kategorija : skupniZneskiOdhodki.entrySet()) {
				String imeKategorije = kategorija.getKey();
				float skupniZnesekKategorije = kategorija.getValue();
				skupaj += skupniZnesekKategorije;
				
				componentId++;				
				TextView tv = new TextView(this);
				tv.setText(imeKategorije + " - " + skupniZnesekKategorije + " €");
				tv.setId(componentId);
				tv.setTextSize(20);
				ll.addView(tv);
				
				// kolicina zneska v procentih glede na najvisji znesek ki predstavlja 100 procentov
				int progressProcenti = (int) ((skupniZnesekKategorije * 100) / najvisjiZnesekOdhodki);
				
				componentId++;
				ProgressBar bar = new ProgressBar(this, null, android.R.attr.progressBarStyleHorizontal);
				bar.setProgressDrawable(getResources().getDrawable(R.drawable.red_progress));
				bar.setProgress(progressProcenti);
				bar.setId(componentId);
				ll.addView(bar);
			}
			componentId++;
			TextView tv = new TextView(this);
			tv.setText("Skupaj: " + skupaj + " €");
			tv.setId(componentId);
			tv.setPadding(0, 20, 0, 0);
			tv.setTextSize(20);
			ll.addView(tv);
			
		}
		// graf prihodkov
		else {
			int componentId = 10;
			for(Entry<String, Float> kategorija : skupniZneskiPrihodki.entrySet()) {
				String imeKategorije = kategorija.getKey();
				float skupniZnesekKategorije = kategorija.getValue();
				skupaj += skupniZnesekKategorije;
				
				componentId++;				
				TextView tv = new TextView(this);
				tv.setText(imeKategorije + " - " + skupniZnesekKategorije + " €");
				tv.setId(componentId);
				tv.setTextSize(20);
				ll.addView(tv);
				
				// kolicina zneska v procentih glede na najvisji znesek ki predstavlja 100 procentov
				int progressProcenti = (int) ((skupniZnesekKategorije * 100) / najvisjiZnesekPrihodki);
				
				componentId++;
				ProgressBar bar = new ProgressBar(this, null, android.R.attr.progressBarStyleHorizontal);
				bar.setProgressDrawable(getResources().getDrawable(R.drawable.green_progress));
				bar.setProgress(progressProcenti);
				bar.setId(componentId);
				ll.addView(bar);
			}
			componentId++;
			TextView tv = new TextView(this);
			tv.setText("Skupaj: " + skupaj + " €");
			tv.setId(componentId);
			tv.setPadding(0, 20, 0, 0);
			tv.setTextSize(20);
			ll.addView(tv);
		}
		
		layout.addView(sv);		
		layout.invalidate();
	}
	
	private void nastaviDatumeObdobja(int datumOdPb, int datumDoPb, String datumOdPrikaz, String datumDoPrikaz) {
		prikazObdobjaInfo = "Obdobje " + datumOdPrikaz + " do " + datumDoPrikaz;
		izbranDatumOdPB = datumOdPb;
		izbranDatumDoPB = datumDoPb;
		
		dobiSkupneZneskeKategorijPrikazi(null);
	}

	private void ustvariDialogZaObdobje() {
		obdobjeDialog = new Dialog(this);

		obdobjeDialog.setContentView(R.layout.izbira_obdobja_layout);
		obdobjeDialog.setTitle("Izbira obdobja");
		obdobjeDialog.setCancelable(true);

		// gumb prikazi obdobje
		Button buttonPrikaziObdobje = (Button) obdobjeDialog.findViewById(R.id.buttonPrikaziObdobje);
		buttonPrikaziObdobje.setOnClickListener(new OnClickListener() {
			//@Override
			public void onClick(View v) {
				DatePicker dateP1Od = (DatePicker) obdobjeDialog.findViewById(R.id.datePickerObdobjeOd);
				DatePicker dateP2Do = (DatePicker) obdobjeDialog.findViewById(R.id.datePickerObdobjeDo);
				
				String letoOd  = Integer.toString(dateP1Od.getYear());
				String mesecOd = Integer.toString(dateP1Od.getMonth() + 1);
				String danOd  = Integer.toString(dateP1Od.getDayOfMonth());
				
				if(danOd.length() == 1)
					danOd = "0"+danOd;
				if(mesecOd.length() == 1)
					mesecOd = "0"+mesecOd;
				

				String letoDo  = Integer.toString(dateP2Do.getYear());
				String mesecDo = Integer.toString(dateP2Do.getMonth() + 1);
				String danDo  = Integer.toString(dateP2Do.getDayOfMonth());
				
				if(danDo.length() == 1)
					danDo = "0"+danDo;
				if(mesecDo.length() == 1)
					mesecDo = "0"+mesecDo;
				
				String datumOdPrikaz = danOd + ". " + mesecOd + ". " + letoOd;
				String datumDoPrikaz = danDo + ". " + mesecDo + ". " + letoDo;
				int izbranDatumOdPB = Integer.parseInt(letoOd + mesecOd + danOd);
				int izbranDatumDoPB = Integer.parseInt(letoDo + mesecDo + danDo);
				
				if(!(izbranDatumOdPB > izbranDatumDoPB)) {
					nastaviDatumeObdobja(izbranDatumOdPB, izbranDatumDoPB, datumOdPrikaz, datumDoPrikaz);				
					
					obdobjeDialog.dismiss();
				} else {
					prikaziToast("Datum od mora biti starejši od datuma do.");
				}
			}
		});
	}
	
	private void prikaziToast(String msg) {
		Toast.makeText(this, msg, Toast.LENGTH_LONG).show();
	}
	
	public void prikaziDialogZaObdobje(View v) {
		obdobjeDialog.show();
	}
}
