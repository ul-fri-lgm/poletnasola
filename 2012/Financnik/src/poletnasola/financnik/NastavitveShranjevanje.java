package poletnasola.financnik;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import android.content.Context;

public class NastavitveShranjevanje {
	
	static Context context;
	
	static final String FILE_NASTAVITVE = "nastavitve_shrani_v";
	static final String FILE_GD_FORM_ID = "google_docs_form_id";
	static boolean nastavitveShraniGDocs;
	static String googleDocsFormId;

	public NastavitveShranjevanje(Context context) {
		NastavitveShranjevanje.context = context;
		googleDocsFormId = "";
		nastavitveShraniGDocs = false;
		preberiNastavitve();
	}
	
    // prebere nastavitve iz datoteke, ce datoteke ni jo ustvari
    public static void preberiNastavitve() {
    	// google docs
    	try {
	        File file = context.getFileStreamPath(FILE_NASTAVITVE);
	        // prebere nastavitve
	        if(file.exists()) {
		        FileInputStream in = context.openFileInput(FILE_NASTAVITVE);
		        InputStreamReader inputStreamReader = new InputStreamReader(in);
		        BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
		        String line;
		        while ((line = bufferedReader.readLine()) != null) {
			        if(line.compareTo("1") == 0)
			        	nastavitveShraniGDocs = true;
			        else
			        	nastavitveShraniGDocs = false;
		        }
		        bufferedReader.close();
		        inputStreamReader.close();
		        in.close();
	        } 
	        // ustvari datoteko z nastavitvami
	        else {
	        	shraniNastavitve();
	        }
        } catch (Exception e) {}
    	
    	// google docs form id
    	try {
	        File file = context.getFileStreamPath(FILE_GD_FORM_ID);
	        // prebere nastavitve
	        if(file.exists()) {
		        FileInputStream in = context.openFileInput(FILE_GD_FORM_ID);
		        InputStreamReader inputStreamReader = new InputStreamReader(in);
		        BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
		        String line;
		        while ((line = bufferedReader.readLine()) != null) {
		        	googleDocsFormId = line;
		        }
		        bufferedReader.close();
		        inputStreamReader.close();
		        in.close();
	        }
        } catch (Exception e) {}
    }
    
    // shrani nastavitve v datoteko
    public static void shraniNastavitve() {
    	try {
    		String nastavitve;
        	
        	if(nastavitveShraniGDocs)
        		nastavitve = "1";
        	else
        		nastavitve = "0";

            FileOutputStream fOut = context.openFileOutput(FILE_NASTAVITVE, Context.MODE_PRIVATE);
            OutputStreamWriter osw = new OutputStreamWriter(fOut); 

            osw.write(nastavitve);

            osw.flush();
            osw.close();
    	} catch (Exception e) {}
    }
    
    public static void spremeniNastavitveShranjevanjaGD(boolean shraniVGD) {
    	nastavitveShraniGDocs = shraniVGD;
    	shraniNastavitve();
    }
    
    public static void shraniGoogleDocsFormId(String formId) {
    	googleDocsFormId = formId;
    	try {

            FileOutputStream fOut = context.openFileOutput(FILE_GD_FORM_ID, Context.MODE_PRIVATE);
            OutputStreamWriter osw = new OutputStreamWriter(fOut); 

            osw.write(formId);

            osw.flush();
            osw.close();
    	} catch (Exception e) {}
    }
}
