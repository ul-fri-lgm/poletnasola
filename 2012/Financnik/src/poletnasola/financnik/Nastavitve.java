package poletnasola.financnik;

import android.app.Activity;
import android.app.Dialog;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

public class Nastavitve extends Activity{
	
	PbazaHelper baza;
	
	CheckBox checkBoxGoogleDocs;
	
	// dialog za google docs
	Dialog googleDocsDialog;
	
	public void onCreate(Bundle savedInstanceState){
		super.onCreate(savedInstanceState);
		setContentView(R.layout.nastavitve_layout);
		
		baza = new PbazaHelper(this);
		
		// nastavitve google docs
		checkBoxGoogleDocs = (CheckBox) findViewById(R.id.checkBox1);
		checkBoxGoogleDocs.setChecked(NastavitveShranjevanje.nastavitveShraniGDocs);

		
		// dialog za google docs
		googleDocsDialog = new Dialog(this);

		googleDocsDialog.setContentView(R.layout.google_docs_dialog_layout);
		googleDocsDialog.setTitle("Google Docs");
		googleDocsDialog.setCancelable(false);

		TextView text = (TextView) googleDocsDialog.findViewById(R.id.text);
		text.setText("Vnesi Google Docs form key.");
		// gumb shrani
		Button buttonShraniGDFormId = (Button) googleDocsDialog.findViewById(R.id.button1);
		buttonShraniGDFormId.setOnClickListener(new OnClickListener() {
			//@Override
			public void onClick(View v) {
				EditText editTextGoogleDocsFormId = (EditText) googleDocsDialog.findViewById(R.id.googleDocsId);
				String gdFormId = editTextGoogleDocsFormId.getText().toString();
				if(gdFormId.compareTo("") != 0) {
					NastavitveShranjevanje.shraniGoogleDocsFormId(gdFormId);
					googleDocsDialog.dismiss();
				}
			}
		});
		// gumb preklici
		Button buttonPreklici = (Button) googleDocsDialog.findViewById(R.id.button2);
		buttonPreklici.setOnClickListener(new OnClickListener() {
			//@Override
			public void onClick(View v) {
				googleDocsDialog.dismiss();
				// odkljukam checbox in pa spremenljivko dam na false
				checkBoxGoogleDocs.setChecked(false);
				spremeniNastavitveShraniV(null);
			}
		});
		
		if(savedInstanceState != null) {
			if(savedInstanceState.getBoolean("prikziDiagram")) {
				googleDocsDialog.show();
				EditText editTextGoogleDocsFormId = (EditText) googleDocsDialog.findViewById(R.id.googleDocsId);
				editTextGoogleDocsFormId.setText( savedInstanceState.getString("googleDocsId") );
			}
		}
	}
	
	protected void onSaveInstanceState (Bundle outState) {
		outState.putBoolean("prikziDiagram", googleDocsDialog.isShowing());

		EditText editTextGoogleDocsFormId = (EditText) googleDocsDialog.findViewById(R.id.googleDocsId);
		String gdFormId = editTextGoogleDocsFormId.getText().toString();
		
		outState.putString("googleDocsId", gdFormId);
	}
	
	public void dodajKategorijo(View v) {
		// Ime kategorije, ki ga je vpisal uporabnik.
		EditText imeKategorijeET = (EditText) findViewById(R.id.editText1);
		String imeKategorije = imeKategorijeET.getText().toString();
		if(imeKategorije.matches("")) {
			Toast.makeText(this, "Vnesite ime kategorije.", Toast.LENGTH_SHORT).show();
			return;
		}
		
		// Id vrste kategorij.
		int vrstaKategorijeId;
		RadioButton odhodek = (RadioButton) findViewById(R.id.radio0);
		// odhodek
		if (odhodek.isChecked())
			vrstaKategorijeId = Financnik.vrstaKategorijeOdhodkiId;
		// prihodek
		else 
			vrstaKategorijeId = Financnik.vrstaKategorijePrihodkiId;
		
		// Doda v bazo.
		baza.vstaviKategorijo(imeKategorije, vrstaKategorijeId);		
		Toast.makeText(this, "Kategorija je bila dodana.", Toast.LENGTH_SHORT).show();
		
		// pobrisem kar je v EditText-u
		imeKategorijeET.setText("");
	}
	
	public void spremeniNastavitveShraniV(View v) {
		NastavitveShranjevanje.spremeniNastavitveShranjevanjaGD(checkBoxGoogleDocs.isChecked() ? true : false);	
		
		// ce je biu check box za google docs obklukan prikaze diagram
		if(checkBoxGoogleDocs.isChecked()) {
			EditText editTextGoogleDocsFormId = (EditText) googleDocsDialog.findViewById(R.id.googleDocsId);
			editTextGoogleDocsFormId.setText(NastavitveShranjevanje.googleDocsFormId);
			googleDocsDialog.show();
		}
	}
	
}
