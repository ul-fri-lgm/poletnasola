package poletnasola.financnik;

import java.util.ArrayList;

import android.content.Context;
import android.widget.Toast;

public class Sinhronizacija {

	PbazaHelper baza;
	
	Context ctx;
	ArrayList<String[]> vnosi;
	float znesek;
	String kategorija, vrstaKategorije, datum, temp;
	
	public Sinhronizacija(PbazaHelper baza, Context ctx) {
		this.ctx = ctx;
		this.baza = baza;
	}
	
	
	public void sinhronizeri() {
		// dobi podatke iz baze
				vnosi = baza.vrniVrsticeSinhGd();
				if (vnosi != null) {
					for (String[] zapis : vnosi) {
						datum = zapis[1];
						vrstaKategorije = zapis[2];
						temp = zapis[3];
						kategorija = zapis[4];
						znesek = Float.parseFloat(temp);
						PostToGD post = new PostToGD(znesek, kategorija, vrstaKategorije, datum, ctx);
						new Thread(post).start();
						baza.odstraniVrsticoTabelaSinhGd(Integer.parseInt(zapis[0]));
					}
					if(vnosi.size() >= 1)
						Toast.makeText(ctx, "Podatki so bili sinhronizirani z Google Docs.", Toast.LENGTH_SHORT).show();
				}
	}
	
}
