package poletnasola.financnik;

import android.net.ConnectivityManager;
import android.os.Bundle;
import android.app.TabActivity;
import android.content.Context;
import android.content.Intent;
import android.widget.TabHost;
import android.widget.TabHost.TabSpec;

public class Financnik extends TabActivity {

	// Vrsti kategorij
	static final int vrstaKategorijeOdhodkiId = 1;
	static final int vrstaKategorijePrihodkiId = 2;
	
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_financnik);
        
        // prebere nastavitve
        new NastavitveShranjevanje(getApplicationContext());
        
        TabHost tabHost = getTabHost();
 
        // Tab for Odhodki
        TabSpec nastavitveTab = tabHost.newTabSpec("Nastavitve");
        // setting Title and Icon for the Tab
        nastavitveTab.setIndicator("Nastavitve");
        Intent nastavitveIntent = new Intent(this, Nastavitve.class);
        nastavitveTab.setContent(nastavitveIntent);
 
        // Tab for Prihodki
        TabSpec vnesiTab = tabHost.newTabSpec("Vnesi");
        vnesiTab.setIndicator("Vnesi");
        Intent vnesiIntent = new Intent(this, Vnesi.class);
        vnesiTab.setContent(vnesiIntent);
 
        // Tab for Pregled
        TabSpec pregledTab = tabHost.newTabSpec("Pregled");
        pregledTab.setIndicator("Pregled");
        Intent pregledIntent = new Intent(this, Pregled.class);
        pregledTab.setContent(pregledIntent);
 
        tabHost.addTab(vnesiTab );
        tabHost.addTab(pregledTab); 
        tabHost.addTab(nastavitveTab); 
        
        // sinhronizacija
        if(isNetworkConnected()) {
	        PbazaHelper baza = new PbazaHelper(this);
	        Sinhronizacija sh = new Sinhronizacija(baza, this);
	        sh.sinhronizeri();
        }
    }
    
    private boolean isNetworkConnected() {
    	Context ctx = this;
		ConnectivityManager cm = (ConnectivityManager) ctx
				.getSystemService(Context.CONNECTIVITY_SERVICE);
		return cm.getActiveNetworkInfo() != null && cm.getActiveNetworkInfo().isConnectedOrConnecting();
    }
    
}
