package example.treznost;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.RadialGradient;
import android.graphics.Shader;
import android.util.AttributeSet;
import android.view.View;

@SuppressLint({ "DrawAllocation", "DrawAllocation" })
public class Gumb extends View{
	Paint paint;
	private static final int xkrog=50;
	private static final int ykrog=50;
	private static final int textSize=20;
	private int xodg=xkrog-6;
	private int yodg=ykrog+(textSize/2)-2;
	private int radij=25;
	private String odgovor;
	private int xSpeed;
	private int ySpeed;
	private int smer;
	private int color;
	
	public Gumb(Context context, AttributeSet attrs) {
		super(context, attrs);
		paint=new Paint();
		color = Color.BLUE;
	}
	
	public void setGumbColor(int c){
		color = c;
	}
	
	protected void onDraw(Canvas c){

		int len = odgovor.length();
		if (len < 2) {
			paint.setAntiAlias(true);
	//		RadialGradient rg = new RadialGradient(60, 45, 25, color, Color.BLACK, Shader.TileMode.CLAMP);
			RadialGradient rg = new RadialGradient(50, 50, 25, new int[] {Color.WHITE, color}, new float[] {0, 20}, Shader.TileMode.CLAMP);
			paint.setDither(true);
			paint.setShader(rg);
		    c.drawCircle(xkrog,ykrog, radij, paint);
			
		    paint.setDither(false);
		    paint.setShader(null);
		    paint.setColor(Color.BLACK);
		    paint.setTextSize(textSize);
		    c.drawText(odgovor, xodg, yodg, paint);
		} else {
			paint.setAntiAlias(true);
			RadialGradient rg = new RadialGradient(55, 45, 40, new int[] {color, Color.BLACK}, new float[] {0, 20}, Shader.TileMode.CLAMP);
			paint.setDither(true);
			paint.setShader(rg);
		    c.drawCircle(xkrog,ykrog, radij, paint);
			
		}
	}
	public void setSmer(int s){
		smer=s;
	}
	public int vrniSmer(){
		return smer;
	}
	public void setXSpeed(int s){
		xSpeed=s;
	}
	public void setYSpeed(int s){
		ySpeed=s;
	}
	public int getXSpeed(){
		return xSpeed;
	}
	public int getYSpeed(){
		return ySpeed;
	}
	public int vrniRadij(){
		return radij;
	}
	public void setOdgovor(String o){
		this.odgovor=o;
	}
	public String vrniOdgovor(){
		return odgovor;
	}
	
	private boolean xUp, yUp;
	
	public boolean getXUp(){
		return xUp;
	}	
	public boolean getYUp(){
		return yUp;
	}
	
	@SuppressLint("NewApi")
	public void collide(Gumb other) {
	      double xDist, yDist;
	      xDist = this.getX() - other.getX();
	      yDist = this.getY() - other.getY();
	      double distSquared = xDist * xDist + yDist * yDist;
	      
	      if (distSquared <= (radij + other.radij) * (radij + other.radij)) {
	          double speedXocity = other.xSpeed - xSpeed;
	          double speedYocity = other.ySpeed - ySpeed;
	          double dotProduct = xDist * speedXocity + yDist * speedYocity;

	          if (dotProduct > 0) {
	               double collisionScale = dotProduct / distSquared;
	               double xCollision = xDist * collisionScale;
	               double yCollision = yDist * collisionScale;
	               if((Math.abs(this.xSpeed + xCollision)<2)&&(Math.abs(this.ySpeed + yCollision)<2)){
	            	   this.xSpeed += xCollision*2;
		               this.ySpeed += yCollision*2;
	               }else{
		               this.xSpeed += xCollision;
		               this.ySpeed += yCollision;
	               }
	               if((Math.abs(other.xSpeed - xCollision)<2)&&(Math.abs(other.ySpeed - yCollision)<2)){
	            	   other.xSpeed -= xCollision*2;
	            	   other.ySpeed -= yCollision*2;
	               }else{
	            	   other.xSpeed -= xCollision;
	            	   other.ySpeed -= yCollision;
	               }	          
	          }
	      }
	}
}
