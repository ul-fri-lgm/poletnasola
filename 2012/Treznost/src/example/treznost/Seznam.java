package example.treznost;


import java.util.ArrayList;

public class Seznam {	

	public Seznam(){
		vprasanja();
		napStevila();
		vprasanjaBarve();
		napacneBarve();
	}
	//vprasanja z pravilnimi odgovori
	
	private ArrayList<Vprasanja> mojaVprasanja=new ArrayList<Vprasanja>();
	
	public void vprasanja(){
		Vprasanja v1=new Vprasanja("3+5","8");
		mojaVprasanja.add(v1);
		Vprasanja v2=new Vprasanja("9/3","3");
		mojaVprasanja.add(v2);
		Vprasanja v3=new Vprasanja("7+1","8");
		mojaVprasanja.add(v3);
		Vprasanja v4=new Vprasanja("2+3","5");
		mojaVprasanja.add(v4);
		Vprasanja v5=new Vprasanja("6/2","3");
		mojaVprasanja.add(v5);
		Vprasanja v6=new Vprasanja("3*3","9");
		mojaVprasanja.add(v6);
		Vprasanja v7=new Vprasanja("2*1","2");
		mojaVprasanja.add(v7);
		Vprasanja v8=new Vprasanja("2*4","8");
		mojaVprasanja.add(v8);
		Vprasanja v9=new Vprasanja("1+8","9");
		mojaVprasanja.add(v9);
		Vprasanja v10=new Vprasanja("1*2","2");
		mojaVprasanja.add(v10);
		Vprasanja v11=new Vprasanja("9*1","9");
		mojaVprasanja.add(v11);
		Vprasanja v12=new Vprasanja("6/3","2");
		mojaVprasanja.add(v12);
		Vprasanja v13=new Vprasanja("7*1","7");
		mojaVprasanja.add(v13);
		Vprasanja v14=new Vprasanja("5-4","1");
		mojaVprasanja.add(v14);
		Vprasanja v15=new Vprasanja("3-1","2");
		mojaVprasanja.add(v15);
		Vprasanja v16=new Vprasanja("9-8","1");
		mojaVprasanja.add(v16);
		Vprasanja v17=new Vprasanja("4/4","1");
		mojaVprasanja.add(v17);
		Vprasanja v18=new Vprasanja("8-2","6");
		mojaVprasanja.add(v18);
		Vprasanja v19=new Vprasanja("8/2","4");
		mojaVprasanja.add(v19);
		Vprasanja v20=new Vprasanja("2/2","1");
		mojaVprasanja.add(v20);
		Vprasanja v21=new Vprasanja("8-5","3");
		mojaVprasanja.add(v21);
		Vprasanja v22=new Vprasanja("7/7","1");
		mojaVprasanja.add(v22);
		Vprasanja v23=new Vprasanja("9-4","5");
		mojaVprasanja.add(v23);
		Vprasanja v24=new Vprasanja("9/3","3");
		mojaVprasanja.add(v24);
		Vprasanja v25=new Vprasanja("4+3","7");
		mojaVprasanja.add(v25);
		Vprasanja v26=new Vprasanja("1/1","1");
		mojaVprasanja.add(v26);
		Vprasanja v27=new Vprasanja("3/3","1");
		mojaVprasanja.add(v27);
		Vprasanja v28=new Vprasanja("5/5","1");
		mojaVprasanja.add(v28);
		Vprasanja v29=new Vprasanja("6-4","2");
		mojaVprasanja.add(v29);
		Vprasanja v30=new Vprasanja("3+2","5");
		mojaVprasanja.add(v30);
	}
	
	public ArrayList<Vprasanja> vrniVprasanja(){
		return mojaVprasanja;
	}
	//"napacni odgovori"-stevila
	private ArrayList<NapacniOdg> napacnaSt=new ArrayList<NapacniOdg>();
	public void napStevila(){
		NapacniOdg n1=new NapacniOdg("1");
		napacnaSt.add(n1);
		NapacniOdg n2=new NapacniOdg("2");
		napacnaSt.add(n2);
		NapacniOdg n3=new NapacniOdg("3");
		napacnaSt.add(n3);
		NapacniOdg n4=new NapacniOdg("4");
		napacnaSt.add(n4);
		NapacniOdg n5=new NapacniOdg("5");
		napacnaSt.add(n5);
		NapacniOdg n6=new NapacniOdg("6");
		napacnaSt.add(n6);
		NapacniOdg n7=new NapacniOdg("7");
		napacnaSt.add(n7);
		NapacniOdg n8=new NapacniOdg("8");
		napacnaSt.add(n8);
		NapacniOdg n9=new NapacniOdg("9");
		napacnaSt.add(n9);
	}
	
	public ArrayList<NapacniOdg> vrniNapacneSt(){
		return napacnaSt;
	}
	
	private ArrayList<Vprasanja> barve=new ArrayList<Vprasanja>();
	
	public void vprasanjaBarve(){
		Vprasanja v1=new Vprasanja("modra + rumena","zelena");
		barve.add(v1);
		Vprasanja v2=new Vprasanja("rdeca + rumena","oranzna");
		barve.add(v2);
		Vprasanja v3=new Vprasanja("rdeca + modra","vijola");
		barve.add(v3);
		Vprasanja v4=new Vprasanja("rdeca + zelena","rjava");
		barve.add(v4);
		Vprasanja v5=new Vprasanja("rumena + zelena","svetlo zelena");
		barve.add(v5);
	}
	
	public ArrayList<Vprasanja> vrniBarve(){
		return barve;
	}
	
	private ArrayList<NapacniOdg> napBarve=new ArrayList<NapacniOdg>();
	
	public void napacneBarve(){
		NapacniOdg b1=new NapacniOdg("rdeca");
		napBarve.add(b1);
		NapacniOdg b2=new NapacniOdg("rumena");
		napBarve.add(b2);
		NapacniOdg b3=new NapacniOdg("zelena");
		napBarve.add(b3);
		NapacniOdg b4=new NapacniOdg("modra");
		napBarve.add(b4);
		NapacniOdg b5=new NapacniOdg("vijola");
		napBarve.add(b5);
		NapacniOdg b6=new NapacniOdg("oranzna");
		napBarve.add(b6);
		NapacniOdg b7=new NapacniOdg("rjava");
		napBarve.add(b7);
		NapacniOdg b8=new NapacniOdg("svetlo zelena");
		napBarve.add(b8);
	}
	
	public ArrayList<NapacniOdg> vrniNapBarve(){
		return napBarve;
	}	
}

