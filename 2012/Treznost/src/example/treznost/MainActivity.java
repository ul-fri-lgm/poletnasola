package example.treznost;

import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.view.View;


public class MainActivity extends Activity {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void newgame(View v){
    	Intent i = new Intent(this, Igra.class);
    	startActivity(i);
    }
    
    public void highscores(View v){
    	Intent i = new Intent(this, HighScores.class);
    	startActivity(i);
    }
    
    public void exit(View v){
    	finish();
    }
}
