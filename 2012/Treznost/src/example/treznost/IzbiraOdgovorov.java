package example.treznost;

import java.util.ArrayList;
import java.util.Random;


public class IzbiraOdgovorov {
	/**
	 * @param args
	 */
	private Vprasanja zadnjeVpr;
	private Vprasanja vprasanje;
	private ArrayList<Vprasanja> vprasanjaNum;
	private ArrayList<NapacniOdg> odgovoriNum;
	private Random generator;
	private ArrayList<String> odgovori;
	private ArrayList<Vprasanja> vprasanjaBarve;
	private ArrayList<NapacniOdg> odgovoriBarve;
	
	public IzbiraOdgovorov() {
		// TODO Auto-generated constructor stub	
		zadnjeVpr = new Vprasanja("", "");
		generator = new Random();
		Seznam seznamVprasanj = new Seznam();
		vprasanjaNum = seznamVprasanj.vrniVprasanja();
		odgovoriNum = seznamVprasanj.vrniNapacneSt();
		odgovori  = new ArrayList<String>();
		vprasanjaBarve = seznamVprasanj.vrniBarve();
		odgovoriBarve = seznamVprasanj.vrniNapBarve();
	}
	
	public Vprasanja vrniVprasanje(){
		int i = generator.nextInt(11);
		if(i>8){
			vrniVprasanje(vprasanjaBarve, odgovoriBarve, generator);
		}else{
			vrniVprasanje(vprasanjaNum, odgovoriNum, generator);
		}
		return vprasanje;
	}
	
	public ArrayList<String> vrniOdgovore(){
		return odgovori;
	}

	private void vrniVprasanje(ArrayList<Vprasanja> vpr, ArrayList<NapacniOdg> odg, Random ran){
		
		int stOdg = 3;
		int num = ran.nextInt(vpr.size());
		
		while(zadnjeVpr.getVprasanje().equals(vpr.get(num).getVprasanje())){
			num = ran.nextInt(vpr.size());
		}
		Vprasanja v = vpr.get(num);
		zadnjeVpr = v;		
		odgovori.clear();
		
		while(odgovori.size()<stOdg){
			num = ran.nextInt(odg.size());
			if(!odgovori.contains(odg.get(num).getNapOd())){
				if(!v.getOdgovor().equals(odg.get(num).getNapOd())){
					odgovori.add(odg.get(num).getNapOd());
				}
			}
		}
		vprasanje=v;	
	}
}
