package example.treznost;

public class NapacniOdg {
	private String napOd;
	
	public NapacniOdg(String nap){
		setNapOd(nap);
	}

	public String getNapOd() {
		return napOd;
	}

	public void setNapOd(String napOd) {
		this.napOd = napOd;
	}
}
