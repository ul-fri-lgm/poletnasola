package example.treznost;

public class Vprasanja {
	private String vprasanje;
	private String odgovor;
	public Vprasanja(String vpr, String odg){
		setVprasanje(vpr);
		setOdgovor(odg);
	}
	public String getVprasanje() {
		return vprasanje;
	}
	public void setVprasanje(String vprasanje) {
		this.vprasanje = vprasanje;
	}
	public String getOdgovor() {
		return odgovor;
	}
	public void setOdgovor(String odgovor) {
		this.odgovor = odgovor;
	}
	
}
