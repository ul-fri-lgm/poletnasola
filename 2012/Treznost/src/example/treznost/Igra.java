package example.treznost;


import java.util.ArrayList;
import java.util.Collections;
import java.util.Random;
import java.util.Timer;
import java.util.TimerTask;
import android.os.Bundle;
import android.os.Handler;
import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.view.Display;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.widget.ProgressBar;
import android.widget.TextView;


public class Igra extends Activity {

	private ProgressBar myProgressBar;
	private int myProgress = 100;
	public Handler myHandler;
	public Handler myHandler2;
	private TextView tocke;
	private Gumb gumb1;
	private Gumb gumb2;
	private Gumb gumb3;
	private Gumb gumb4;
	private VprasanjaView vprVrstica;
	private Vprasanja vprasanje;
	private IzbiraOdgovorov odgovor;
	private ArrayList<String> napacniOdgovori;
	private Timer gumbTimer;
	private float ballSpeedX = 5;  // Ball's speed (x,y)
	private float ballSpeedY = 3;
	private float ballX = 0;  // Ball's center (x,y)
	private float ballY = 0;
	private float ballRadius = 0;
	private int xMin = 0;          // This view's bounds
	private int xMax;
	private int yMin = 0;
	private int yMax;	 
	private int width;
	private int height;
	public int actX;
	public int actY;
	private Random rand;
	private Gumb[] balls;
	private static final int NUM_BALLS=4;
	
	@SuppressWarnings("deprecation")
	@Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_igra);

		Display display = getWindowManager().getDefaultDisplay(); 
		width = display.getWidth();
		height= display.getHeight();
		
        actX = height-findViewById(R.id.vprasanjaView1).getHeight();
        actY = width-findViewById(R.id.vprasanjaView1).getWidth();
		startGame();		
    }

	private Runnable myThread = new Runnable() {

		public void run() {
			// TODO Auto-generated method stub
			while (myProgress > 0) {
				try {
					//myHandle.sendMessage(myHandle.obtainMessage());
					myProgress--;
					myProgressBar.setProgress(myProgress);
					Thread.sleep(200);
				} catch (Throwable t) {
				}
			}
		}
	};  
	
	private void gameOver() {
		// TODO Auto-generated method stub
		if(!this.isFinishing()){
	    	Intent i = new Intent(this, GameOver.class);
	    	startActivity(i);
	    	Tocke mojeTocke=new Tocke((String)tocke.getText());
	    	mojeTocke.datoteka();
	    	finish();
		}
	}
	
	private OnTouchListener touchListener1 = new OnTouchListener() {

		public boolean onTouch(View view, MotionEvent event) {
			// TODO Auto-generated method stub
			int action=event.getAction();
			switch (action) {
				case MotionEvent.ACTION_UP:
	
					if(vprasanje.getOdgovor().equals(gumb1.vrniOdgovor())){
						int temp = Integer.parseInt((String) tocke.getText());
						temp+=10;
						tocke.setText(Integer.toString(temp));
						if(myProgress<95){
							myProgress+=5;
						}else{
							myProgress=100;
						}
					}else{
						if(myProgress>10){
							myProgress-=10;
						}else{
							gameOver();
						}
					}
					novoVprasanje();	
					break;
			}
			return true;
		}
		
	};
	
	private OnTouchListener touchListener2 = new OnTouchListener() {

		public boolean onTouch(View view, MotionEvent event) {
			// TODO Auto-generated method stub
			int action=event.getAction();
			switch (action) {
				case MotionEvent.ACTION_UP:
					if(vprasanje.getOdgovor().equals(gumb2.vrniOdgovor())){
						int temp = Integer.parseInt((String) tocke.getText());
						temp+=10;
						tocke.setText(Integer.toString(temp));
						if(myProgress<95){
							myProgress+=5;
						}else{
							myProgress=100;
						}
					}else{
						if(myProgress>10){
							myProgress-=10;
						}else{
							gameOver();
						}
					}
					novoVprasanje();
					break;
			}
			return true;
		}
		
	};
	
	private OnTouchListener touchListener3 = new OnTouchListener() {

		public boolean onTouch(View view, MotionEvent event) {
			// TODO Auto-generated method stub
			int action=event.getAction();
			switch (action) {
				case MotionEvent.ACTION_UP:
					if(vprasanje.getOdgovor().equals(gumb3.vrniOdgovor())){
						int temp = Integer.parseInt((String) tocke.getText());
						temp+=10;
						tocke.setText(Integer.toString(temp));
						if(myProgress<95){
							myProgress+=5;
						}else{
							myProgress=100;
						}
					}else{
						if(myProgress>10){
							myProgress-=10;
						}else{
							gameOver();
						}
					}
					novoVprasanje();
					break;
			}
			return true;
		}
		
	};	
	
	private OnTouchListener touchListener4 = new OnTouchListener() {

		public boolean onTouch(View view, MotionEvent event) {
			// TODO Auto-generated method stub
			int action=event.getAction();
			switch (action) {
				case MotionEvent.ACTION_UP:
					if(vprasanje.getOdgovor().equals(gumb4.vrniOdgovor())){
						int temp = Integer.parseInt((String) tocke.getText());
						temp+=10;
						tocke.setText(Integer.toString(temp));
						if(myProgress<95){
							myProgress+=5;
						}else{
							myProgress=100;
						}
					}else{
						if(myProgress>10){
							myProgress-=10;
						}else{
							gameOver();
						}
					}
					novoVprasanje();
					break;
			}
			return true;
		}
		
	};

	
	private void novoVprasanje(){
		napacniOdgovori.clear();
		vprasanje=odgovor.vrniVprasanje();

		napacniOdgovori=odgovor.vrniOdgovore();
		napacniOdgovori.add(vprasanje.getOdgovor());
		vprVrstica.setText(vprasanje.getVprasanje());

		if(vprasanje.getOdgovor().length()>1){
			Collections.shuffle(napacniOdgovori);
			
			gumb1.setOdgovor(napacniOdgovori.get(0));
			gumb2.setOdgovor(napacniOdgovori.get(1));
			gumb3.setOdgovor(napacniOdgovori.get(2));
			gumb4.setOdgovor(napacniOdgovori.get(3));
			int [] b = new int[4];
			int odgSize = napacniOdgovori.size();
			for(int i=0; i<odgSize; i++){
				String odg = napacniOdgovori.get(i);
				if(odg.equals("rdeca")) b[i]=barve[0];
				else if(odg.equals("rumena")) b[i]=barve[1];
				else if(odg.equals("zelena")) b[i]=barve[2];
				else if(odg.equals("modra")) b[i]=barve[3];
				else if(odg.equals("vijola")) b[i]=barve[4];
				else if(odg.equals("oranzna")) b[i]=barve[5];
				else if(odg.equals("rjava")) b[i]=barve[6];
				else if(odg.equals("svetlo zelena")) b[i]=barve[7];
			}
			gumb1.setGumbColor(b[0]);
			gumb2.setGumbColor(b[1]);
			gumb3.setGumbColor(b[2]);
			gumb4.setGumbColor(b[3]);

		}else{
			gumb1.setGumbColor(barve[rand.nextInt(8)]);
			gumb2.setGumbColor(barve[rand.nextInt(8)]);
			gumb3.setGumbColor(barve[rand.nextInt(8)]);
			gumb4.setGumbColor(barve[rand.nextInt(8)]);
			Collections.shuffle(napacniOdgovori);
			gumb1.setOdgovor(napacniOdgovori.get(0));
			gumb2.setOdgovor(napacniOdgovori.get(1));
			gumb3.setOdgovor(napacniOdgovori.get(2));
			gumb4.setOdgovor(napacniOdgovori.get(3));
		}
		gumb1.invalidate();
		gumb2.invalidate();
		gumb3.invalidate();
		gumb4.invalidate();
	}
	
	public int[] barve;
	private void pretvori(int[] b){
		for(int i=0; i<b.length; i++){
			barve[i]=b[i];
		}
	}
	public void startGame(){
		barve = new int[8];
		 int[] vseBarve= {Color.RED, Color.YELLOW, Color.rgb(34, 139, 34), Color.BLUE, Color.rgb(186, 85, 211), Color.rgb(244, 164, 96),
							Color.rgb(139, 69, 19), Color.rgb(124, 252, 0)}; 
		pretvori(vseBarve);
		myHandler = new Handler();
		myHandler2 = new Handler();
		myProgressBar = (ProgressBar) findViewById(R.id.progress);
		myProgressBar.setEnabled(false);
		new Thread(myThread).start();
		rand = new Random();
		tocke = (TextView) findViewById(R.id.tocke);

		gumb1= (Gumb) findViewById(R.id.gumb1);
		gumb2= (Gumb) findViewById(R.id.gumb2);
		gumb3= (Gumb) findViewById(R.id.gumb3);
		gumb4= (Gumb) findViewById(R.id.gumb4);
		
		gumb1.setOdgovor("prvi");
		gumb2.setOdgovor("drugi");
		gumb3.setOdgovor("tretji");
		gumb4.setOdgovor("cetrti");
		
		gumb1.setXSpeed(rand.nextInt(5)+2);
		gumb1.setYSpeed(rand.nextInt(4)+1);
		gumb2.setXSpeed(rand.nextInt(6)+2);
		gumb2.setYSpeed(rand.nextInt(5)+2);
		gumb3.setXSpeed(rand.nextInt(2)+3);
		gumb3.setYSpeed(rand.nextInt(4)+1);
		gumb4.setXSpeed(rand.nextInt(2)+3);
		gumb4.setYSpeed(rand.nextInt(4)+2);
		
		gumb1.setSmer(rand.nextInt(4));
		gumb2.setSmer(rand.nextInt(4));
		gumb3.setSmer(rand.nextInt(4));
		gumb4.setSmer(rand.nextInt(4));
		
		vprVrstica = (VprasanjaView) findViewById(R.id.vprasanjaView1);
		vprVrstica.setText("Prvo vprasanje");
		gumb1.setOnTouchListener(touchListener1);
		gumb2.setOnTouchListener(touchListener2);
		gumb3.setOnTouchListener(touchListener3);
		gumb4.setOnTouchListener(touchListener4);
		napacniOdgovori = new ArrayList<String>();
		odgovor = new IzbiraOdgovorov();

		balls = new Gumb[NUM_BALLS];
		balls[0]=gumb1;
		balls[1]=gumb2;
		balls[2]=gumb3;
		balls[3]=gumb4;
		
		final Runnable mUpdateResults = new Runnable() {
            public void run() {
                gameLoop();
            }
        };
        
		gumbTimer = new Timer("PremikanjeGumbov");
		gumbTimer.scheduleAtFixedRate(new TimerTask() {			
			@Override
			public void run() {
				myHandler.post(mUpdateResults);
			}
		}, 0, 20);
		
		novoVprasanje();
	}
	
	private void gameLoop(){

		if(myProgress<1){
			gameOver();
		}else{			
		    for ( int i = 0; i <NUM_BALLS; i++ ) {
		      	  for ( int j = 0; j < i; j ++ ){
		      		  balls[i].collide(balls[j]);
		      	}
		    }
			onProgressUpdate(gumb1);
			gumb1.invalidate();
			onProgressUpdate(gumb2);
			gumb2.invalidate();
			onProgressUpdate(gumb3);
			gumb3.invalidate();
			onProgressUpdate(gumb4);
			gumb4.invalidate();			
		}
	}
	
	protected void onProgressUpdate(Gumb g){
		update(g);
	}
	
	private void update(Gumb g) {
		
		ballX = g.getX();
		ballY = g.getY();
		ballRadius = g.vrniRadij();

		ballSpeedX = g.getXSpeed();
		ballSpeedY = g.getYSpeed();
		
		ballX += ballSpeedX;
		ballY += ballSpeedY;
		
	    xMin=0;
	    yMin=1*height/8;
	    xMax=9*width/10;
	    yMax=8*height/10;
	    
	    // Detect collision and react
	    if (ballX + ballRadius > xMax) {
	       ballSpeedX = -ballSpeedX;
	       ballX = xMax-ballRadius;
	    } else if (ballX - ballRadius < xMin) {
	       ballSpeedX = -ballSpeedX;
	       ballX = xMin+ballRadius;
	    }
	    if (ballY + ballRadius > yMax) {
	       ballSpeedY = -ballSpeedY;
	       ballY = yMax - ballRadius;
	    } else if (ballY - ballRadius < yMin) {
	       ballSpeedY = -ballSpeedY;
	       ballY = yMin + ballRadius;
	    }

	    g.setXSpeed((int) ballSpeedX);
	    g.setYSpeed((int) ballSpeedY);
	    g.setX(ballX);
	    g.setY(ballY);
	}		
}
