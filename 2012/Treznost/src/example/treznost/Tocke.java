package example.treznost;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

import android.os.Environment;
import android.util.Log;


public class Tocke {
	private int noveTocke;
	private DataOutputStream dat;
	private DataInputStream dat2;
	private int max1=0;
	private int max2=0;
	private int max3=0;
	private int max4=0;
	private int max5=0;
	private int t;
	private String FILE;
	
	
	public Tocke(){
		FILE=Environment.getExternalStorageDirectory()+"/tocka.bin";
	}
	
	public Tocke(String tocke){
		noveTocke=Integer.parseInt(tocke);		
	}
	
	public int vrnimax1(){
		return max1;
	}
	public int vrnimax2(){
		return max2;
	}
	public int vrnimax3(){
		return max3;
	}
	public int vrnimax4(){
		return max4;
	}
	public int vrnimax5(){
		return max5;
	}
	
	
	public boolean datoteka(){
    	if(Environment.MEDIA_MOUNTED.equals(Environment.getExternalStorageState())) Log.d("SD", "card available");
    	else{
    		Log.d("SD", "no memory card available"); 
    		return false;
    	}
    	FILE=Environment.getExternalStorageDirectory()+"/tocka.bin";
    	
    	try {
			dat=new DataOutputStream(new FileOutputStream(FILE, true));
			
			
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return false;
		}
    	
    	
	    try {
			dat.writeInt(noveTocke);
			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	    try {
			dat.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return false;
		}
	   // Log.d("tocke", noveTocke+"");
	  
	preberiTocke();
		return true;
    	
    }

	public void preberiTocke() {
		try {
			dat2=new DataInputStream(new FileInputStream(FILE));
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	    	    
	    while(true){
			   
			   try {
				t=dat2.readInt();
				
			} catch (IOException e) {
				Log.d("read", "End of file");
				break;
			}
			  if(t>max1){
				  max1=t;
				
			  }
			  else{
				  if(t>max2){
					  max2=t;
					
				  }
				  else{
					  if(t>max3){
				  
					  max3=t;
					  }
					  else{
						  if(t>max4){
							  max4=t;
						  }
						  else if(t>max5){
							  max5=t;
						  }
					  }
					
				  }
			  }
	    }	    
	}
	
	public int zadnjeTocke(){

		try {
			dat2=new DataInputStream(new FileInputStream(FILE));
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		while(true){
			 try {
				t=dat2.readInt();
				
			} catch (IOException e) {
				Log.d("read", "End of file");
				break;
			}
		}		
		return t;		
	}
}
