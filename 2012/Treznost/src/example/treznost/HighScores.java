package example.treznost;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

public class HighScores extends Activity {
		
	private TextView view;
	private TextView view2;
	private TextView view3;
	private TextView view4;
	private TextView view5;
	
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_high_scores);
        
        Tocke mojeTocke=new Tocke();
        mojeTocke.preberiTocke();   
        
        view=(TextView) findViewById(R.id.tocke);
        view.setText(Integer.toString(mojeTocke.vrnimax1()));
        view2=(TextView) findViewById(R.id.tocke2);
        view2.setText(Integer.toString(mojeTocke.vrnimax2()));       
        view3=(TextView) findViewById(R.id.tocke3);
       	view3.setText(Integer.toString(mojeTocke.vrnimax3()));
       	view4=(TextView) findViewById(R.id.tocke4);
       	view4.setText(Integer.toString(mojeTocke.vrnimax4()));    
       	view5=(TextView) findViewById(R.id.tocke5);
       	view5.setText(Integer.toString(mojeTocke.vrnimax5()));    
    }
      
    public void back(View v){
    	finish();
    }   
}
