package example.treznost;

import android.os.Bundle;
import android.view.View;
import android.widget.TextView;
import android.app.Activity;


public class GameOver extends Activity {
	
    @Override
    public void onCreate(Bundle savedInstanceState) {
       super.onCreate(savedInstanceState);
       setContentView(R.layout.activity_game_over);
       Tocke mojeTocke=new Tocke();
       int tocke = mojeTocke.zadnjeTocke();
       TextView view=(TextView) findViewById(R.id.scoreText);
       view.setText(Integer.toString(tocke));
       
       TextView trezn=(TextView) findViewById(R.id.treznost);
       if(tocke<20){
    	   trezn.setText("Totaly wasted!");
       }else if(tocke<30){
    	   trezn.setText("Drunk!");
       }else if(tocke<50){
    	   trezn.setText("A bit too much!");
       }else if(tocke<70){
    	   trezn.setText("Tipsy!");
       }else if(tocke>=70){
    	   trezn.setText("Sober!");
       }        
    }  
    
    public void nazaj(View v){
    	finish();
    }
    
}
