package fri.android;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;

public class NastavitevDolga2 extends Activity{

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
        setContentView(R.layout.nastavitve2);

        Button b55= (Button) findViewById(R.id.button55);
        b55.setOnClickListener(new OnClickListener() {
			public void onClick(View arg0) {
				Intent i = new Intent(NastavitevDolga2.this, SMS.class);
				startActivity(i);
			}
		});
	}
}
