

package fri.android;

import java.util.ArrayList;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnKeyListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;

public class DolgoviActivity extends Activity implements OnClickListener, OnKeyListener {
    private EditText mUserText;
    
    private ArrayAdapter<String> mAdapter;
    
    private ArrayList<String> mStrings = new ArrayList<String>();
    
    
    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);

        
        mAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, mStrings);
        
        //setListAdapter(mAdapter);
        ListView lv = (ListView) findViewById(R.id.listView1);
        lv.setAdapter(mAdapter);
        
        
        mUserText = (EditText) findViewById(R.id.userText);

        mUserText.setOnClickListener(this);
        mUserText.setOnKeyListener(this);
        
        Button b= (Button) findViewById(R.id.button2);
        b.setOnClickListener(new OnClickListener() {
			public void onClick(View arg0) {
				Intent i = new Intent(DolgoviActivity.this, Posojila.class);
				startActivity(i);
			}
		});
        
        Button b3 = (Button) findViewById(R.id.button3);
        b3.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				Intent i = new Intent(DolgoviActivity.this, NastavitevDolga.class);
				startActivity(i);
			}
		});
        
//        lv.setOnClickListener(new OnClickListener() {
//			
//			@Override
//			public void onClick(View v) {
//     		   Intent i = new Intent(DolgoviActivity.this, NastavitevDolga.class);
//  				startActivity(i);  
//				
//			}
//		}); 
        
        lv.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
					long arg3) {
				Intent i = new Intent(DolgoviActivity.this, NastavitevDolga.class);
				startActivity(i);  
				
			}
		});
    }

    public void onClick(View v) {
        sendText();
        
    }

    private void sendText() {
        String text = mUserText.getText().toString();
        mAdapter.add(text);
        mUserText.setText(null);
       
    }

    public boolean onKey(View v, int keyCode, KeyEvent event) {
        if (event.getAction() == KeyEvent.ACTION_DOWN) {
            switch (keyCode) {
                case KeyEvent.KEYCODE_DPAD_CENTER:
                case KeyEvent.KEYCODE_ENTER:
                    sendText();
                    
                    return true;
                    
            }
        }
        return false;
    }
}

