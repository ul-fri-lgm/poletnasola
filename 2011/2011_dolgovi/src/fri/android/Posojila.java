

package fri.android;

import java.util.ArrayList;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnKeyListener;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.AdapterView.OnItemClickListener;

public class Posojila extends Activity  implements OnClickListener, OnKeyListener {
    private EditText mUserText;
    private ArrayAdapter<String> mAdapter;
    private ArrayList<String> mStrings = new ArrayList<String>();
    
    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.posojila);
        
        Button b1= (Button) findViewById(R.id.button1);
        b1.setOnClickListener(new OnClickListener() {
			public void onClick(View arg0) {
				Intent i = new Intent(Posojila.this, DolgoviActivity.class);
				startActivity(i);
			}
		});
       
        Button b3= (Button) findViewById(R.id.button3);
        b3.setOnClickListener(new OnClickListener() {
			public void onClick(View arg0) {
				Intent i = new Intent(Posojila.this, NastavitevDolga2.class);
				startActivity(i);
			}
        });

        mAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, mStrings);

        //setListAdapter(mAdapter);
        ListView lv = (ListView) findViewById(R.id.listView1);
        lv.setAdapter(mAdapter);
        
        mUserText = (EditText) findViewById(R.id.userText1);

        mUserText.setOnClickListener(this);
        mUserText.setOnKeyListener(this);
        

    
    lv.setOnItemClickListener(new OnItemClickListener() {

		@Override
		public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
				long arg3) {
			Intent i = new Intent(Posojila.this, NastavitevDolga2.class);
			startActivity(i);  
			
		}
	});
}
    private void sendText() {
        String text = mUserText.getText().toString();
        mAdapter.add(text);
        mUserText.setText(null);
       
    }

    public boolean onKey(View v, int keyCode, KeyEvent event) {
        if (event.getAction() == KeyEvent.ACTION_DOWN) {
            switch (keyCode) {
                case KeyEvent.KEYCODE_DPAD_CENTER:
                case KeyEvent.KEYCODE_ENTER:
                    sendText();
                    
                    return true;
                    
            }
        }
        return false;
    }

	@Override
	public void onClick(View v) {
        sendText();
        //setListAdapter(mAdapter);
        ListView lv = (ListView) findViewById(R.id.listView1);
        lv.setAdapter(mAdapter);
		
	}
}

