package fri.android;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;

public class Zmaga extends Activity {
	
	public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.zmagovalec);
        
        SharedPreferences sharedPrefs = getSharedPreferences("PrefFile", MODE_PRIVATE);
        int tockePrvega = sharedPrefs.getInt("TockePrvega",0);
        //int tockeDrugega = sharedPrefs.getInt("TockeDrugega",0);
		String ime3 = sharedPrefs.getString("PrviIgralec", "");
		String ime4 = sharedPrefs.getString("DrugiIgralec", "");
		
        
		if(tockePrvega == 5){
        	TextView L=(TextView)findViewById(R.id.textView22);
        	L.setText(ime3);
        	
		} else {
			TextView M = (TextView)findViewById(R.id.textView22);
			M.setText(ime4);
        }

		
		Button D = (Button) findViewById(R.id.button111);
		D.setOnClickListener(new OnClickListener() {
			public void onClick(View arg0) {

				SharedPreferences preferences = getSharedPreferences("PrefFile",MODE_PRIVATE);
				SharedPreferences.Editor editor = preferences.edit();
				editor.putString("pavza", "false");
				editor.commit();
				Intent i4 = new Intent(Zmaga.this, RefleksActivity.class);
				startActivity(i4);
				
			};
			});
	

}
}