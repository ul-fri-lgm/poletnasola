package fri.android;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;

public class Imeno extends Activity{
	boolean firstTime = true;
	
	public void onCreate(Bundle savedInstanceState){
		super.onCreate(savedInstanceState);
        setContentView(R.layout.imena);
        
        
        Button p=(Button) findViewById(R.id.button17);
        p.setOnClickListener(new OnClickListener(){
        	public void onClick(View arg0){
        		Intent i6 = new Intent(Imeno.this, progressbar.class);
        		EditText et1 = (EditText)findViewById(R.id.editText1);
        		EditText et2 = (EditText)findViewById(R.id.editText2);
        		
        		SharedPreferences preferences = getSharedPreferences("PrefFile", MODE_PRIVATE);
        		SharedPreferences.Editor editor = preferences.edit();
        		editor.putString("PrviIgralec", et1.getText().toString());
        		editor.putString("DrugiIgralec", et2.getText().toString());
        		editor.commit();
        		
        		startActivity(i6);

			}
        });

	}
	@Override
	public void onStart() {
		super.onStart();
		if(!firstTime) {
			finish();
		}
		firstTime = false;
	}
	
}



