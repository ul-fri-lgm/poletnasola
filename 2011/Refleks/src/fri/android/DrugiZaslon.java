package fri.android;

import java.io.File;
import java.io.IOException;
import java.util.Random;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.media.AudioFormat;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class DrugiZaslon extends Activity {
	public static int tockePrvega = 0, tockeDrugega = 0;
	public static boolean JsSmPrvi = false;
	int nene = 0;
	int Nene = 0;
	int zacetek = 0;
	long sistemskiCas = 0;
	long sekunde = 0;
	boolean flag = true;
	int izbranaBarva = 0;
	int prikazanaBarva = 0;
	MyHandler handler = new MyHandler();
	final String imenaBarv[] = { "rdeca", "modra", "zelena", "siva", "crna",
			"rumena", "bela" };
	final int barve[] = { Color.RED, Color.BLUE, Color.GREEN, Color.GRAY,
			Color.BLACK, Color.YELLOW, Color.WHITE };

	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.second);
	

		SharedPreferences preferences = getSharedPreferences("PrefFile",MODE_PRIVATE);
		boolean pavza = preferences.getString("pavza", "false").equals("true") ? true : false;
		
		if(!pavza)
		{
		SharedPreferences.Editor editor = preferences.edit();
		editor.putInt("TockeDrugega", 0);
		editor.putInt("TockePrvega", 0);
		editor.commit();
		tockePrvega = 0; 
		tockeDrugega = 0;
		JsSmPrvi=false;
		zacetek=0;
		sistemskiCas=0;
		sekunde=0;
		flag=true;
		izbranaBarva=0;
		prikazanaBarva=0;
		}
		else

		sistemskiCas = System.currentTimeMillis();
		izbranaBarva = (int) (Math.random() * imenaBarv.length);
		TextView tv = (TextView) findViewById(R.id.TextView15);
		tv.setText("Klikni ko bo barva ozadja " + imenaBarv[izbranaBarva] + "!");
		tv.invalidate();
		tv = (TextView) findViewById(R.id.cas);
		tv.setText("Klikni ko bo barva ozadja " + imenaBarv[izbranaBarva] + "!");
		tv.invalidate();

		Thread t = new Thread(new Runnable() {

			@Override
			public void run() {
				while (flag) {
					sekunde = (System.currentTimeMillis() - sistemskiCas);
					if (sekunde > 1000) {
						sistemskiCas = System.currentTimeMillis();
						handler.post(new Runnable() {
							public void run() {
								Random r = new Random();
								View v = findViewById(R.id.ozadje);
								prikazanaBarva = r.nextInt(barve.length);
								v.setBackgroundColor(barve[prikazanaBarva]);
							}
						});
					}
					handler.post(new Runnable() {
						@Override
						public void run() {
							TextView textViewCas = (TextView) findViewById(R.id.cas);
							textViewCas.setText("Klikni ko bo barva ozadja "
									+ imenaBarv[izbranaBarva] + "!");
							textViewCas.invalidate();
							textViewCas = (TextView) findViewById(R.id.TextView15);
							textViewCas.setText("Klikni ko bo barva ozadja "
									+ imenaBarv[izbranaBarva] + "!");
						}
					});
					try {
						Thread.sleep(100);
					} catch (Exception e) {
					}
				}
			}
		});
		t.start();

		Random r = new Random();
		View v = findViewById(R.id.ozadje);
		v.setBackgroundColor(barve[r.nextInt(barve.length)]);

		Button I = (Button) findViewById(R.id.button11);
		I.setOnClickListener(new OnClickListener() {
			public void onClick(View arg0) {
				SharedPreferences preferences = getSharedPreferences("PrefFile",MODE_PRIVATE);
				SharedPreferences.Editor editor = preferences.edit();
				editor.putString("pavza", "true");
				editor.commit();
				Intent i = new Intent(DrugiZaslon.this, RefleksActivity.class);
				startActivity(i);

				zacetek++;
				if (sekunde < 10) {
					flag = false;
					Intent i1 = new Intent(DrugiZaslon.this,
							RefleksActivity.class);
					startActivity(i1);
				}
			}
		});

		TextView tv1 = (TextView) findViewById(R.id.TextView15);
		TextView tv2 = (TextView) findViewById(R.id.cas);
		tv1.setOnTouchListener(new OnTouchListener() {

			@Override
			public boolean onTouch(View v, MotionEvent event) {
				if (event.getAction() == MotionEvent.ACTION_DOWN) {
					Log.d("INTERAKCIJA", "Prvi");

					if (izbranaBarva == prikazanaBarva) {
						Log.d("INTERAKCIJA", "Juhej");
						if (JsSmPrvi == false) {
							JsSmPrvi = true;
							KdojeBoljsi(1);

						}
					} else {
						OdstejTockoBednemuIgralcu(1);
						Log.d("INTERAKCIJA", "BU!");
					}
					SharedPreferences preferences = getSharedPreferences(
							"PrefFile", MODE_PRIVATE);
					String ime1 = preferences.getString("PrviIgralec", "");

					TextView tv = (TextView) findViewById(R.id.textView19);
					tv.setText(ime1 + " ima " + tockePrvega + " tock.");

					izbranaBarva = (int) (Math.random() * imenaBarv.length);
					TextView textViewCas = (TextView) findViewById(R.id.cas);
					textViewCas.setText("Klikni ko bo barva ozadja "
							+ imenaBarv[izbranaBarva] + "!");
					textViewCas.invalidate();
					textViewCas = (TextView) findViewById(R.id.TextView15);
					textViewCas.setText("Klikni ko bo barva ozadja "
							+ imenaBarv[izbranaBarva] + "!");
				}

				return false;
			}
		});
		tv2.setOnTouchListener(new OnTouchListener() {

			@Override
			public boolean onTouch(View v, MotionEvent event) {
				if (event.getAction() == MotionEvent.ACTION_DOWN) {
					Log.d("INTERAKCIJA", "Drugi");

					if (izbranaBarva == prikazanaBarva) {
						Log.d("INTERAKCIJA", "Juhej");
						if (JsSmPrvi == false) {
							JsSmPrvi = true;
							KdojeBoljsi(2);
						}
					} else {
						OdstejTockoBednemuIgralcu(2);
						Log.d("INTERAKCIJA", "BU!");
					}

					SharedPreferences preferences = getSharedPreferences(
							"PrefFile", MODE_PRIVATE);
					String ime2 = preferences.getString("DrugiIgralec", "");

					TextView tv = (TextView) findViewById(R.id.textView20);
					tv.setText(ime2 + " ima " + tockeDrugega + " tock.");

					izbranaBarva = (int) (Math.random() * imenaBarv.length);
					TextView textViewCas = (TextView) findViewById(R.id.cas);
					textViewCas.setText("Klikni ko bo barva ozadja "
							+ imenaBarv[izbranaBarva] + "!");
					textViewCas.invalidate();
					textViewCas = (TextView) findViewById(R.id.TextView15);
					textViewCas.setText("Klikni ko bo barva ozadja "
							+ imenaBarv[izbranaBarva] + "!");

				}

				return false;
			}

		});
	}

	protected void OdstejTockoBednemuIgralcu(int player) {
		if (player == 1) {
			tockePrvega--;
		} else {
			tockeDrugega--;
		}
		if (tockePrvega == -5 || tockeDrugega == -5) {
			Log.d("INTERAKCIJA", "BEJZ VN!!!");

			TextView et13 = (TextView) findViewById(R.id.textView22);
			TextView et24 = (TextView) findViewById(R.id.textView22);

			SharedPreferences preferences = getSharedPreferences("PrefFile",
					MODE_PRIVATE);
			SharedPreferences.Editor editor = preferences.edit();
			editor.putInt("TockeDrugega", tockeDrugega);
			editor.putInt("TockePrvega", tockePrvega);
			editor.commit();
			Intent i9 = new Intent(DrugiZaslon.this, lols.class);
			startActivity(i9);

		}
	}

	protected void KdojeBoljsi(int igralec) {
		if (igralec == 1)
			tockePrvega++;
		else
			tockeDrugega++;

		Log.d("INTERAKCIJA", "Prvi ima " + tockePrvega + " tock.");
		Log.d("INTERAKCIJA", "Drugi ima " + tockeDrugega + " tock.");

		JsSmPrvi = false;

		if (tockePrvega == 5 || tockeDrugega == 5) {
			Log.d("INTERAKCIJA", "BEJZ VN!!!");

			TextView et13 = (TextView) findViewById(R.id.textView22);
			TextView et24 = (TextView) findViewById(R.id.textView22);

			SharedPreferences preferences = getSharedPreferences("PrefFile",MODE_PRIVATE);
			SharedPreferences.Editor editor = preferences.edit();
			editor.putInt("TockeDrugega", tockeDrugega);
			editor.putInt("TockePrvega", tockePrvega);
			editor.commit();
			Intent i8 = new Intent(DrugiZaslon.this, Zmaga.class);
			startActivity(i8);
		}

	}

}

class MyHandler extends Handler {

}
