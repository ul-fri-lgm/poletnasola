package fri.android;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;

public class RefleksActivity extends Activity {
	/** Called when the activity is first created. */
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.main);
		final Button start = (Button) findViewById(R.id.button1);
		start.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				// start new activity -

				Intent i3 = new Intent(RefleksActivity.this, Imeno.class);
				startActivity(i3);
			}
		});

		Button K = (Button) findViewById(R.id.Button01);
		K.setOnClickListener(new OnClickListener() {
			public void onClick(View arg0) {
				Intent i4 = new Intent(RefleksActivity.this, DrugiZaslon.class);
				startActivity(i4);
			}
		});
		
		Button L = (Button) findViewById(R.id.izhod);
		L.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				Intent intent = new Intent(Intent.ACTION_MAIN);
				intent.addCategory(Intent.CATEGORY_HOME);
				intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
				startActivity(intent);
			}

		});
		
		Button D = (Button) findViewById(R.id.button2);
		D.setOnClickListener(new OnClickListener() {
			public void onClick(View arg0) {
				Intent i5 = new Intent(RefleksActivity.this, pravila.class);
				startActivity(i5);
			}
		});
	}
}