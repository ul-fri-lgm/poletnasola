package fri.android;

import java.util.Random;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

public class Iqtestscreen extends Activity {
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.firstscreen);

		final int[] w = new int[20];
		TextView tv = (TextView) findViewById(R.id.textView3);

		final Random rand = new Random();
		String[] questions = getResources().getStringArray(R.array.questions);
		final SharedPreferences preferences = getSharedPreferences(
				"Nastavitve", MODE_PRIVATE);

		int index = preferences.getInt("IndexVprasanja", -1);
		int r = 0;
		if (index == -1) {
			r = rand.nextInt(questions.length - 1);
		} else {
			r = index;
		}
		// SharedPreferences.Editor editor = preferences.edit();
		// editor.putInt("Indexer", r);
		// editor.commit();

		Button naprej = (Button) findViewById(R.id.button1);
		naprej.setOnClickListener(new OnClickListener() {
			final int[] w = new int[20];
			int answercount = 0;
			@Override
			public void onClick(View v) {
				TextView rb1 = (TextView) findViewById(R.id.radio0);
				TextView rb2 = (TextView) findViewById(R.id.radio1);
				TextView tv = (TextView) findViewById(R.id.textView3);

				final Random rand = new Random();

				String[] ca = getResources().getStringArray(
						R.array.correctanswers);
				String[] questions = getResources().getStringArray(
						R.array.questions);
				String[] wa = getResources().getStringArray(
						R.array.wronganswers);

				boolean flag = false;
				for (int i = 0; i < w.length; i++) {
					if (w[i] == 0) {
						SharedPreferences preferences = getSharedPreferences(
								"Nastavitve", MODE_PRIVATE);
						int index = preferences.getInt("IndexVprasanja", -1);
						int r = 0;
						r = rand.nextInt(questions.length - 1);
						SharedPreferences.Editor editor = preferences.edit();
						editor.putInt("Indexer", r);
						editor.commit();
						tv.setText(questions[r]);
						Random random = new Random();
						if (random.nextFloat() < 0.5) {
							rb1.setText(ca[r]);
							rb2.setText(wa[r]);
						} else {
							rb2.setText(ca[r]);
							rb1.setText(wa[r]);
						}
						w[i]++;
						break;
					}
					int roll = preferences.getInt("Indexer", 0);
					SharedPreferences.Editor editor = preferences.edit();
					editor.putInt("Index", roll);
					editor.commit();

					int odgovor = 0;
					RadioGroup rg = (RadioGroup) findViewById(R.id.radioGroup1);

					for (int d = 0; d < rg.getChildCount(); d++) {
						if (((RadioButton) rg.getChildAt(d)).isChecked())
							odgovor = d;
					}
					String ans = ((RadioButton) rg.getChildAt(odgovor))
							.getText().toString();
					for (int j = 0; j < ca.length; j++) {
						if (ca[j].equals(ans)) {
							flag = true;
							break;
						}
					}
				}
				if (flag) {
					answercount++;
					flag = false;
					Log.i("fu", ""+answercount);
				}

				if (w[w.length - 1] == 1) {
					Context context = getApplicationContext();
					CharSequence text = "Your final score is: " + answercount
							+ "/" + (w.length);
					int duration = 100;

					Toast toast = Toast.makeText(context, text, duration);
					toast.show();
					Thread t = new Thread(new Runnable() {

						@Override
						public void run() {
							try {
								Thread.sleep(5000);
								System.exit(1);
							} catch (Exception e) {
							}
						}
					});
					t.start();
				}

			}
		});
	}
    	protected void onSaveInstanceState(Bundle saveFile) {
    	int odgovor = 0;
    	RadioGroup rg = (RadioGroup)findViewById(R.id.radioGroup1);
    	TextView tv = (TextView)findViewById(R.id.textView3);
    	String r = tv.getText().toString();
    	saveFile.putString("question", r);
    	for (int i = 0; i < rg.getChildCount(); i++) {
    		if (((RadioButton)rg.getChildAt(i)).isChecked())
    			odgovor = i;
    	}
    	for (int i=0; i<20; i++) {
    		saveFile.putString ("odgovor "+i , ""+odgovor );
    	}
    
    }
}