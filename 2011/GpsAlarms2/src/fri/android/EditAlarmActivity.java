package fri.android;

import fri.android.GPSAlarmsActivity.AlarmListAdapter;
import fri.android.GPSAlarmsService.MyInterface;
import android.app.Activity;
import android.content.ComponentName;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.IBinder;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ExpandableListView;
import android.widget.SeekBar;
import android.widget.TextView;

public class EditAlarmActivity extends Activity {
	Alarm toEdit;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.main);
		Button ok = (Button) findViewById(R.id.button1);
		ok.setText(R.string.edit);
		bindService( new Intent("fri.android.SERVICE", null, EditAlarmActivity.this, GPSAlarmsService.class), new ServiceConnection() {	
			@Override
			public void onServiceDisconnected(ComponentName name) {finish();}			
			@Override
			public void onServiceConnected(ComponentName cname, IBinder service) {				
				toEdit = ((MyInterface) service).get(getIntent().getIntExtra("INDEX", 0));
				((EditText) findViewById(R.id.name_edit)).setText(toEdit.getName());
				((EditText) findViewById(R.id.description_edit)).setText(toEdit.getDescription());
				((SeekBar) findViewById(R.id.radius_bar)).setProgress(toEdit.getRadius()-50);
				((TextView) findViewById(R.id.radius_view)).setText(getString(R.string.alarm_radius) + toEdit.getRadius() + " m");
				unbindService(this);
			}
		}, 0);
		SeekBar bar = (SeekBar) findViewById(R.id.radius_bar);
        bar.setPadding(10, 0, 10, 0);
        bar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
			
			@Override
			public void onStopTrackingTouch(SeekBar seekBar) {
				
			}
			
			@Override
			public void onStartTrackingTouch(SeekBar seekBar) {
				
			}
			
			@Override
			public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
				((TextView) findViewById(R.id.radius_view)).setText(getString(R.string.alarm_radius) + (progress + 50) + " m");
			}
		});
		ok.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				bindService( new Intent("fri.android.SERVICE", null, EditAlarmActivity.this, GPSAlarmsService.class), new ServiceConnection() {	
					@Override
					public void onServiceDisconnected(ComponentName name) {finish();}			
					@Override
					public void onServiceConnected(ComponentName cname, IBinder service) {				
						((MyInterface) service).remove(getIntent().getIntExtra("INDEX", 0));
						EditText name = (EditText) findViewById(R.id.name_edit);
						EditText description = (EditText) findViewById(R.id.description_edit);
						SeekBar radius = (SeekBar) findViewById(R.id.radius_bar);
						toEdit =  new Alarm(name.getText().toString(), description.getText().toString(), radius.getProgress() + 50);
						((MyInterface) service).add(toEdit);
					}
				}, 0);
				finish();
			}
		});
	}
	
}
