package fri.android;

import java.util.ArrayList;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.location.Location;
import android.location.LocationManager;
import android.os.Binder;
import android.os.IBinder;
import android.os.SystemClock;
import android.util.Log;

public class GPSAlarmsService extends Service {
	ArrayList<Alarm> alarmlist = new ArrayList<Alarm>();
	
	public class MyInterface extends Binder {
		public void add(Alarm object) {
			synchronized (alarmlist) {
				GPSAlarmsService.this.alarmlist.add(object);
				storeAlarms();
			}
		}
		
		public void remove(int index) {
			synchronized (alarmlist) {
				GPSAlarmsService.this.alarmlist.remove(index);
				storeAlarms();
			}
		}
		
		public Alarm get(int index) {
			synchronized (alarmlist) {
				return GPSAlarmsService.this.alarmlist.get(index);
			}
		}
		
		public ArrayList<Alarm> getList() {
			synchronized (alarmlist) {
				return new ArrayList<Alarm>(GPSAlarmsService.this.alarmlist);
			}
		}
		
		public void storeAlarms() {
			synchronized (alarmlist) {
				SharedPreferences prefs = getSharedPreferences("GPSAlarmsPrefs",
					MODE_PRIVATE);
				Editor e = prefs.edit();
				for (String key : prefs.getAll().keySet()) {
					if (key.startsWith("A_"))
						e.remove(key);
					Log.i("EST", "KEY: " + key);
				}
				e.commit();
				for (int i=0;i<alarmlist.size();i++) {
					e.putString("A_name:" + i, alarmlist.get(i).getName());
					e.putString("A_desc:" + i, alarmlist.get(i).getDescription());
					e.putInt("A_rad:" + i, alarmlist.get(i).getRadius());
					e.putFloat("A_zdol:" + i, (float) alarmlist.get(i).getZemljepisnaDolzina());
					e.putFloat("A_zsir:" + i, (float) alarmlist.get(i).getZemljepisnaSirina());
					Log.i("EST", "SAVING");
				}
				e.commit();
			}
		}
		
		public void loadAlarms() {
			synchronized (alarmlist) {
				SharedPreferences prefs = getSharedPreferences("GPSAlarmsPrefs", MODE_PRIVATE);
				alarmlist.clear();
				for (int i=0;true;i++) {
					if(!prefs.contains("A_name:" + i))
						break;
					Alarm tmp = new Alarm("", "", 0);
					tmp.setName(prefs.getString("A_name:" + i, ""));
					tmp.setDescription(prefs.getString("A_desc:" + i, ""));
					tmp.setRadius(prefs.getInt("A_rad:" + i, 0));
					tmp.setPosition(prefs.getFloat("A_zdol:" + i, 0), prefs.getFloat("A_zsir:" + i, 0));
					alarmlist.add(tmp);
				}
				Log.i("EST", "LOADED");
			}
		}
	}
	
	@Override
	public IBinder onBind(Intent intent) {
		if("fri.android.SERVICE".equals(intent.getAction()))
			Log.i("EST", "ONBIND");
		return new MyInterface();
	}

	@Override
	public void onCreate() {
		super.onCreate();
		Log.i("EST", "ONCREATE");
		AlarmManager am = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
		PendingIntent pi = PendingIntent.getService(this, 0, new Intent("fri.android.CHECK"), 0);
		am.set(AlarmManager.ELAPSED_REALTIME_WAKEUP, SystemClock.elapsedRealtime() + 30000, pi);
	}

	@Override
	public void onDestroy() {
		super.onDestroy();
		Log.i("EST", "ONDESTROY");
	}

	@Override
	public int onStartCommand(Intent intent, int flags, int startId) {
		Log.i("EST", "ONSTARTCOMMAND");
//		if(!intent.getAction().equals("fri.android.CHECK"))
//			return START_NOT_STICKY;
		Location test = ((LocationManager) getSystemService(LOCATION_SERVICE)).getLastKnownLocation(LocationManager.GPS_PROVIDER);
		if(test != null) {
			synchronized (alarmlist) {
				for(Alarm alarm : alarmlist) {
					Location alarmloc = new Location(LocationManager.GPS_PROVIDER);
					alarmloc.setLatitude(alarm.getZemljepisnaDolzina());
					alarmloc.setLongitude(alarm.getZemljepisnaSirina());
					if(test.distanceTo(alarmloc) <= alarm.getRadius()) {
						Log.i("EST", "ALARM!");
					}
				}
			}
		}
		AlarmManager am = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
		PendingIntent pi = PendingIntent.getService(this, 0, new Intent("fri.android.CHECK"), 0);
		am.set(AlarmManager.ELAPSED_REALTIME_WAKEUP, SystemClock.elapsedRealtime() + 30000, pi);
		return START_NOT_STICKY;
	}
}
