package fri.android;

import java.util.ArrayList;

import fri.android.GPSAlarmsService.MyInterface;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.ComponentName;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.database.DataSetObserver;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.IBinder;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ExpandableListAdapter;
import android.widget.ExpandableListView;
import android.widget.ListView;
import android.widget.TextView;


public class GPSAlarmsActivity extends Activity {

	class AlarmListAdapter implements ExpandableListAdapter {

		class DeleteListener implements OnClickListener {
			private int index;
			
			class RemoveListener implements DialogInterface.OnClickListener {

				@Override
				public void onClick(DialogInterface dialog, int which) {
					bindService( new Intent("fri.android.SERVICE", null, GPSAlarmsActivity.this, GPSAlarmsService.class), new ServiceConnection() {	
						@Override
						public void onServiceDisconnected(ComponentName name) {finish();}			
						@Override
						public void onServiceConnected(ComponentName cname, IBinder service) {				
							((MyInterface) service).remove(DeleteListener.this.index);
							((ExpandableListView) GPSAlarmsActivity.this.findViewById(R.id.listView1)).setAdapter(new AlarmListAdapter(((MyInterface) service).getList()));
							unbindService(this);
						}
					}, 0);
				}		
			}

			public DeleteListener(int obj) {
				index = obj;
			}

			@Override
			public void onClick(View v) {
				AlertDialog alertDialog = new AlertDialog.Builder(GPSAlarmsActivity.this).create();
				alertDialog.setButton(getString(R.string.delete), new RemoveListener());
				alertDialog.setButton2(getString(R.string.cancel), new DialogInterface.OnClickListener() {public void onClick(DialogInterface dialog, int which) {return;}});
				alertDialog.setMessage(getString(R.string.delete_alert));
				alertDialog.show();
			}

		}
		
		class CorrectListener implements OnClickListener {			
			private int index;
			
			public CorrectListener(int idx) {
				index = idx;
			}

			@Override
			public void onClick(View v) {
				Intent i = new Intent(GPSAlarmsActivity.this, EditAlarmActivity.class);
				i.putExtra("INDEX", index);
				startActivity(i);
			}
		}

		private ArrayList<Alarm> alarmlist;
		
		public AlarmListAdapter(ArrayList<Alarm> list) {
			alarmlist=list;
		}

		@Override
		public boolean areAllItemsEnabled() {
			return true;
		}

		@Override
		public Object getChild(int groupPosition, int childPosition) {
			return alarmlist.get(groupPosition);
		}

		@Override
		public long getChildId(int groupPosition, int childPosition) {
			return childPosition;
		}

		@Override
		public View getChildView(int groupPosition, int childPosition,
				boolean isLastChild, View convertView, ViewGroup parent) {
			View view = null;
			if (convertView != null) {
				if (convertView.findViewById(R.id.description_view) != null)
					view = convertView;
			}
			if (view == null)
				view = getLayoutInflater().inflate(R.layout.revealed, parent,
						false);
			TextView desc = (TextView) view.findViewById(R.id.description_view);
			desc.setText(alarmlist.get(groupPosition).getDescription());
			Button del = (Button) view.findViewById(R.id.button_delete);
			del.setOnClickListener(new DeleteListener(groupPosition));
			del = (Button) view.findViewById(R.id.button_correct);
			del.setOnClickListener(new CorrectListener(groupPosition));
			return view;
		}

		@Override
		public int getChildrenCount(int groupPosition) {
			return 1;
		}

		@Override
		public long getCombinedChildId(long groupId, long childId) {
			return groupId;
		}

		@Override
		public long getCombinedGroupId(long groupId) {
			return groupId;
		}

		@Override
		public Object getGroup(int groupPosition) {
			return alarmlist.get(groupPosition).getName();
		}

		@Override
		public int getGroupCount() {
			return alarmlist.size();
		}

		@Override
		public long getGroupId(int groupPosition) {
			return groupPosition;
		}

		@Override
		public View getGroupView(int groupPosition, boolean isExpanded,
				View convertView, ViewGroup parent) {
			View view = null;
			if (convertView != null) {
				if (convertView.findViewById(R.id.itemTextView) != null)
					view = convertView;
			}
			if (view == null)
				view = getLayoutInflater().inflate(R.layout.list_item, parent,
						false);
			TextView name = (TextView) view.findViewById(R.id.itemTextView);
			name.setText(alarmlist.get(groupPosition).getName());
			return view;
		}

		@Override
		public boolean hasStableIds() {
			return true;
		}

		@Override
		public boolean isChildSelectable(int groupPosition, int childPosition) {
			return false;
		}

		@Override
		public boolean isEmpty() {
			return alarmlist.isEmpty();
		}

		@Override
		public void onGroupCollapsed(int groupPosition) {
			// ni treba

		}

		@Override
		public void onGroupExpanded(int groupPosition) {
			// ni treba

		}

		@Override
		public void registerDataSetObserver(DataSetObserver observer) {
			// ni treba

		}

		@Override
		public void unregisterDataSetObserver(DataSetObserver observer) {
			// ni treba

		}

	}

	/** Called when the activity is first created. */
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.alarmlist);
		
		bindService( new Intent("fri.android.SERVICE", null, GPSAlarmsActivity.this, GPSAlarmsService.class), new ServiceConnection() {	
			@Override
			public void onServiceDisconnected(ComponentName name) {finish();}			
			@Override
			public void onServiceConnected(ComponentName cname, IBinder service) {				
				Log.d("EST", "Refresh");
				((MyInterface) service).loadAlarms();
				ExpandableListView listview = (ExpandableListView) findViewById(R.id.listView1);
				listview.setAdapter(new AlarmListAdapter(((MyInterface) service).getList()));
				unbindService(this);
			}
		}, 0);
		DisplayMetrics metrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(metrics);
		((ExpandableListView) findViewById(R.id.listView1)).setIndicatorBounds(metrics.widthPixels - (int) (50 * getResources().getDisplayMetrics().density + 0.5f), metrics.widthPixels - (int) (10 * getResources().getDisplayMetrics().density + 0.5f));
		Intent intent = new Intent(this, GPSAlarmsService.class);
		startService(intent);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.menu, menu);
		return true;
	}

	public boolean onOptionsItemSelected(MenuItem item) {
		Intent i = new Intent(this, AddAlarmActivity.class);
		startActivity(i);
		return true;

		/*
		 * switch(item.getItemId()) {
		 * 
		 * case (R.id.item1-1):
		 * 
		 * Log.i("EST","TO SE IZVEDE"); break; default:
		 * Log.i("EST",item.getItemId() + "-" + R.id.item1); } return true;
		 */
	}

	@Override
	protected void onResume() {
		super.onResume();
		bindService( new Intent("fri.android.SERVICE", null, GPSAlarmsActivity.this, GPSAlarmsService.class), new ServiceConnection() {	
			@Override
			public void onServiceDisconnected(ComponentName name) {finish();}			
			@Override
			public void onServiceConnected(ComponentName cname, IBinder service) {				
				Log.d("EST", "Refresh");
				ExpandableListView listview = (ExpandableListView) findViewById(R.id.listView1);
				listview.setAdapter(new AlarmListAdapter(((MyInterface) service).getList()));
				unbindService(this);
			}
		}, 0);
	}

	@Override
	protected void onStart() {
		super.onStart();
		
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
	}
}