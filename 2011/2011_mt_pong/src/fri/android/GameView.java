package fri.android;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Point;
import android.graphics.PointF;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;

public class GameView extends View {
	private int color = Color.WHITE;
	private Point no_size = new Point(0, 0);
	private Point pad_size = new Point(100, 40);
	private Point ball_size = new Point(20, 20);
	private Point screen_size = new Point();
	private Ball ball = null;
	private Players players = null;

	private Point fingerPos[] = new Point[2];
	
	/**
	 * Create a new view
	 * @param context just calling super..
	 * @param attrs just calling super...
	 */
	public GameView(Context context, AttributeSet attrs) {
		super(context, attrs);
		
		for (int i = 0; i != fingerPos.length; ++i)
			fingerPos[i] = new Point();
		
		Point bWindowStart = new Point (0, pad_size.y);
		Point bWindowSize = new Point (screen_size.x, screen_size.y - 2 * pad_size.y);
		ball = new Ball(this, bWindowStart, bWindowSize, ball_size, color);
	}
	
	/**
	 * intermediary function to get the Player object from players, called by Ball when something happens
	 * @param player the player to return (first player is number 0)
	 * @return player 
	 */
	public Player getPlayer(int player) {
		return players.getPlayer(player);
	}
	
	/**
	 * Overload required by extending View, redraws the screen
	 * @param canvas the canvas to draw on
	 */
	protected void onDraw(Canvas canvas) {
		super.onDraw(canvas);
		
		ball.draw(canvas);
		players.draw(canvas);
	}
	
	/**
	 * used for debugging, to be removed in final release
	 */
	void offsetView(PointF delta) {
		ball.translate(delta);
	}

	/**
	 * Overload required by extending View, (re)initializes internal structures, 
	 * should lock the screen into portrait mode
	 * @param w width of the new screen
	 * @param h height of the new screen
	 * @param oldw unused
	 * @param oldh unused
	 */
	protected void onSizeChanged(int w, int h, int oldw, int oldh) {
		super.onSizeChanged(w, h, oldw, oldh);
		
		screen_size.x = w;
		screen_size.y = h;
		players = new Players(screen_size, pad_size, color);
		
		ball.setScreenStart(new Point(0, players.getPlayer(1).getSize().y));
		ball.setScreenSize(new Point(screen_size.x, screen_size.y - pad_size.y));
		ball.setLocationCentered();
	}
	
	/**
	 * Checks if both players have their fingers on their pads and starts the game
	 */
	/*private boolean finger_0_is_on_pad_0;
	public void checkFingers() {
		if (getPlayer(0).elementIsOnSurface(fingerPos[0], no_size)) 
			finger_0_is_on_pad_0 = true;
		else if (getPlayer(1).elementIsOnSurface(fingerPos[0], no_size)) 
			finger_0_is_on_pad_0 = false;
		else
			return;
		
		if(finger_0_is_on_pad_0 == true && getPlayer(1).elementIsOnSurface(fingerPos[1], no_size))
			ball.start();
		else if (finger_0_is_on_pad_0 == false && getPlayer(0).elementIsOnSurface(fingerPos[1], no_size))
			ball.start();
		
	}*/
	
	/**
	 * Handles player input, moves the pads, makes people happy...
	 * @param event what had happened
	 * @return there was no time to read the docs
	 */
	public boolean onTouchEvent(MotionEvent event) {
		int actionCode = event.getAction() & MotionEvent.ACTION_MASK;
		
		switch (actionCode) {
			case MotionEvent.ACTION_DOWN:
				fingerPos[0].x = (int)event.getX();
				fingerPos[0].y = (int)event.getY();
				return true;
			case MotionEvent.ACTION_POINTER_DOWN:
				for (int i = 0; i != fingerPos.length; ++i) {
					fingerPos[i].x = (int)event.getX(i);
					fingerPos[i].y = (int)event.getY(i);
				}

				//checkFingers();
				ball.start();
        		return true;
			case MotionEvent.ACTION_UP:
				return true;
			case MotionEvent.ACTION_POINTER_UP:
        		if (event.getPointerCount() == 2) //count==2 if two fingers were down, but now one has gone up
        			ball.stop(); // if one player removed his finger, pause the game
        		return true;
			case MotionEvent.ACTION_MOVE:
				
				players.handle_pad_move(event, fingerPos);
				
				invalidate();
				return true;
 		}
		
		return super.onTouchEvent(event);
	}

}
