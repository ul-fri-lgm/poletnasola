package fri.android;

import android.graphics.Point;

public class Ball extends GElement {
	private GameView my_view;
	private boolean running = false;
	/**
	 * The ball animation thread
	 */
	private Runnable my_thread = new Runnable() {
		public void run() {
			try {
				Thread.sleep(1000);
			} catch (InterruptedException e) {
			}
			while (true) {
				try {
					if(running) {
					timedMove();
					my_view.postInvalidate();
					}
					Thread.sleep(5);
				} catch (Throwable t) {
				}
			}
		}
	};
	
	/**
	 * Creates a new Ball
	 * @param v the GameView that this ball's thread will call to update the screen
	 * @param sStart movement window's starting coordinates (x,y)
	 * @param sSize	 movement window's size (x,y)
	 * @param size	 the size of this ball (x,y)
	 * @param color	 the color to paint this ball with
	 */
	public Ball(GameView v, Point sStart, Point sSize, Point size, int color) {
		super(sStart, sSize, size, color);
		my_view = v;
		new Thread(my_thread).start();
	}

	/**
	 * Take some actions if the ball went out of the playground
	 * @param player the player that scored (last touched the ball before it went out)
	 */
	private void playerWon(int player) {
		my_view.getPlayer(player).increaseScore();
		
		setLocationCentered();
		setMoveVector(Util.randomDirection());
	}

	/**
	 * collision detection
	 * @return not used
	 */
	public boolean onTimedMove() {
		// left side
		if (my_location.x < my_screen_start.x)
			my_move_vector.x *= -1;
		// right side
		else if (my_location.x > my_screen_size.x) 
			my_move_vector.x *= -1;
		// top
		else if (my_location.y < my_screen_start.y) {
			if(my_view.getPlayer(0).elementIsOnSurface(my_location, my_size)) // ball is on my pad
				my_move_vector.y *= -1;
			else
				playerWon(1);
		// bottom
		} else if (my_location.y > my_screen_size.y) {
			if(my_view.getPlayer(1).elementIsOnSurface(my_location, my_size)) // ball is on my pad
				my_move_vector.y *= -1;
			else
				playerWon(0);
		}
		return true;
	}
	
	/**
	 * Start the animation thread
	 */
	public void start() {
		running = true;
	}

	/**
	 * stop the animation thread
	 */
	public void stop() {
		running = false;
	}
}
