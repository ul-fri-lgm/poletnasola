package fri.android;

import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.graphics.Canvas;
import android.graphics.Point;
import android.graphics.PointF;

public class GElement {
	private Bitmap my_bitmap = null;
	protected Point my_size = null;
	protected Point my_location = new Point(0, 0);
	protected Point my_screen_start;
	protected Point my_screen_size;
	protected PointF my_move_vector; 

	/**
	 * Create a new graphical element and initialise its direction to a random value
	 * @param size the size (x,y) of this element
	 * @param c the Color to be filled into the newly created element
	 */
	public GElement (Point size, int color) {
		my_move_vector = Util.randomDirection();
		my_size = size;

		int colorArray[] = new int [my_size.y * my_size.x];
		
		for (int i = 0; i != colorArray.length; ++i)
			colorArray[i] = color;
		my_bitmap = Bitmap.createBitmap(colorArray, my_size.x, my_size.y, Config.ARGB_8888);
	}
	
	/**
	 * Create a new graphical element and initialise its properties
	 * @param sStart movement window starting offset from 0, 0
	 * @param sSize movement window size
	 * @param size size of this element
	 * @param color its color
	 */
	public GElement (Point sStart, Point sSize, Point size, int color) {
		this(size, color);
		setScreenStart(sStart);
		setScreenSize(sSize);
	}

	/**
	 * Set this element's location to the center of its movement window, but do not draw it yet
	 */
	public void setLocationCentered() {
		my_location.x = my_screen_size.x / 2 + my_screen_start.x;
		my_location.y = my_screen_size.y / 2 + my_screen_start.y;
	}

	/**
	 * Set this element's movement window x and y coordinates
	 * @param start (x,y)
	 */
	public void setScreenStart(Point start) {
		my_screen_start = start;
	}

	/**
	 * Set this element's movement window size and update it by taking its width and height into account
	 * @param size (x,y)
	 */
	public void setScreenSize(Point size) {
		my_screen_size = size;
		my_screen_size.x -= my_size.x;
		my_screen_size.y -= my_size.y;
	}

	/**
	 * Set the movement vector governing where the element will move on the next update
	 * @param new_vector the new vector
	 */
	public void setMoveVector(PointF new_vector) {
		my_move_vector = new_vector;
	}

	/**
	 * Draw this graphical element on the canvas
	 * @param canvas the android.graphics.Canvas to draw to
	 */
	public void draw(Canvas canvas) {
		if(my_bitmap != null)
			canvas.drawBitmap(my_bitmap, my_location.x, my_location.y, null);
	}

	/**
	 * Set this element's X position
	 * @param pixels the new position
	 */
	public void setX(int x) {
		my_location.x = x;
	}
	
	/**
	 * Move this element on the X axis
	 * @param pixels the number to move
	 */
	public void moveX(int pixels) {
		my_location.x += pixels;
	}
	
	/**
	 * Move this graphical element relative to its current position 
	 * @param delta relative coordinates
	 */
	public void translate(PointF delta) {
		my_location.x += (int)Math.round(delta.x);
		my_location.y += (int)Math.round(delta.y);
	}
	
	/**
	 * To be executed from timer Threads
	 */
	public void timedMove() {
		if(onTimedMove())
			translate (my_move_vector);
	}

	/**
	 * Overridden by inheriting classes, get called by timedMove()
	 * @return whether we should update its position
	 */
	public boolean onTimedMove() {	
		return true;
	}
	
	/**
	 * Gets the element size (x,y)
	 * @return element size
	 */
	public Point getSize() {
		return my_size;
	}
	
	/**
	 * Checks if the given element is on this element
	 * @param loc the location of the given element
	 * @param size the size of the given element
	 * @return true if the given element is on this element
	 */
	public boolean elementIsOnSurface(Point loc, Point size) {
		if (   // left side
			my_location.x - size.x < loc.x
			&& // right side 
			loc.x < my_location.x + my_size.x
			&& // top
			my_location.y - size.y < loc.y
			&& // bottom
			loc.y < my_location.y + my_size.y
			)
				return true;
		return false;
	}
}
