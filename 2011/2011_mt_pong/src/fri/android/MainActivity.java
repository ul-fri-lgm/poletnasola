package fri.android;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;

public class MainActivity extends Activity {
	@Override
	protected void onPause() {
		// TODO Auto-generated method stub
		super.onPause();
		finish();
	}

	/** Called when the activity is first created. */
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.main);

		Button new_game_button			= (Button) findViewById (R.id.button1);
		Button enter_settings_button	= (Button) findViewById (R.id.button2);
		Button exit_button				= (Button) findViewById (R.id.button3);

		new_game_button.setOnClickListener (new OnClickListener() {
			public void onClick (View view) {
				Intent i = new Intent (MainActivity.this, GameActivity.class);
				startActivity (i);
			}
		});
        
		enter_settings_button.setOnClickListener (new OnClickListener() {
			public void onClick (View view) {
				Intent i = new Intent (MainActivity.this, Settings.class);
				startActivity (i);
			}
		});
        
		exit_button.setOnClickListener (new OnClickListener() {
			public void onClick (View view) {
				finish();
			}
		});
		
	}
	
	
}
