package fri.android;

import android.graphics.PointF;
import android.util.Log;

public class Util {
	static final int directions_x[] = { 1,  1, -1, -1 };
	static final int directions_y[] = { 1, -1, -1,  1 };
	/**
	 * returns a random direction expressed by x and y coordinates (-1, 0, 1)
	 * @return new Point holding the two coordinates
	 */
	static PointF randomDirection() {
		int i = (int)Math.round(Math.random() * directions_x.length);
		return new PointF(directions_x[i], directions_y[i]);
	}
	
	/**
	 * Sends a message to android.util.Log. Tag is "pong"
	 * @param s message
	 */
	public static void d(String s) {
		Log.d("pong", s);
	}
	
	// Utility functions for formating
	public static String fmt(android.graphics.Point p) {
		return "(" + p.y + ", " + p.x + ")";
	}
	public static String fmt(android.graphics.PointF p) {
		return "(" + p.y + ", " + p.x + ")";
	}
}
