package fri.android;

import android.graphics.Point;

public class Player extends GElement {
	public int score = 0;

	/**
	 * Creates a new player
	 * @param padSize the size of his pad
	 * @param color the color of his visible properties
	 */
	public Player(Point padSize, int color) {
		super(padSize, color);
	}

	/**
	 * Increases the player's score by 1
	 */
	public void increaseScore() {
		++score;
	}
}
