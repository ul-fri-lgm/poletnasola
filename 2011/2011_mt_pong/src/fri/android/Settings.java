package fri.android;

import android.app.Activity;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.SeekBar;
import android.widget.SeekBar.OnSeekBarChangeListener;


public class Settings extends Activity{
	@Override
	protected void onPause() {
		super.onPause();
		edit.commit();
		finish();
	}

	public static final String PREFS_NAME = "Preferences";
	private SharedPreferences setting;
	private SharedPreferences.Editor edit;
	
	    /** Called when the activity is first created. */
	    @Override
	    public void onCreate(Bundle savedInstanceState) {
	        super.onCreate(savedInstanceState);
	        setContentView(R.layout.settings);
	        
	        setting = getSharedPreferences(PREFS_NAME, 0);
	        edit = setting.edit();
	        
	        Button b1=(Button) findViewById(R.id.button1);
	        b1.setOnClickListener(new OnClickListener(){
    				public void onClick(View arg0){  
    					edit.commit();
    					finish();
    				}
	        });
	        
	        final SeekBar sb1=(SeekBar) findViewById(R.id.seekBar1);
	        int padSize = setting.getInt("padSize", 0);
	        sb1.setMax(10);
	        sb1.setProgress(padSize);
	        sb1.setOnSeekBarChangeListener(new OnSeekBarChangeListener() {
				
				@Override
				public void onStopTrackingTouch(SeekBar seekBar) {
					
				}
				
				@Override
				public void onStartTrackingTouch(SeekBar seekBar) {
					
				}
				
				@Override
				public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
					sb1.setProgress(progress);
					edit.putInt("padSize", progress);
				}
			   });
				
	        final SeekBar sb2=(SeekBar) findViewById(R.id.seekBar2);
		        int ballSpd = setting.getInt("ballSpd", 0);
		        sb2.setMax(10);
		        sb2.setProgress(ballSpd);
		        sb2.setOnSeekBarChangeListener(new OnSeekBarChangeListener() {
					
					@Override
					public void onStopTrackingTouch(SeekBar seekBar) {
						
					}
					
					@Override
					public void onStartTrackingTouch(SeekBar seekBar) {
						
					}
					
					@Override
					public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
						sb2.setProgress(progress);
						edit.putInt("ballSpd", progress);
					}
		   });
	 }
}
