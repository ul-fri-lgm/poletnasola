package fri.android;

import android.app.Activity;
import android.os.Bundle;

public class GameActivity extends Activity {
	@Override
	protected void onPause() {
		super.onPause();
		finish();
	}

	@Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.game_view);
	}
}
