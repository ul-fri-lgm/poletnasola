package fri.android;

import android.graphics.Canvas;
import android.graphics.Point;
import android.view.MotionEvent;

public class Players {
	private Player my_players[] = new Player[2];

	/**
	 * Initializes the players array
	 * @param screenSize the size of the screen
	 * @param padSize the size of this player's pad
	 * @param color the color to use for the pad
	 */
	public Players(Point screenSize, Point padSize, int color) {
		final int screenYPos[] = { 0, screenSize.y - padSize.y };
		final int screenXPos[] = { 0, 0 };
		
		for (int i = 0; i != my_players.length; ++i) {
			my_players[i] = new Player(padSize, color);
			
			my_players[i].setScreenStart(new Point(screenXPos[i], screenYPos[i]));
			my_players[i].setScreenSize(new Point(screenSize.x, padSize.y));
			my_players[i].setLocationCentered();
		}
	}
	
	/**
	 * returns a reference to player with the ID
	 * @param player the ID of the player, starts at 0
	 * @return reference to Player
	 */
	public Player getPlayer(int player) {
		return my_players[player];
	}

	/**
	 * Draws the player's visible properties on the canvas by calling each Player's draw() method
	 * @param canvas the canvas to draw on
	 */
	void draw(Canvas canvas) {
		for (int i = 0; i != my_players.length; ++i)
			my_players[i].draw(canvas);
	}
	
	/**
	 * Move the pads
	 * @param event current finger position
	 * @param fingerPos last finger position
	 */
	void handle_pad_move(MotionEvent event, Point[] fingerPos) {
		for (int i = 0; i != fingerPos.length; ++i) {
			getPlayer(i).setX((int)event.getX(i));
		}
	}
}
