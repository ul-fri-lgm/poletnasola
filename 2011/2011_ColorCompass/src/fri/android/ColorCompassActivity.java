package fri.android;


import android.app.Activity;
import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnKeyListener;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class ColorCompassActivity extends Activity implements
		SensorEventListener {

	private float azimutOriginal = 0; 
	private TextView azimut; // Dejanski Azimut
	private TextView DoloceniAzimut; // Dolocen azimut
	private EditText VnosnoPolje;
	String VneseniAzimut;
	private TextView VnesAzim;
	private Button ShraniAzimut; // Vnos texta za azimut

	private SensorManager sm = null;

    private float[] mValues;
    
    private ImageView kompas;
	
    private int DolAzimut = 600;
    private long AzimutVneseni;
    
    private boolean senzor = false;

	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.main);
		
		
		
		
		
		// Sklicevanje na SensorManeger
		sm = (SensorManager) getSystemService(Context.SENSOR_SERVICE);
		
		DoloceniAzimut = (TextView) findViewById(R.id.DoloceniAzimut);
		azimut = (TextView) findViewById(R.id.Azimut);
		kompas = (ImageView) findViewById(R.id.kompas);
		VnosnoPolje = (EditText) findViewById(R.id.VnosnoPolje);
		VnesAzim = (TextView) findViewById(R.id.VnesAzim);
		
		sm.registerListener(this, sm.getDefaultSensor(Sensor.TYPE_ORIENTATION),
				SensorManager.SENSOR_DELAY_NORMAL);

		azimut.setText("0.00");
		
		 VnosnoPolje.setFocusable(true);
		 VnosnoPolje.setFocusableInTouchMode(true);
		 VnosnoPolje.requestFocus();
//		 VnosnoPolje.setText("blabla");
		
		
		  OnKeyListener keyListener = new OnKeyListener() {
				
				@Override
				public boolean onKey(View main, int keyCode, KeyEvent event) {
					int action=event.getAction();
					
					Log.d("INTERAKCIJA","Tipka back - koda: " + event.getKeyCode());
					switch (action) {
						case KeyEvent.ACTION_DOWN:
								if(keyCode == KeyEvent.KEYCODE_ENTER) {		
									
																	
									VneseniAzimut = (String) ((TextView) VnosnoPolje).getText().toString();
									AzimutVneseni = (long)(Long.parseLong(VneseniAzimut));
									
									if(VneseniAzimut.length() > 3)
									{
										VnesAzim.setText("Do 360�");
									}
									
									else if(AzimutVneseni > 360 )
									{
										VnesAzim.setText("Do 360�");
									}
									else {
										VnesAzim.setText(VneseniAzimut+"�");
										
										
										InputMethodManager in = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
						                in.hideSoftInputFromWindow(VnosnoPolje
						                        .getApplicationWindowToken(),
						                        InputMethodManager.HIDE_NOT_ALWAYS);
										
										
									}
									
									
								}
								else if(keyCode == KeyEvent.KEYCODE_BACK){
									
									VnesAzim.setText("");
								}
								
								
														
							break;
						case KeyEvent.ACTION_UP:
							VnosnoPolje.setText("");
							
							break;
					}
					return true;
				}
			};
		
			
		
		
		//Gumb za shraniti azimut
		ShraniAzimut = (Button) findViewById(R.id.ShraniAzimut);
		ShraniAzimut.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				
					final String a = (String) azimut.getText();
					DoloceniAzimut.setText("Azimut: "+a);
					senzor = true;
					
					
				
			}
		});
		
		VnosnoPolje.setOnKeyListener(keyListener);
	}

	

	
	public void onAccuracyChanged(Sensor sensor, int accuracy) {
		// TODO Auto-generated method stub

	}

	public void onSensorChanged(SensorEvent event) {
		azimutOriginal = Math.round((event.values[0]));
		azimut.setText((int)azimutOriginal+"�");
		kompas.setVrtenje(azimutOriginal);
		
		

		
		
		if (senzor == true){
		DolAzimut = (int)(Math.round((event.values[0])));
		senzor = false;
		}
		
		
		//Spreminjanje barve!
				View v = findViewById(R.id.ozadje);
				int vrednost=(int) event.values[0];
				int vrednost2=vrednost;
				int razlika=(int) (DolAzimut-azimutOriginal);
				razlika= Math.abs(razlika);
				if (razlika>180) {
					razlika=Math.abs(360-razlika);
				}
				razlika=180-razlika;
				if(DolAzimut==600){
					razlika=180;
				}
				
				if(vrednost>=180){
					vrednost=(vrednost-180)*255/179;
				} else {
					vrednost = (180-vrednost)*255/180;
				}


				if(vrednost2>=180) {
					vrednost2=(360-vrednost2)*255/180;
				} else {
					vrednost2=vrednost2*255/180;
				}
				
				v.setBackgroundColor(0x86008800+vrednost+vrednost2*256*256+razlika/3*2*256*256*256);

				
				
			}
		
		
		
		
	}
	
	



