package fri.android;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;

public class ImageView extends View
{
	
	//Slika
		private Bitmap bitmap=null;
		private int bitmapSizeX=0;
		private int bitmapSizeY=0;
	
	//Parametri slike
		private Matrix imageMatrix=new Matrix();
		private boolean imageScaleSet=false;
		private int viewSizeX=0;
		private int viewSizeY=0;
		
	// Vrtenje slike
		private float vrtenje; 
	
	// Prej�na vrednost kompasa	
		private float prej = 0;
		
	// Sredina ImageView
		//public int komX = kompass.getHi
		
		
	
	
	public ImageView (Context context, AttributeSet attrs){
		super(context,attrs);
		
		try {
			bitmap=BitmapFactory.decodeResource(context.getResources(), R.drawable.kompass);
    	} catch (OutOfMemoryError e) {
    		Log.d("IMAGETEST","Premalo memorja");
    	}
    	if (bitmap!=null) {
    		Log.d("IMAGETEST","Slika nalo�ena");
    		bitmapSizeX=bitmap.getWidth();
    		bitmapSizeY=bitmap.getHeight();
    		    		
    	} else {
    		Log.d("IMAGETEST","Ni slike?");
    	}
	}
	
	protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        if (bitmap!=null) {
        	canvas.drawBitmap(bitmap, imageMatrix, null);
        }
	}
	
	public void setVrtenje (float kompas){
		imageMatrix.postRotate(prej-kompas,(viewSizeX/2),(viewSizeY/2));
		prej = kompas;
		
	}
	/*
	void scaleRotateView(float scale, float rotate,float centerX, float centerY) {
		imageScaleSet=true;
		imageMatrix.postScale(scale,scale,centerX,centerY);
		
	}
	*/
	void offsetView(float viewSizeX,float viewSizeY) {
		imageMatrix.postTranslate(viewSizeX/2,viewSizeY/2);
		
	}
	
        protected void onSizeChanged(int w, int h, int oldw, int oldh) {    	
            super.onSizeChanged(w, h, oldw, oldh);
            viewSizeX=w;
            viewSizeY=h;
            if (!imageScaleSet) {
            	//change image scale if not already set!
            	float scale=1.0f*viewSizeX/bitmapSizeX;
            	float scaleY=1.0f*viewSizeY/bitmapSizeY;
            	
            	if (scaleY<scale) scale=scaleY; //set scale to smaller of both!
            	imageMatrix.postScale(scale,scale);
            	offsetView(viewSizeX-viewSizeX, viewSizeY-viewSizeX);
            }
}
}

