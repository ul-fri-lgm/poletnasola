package fri.android;

import android.app.Activity;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.SeekBar;
import android.widget.TextView;

public class AddAlarmActivity extends Activity {
	
	@Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
        SeekBar bar = (SeekBar) findViewById(R.id.radius_bar);
        bar.setPadding(10, 0, 10, 0);
        bar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
			
			@Override
			public void onStopTrackingTouch(SeekBar seekBar) {
				
			}
			
			@Override
			public void onStartTrackingTouch(SeekBar seekBar) {
				
			}
			
			@Override
			public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
				((TextView) findViewById(R.id.radius_view)).setText(getString(R.string.alarm_radius) + (progress + 50) + " m");
			}
		});
        ((TextView) findViewById(R.id.radius_view)).setText(getString(R.string.alarm_radius) + "50 m");
        Button add = (Button) findViewById(R.id.button1);
        add.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				EditText name = (EditText) findViewById(R.id.name_edit);
				EditText description = (EditText) findViewById(R.id.description_edit);
				SeekBar radius = (SeekBar) findViewById(R.id.radius_bar);
				Alarm val =  new Alarm(name.getText().toString(), description.getText().toString(), radius.getProgress() + 50);
				GPSAlarmsActivity.alarmlist.add(val);
				finish();
			}
		});
    }
}
