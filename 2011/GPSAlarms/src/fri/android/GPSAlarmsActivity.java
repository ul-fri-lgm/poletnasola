package fri.android;

import java.util.ArrayList;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.database.DataSetObserver;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;

public class GPSAlarmsActivity extends Activity {
	public static ArrayList<Alarm> alarmlist = new ArrayList<Alarm>();
	
	class AlarmListAdapter implements ListAdapter {

		@Override
		public int getCount() {
			return alarmlist.size();
		}

		@Override
		public Object getItem(int position) {
			return alarmlist.get(position);
		}

		@Override
		public long getItemId(int position) {
			return position;
		}

		@Override
		public int getItemViewType(int position) {
			return 0;
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			TextView view = null;			
			if (convertView != null) {
				if (convertView instanceof TextView)
					view = (TextView) convertView;
			}
			if(view == null)
				view = new TextView(parent.getContext());
			view.setText(alarmlist.get(position).getName());
			return view;
		}

		@Override
		public int getViewTypeCount() {
			return 1;
		}

		@Override
		public boolean hasStableIds() {
			return true;
		}

		@Override
		public boolean isEmpty() {
			return alarmlist.isEmpty();
		}

		@Override
		public void registerDataSetObserver(DataSetObserver observer) {
			//Ni treba kode
		}

		@Override
		public void unregisterDataSetObserver(DataSetObserver observer) {
			//Ni treba kode
			
		}

		@Override
		public boolean areAllItemsEnabled() {
			return true;
		}

		@Override
		public boolean isEnabled(int position) {
			return true;
		}
		
	}
	
    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.alarmlist);
        ListView listview = (ListView) findViewById(R.id.listView1);
        listview.setAdapter(new AlarmListAdapter());
        loadAlarms();
    }
    
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
    	MenuInflater inflater = getMenuInflater();
    	inflater.inflate(R.menu.menu, menu);
    	return true;
    }
    
	public boolean onOptionsItemSelected(MenuItem item) {
		switch(item.getItemId()) {
		case R.id.item1:
			Intent i = new Intent(this, AddAlarmActivity.class);
			startActivity(i);
			break;
		}
		return true;
	}
    
    
    void storeAlarms() {
    	SharedPreferences prefs = getSharedPreferences("GPSAlarmsPrefs", MODE_PRIVATE);
    	Editor e = prefs.edit();
    	for (String key : prefs.getAll().keySet()) {
    		if(key.startsWith("ALARM:"))
    			e.remove(key); 
    		Log.i("EST","KEY: "+key);
    	}
    	e.commit();
    	for(Alarm val : alarmlist) {
    		e.putString("ALARM:" + val.getName(), val.encode());
    		Log.i("EST","SAVING: "+val.getName());
    	}
    	e.commit();
    }
    
    @Override
	protected void onResume() {
		super.onResume();
		ListView listview = (ListView) findViewById(R.id.listView1);
        Log.i("EST", listview.getAdapter().getCount() + "");
        Log.i("EST", alarmlist.size() + "");
    }		

    @Override
    protected void onStart() {
    	super.onStart();
    	ListView listview = (ListView) findViewById(R.id.listView1);
		listview.setAdapter(new AlarmListAdapter());
    	storeAlarms();
    }
    
    @Override
    protected void onDestroy() {
		super.onDestroy();
		storeAlarms();
	}

	void loadAlarms() {
    	SharedPreferences prefs = getSharedPreferences("GPSAlarmsPrefs", MODE_PRIVATE);
    	alarmlist.clear();
    	for (String key : prefs.getAll().keySet()) {
    		if(key.startsWith("ALARM:")) {
    			alarmlist.add(Alarm.decode(prefs.getString(key, "")));
    		}
    	}
    }
}