package fri.android;

public class Alarm {
	private String name;
	private String description;
	private double z_dolzina, z_sirina;
	private int radius;
	
	public Alarm(String name, String description, int radius) {
		this.name = name;
		this.description = description;
		this.radius = radius;
		//Nastavi z_dolzina in z_sirina na trenutno lokacijo
	}
	
	public Alarm(String name, String description, int radius, double z_dolzina, double z_sirina) {
		this.name = name;
		this.description = description;
		this.radius = radius;
		this.z_dolzina = z_dolzina;
		this.z_sirina = z_sirina;
	}
	
	
	public String getName() {
		return this.name;
	}
	
	public String getDescription() {
		return this.description;
	}
	
	public double getZemljepisnaDolzina() {
		return this.z_dolzina;
	}
	
	public double getZemljepisnaSirina() {
		return this.z_sirina;
	}
	
	public int getRadius() {
		return this.radius;
	}
	
	
	public void setName(String name) {
		this.name = name;
	}
	
	public void setDescription(String description) {
		this.description = description;
	}
	
	public void setPosition(double z_dolzina, double z_sirina) {
		this.z_sirina = z_sirina;
		this.z_dolzina = z_dolzina;
	}
	
	public void setRadius(int meters) {
		this.radius = meters;
	}
	
	
	public String encode() {
		return this.name + "\0" + this.description + "\0" + this.z_dolzina + "\0" + this.z_sirina + "\0" + this.radius;
	}
	
	public static Alarm decode(String code) {		
		String tokens[] = code.split("\0");
		return new Alarm(tokens[0], tokens[1], Integer.parseInt(tokens[4]), Double.parseDouble(tokens[2]), Double.parseDouble(tokens[3]));
	}
	
	@Override
	public String toString() {
		return "Alarm: " + this.name + "\n" + this.description + "\nLokacija: zemljepisna dolzina: " +this.z_dolzina + " zemljepisna sirina: " + this.z_sirina +" radij: " +this.radius;
	}
}
