package fri.android;


public class toString 
{
	static public String toNajdiSiStringSearch(String input)
	{
		String returN = "http://www.najdi.si/mobile/search.jsp?q=";
		input = input.replaceAll( " ", "+");
		
		returN += input;
		
		//returN = Uri.encode(returN);
		
		return returN;
	}
	
	static public String toGoogleStringSearch(String input)
	{
		String returN = "http://www.google.si/search?q=";
		input = input.replaceAll( " ", "+");
		
		returN += input;
		
		return returN;
	}
	
	static public String toWikiStringSearch(String input)
	{
		String returN = "http://sl.wikipedia.org/w/index.php?search=";
		input = input.replaceAll( " ", "+");
		
		returN += input;
		
		return returN;
	}
	
	static public String toYahooStringSearch(String input)
	{
		String returN = "http://search.yahoo.com/search;_ylt=?p=";
		input = input.replaceAll( " ", "+");
		
		returN += input;
		
		return returN;
	}
	
	static public String toAmazonStringSearch(String input)
	{
		String returN = "http://www.amazon.com/s/ref=nb_sb_noss?field-keywords=";
		input = input.replaceAll( " ", "+");
		
		returN += input;
		
		return returN;
	}
	
	static public String toBingStringSearch(String input)
	{
		String returN = "http://m.bing.com/search/search.aspx?Q=";
		input = input.replaceAll( " ", "+");
		
		returN += input;
		returN += "&a=results&MID=1";
		
		return returN;
	}
	
	static public String toAskStringSearch(String input)
	{
		String returN = "http://www.ask.com/web?q=";
		input = input.replaceAll( " ", "+");
		
		returN += input;
		
		return returN;
	}

	static public String toGAMStringSearch(String input)
	{
		String returN = "https://market.android.com/search?q=";
		input = input.replaceAll( " ", "+");
		
		returN += input;
		
		return returN;
	}
}
