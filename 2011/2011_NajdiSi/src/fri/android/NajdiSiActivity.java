package fri.android;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import fri.android.toString;

public class NajdiSiActivity extends Activity 
{
    @Override
    public void onCreate(Bundle savedInstanceState) 
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
        
    	Button buttonNajdi = (Button) findViewById(R.id.button1);
    	buttonNajdi.setOnClickListener(new View.OnClickListener() 
    	{	
			@Override
			public void onClick(View v) 
			{
				String searchString = ((EditText)findViewById(R.id.editText1) ).getText().toString();
				
				if( searchString.length() != 0)				
					openInBrowser(toString.toNajdiSiStringSearch(searchString));
			}
		});
    	
    	Button buttonGoogle = (Button) findViewById(R.id.button2);
    	buttonGoogle.setOnClickListener( new View.OnClickListener() 
    	{
			@Override
			public void onClick(View v) 
			{
				String searchString = ((EditText)findViewById(R.id.editText1) ).getText().toString();
				
				if( searchString.length() != 0)				
					openInBrowser(toString.toGoogleStringSearch(searchString));
			}
		});
    	
    	Button buttonWiki = (Button) findViewById(R.id.button4);
    	buttonWiki.setOnClickListener( new View.OnClickListener() 
    	{	
			@Override
			public void onClick(View v) 
			{
				String searchString = ((EditText)findViewById(R.id.editText1) ).getText().toString();
				
				if( searchString.length() != 0)				
					openInBrowser(toString.toWikiStringSearch(searchString));
			}
		});
    	
    	Button buttonYahoo = (Button) findViewById(R.id.button3);
    	buttonYahoo.setOnClickListener( new View.OnClickListener() 
    	{	
			@Override
			public void onClick(View v) 
			{
				String searchString = ((EditText)findViewById(R.id.editText1) ).getText().toString();
				
				if( searchString.length() != 0)				
					openInBrowser(toString.toYahooStringSearch(searchString));
			}
		});
    	
    	Button buttonBing = (Button) findViewById(R.id.button6);
    	buttonBing.setOnClickListener( new View.OnClickListener() 
    	{	
			@Override
			public void onClick(View v) 
			{
				String searchString = ((EditText)findViewById(R.id.editText1) ).getText().toString();
				
				if( searchString.length() != 0)				
					openInBrowser(toString.toBingStringSearch(searchString));
			}
		});
    	
    	Button buttonAsk = (Button) findViewById(R.id.button7);
    	buttonAsk.setOnClickListener( new View.OnClickListener() 
    	{	
			@Override
			public void onClick(View v) 
			{
				String searchString = ((EditText)findViewById(R.id.editText1) ).getText().toString();
				
				if( searchString.length() != 0)				
					openInBrowser(toString.toAskStringSearch(searchString));
			}
		});
    	
    	Button buttonAmazon = (Button) findViewById(R.id.button5);
    	buttonAmazon.setOnClickListener( new View.OnClickListener() 
    	{	
			@Override
			public void onClick(View v) 
			{
				String searchString = ((EditText)findViewById(R.id.editText1) ).getText().toString();
				
				if( searchString.length() != 0)				
					openInBrowser(toString.toAmazonStringSearch(searchString));
			}
		});
    	
    	Button buttonGAM = (Button) findViewById(R.id.button8);
    	buttonGAM.setOnClickListener( new View.OnClickListener() 
    	{	
			@Override
			public void onClick(View v) 
			{
				String searchString = ((EditText)findViewById(R.id.editText1) ).getText().toString();
				
				if( searchString.length() != 0)				
					openInBrowser(toString.toGAMStringSearch(searchString));
			}
		});
    	
    }
    
    public void openInBrowser(String url)
    {
    	Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
    	startActivity(browserIntent);
    }
}